<style type="text/css">
	.panel-body{
		min-height: 400px;
	}
	.vertical .carousel-inner {
	  height: 100%;
	}

	.carousel.vertical .item {
	  -webkit-transition: 0.6s ease-in-out top;
	     -moz-transition: 0.6s ease-in-out top;
	      -ms-transition: 0.6s ease-in-out top;
	       -o-transition: 0.6s ease-in-out top;
	          transition: 0.6s ease-in-out top;
	}

	.carousel.vertical .active {
	  top: 0;
	}

	.carousel.vertical .next {
	  top: 400px;
	}

	.carousel.vertical .prev {
	  top: -400px;
	}

	.carousel.vertical .next.left,
	.carousel.vertical .prev.right {
	  top: 0;
	}

	.carousel.vertical .active.left {
	  top: -400px;
	}

	.carousel.vertical .active.right {
	  top: 400px;
	}

	.carousel.vertical .item {
	    left: 0;
	}
</style>
<?php

	//Olah data tampil untuk buku kapal
	$bgcolor = '#3399FF';

?>
<!-- Data ditampilkan -->
<?php
	
	$banyak_item = 10;
	$banyak_data = count($list_progress);
	$banyak_page = round($banyak_data/$banyak_item);
	$page = 0;
	$temp_no = 0;
?>
<div id="carousel_monitoring" class="carousel slide vertical" data-ride="carousel">

<div class="carousel-inner">
<?php
	while($page <= $banyak_page){

?>
<div class="item <?php echo ($page==0)? 'active' : ''; ?>" >
<div>
<table class="table table-bordered table-monitoring">
	<thead>
		<tr>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>Pemilik Kapal</th>
			<th>Pendok</th>
			<th>Entri Kapal</th>
			<th>Verifikasi Kapal</th>
			<th>Cetak BKP</th>
			<th>Terima BKP</th>
		</tr>
	</thead>
	<tbody>
<?php
		foreach ($list_progress as $no => $item) {
			# code...
			echo "<tr>
					<td style='font-size: 1.2em;font-weight: bold;text-align:center;'>".$list_progress[$temp_no]->no_rekam_pendok."</td>
					<td>".$list_progress[$temp_no]->nama_kapal."</td>
					<td>".$list_progress[$temp_no]->nama_penanggung_jawab."</td>";
		    
			echo "<td>";
		    echo $list_progress[$temp_no]->status_pendok!=NULL ? '<center>&radic;</center>' : '';
			echo "</td>
					<td ";
			echo $list_progress[$temp_no]->status_entry_bkp=="2"? '<center>&radic;</center>' : '';					
			echo "</td>
					<td ";
			echo $list_progress[$temp_no]->status_verifikasi=="YA" ? '<center>&radic;</center>' : '';
			echo "</td>
					<td ";
			echo $list_progress[$temp_no]->status_cetak_bkp=="YA" ? '<center>&radic;</center>' : '';		
			echo "</td>
					<td ";
			echo $list_progress[$temp_no]->status_terima_bkp=="YA" ? '<center>&radic;</center>' : '';	
			echo "</td>";
			
			echo "</tr>";
			$temp_no++;
			if((($temp_no % $banyak_item == 0)&&($temp_no!=0))||$temp_no==$banyak_data)
			{
				break;
			}

		}
	$page++;
?>
</tbody>
</table>
</div>
</div>
<?php } ?>
</div>
</div>

<!-- ADTTIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
      // $('.carousel').carousel({interval: 10000});
	} );
</script>