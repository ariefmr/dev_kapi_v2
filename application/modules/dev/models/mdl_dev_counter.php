<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_dev_counter extends CI_Model
{
    private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

	public function get_table()
    {
        
    	$query =   "SELECT * FROM mst_kabupaten_kota";
      
    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function insert_into($row){
    
        $this->db_kapi->insert_batch('counter_register',$row);                            
        
        return 0;
    }

}