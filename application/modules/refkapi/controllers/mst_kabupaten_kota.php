<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_kabupaten_kota extends MX_Controller {

	function __construct()
	{
		parent::__construct();
				
		$this->load->model('mdl_pejabat');
	
	}

	public function pilih_kabupaten_kota($id_lokasi = "",$nama_lokasi = "")
	{
		$data['lokasi_pengesahan_id'] 		= $id_lokasi;
		$data['nama_lokasi'] 	= $nama_lokasi;
		$this->load->view('pilih_kabupaten_kota',$data);
	}

	public function search_kabupaten_kota()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_search_like = $this->input->get('q', FALSE);
		$get_search_filter = $this->input->get('filter', FALSE);

		$search_result = $this->mdl_pejabat->cari_pejabat($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		if($get_filter === 'count'){
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result
						);
		}else{
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result,
						'result' => $search_result
						);
		}

		echo json_encode($json);
	}

}

?>