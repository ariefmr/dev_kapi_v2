<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_kapal extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_refkapi_kapal');
		}

	public function index()
	{
		$this->kapal();

	}

	public function kapal( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'jquery.dataTables.css', 'validationEngine.jquery.css');

		if(($filter === 'daerah')){
			$labels = 'label_view_kapal_daerah_kapi';
			$data['list_kapal'] = $this->mdl_refkapi_kapal->list_kapal_daerah();
		}else{
			$labels = 'label_view_kapal_kapi';
			$data['list_kapal'] = $this->mdl_refkapi_kapal->list_kapal();
		}
			echo Modules::run('templates/page/v_form', 'refkapi', 'tabel_kapal_refkapi', $labels, $add_js, $add_css, $data);

	}

	public function detail_kapal()
	{
		$get_id_kapal = $this->input->get('ik', FALSE);
		$detail_result = $this->mdl_refkapi_kapal->detil_kapal($get_id_kapal);

		echo json_encode($detail_result);

	}

	public function search_kapal()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_refkapi_kapal->cari_kapal($get_filter, $get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}

		if($get_filter === 'count'){
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result
						);
		}else{
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result,
						'result' => $search_result
						);
		}

		echo json_encode($json);
	}

	public function search_by_noreg()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_refkapi_kapal->cari_by_noreg($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function search_by_nama()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_refkapi_kapal->cari_by_nama($get_search_like);

		$array_combine = array(
							(object) array(
										'id_bkp'			=>	'',
										'id_kapal'			=>	'',
										'id_pendok'			=>	'',
										'nama_kapal' 		=> 	$get_search_like,
										'nama_perusahaan'	=> 	'',
										'gt_kapal'			=>	'',
										'no_grosse_akte'	=>	''
									)
						);

		$jml_search_result = count($search_result);
		if(!$search_result){
			
			$jml_search_result = 0;
			$hasil = $array_combine;
			
		}else{

			$hasil = array_merge($array_combine,$search_result);

		}

		$json = array(
					'search_like'	=>	$get_search_like,
					'jumlah_result'	=>	$jml_search_result,
					'result'		=>	$hasil
				);
		
		echo json_encode($json);
	}

	public function pilih_no_register($cari_noreg = "No.Register")
	{
		$data['cari_noreg'] = $cari_noreg;
		$data['search_by'] = 'no_register';
		$this->load->view('pilih_kapal',$data);
	}

	public function pilih_nama_kapal()
	{
		$this->load->view('pilih_kapal_by_name');
	}

	public function pilih_kapal_history()
	{
		$data['cari_noreg'] = "No Register Kapal";
		$this->load->view('pilih_kapal_history', $data);
	}

	public function set_data()
	{
		//$data = $this->input->post('data');
		//$data = json_decode($data);
		//$array 		= $this->input->get('array');
		$id_kapal = $this->input->post('data1');
		$id_bkp = $this->input->post('data2');
		
		echo $this->mdl_refkapi_kapal->history($id_kapal, $id_bkp);
		//echo Modules::run('templates/page/v_form', 'refkapi', 'history_perubahan_pendok', '', '', '', $data);
		/*$this->load->view('history_perubahan_pendok',$data);*/
	}

	public function list_tempat_array()
	{
		
		$list_opsi = $this->mdl_refkapi_kapal->list_tempat_pembangunan();

		// vdump($list_opsi, true);

		return $list_opsi;
	}

}