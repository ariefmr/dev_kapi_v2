<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_tipe_perubahan extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_tipe_perubahan_json()
	{
		$this->load->model('mdl_tipe_perubahan');

		$list_opsi = $this->mdl_tipe_perubahan->list_opsi_perubahan();

		echo json_encode($list_opsi);
	}

	public function list_tipe_perubahan_array()
	{
		$this->load->model('mdl_tipe_perubahan');

		$list_opsi = $this->mdl_tipe_perubahan->list_opsi_perubahan();

		return $list_opsi;
	}

	public function get_checked_perubahan($id_pendok)
	{
		$this->load->model('mdl_tipe_perubahan');

		$list_opsi = $this->mdl_tipe_perubahan->list_perubahan_per_pendok($id_pendok);

		return $list_opsi;
	}
	
}
?>