<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_pemohon extends MX_Controller {

		function __construct()
			{
				parent::__construct();
				
				$this->load->model('mdl_pemohon');
			}

	public function index()
	{
		$this->pemohon();
	}

	public function pilih_pemohon($nama_pemohon,$id_pemohon)
	{	
		$data['nama_pemohon'] = $nama_pemohon;
		$data['id_pemohon'] = $id_pemohon;
		$this->load->view('pilih_pemohon', $data);
	}

	public function pemohon()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'tabel_pemohon';
		$labels = 'label_view_pemohon';

		$data['list_pemohon'] = $this->mdl_pemohon->list_pemohon();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function detail_pemohon()
	{
		$get_id_pemohon = $this->input->get('ip', FALSE);
		$detail_result = $this->mdl_pemohon->detil_pemohon($get_id_pemohon);

		echo json_encode($detail_result);

	}

	public function search_pemohon()
	{
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_pemohon->cari_pemohon($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);

		echo json_encode($json);
	}

	public function entry()
	{
		$data['submit_form'] = 'refkapi/mst_pemohon/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'form_entry_pemohon';
		$labels = 'form_pemohon';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		if( $this->mdl_pemohon->input($array_input) ){
			$url = base_url('refkapi/mst_pemohon');
			redirect($url);
		}else{
			$url = base_url('refkapi/mst_pemohon');
			redirect($url);
		}
	}

	public function edit($id_record)
	{
		$this->load->model('mdl_pemohon');

		$get_detail = $this->mdl_pemohon->detil_pemohon($id_record);

		if( !$get_detail )
		{
			$data['list_pemohon'] = 'Data tidak ditemukan';
		}else{
			$data['list_pemohon'] = (array)$get_detail;
		}

		$data['submit_form'] = 'refkapi/mst_pemohon/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'form_edit_pemohon';
		$labels = 'form_pemohon';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_to_edit = $array_input;

		if( $this->mdl_pemohon->update($array_to_edit) ){
			$url = base_url('refkapi/mst_pemohon');
			redirect($url);
		}else{
			$url = base_url('refkapi/mst_pemohon');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_pemohon->delete($id);

        $url = base_url('refkapi/mst_pemohon');
        redirect($url);
    }

}
?>