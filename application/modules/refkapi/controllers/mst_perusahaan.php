<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_perusahaan extends MX_Controller {

		function __construct()
			{
				parent::__construct();
				
				$this->load->model('mdl_perusahaan');
			}

	public function index()
	{
		$this->perusahaan();
	}

	public function perusahaan( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if(($filter === 'daerah')){
			$labels = 'label_view_perusahaan_daerah_kapi';
			$data['list_perusahaan'] = $this->mdl_perusahaan->list_perusahaan_daerah();
		}else{
			$labels = 'label_view_perusahaan_kapi';
			$data['list_perusahaan'] = $this->mdl_perusahaan->list_perusahaan();
		}
			echo Modules::run('templates/page/v_form', 'refdss', 'tabel_perusahaan', $labels, $add_js, $add_css, $data);

	}

	public function detail_perusahaan()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_id_perusahaan = $this->input->get('ip', FALSE);
		$detail_result = $this->mdl_perusahaan->detil_perusahaan($get_filter, $get_id_perusahaan);

		echo json_encode($detail_result);

	}

	public function search_perusahaan()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_perusahaan->cari_perusahaan($get_filter, $get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'filter' => $get_filter, 
					'search_like ' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);

		echo json_encode($json);
	}



}