<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_history extends MX_Controller {

	function __construct()
    {
      parent::__construct();
      $this->load->model('mdl_history');
    }

	public function tampilkan_carousel()
	{

		$id_kapal = $this->input->post('data1');
		$id_bkp = $this->input->post('data2');
		
		$data['data_history'] = $this->mdl_history->history($id_kapal, $id_bkp);
		$data['tipe_perubahan'] = $this->mdl_history->tipe_perubahan($id_kapal,$id_bkp);
		//echo Modules::run('templates/page/v_form', 'refkapi', 'history_perubahan_pendok', '', '', '', $data);
		$this->load->view('carousel_history',$data);
	}

	public function tampilkan_table()
	{

		$id_kapal = $this->input->post('data1');
		$id_bkp = $this->input->post('data2');

		$data['data_nama'] = (array)$this->mdl_history->get_nama_kapal_register($id_kapal);
		$data['data_history'] = $this->mdl_history->history($id_kapal, $id_bkp);
		$data['tipe_perubahan'] = $this->mdl_history->tipe_perubahan($id_kapal,$id_bkp);
		//echo Modules::run('templates/page/v_form', 'refkapi', 'history_perubahan_pendok', '', '', '', $data);
		$this->load->view('table_history',$data);
		// vdump($data, true);
	}

	public function views()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'form_history';
		$labels = 'label_view_history';
		$data = '';

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function xls($id_kapal)
	{

	    $list_history = $this->mdl_history->history($id_kapal, '0');
	    // vdump($list_history, true);
	    $this->load->library('excel');

	    // $filename = "Daftar Histori Kapal.xlsx";

		// JUDUL
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Daftar Histori');

		$sheet = $this->excel->getActiveSheet();
		$sheet->setCellValue('A1', 'Daftar Histori Kapal');
		$styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$sheet->getStyle('A1')->applyFromArray($styleArray);
		$sheet->mergeCells('A1:G1');

	    $array_thead = array( 'No. ',
	                          'No Register',
	                          'Tanda Pengenal',
	                          'Nama Kapal',
	                          'Nama Kapal Sebelumnya',
	                          'Tempat Pembangunan',
	                          'Bahan Utama Kapal',
	                          'Fungsi Kapal',
	                          'Jenis Alat Tangkap',
	                          'Merek Mesin Utama',
	                          'Daya Mesin Utama',
	                          'No Mesin Utama',
	                          'Jumlah Palka',
	                          'Kapasitas Palka',
	                          'Tempat Pendaftaran',
	                          'Tempat No & Tanggal Gross Akte',
	                          'Panjang',
	                          'Lebar',
	                          'Dalam',
	                          'GT',
	                          'NT',
	                          'Nama Pemilik',
	                          'Alamat Pemilik',
	                          'Keterangan',
	                          'Tanggal Tanda Tangan Direktur'
	                        );
	  
	    $column_letter = 'A';

		foreach ($array_thead as $key => $value) {
			$sheet      -> setCellValue($column_letter.'3', $value);
			$column_letter++;
		}                  
		$styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
		// MASUKKAN DATA
		$cell_number = 4;

		$number = 1;

	    foreach ($list_history as $item) {

        	$tempat_pembangunan = $item->tempat_pembangunan;

	        switch ($item->kategori_pendaftaran) {
				case 'PUSAT':
				    $tempat_pendaftaran = "KKP";
				    break;

				case 'PROPINSI':
				    $tempat_pendaftaran = $item->propinsi_pendaftaran;
				    break;

				case 'KAB/KOTA':
				    $tempat_pendaftaran = $item->kabkota_pendaftaran;
				    break;

				default:
				    $tempat_pendaftaran = "KKP";
				    break;
	        }

	      	$gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));

			$sheet->setCellValue('A'.$cell_number, $number);
			$sheet->getStyle('B')->getNumberFormat()->setFormatCode('000000'); //format untuk noregister supaya formated
			$sheet->setCellValue('B'.$cell_number, $item->no_register);
			$sheet->setCellValue('C'.$cell_number, $item->no_tanda_pengenal);
			$sheet->setCellValue('D'.$cell_number, $item->nama_kapal);
			$sheet->setCellValue('E'.$cell_number, $item->nama_kapal_sblm );
			$sheet->setCellValue('F'.$cell_number, $tempat_pembangunan);
			$sheet->setCellValue('G'.$cell_number, $item->nama_bahan_kapal);
			$sheet->setCellValue('H'.$cell_number, $item->nama_jenis_kapal);
			$sheet->setCellValue('I'.$cell_number, $item->alat_tangkap);
			$sheet->setCellValue('J'.$cell_number, $item->merek_mesin);
			$sheet->setCellValue('K'.$cell_number, $item->daya_kapal);
			$sheet->setCellValue('L'.$cell_number, $item->no_mesin);
			$sheet->setCellValue('M'.$cell_number, $item->jumlah_palka);
			$sheet->setCellValue('N'.$cell_number, $item->kapasitas_palka);
			$sheet->setCellValue('O'.$cell_number, $tempat_pendaftaran);
			$sheet->setCellValue('P'.$cell_number, $gross_akte);
			$sheet->setCellValue('Q'.$cell_number, $item->panjang_kapal);
			$sheet->setCellValue('R'.$cell_number, $item->lebar_kapal);
			$sheet->setCellValue('S'.$cell_number, $item->dalam_kapal);
			$sheet->setCellValue('T'.$cell_number, $item->gt_kapal);
			$sheet->setCellValue('U'.$cell_number, $item->nt_kapal);
			$sheet->setCellValue('V'.$cell_number, $item->nama_perusahaan);
			$sheet->setCellValue('W'.$cell_number, $item->alamat_perusahaan);
			$sheet->setCellValue('X'.$cell_number, $item->keterangan_pendok);
			$sheet->setCellValue('Y'.$cell_number, fmt_tgl($item->tgl_ttd_direktur));
			$cell_number++;           
			$number++;           

	    }                      

	    //MENGATUR UKURAN KOLOM
	    $array_thwidth = array( 'A' => 5,
	                            'B' => 40,
	                            'C' => 50,
	                            'D' => 40,
	                            'E' => 40,
	                            'F' => 40,
	                            'G' => 40,
	                            'H' => 40,
	                            'I' => 40,
	                            'J' => 50,
	                            'K' => 40,
	                            'L' => 40,
	                            'M' => 40,
	                            'N' => 40,
	                            'O' => 40,
	                            'P' => 40,
	                            'Q' => 50,
	                            'R' => 40,
	                            'S' => 40,
	                            'T' => 40,
	                            'U' => 40,
	                            'V' => 40,
	                            'W' => 100,
	                            'X' => 50,
	                            'Y' => 50
	                          );
		foreach ($array_thwidth as $column => $width) {
			$sheet->getColumnDimension($column)->setWidth($width);
		}

		$styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
		$sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

		// Redirect output to a client’s web browser (Excel2007)
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Daftar Histori Kapal.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		// $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

}