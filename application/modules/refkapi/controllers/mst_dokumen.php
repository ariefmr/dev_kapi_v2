<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_dokumen extends MX_Controller {

	function __construct()
		{
			parent::__construct();

			$this->load->model('mdl_dokumen');
		}

	public function index()
	{
		$this->pengaturan();
	}

	public function pengaturan()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'tabel_dokumen_pengaturan';
		$labels = 'label_view_dokumen';

		$data['list_dokumen'] = $this->mdl_dokumen->list_dokumen();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function entry()
	{
		$data['submit_form'] = 'refkapi/mst_dokumen/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'form_edit_dokumen';
		$labels = 'form_dokumen';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		if( $this->mdl_dokumen->input($array_input) ){
			$url = base_url('refkapi/mst_dokumen');
			redirect($url);
		}else{
			$url = base_url('refkapi/mst_dokumen');
			redirect($url);
		}
	}

	public function edit($id_record)
	{

		$get_detail = $this->mdl_dokumen->detil_dokumen($id_record);

		if( !$get_detail )
		{
			$data['list_dokumen'] = 'Data tidak ditemukan';
		}else{
			$data['list_dokumen'] = (array)$get_detail;
		}

		$data['submit_form'] = 'refkapi/mst_dokumen/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'refkapi';
		$views = 'form_edit_dokumen';
		$labels = 'form_dokumen';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_to_edit = $array_input;

		if( $this->mdl_dokumen->update($array_to_edit) ){
			$url = base_url('refkapi/mst_dokumen');
			redirect($url);
		}else{
			$url = base_url('refkapi/mst_dokumen/pengaturan');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_dokumen->delete($id);

        $url = base_url('refkapi/mst_dokumen');
        redirect($url);
    }

	public function list_dokumen_json()
	{
		$this->load->model('mdl_dokumen');

		$list_opsi = $this->mdl_dokumen->list_opsi_perubahan();

		echo json_encode($list_opsi);
	}

	public function list_dokumen_array()
	{
		$this->load->model('mdl_dokumen');

		$list_opsi = $this->mdl_dokumen->list_opsi_perubahan();

		vdump($list_opsi,false);
		return $list_opsi;
	}

	public function list_trs_pendok_dokumen_per_id($id_pendok)
	{
		$this->load->model('mdl_dokumen');

		$list_opsi = $this->mdl_dokumen->list_trs_pendok_dokumen($id_pendok);

		vdump($list_opsi,false);
		return $list_opsi;
	}
	
}
?>