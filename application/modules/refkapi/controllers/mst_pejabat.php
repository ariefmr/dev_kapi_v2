<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_pejabat extends MX_Controller {

		function __construct()
			{
				parent::__construct();
				
				$this->load->model('mdl_pejabat');
			}

	public function index()
	{
		$this->pejabat();
	}

	public function pilih_pejabat($judul_jabatan = "Pejabat",$marking = 0,$nama_pejabat,$id_pejabat)
	{
		$data['judul_jabatan'] = $judul_jabatan;
		$data['nama_pejabat'] = $nama_pejabat;
		$data['id_pejabat'] = $id_pejabat;
		$data['marking']	   = $marking;
		$this->load->view('pilih_pejabat',$data);
	}
	
	public function pejabat( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if(($filter === 'daerah')){
			$labels = 'label_view_pejabat_daerah';
			$data['list_pejabat'] = $this->mdl_pejabat->list_pejabat_daerah();
		}else{
			$labels = 'label_view_pejabat';
			$data['list_pejabat'] = $this->mdl_pejabat->list_pejabat();
		}
			echo Modules::run('templates/page/v_form', 'refdss', 'tabel_pejabat', $labels, $add_js, $add_css, $data);

	}

	public function detail_pejabat()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_id_pejabat = $this->input->get('ip', FALSE);
		$detail_result = $this->mdl_pejabat->detil_pejabat($get_filter, $get_id_pejabat);

		echo json_encode($detail_result);

	}

	public function search_pejabat()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_search_like = $this->input->get('q', FALSE);
		$get_search_filter = $this->input->get('filter', FALSE);

		$search_result = $this->mdl_pejabat->cari_pejabat($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		if($get_filter === 'count'){
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result
						);
		}else{
			$json = array( 
						'filter' => $get_filter, 
						'search_like' => $get_search_like,
						'jumlah_result' => $jml_search_result,
						'result' => $search_result
						);
		}

		echo json_encode($json);
	}



}