<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_tipe_perubahan extends CI_Model
{
    private $db_kapi;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_opsi_perubahan()
    {
        $query = "SELECT tipe.id_tipe_perubahan AS id, 
                         tipe.id_kategori_perubahan as id_kategori,
                         kat.nama_kategori as kategori,
                         tipe.jenis_perubahan as text,
                         tipe.kolom_db,
                         kat.variable
                  FROM db_pendaftaran_kapal.mst_tipe_perubahan tipe,
                       db_pendaftaran_kapal.mst_kategori_perubahan kat
                  WHERE tipe.id_kategori_perubahan = kat.id_kategori_perubahan
                  ORDER BY tipe.id_kategori_perubahan ASC";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_perubahan_per_pendok($id_pendok)
    {
        $query = "SELECT 
                        tp.id_perubahan,
                        tp.id_pendok,
                        tp.id_tipe_perubahan,
                        mtp.jenis_perubahan
                        FROM db_pendaftaran_kapal.trs_perubahan tp,
                            db_pendaftaran_kapal.mst_tipe_perubahan mtp
                        WHERE tp.id_pendok = '".$id_pendok."'
                            and tp.id_tipe_perubahan = mtp.id_tipe_perubahan
                            and tp.aktif = 'ya'
                    ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}