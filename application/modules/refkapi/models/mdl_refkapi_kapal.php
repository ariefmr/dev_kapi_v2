<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_refkapi_kapal extends CI_Model
{
    private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_kapal($limit = '100')
    {
        $query = '  SELECT 
                        db_pendaftaran_kapal.mst_kapal.id_kapal, 
                        db_pendaftaran_kapal.mst_kapal.nama_kapal, 
                        db_pendaftaran_kapal.mst_kapal.nama_kapal_sebelumnya,
                        db_pendaftaran_kapal.mst_kapal.tahun_pembangunan,
                        db_pendaftaran_kapal.mst_kapal.gt_kapal,
                        db_pendaftaran_kapal.mst_kapal.nt_kapal,
                        db_pendaftaran_kapal.mst_kapal.panjang_kapal,
                        db_pendaftaran_kapal.mst_kapal.lebar_kapal,
                        db_pendaftaran_kapal.mst_kapal.dalam_kapal,
                        db_pendaftaran_kapal.mst_kapal.no_mesin,
                        db_pendaftaran_kapal.mst_kapal.merek_mesin,
                        db_pendaftaran_kapal.mst_kapal.tipe_mesin,
                        db_pendaftaran_kapal.mst_kapal.daya_kapal,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap,
                        db_master.mst_perusahaan.nama_penanggung_jawab, 
                        db_master.mst_perusahaan.nama_perusahaan
                    FROM db_pendaftaran_kapal.mst_kapal
                    LEFT JOIN ( db_master.mst_perusahaan,
                                db_master.mst_alat_tangkap,
                                db_master.mst_jenis_kapal,
                                db_master.mst_bahan_kapal
                               )
                        ON (    db_pendaftaran_kapal.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan
                                AND db_pendaftaran_kapal.mst_kapal.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                                AND db_pendaftaran_kapal.mst_kapal.id_jenis_kapal = db_master.mst_jenis_kapal.id_jenis_kapal
                                AND db_pendaftaran_kapal.mst_kapal.id_bahan_kapal = db_master.mst_bahan_kapal.id_bahan_kapal
                            )
                    LIMIT 0, '.$limit.' ';
        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_kapal($id_kapal)
    {

        $sql = "SELECT
                db_pendaftaran_kapal.mst_kapal.id_kapal, 
                db_pendaftaran_kapal.mst_kapal.nama_kapal, 
                db_pendaftaran_kapal.mst_kapal.nama_kapal_sebelumnya,
                db_pendaftaran_kapal.mst_kapal.tahun_pembangunan,
                db_pendaftaran_kapal.mst_kapal.gt_kapal,
                db_pendaftaran_kapal.mst_kapal.nt_kapal,
                db_pendaftaran_kapal.mst_kapal.panjang_kapal,
                db_pendaftaran_kapal.mst_kapal.lebar_kapal,
                db_pendaftaran_kapal.mst_kapal.dalam_kapal,
                db_pendaftaran_kapal.mst_kapal.no_mesin,
                db_pendaftaran_kapal.mst_kapal.merek_mesin,
                db_pendaftaran_kapal.mst_kapal.tipe_mesin,
                db_pendaftaran_kapal.mst_kapal.daya_kapal,
                db_master.mst_jenis_kapal.nama_jenis_kapal,
                db_master.mst_bahan_kapal.nama_bahan_kapal,
                db_master.mst_alat_tangkap.nama_alat_tangkap, 
                db_master.mst_perusahaan.nama_penanggung_jawab, 
                db_master.mst_perusahaan.nama_perusahaan
            FROM db_pendaftaran_kapal.mst_kapal
            LEFT OUTER JOIN (   db_master.mst_perusahaan, 
                                db_master.mst_alat_tangkap, 
                                db_master.mst_jenis_kapal, 
                                db_master.mst_bahan_kapal
                            )
            ON (    db_pendaftaran_kapal.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan
                    AND db_pendaftaran_kapal.mst_kapal.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                    AND db_pendaftaran_kapal.mst_kapal.id_jenis_kapal = db_master.mst_jenis_kapal.id_jenis_kapal
                    AND db_pendaftaran_kapal.mst_kapal.id_bahan_kapal = db_master.mst_bahan_kapal.id_bahan_kapal
                )
            WHERE mst_kapal.id_kapal = $id_kapal ";

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }  

    public function cari_kapal($filter, $search_like)
    {

        switch ($filter) {
            
            case 'data':
                $sql = "SELECT 
                        db_pendaftaran_kapal.mst_kapal.id_kapal, 
                        db_pendaftaran_kapal.mst_kapal.nama_kapal, 
                        db_pendaftaran_kapal.mst_kapal.nama_kapal_sebelumnya,
                        db_pendaftaran_kapal.mst_kapal.tahun_pembangunan,
                        db_pendaftaran_kapal.mst_kapal.gt_kapal,
                        db_pendaftaran_kapal.mst_kapal.nt_kapal,
                        db_pendaftaran_kapal.mst_kapal.panjang_kapal,
                        db_pendaftaran_kapal.mst_kapal.lebar_kapal,
                        db_pendaftaran_kapal.mst_kapal.dalam_kapal,
                        db_pendaftaran_kapal.mst_kapal.no_mesin,
                        db_pendaftaran_kapal.mst_kapal.merek_mesin,
                        db_pendaftaran_kapal.mst_kapal.tipe_mesin,
                        db_pendaftaran_kapal.mst_kapal.daya_kapal,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap,
                        db_master.mst_perusahaan.nama_penanggung_jawab, 
                        db_master.mst_perusahaan.nama_perusahaan
                    FROM db_pendaftaran_kapal.mst_kapal
                    LEFT JOIN ( db_master.mst_perusahaan,
                                db_master.mst_alat_tangkap,
                                db_master.mst_jenis_kapal,
                                db_master.mst_bahan_kapal
                               )
                        ON (    db_pendaftaran_kapal.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan
                                AND db_pendaftaran_kapal.mst_kapal.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                                AND db_pendaftaran_kapal.mst_kapal.id_jenis_kapal = db_master.mst_jenis_kapal.id_jenis_kapal
                                AND db_pendaftaran_kapal.mst_kapal.id_bahan_kapal = db_master.mst_bahan_kapal.id_bahan_kapal
                            )
                    WHERE db_pendaftaran_kapal.mst_kapal.nama_kapal LIKE '%".$search_like."%' ";
                break;
                
            case 'count':
                $sql = "SELECT count(db_pendaftaran_kapal.mst_kapal.nama_kapal)
                    FROM db_pendaftaran_kapal.mst_kapal
                    LEFT JOIN ( db_master.mst_perusahaan,
                                db_master.mst_alat_tangkap,
                                db_master.mst_jenis_kapal,
                                db_master.mst_bahan_kapal
                               )
                        ON (    db_pendaftaran_kapal.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan
                                AND db_pendaftaran_kapal.mst_kapal.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                                AND db_pendaftaran_kapal.mst_kapal.id_jenis_kapal = db_master.mst_jenis_kapal.id_jenis_kapal
                                AND db_pendaftaran_kapal.mst_kapal.id_bahan_kapal = db_master.mst_bahan_kapal.id_bahan_kapal
                            )
                    WHERE db_pendaftaran_kapal.mst_kapal.nama_kapal LIKE '%".$search_like."%' ";
                    
                break;
        }

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_by_noreg($search_like)
    {
        /*$sql = "SELECT 
                    bkp.*,
                    kapal.no_register,
                    perusahaan.nama_perusahaan,
                    jenis_kapal.nama_jenis_kapal,
                    bahan_kapal.nama_bahan_kapal

                FROM 
                    db_pendaftaran_kapal.trs_pendok pendok,
                    db_pendaftaran_kapal.trs_bkp bkp,
                    db_pendaftaran_kapal.mst_kapal kapal,
                    db_master.mst_perusahaan perusahaan,
                    (select id_bkp_terakhir, id_kapal
                        from db_pendaftaran_kapal.mst_kapal 
                        where no_register like '%".$search_like."%') subq,
                    db_master.mst_jenis_kapal as jenis_kapal,
                    db_master.mst_bahan_kapal as bahan_kapal
                WHERE bkp.id_bkp = subq.id_bkp_terakhir
                        and kapal.id_kapal = subq.id_kapal
                        and bkp.id_perusahaan = perusahaan.id_perusahaan
                        and kapal.id_pendok_terakhir = pendok.id_pendok
                        and pendok.status_cetak_bkp = 'YA' 
                        AND bkp.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                        AND bkp.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                        ";*/
        $sql =   "SELECT    jenis_kapal.nama_jenis_kapal,
                            bahan_kapal.nama_bahan_kapal,
                            subq.*
                    FROM    (select 
                                    kapal.no_register,
                                    kapal.nama_kapal_terbaru,
                                    kapal.id_bkp_terakhir,
                                    kapal.id_pendok_terakhir,
                                    buku.*
                                from
                                    db_pendaftaran_kapal.mst_kapal as kapal,
                                    db_pendaftaran_kapal.trs_bkp as buku
                                where
                                    kapal.id_bkp_terakhir = buku.id_bkp
                                        and kapal.no_register like '%".$search_like."%') subq
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                       )
                            ON          (
                                        subq.id_bkp = kapal.id_bkp_terakhir
                                        and subq.id_pendok = pendok.id_pendok
                                        and subq.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        and subq.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and subq.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        )
                    ";

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_by_nama($search_like)
    {
        // echo 'OK';
        $sql = "SELECT 
                    bkp.*,
                    perusahaan.nama_perusahaan
                FROM 
                    db_pendaftaran_kapal.trs_bkp bkp
                LEFT JOIN 
                    db_master.mst_perusahaan perusahaan
                ON 
                    bkp.id_perusahaan = perusahaan.id_perusahaan
                WHERE 
                    bkp.nama_kapal like '%".$search_like."%'
                    AND bkp.aktif = 'YA' ";

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function history($id_kapal,$id_bkp)
    {
        $sql = "SELECT 
                    bkp.*,
                    kapal.no_register,
                    perusahaan.nama_perusahaan

                FROM 
                    db_pendaftaran_kapal.trs_bkp bkp,
                    db_pendaftaran_kapal.mst_kapal kapal,
                    db_master.mst_perusahaan perusahaan
                WHERE bkp.id_perusahaan = perusahaan.id_perusahaan 
                      AND bkp.id_kapal = '".$id_kapal."'
                      AND bkp.id_bkp <> '".$id_bkp."'";

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($filter, $data)
    {
        switch ($filter) {
            case 'pusat':
                $this->db_kapi->insert('mst_kapal', $data);
                break;
            
            case 'daerah':
                $this->db_kapi->insert('mst_kapal_daerah', $data);
                break;
        }
    }

    public function list_tempat_pembangunan()
    {
        $query = "SELECT 
                        uni.nama AS ID, CONCAT(uni.nama, ' , ', uni.tingkat) AS TEXT
                    FROM
                        (SELECT 
                            nama_desa AS nama, 'desa' AS tingkat
                        FROM
                            mst_desa_pembangunan union
                        SELECT 
                            nama_kecamatan AS nama, 'kecamatan' AS tingkat
                        FROM
                            mst_kecamatan_pembangunan union
                        SELECT 
                            nama_kabupaten_kota AS nama, 'kabkota' AS tingkat
                        FROM
                            mst_kabupaten_kota union 
                        SELECT 
                            nama_negara AS nama, 'negara' AS tingkat
                        FROM
                            db_master.mst_negara) uni
                    ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}