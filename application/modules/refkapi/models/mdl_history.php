<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_history extends CI_Model
{
	// private $db_dss;
	// private $db_kapi;

	function __construct()
	{
		// $this->db_dss = $this->load->database('db_dss', TRUE);
		// $this->db_kapi = $this->load->database('default', TRUE);
	}

	// get data history kapal
	public function history($id_kapal = 0,$id_bkp = 0)
	{
		$sql = "SELECT 
					bkp.*,
					kapal.no_register,
					jenis_kapal.nama_jenis_kapal,
					alat_tangkap.nama_alat_tangkap,
	                bahan_kapal.nama_bahan_kapal,
	                pendok.keterangan_pendok,
	                pendok.kategori_pendaftaran

					FROM 
					db_pendaftaran_kapal.trs_bkp bkp,
					db_pendaftaran_kapal.mst_kapal kapal,
					db_pendaftaran_kapal.trs_pendok pendok,
					db_master.mst_jenis_kapal as jenis_kapal,
					db_master.mst_alat_tangkap as alat_tangkap,
					db_master.mst_bahan_kapal as bahan_kapal

					WHERE 
					bkp.id_kapal = '".$id_kapal."'
					AND bkp.id_kapal = kapal.id_kapal 
					AND pendok.id_pendok = bkp.id_pendok
					AND bkp.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                    AND bkp.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                    AND bkp.id_bahan_kapal = bahan_kapal.id_bahan_kapal
					";

		// $run_query = $this->db_dss->query($sql);                            
		$run_query = $this->db->query($sql);                            

		if($run_query->num_rows() > 0){
			$result = $run_query->result();
		}else{
			$result = false;
		}
		return $result;
	}
	
	public function tipe_perubahan($id_kapal = 0,$id_bkp = 0)
	{
		$sql = " SELECT 
					bkp.id_bkp, 
					tipe.jenis_perubahan
					FROM 
						db_pendaftaran_kapal.trs_bkp bkp,
						db_pendaftaran_kapal.mst_kapal kapal,
						db_master.mst_perusahaan perusahaan,
						db_pendaftaran_kapal.trs_perubahan ubah,
						db_pendaftaran_kapal.mst_tipe_perubahan tipe
					WHERE bkp.id_perusahaan = perusahaan.id_perusahaan 
						AND bkp.id_kapal = '".$id_kapal."'
						AND bkp.id_kapal = kapal.id_kapal
						AND bkp.id_pendok = ubah.id_pendok 
						AND tipe.id_tipe_perubahan = ubah.id_tipe_perubahan
						AND ubah.aktif = 'ya'
					ORDER BY bkp.id_bkp ASC
					";

		$run_query = $this->db->query($sql);

		if($run_query->num_rows() > 0){
			$result = $run_query->result();
		}else{
			$result = false;
		}
		return $result;
	}

	public function get_nama_kapal_register($id_kapal){

		$sql = "
				select nama_kapal_terbaru, no_register, id_kapal
				from db_pendaftaran_kapal.mst_kapal
				where id_kapal = '".$id_kapal."'
				";

		$run_query = $this->db->query($sql);

		if($run_query->num_rows() > 0){
			$result = $run_query->row();
		}else{
			$result = false;
		}
		return $result;
	}

}

?>