<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pejabat extends CI_Model
{
    private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_pejabat()
    {
        $query = "

                 ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_pejabat($id)
    {
        $sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_pejabat
                    WHERE mst_pejabat.id_pejabat = $id ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_pejabat($search_like)
    {
        $sql = "SELECT *
                FROM db_pendaftaran_kapal.mst_pejabat
                WHERE mst_pejabat.aktif = 'YA'
                AND mst_pejabat.nama_pejabat LIKE '%".$search_like."%'
                ";

        $run_query = $this->db_kapi->query($sql);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {

        $this->db_kapi->insert('mst_pejabat', $data);
        
    }

    public function update($data)
    {
        $this->db_kapi->where('id_pejabat', $data['id_pejabat']);
        $query = $this->db_kapi->update('mst_pejabat',$data);

        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE db_pendaftaran_kapal.mst_pejabat SET aktif='Tidak' WHERE id_pejabat=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }



}