<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_dokumen extends CI_Model
{
    private $db_kapi;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_dokumen()
    {
        $query = "SELECT *
                    FROM db_pendaftaran_kapal.mst_dokumen;
                    ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_dokumen($id)
    {
        $sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_dokumen
                    WHERE mst_dokumen.id_dokumen = $id ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_kapi->insert('mst_dokumen', $data);
    }

    public function update($data)
    {
        $this->db_kapi->where('id_dokumen', $data['id_dokumen']);
        $query = $this->db_kapi->update('mst_dokumen',$data);

        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_perubahan()
    {
        $query = "SELECT id_dokumen AS id, nama_dokumen as text 
                    FROM mst_dokumen 
                    WHERE peraturan = 'SEMUA'
                        and status_tampil = 'ya'
                        and aktif = 'ya' ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_trs_pendok_dokumen($id_pendok)
    {
        $query = "  SELECT 
                        mst.nama_dokumen as text,
                        trs.id_dokumen as id,
                        trs.isi,
                        trs.path_dok
                    FROM
                        db_pendaftaran_kapal.trs_pendok_dokumen trs
                    JOIN
                        (
                        db_pendaftaran_kapal.mst_dokumen mst
                        )
                    ON (
                        trs.id_dokumen = mst.id_dokumen
                        )
                    WHERE mst.peraturan = 'SEMUA'
                        and mst.status_tampil = 'ya'
                        and mst.aktif = 'ya'
                        and trs.id_pendok = '".$id_pendok."'
                  ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}