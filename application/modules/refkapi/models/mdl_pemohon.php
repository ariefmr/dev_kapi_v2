<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pemohon extends CI_Model
{
    private $db_dss;
	private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_pemohon()
    {
        $query = "SELECT *,
                    db_master.mst_kabupaten_kota.nama_kabupaten_kota
                    FROM db_pendaftaran_kapal.mst_pemohon
                    LEFT JOIN (db_master.mst_kabupaten_kota)
                    ON ( db_master.mst_kabupaten_kota.id_kabupaten_kota = db_pendaftaran_kapal.mst_pemohon.id_kabupaten_kota
                        )
                    WHERE db_pendaftaran_kapal.mst_pemohon.aktif = 'ya'
                    ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_pemohon($id)
    {
        $sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_pemohon
                    WHERE mst_pemohon.id_pemohon = $id ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_pemohon($search_like)
    {
        $sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_pemohon
                    WHERE mst_pemohon.aktif = 'Ya'
                        AND mst_pemohon.nama_pemohon LIKE '%".$search_like."%'
                        OR mst_pemohon.no_ktp LIKE '%".$search_like."%'  
                        OR mst_pemohon.no_telp_pemohon LIKE '%".$search_like."%' ";

        $run_query = $this->db_kapi->query($sql);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {
        $this->db_kapi->insert('mst_pemohon', $data);
    }

    public function update($data)
    {
        $this->db_kapi->where('id_pemohon', $data['id_pemohon']);
        $query = $this->db_kapi->update('mst_pemohon',$data);

        if($this->db_kapi->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {

        $sql = " UPDATE db_pendaftaran_kapal.mst_pemohon SET aktif='Tidak' WHERE id_pemohon=$id ";
        
        $query = $this->db->query($sql);
        //var_dump($sql);

    }



}