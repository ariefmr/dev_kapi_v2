<?php
		$tab_now  	= explode(" ", $data_pendok->tanggal_buat);

		$tabs 		= "";
		$content 	= "";

		if($history != NULL)
		{
			foreach ($history as $key) 
			{
				
				$tab_next	= explode(" ", $key->tanggal_buat);
				$tabs 		.= '<li><a href="#'.$tab_next[0].'" >'.$tab_next[0].'</a></li>';
				$content 	.= '<div class="tab-pane" id="'.$tab_next[0].'">
				                  <div class="row">
				                      <dl class="dl-horizontal text-ref">
				                        <dt>Nama Kapal : </dt>
				                        <dd id="nama_kapal" style="margin-left: 200px;"><p  id="viewNama_kapal" >'.$key->nama_kapal.'</p></dd>
				                        
				                        <dt>Perusahaan :</dt>
				                        <dd id="nama_perusahaan" style="margin-left: 200px;"><p id="viewNama_perusahaan" >'.$key->nama_perusahaan.'</p></dd>

				                        <dt>No Register : </dt>
				                        <dd id="no_register" style="margin-left: 200px;"><p id="viewNo_register" >'.$key->no_register.'</p></dd>
				                        
				                        <dt>No Seri Buku Kapal :</dt>
				                        <dd id="wpp_info" style="margin-left: 200px;"><p id="viewNo_seri_bkp" >'.$key->no_seri_bkp.'</p></dd>

				                        <dt>No Tanda Pengenal :</dt>
				                        <dd id="no_tanda_pengenal" style="margin-left: 200px;"><p id="viewNo_tanda_pengenal" >'.$key->no_tanda_pengenal.'</p></dd>
				                        
				                        <dt>GT :</dt>
				                        <dd id="gt_kapal" style="margin-left: 200px;"><p id="viewGt" >'.$key->gt_kapal.'</p></dd>
				                        
				                        <dt>NT :</dt>
				                        <dd id="nt_kapal" style="margin-left: 200px;"><p id="viewNt" >'.$key->nt_kapal.'</p></dd>
				                        
				                        <dt>Panjang Kapal :</dt>
				                        <dd id="panjang_kapal" style="margin-left: 200px;"><p id="viewPanjang" >'.$key->panjang_kapal.' Meter</p></dd>

				                        <dt>Lebar Kapal :</dt>
				                        <dd id="lebar_kapal" style="margin-left: 200px;"><p id="viewLebar" >'.$key->lebar_kapal.' Meter</p></dd>
				                        
				                        <dt>Dalam Kapal :</dt>
				                        <dd id="dalam_kapal" style="margin-left: 200px;"><p id="viewDalam" >'.$key->dalam_kapal.' Meter</p></dd>
				                        
				                        <dt>Panjang LOA Kapal :</dt>
				                        <dd id="panjang_loa_kapal" style="margin-left: 200px;"><p id="viewLoa" >'.$key->panjang_loa_kapal.' Meter</p></dd>
				                      </dl>
				                  </div>
				                </div>';

			}
		}

		//print_r(Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array'));

		/* check_nama_kapal */
		    /*
		    $check_nama_kapal = array(   
		    							'name' 	=> 	'check_nama_kapal',
		                                'label' => 	'Nama Kapal',
		                                'value'	=> 	''
		                          	);
		    $check_nama_perusahaan	=	array(
		    									'name' 	=> 	'check_nama_perusahaan',
				                                'label' => 	'Nama Perusahaan',
				                                'value'	=> 	''
		    								);
		    $check_no_register		=	array(
		    									'name' 	=> 	'check_no_register',
				                                'label' => 	'No Register',
				                                'value'	=> 	''
		    								);
		    $check_no_seri_bkp		=	array(
		    									'name' 	=> 	'check_no_seri_bkp',
				                                'label' => 	'No Seri BUku Kapal',
				                                'value'	=> 	''
		    								);
		    $check_no_tanda_pengenal	=	array(
		    									'name' 	=> 	'check_no_tanda_pengenal',
				                                'label' => 	'No Tanda Pengenal',
				                                'value'	=> 	''
		    								);
		    $check_gt_kapal		=	array(
		    									'name' 	=> 	'check_gt_kapal',
				                                'label' => 	'GT',
				                                'value'	=> 	''
		    								);
		    $check_nt_kapal		=	array(
		    									'name' 	=> 	'check_nt_kapal',
				                                'label' => 	'NT',
				                                'value'	=> 	''
		    								);
		    $check_panjang_kapal=	array(
		    									'name' 	=> 	'check_panjang_kapal',
				                                'label' => 	'Panjang',
				                                'value'	=> 	''
		    								);
		    $check_lebar_kapal	=	array(
		    									'name' 	=> 	'check_lebar_kapal',
				                                'label' => 	'Lebar',
				                                'value'	=> 	''
		    								);
		    $check_dalam_kapal	=	array(
		    									'name' 	=> 	'check_dalam_kapal',
				                                'label' => 	'Dalam',
				                                'value'	=> 	''
		    								);
		    $check_loa_kapal	=	array(
		    									'name' 	=> 	'check_loa_kapal',
				                                'label' => 	'LOA',
				                                'value'	=> 	''
		    								);
			*/

?>
				<style type="text/css">
					.list-group-item{
						padding: 2px 30px 0px 15px;
						background-color:transparent;
						border:0px;
					}
				</style>
				  <ul id="myTab" class="nav nav-tabs">
	                <li class="active"><a href="#<?=$tab_now[0]?>" data-toggle="tab"><?=$tab_now[0]?></a></li>
	                <?=$tabs?>
	              </ul>
	              <div class="tab-content" >
	                <div class="tab-pane active" id="<?=$tab_now[0]?>">
	                  <div class="col-sm-7">
	                      <dl class="dl-horizontal text-ref">
	                        <dt>Nama Kapal : </dt>
	                        <dd id="nama_kapal" style="margin-left: 200px;"><p  id="viewNama_kapal" ><?=$data_pendok->nama_kapal?></p></dd>
	                        
	                        <dt>Perusahaan :</dt>
	                        <dd id="nama_perusahaan" style="margin-left: 200px;"><p id="viewNama_perusahaan" ><?=$data_pendok->nama_perusahaan?></p></dd>

	                        <dt>No Register : </dt>
	                        <dd id="no_register" style="margin-left: 200px;"><p id="viewNo_register" ><?=$data_pendok->no_register?></p></dd>
	                        
	                        <dt>No Seri Buku Kapal :</dt>
	                        <dd id="wpp_info" style="margin-left: 200px;"><p id="viewNo_seri_bkp" ><?=$data_pendok->no_seri_bkp?></p></dd>

	                        <dt>No Tanda Pengenal :</dt>
	                        <dd id="no_tanda_pengenal" style="margin-left: 200px;"><p id="viewNo_tanda_pengenal" ><?=$data_pendok->no_tanda_pengenal?></p></dd>
	                        
	                        <dt>GT :</dt>
	                        <dd id="gt_kapal" style="margin-left: 200px;"><p id="viewGt" ><?=$data_pendok->gt_kapal?></p></dd>
	                        
	                        <dt>NT :</dt>
	                        <dd id="nt_kapal" style="margin-left: 200px;"><p id="viewNt" ><?=$data_pendok->nt_kapal?></p></dd>
	                        
	                        <dt>Panjang Kapal :</dt>
	                        <dd id="panjang_kapal" style="margin-left: 200px;"><p id="viewPanjang" ><?=$data_pendok->panjang_kapal?> Meter</p></dd>

	                        <dt>Lebar Kapal :</dt>
	                        <dd id="lebar_kapal" style="margin-left: 200px;"><p id="viewLebar" ><?=$data_pendok->lebar_kapal?> Meter</p></dd>
	                        
	                        <dt>Dalam Kapal :</dt>
	                        <dd id="dalam_kapal" style="margin-left: 200px;"><p id="viewDalam" ><?=$data_pendok->dalam_kapal?> Meter</p></dd>
	                        
	                        <dt>Panjang LOA Kapal :</dt>
	                        <dd id="panjang_loa_kapal" style="margin-left: 200px;"><p id="viewLoa" ><?=$data_pendok->panjang_loa_kapal?> Meter</p></dd>
	                      </dl>
	                  </div>
	                  <div class="col-sm-5" style="margin-top:10px;" >
	                  	<!-- 
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_nama_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_nama_perusahaan)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_no_register)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_no_seri_bkp)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_no_tanda_pengenal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_gt_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_nt_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_panjang_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_lebar_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_dalam_kapal)?>
	                  	</li>
	                  	<li class="list-group-item kel-dok">
	                  		<?=$this->mkform->input_checkbox($check_loa_kapal)?>
	                  	</li>
	                  	 -->
	        <?php 

			/*	        
			$array_checkbox = array(
	        						'name'	=> 'check_nama_kapal',
	        						'labels' => 'check_nama_kapal'
	        						);
	        $this->mkform->input_checkbox($array_checkbox);
	        */
			
	    	$checkbox_tag = array(  
                                    'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array')
                                  );	

	    	echo $this->mkform->input_checkbox_perubahan($checkbox_tag);
            //var_dump($checkbox_tag);
            //AREA CEKBOX PERUBAHAN
            /*
            $html .= '<style>
            			.list-group-item{
            				padding: 2px 0px 2px 0px;
            			}
            		  </style>';
            $html .= "<div id='area_cekbox_perubahan' class='col-sm-3' >";
            $html .= $this->mkform->input_checkbox_perubahan($checkbox_tag);
            $html .= "</div>";
			
            //CLEAR FIX
            $html .= "<div class='clearfix'></div>";
			*/
            ?>
            		</div>
		    		</div>
		           <?=$content?>
		          </div>
		    <script>
				$('#myTab a').click(function (e) {
					  e.preventDefault()
					  $(this).tab('show')
				})
			</script>
