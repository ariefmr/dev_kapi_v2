<style type="text/css">
  .first-column {
    width: 40%;
    float: left;
  }

  .second-column {
    width: 40%;
    float: right;
  }
</style>
<?php 
  // vdump($data_nama, true);
if($data_history==false){ 

      echo "Data history kapal tidak ada.";
      die;

  }?>

<div class="panel-heading">
  <h3 class="panel-title">Nama Kapal: <?php echo $data_nama['nama_kapal_terbaru']; ?></h3>
</div>
<div class="panel-heading">
  <h3 class="panel-title">No. Register: <?php echo noreg($data_nama['no_register']); ?></h3>
</div>

<?php
  //OLAH DATA TAMPIL
  $link_entry = '<a href="xls/'.$data_nama['id_kapal'].'" class="btn btn-primary">Export to XLS</a>';
  ?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
              <?php echo $link_entry; ?><br />
            </div>
          </div>
  </div>
</div>
  <?php
  $template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
  $this->table->set_template($template);
  $this->table->set_heading('No. ',
                              'No Register',
                              'Tanda Pengenal',
                              'Nama Kapal',
                              'Nama Kapal Sebelumnya',
                              'Tempat Pembangunan',
                              'Bahan Utama Kapal',
                              'Fungsi Kapal',
                              'Jenis Alat Tangkap',
                              'Merek Mesin Utama',
                              'Daya Mesin Utama',
                              'No Mesin Utama',
                              'Jumlah Palka',
                              'Kapasitas Palka',
                              'Tempat Pendaftaran',
                              'Tempat No & Tanggal Gross Akte',
                              'Panjang',
                              'Lebar',
                              'Dalam',
                              'GT',
                              'NT',
                              'Nama Pemilik',
                              'Alamat Pemilik',
                              'Keterangan',
                              'Tanggal Tanda Tangan Direktur',
                              'Halaman 1 Dicetak oleh',
                              'Halaman 2 Dicetak oleh',
                              'Halaman 3 Dicetak oleh',
                              'Halaman 4 Dicetak oleh'
                               );
      $number=1;
        foreach ($data_history as $key => $item) {

          /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
          $tempat_pembangunan = $item->tempat_pembangunan;
          switch ($item->kategori_pendaftaran) {
                                              case 'PUSAT':
                                                  $tempat_pendaftaran = "KKP";
                                                  break;

                                              case 'PROPINSI':
                                                  $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
                                                  break;
                                              
                                              case 'KAB/KOTA':
                                                  $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                                  break;

                                              default:
                                                  $tempat_pendaftaran = "KKP";
                                                  break;
                                          }
          $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));
          $this->table->add_row( 
                                $number ,
                                noreg($item->no_register) ,
                                $item->no_tanda_pengenal ,
                                $item->nama_kapal,
                                $item->nama_kapal_sblm ,
                                $tempat_pembangunan ,
                                $item->nama_bahan_kapal,
                                $item->nama_jenis_kapal ,
                                $item->alat_tangkap ,
                                $item->merek_mesin ,
                                $item->daya_kapal ,
                                $item->no_mesin,
                                $item->jumlah_palka,
                                $item->kapasitas_palka,
                                $tempat_pendaftaran,
                                $gross_akte ,
                                $item->panjang_kapal ,
                                $item->lebar_kapal ,
                                $item->dalam_kapal ,
                                $item->gt_kapal ,
                                $item->nt_kapal ,
                                kos($item->nama_perusahaan,'-' ),
                                kos($item->alamat_perusahaan,'-' ),
                                kos($item->keterangan_pendok,'-' ),
                                fmt_tgl($item->tgl_ttd_direktur),
                                kos($item->nama_pengguna,'-' ),
                                kos($item->nama_pengguna,'-' ),
                                kos($item->nama_pengguna,'-' ),
                                kos($item->nama_pengguna,'-' )
                                );
          $number++;
        }

        $table_histori = $this->table->generate();

        $this->table->clear();

      ?>
<div class="row">
    <div class="col-lg-12" style="overflow-x: auto;">
      <?php echo $table_histori; ?>    
    </div>
  </div>

<script>
  $(document).ready( function () {
    // $("#table_container").hide();

    $('#table_daftar_pendok').dataTable( {
      "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      "aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
          "bFilter": false,
          "bAutoWidth": false,
          "bInfo": false,
          "bPaginate": true,
          "bSort": true,
          "fnInitComplete": function(oSettings, json) {
            $("#loading_info").hide();
        $("#table_container").removeClass('hidden');
        }
    } );
  } );
</script>