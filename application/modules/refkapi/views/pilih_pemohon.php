<!-- TAMPIL DATA -->
<!--     <div id="panel-pilih-pemohon" class="panel">
        <div class="panel-body"> -->
      <div class="row">
            <div class="col-sm-3 control-label text-right" style=" padding-right: 15px;">
             <!--  <button id="toggle_gt" type="button" class="btn btn-primary" data-current-gt="below" data-toggle="button">&#8804 30 GT</button>
 -->
             <!--  <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  Cari <text id="info_filter">Nama pemohon  </text>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#" class="ubah_filter" data-this-filter="nama_pemohon">Nama pemohon</a></li>
                  <li><a href="#" class="ubah_filter" data-this-filter="no_telp_pemohon">Nomor Telepon Pemohon</a></li>
                  <li><a href="#" class="ubah_filter" data-this-filter="no_ktp">Nomor KTP</a></li>                  
                </ul>
              </div>
 -->
            <strong>Pemohon</strong> 
          </div>
          <div class="col-sm-8" style="margin-left: 5px;padding-left: 10px;padding-right: 20px;">
             <!-- <input id="search_pemohon" name="id_pemohon" type="hidden" class="bigdrop"> -->
             <input type="hidden" id="search_pemohon" data-init-text="<?=$nama_pemohon?>" value="<?=$nama_pemohon?>" class="bigdrop">
             <input type="hidden" name="id_pemohon" data-init-text="<?=$id_pemohon?>" value="<?=$id_pemohon?>" id="id_pemohon" class="bigdrop" />
             <input type="hidden" name="nama_pemohon" data-init-text="<?=$nama_pemohon?>" value="<?=$nama_pemohon?>" id="nama_pemohon" class="bigdrop" />

          </div>
           <!-- <input type="hidden" name="nama_pemohon" id="inputNama_pemohon" class="form-control" value="">
           <input type="hidden" name="tempat_lahir_pemohon" id="inputTempat_lahir_pemohon" class="form-control" value="">
           <input type="hidden" name="tanggal_lahir_pemohon" id="inputTanggal_lahir_pemohon" class="form-control" value="">
           <input type="hidden" name="no_ktp" id="inputNo_ktp" class="form-control" value=""> 
           <input type="hidden" name="email_pemohon" id="inputEmail_pemohon" class="form-control" value="">  
           <input type="hidden" name="alamat_pemohon" id="inputAlamat_pemohon" class="form-control" value="">
           <input type="hidden" name="jabatan_pemohon" id="inputJabatan_pemohon" class="form-control" value="">
           <input type="hidden" name="no_telp_pemohon" id="inputNo_telp_pemohon" class="form-control" value=""> -->
      </div>
<!--     </div>
  </div> -->


<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_pemohon = "<?php echo base_url('refkapi/mst_pemohon/search_pemohon'); ?>",
    current_gt = function(){ return $("#toggle_gt").data("currentGt") },
    current_filter = 'nama_pemohon';

  function formatListpemohonResult(pemohon)
  {
    html = "<table id='"+pemohon.id_pemohon+"' class='table table-condensed table-bordered'><tr>"
       + "<td>"+pemohon.no_ktp+"</td>"
       + "<td>"+pemohon.nama_pemohon+"</td>"
       + "<td>"+pemohon.alamat_pemohon+". <br>Telp: ("+pemohon.no_telp_pemohon+").</td>"       
       + "<td>"+pemohon.jabatan_pemohon+"</td>"
       + "</tr></table>"
    return  html;

  }

  function set_data_pemohon(pemohon)
  {
    console.dir(pemohon);

    document.getElementById('search_pemohon').onclick = function(){
      $("#id_pemohon").val(pemohon.id_pemohon);
      $("#nama_pemohon").val(pemohon.nama_pemohon);
    }
    /*$("#inputNama_pemohon").val(pemohon.nama_pemohon);
    $("#inputJabatan_pemohon").val(pemohon.jabatan_pemohon);
    $("#inputAlamat_pemohon").val(pemohon.alamat_pemohon);
    $("#inputEmail_pemohon").val(pemohon.email_pemohon);
    $("#inputNo_ktp").val(pemohon.no_ktp);
    $("#inputTanggal_lahir_pemohon").val(pemohon.tanggal_lahir_pemohon);
    $("#inputTempat_lahir_pemohon").val(pemohon.tempat_lahir_pemohon);
    $("#inputNo_telp_pemohon").val(pemohon.no_telp_pemohon);*/
  }

  function formatListpemohonSelection(pemohon)
  {
    set_data_pemohon(pemohon);
    return pemohon.nama_pemohon;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
      info_filter = $("#info_filter").text();
      info_gt = $("#toggle_gt").text();
      text_info = "Pencarian pemohon "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/pemohon/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/pemohon/add/" ;
    //     }
     
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. pemohon Belum Terdaftar?";
  }


  function searchURLGenerator()
  {

  }

  function formatInisialPemohon(element,callback)
  {
      var elementText = $(element).attr('data-init-text');
      callback({"nama_pemohon":elementText});
  }

  $(document).ready( function () {
    
    // $("#toggle_gt").click(function(){
    //   var currentGt = $(this).data("currentGt");
    //     if(currentGt === "below")
    //     {
    //       $(this).text("> 30 GT");
    //       $(this).data("currentGt","above");
          
    //     }else
    //     {
    //       $(this).html("&#8804 30 GT");
    //       $(this).data("currentGt","below");      
    //     }
    //     $("#search_pemohon").select2("open");
    // });

    $("#search_pemohon").select2({
                  id: function(e) { return e.id_pemohon },
                  allowClear: false,    
                  placeholder: "Pilih pemohon..",
                  width: "50%",
                  cache: true,
                  minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
                          url: url_search_pemohon,
                          dataType: "json",
                          quietMillis: 2000,
                          data: function(term, page){
                                         return {
                                  q: term,
                                  // i: 'data',
                                  // filter: current_filter,
                                  // limit: 100 // TODO : tentuin limit result
                                     };
                      },
                      results: function(data, page){
                                   return {results: data.result};
                          }
                  },
                                    formatResult: formatListpemohonResult,
                                    formatSelection: formatListpemohonSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound,
                                    initSelection: formatInisialPemohon
                                    });

    $("#search_pemohon").on("change",function(e) { 
                    //console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
                      // get_detail_pemohon(e.val, current_gt());
                      // var nama_pemohon_chosen = $("span.select2-chosen").text();
                      // $("#inputNama_pemohon").val(nama_pemohon_chosen);
                    });
    /*

     ajax: {
                                          url: "<?php echo base_url('mst_pemohon/json_pemohon'); ?>",
                                          dataType: "jsonp",
                                          data: function(term, page){
                                            return {
                                              q: "term",
                                              limit: 100 // TODO : tentuin limit result
                                            };
                                          },
                                          results: function(data, page){
                                            return {results: data}
                                          }
    $('#search_pemohon').keyup(function(e){
      clearTimeout(thread);
      
      var keyword = $(this).val();

      thread = setTimeout(function(){
        update_result_pemohon(keyword);
      } ,search_response_time);
    });
    */  
  } );
</script>