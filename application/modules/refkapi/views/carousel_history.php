<style type="text/css">
  .first-column {
    width: 40%;
    float: left;
  }

  .second-column {
    width: 40%;
    float: right;
  }
</style>
<?php if($data_history==false){ 

      echo "Data history kapal tidak ada.";
      die;

  }?>
  <div id="carousel_history_bukukapal" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php
      $menu = count($data_history);
      $first = true;
      $i    = 0;
      while($i<$menu){
        ?>
        <li data-target="#carousel_history_bukukapal" data-slide-to="<?=$i?>" class="<?php echo ($first===true)? 'active' : ''; ?>" ></li>
        <?php
        $first = false;
        $i++;
      }

      ?>
    </ol>
    <div class="carousel-inner">
      <?php 
        $first = true;
        foreach ($data_history as $key => $value) {

          ?>
          <div class="item <?php echo ($first===true)? 'active' : ''; ?>">
            <div class="container">
              <dl class="dl-horizontal text-ref">
                <dt>Tanggal Perubahan : </dt>
                <dd><?=$value->tanggal_ubah?></dd>
                <dt>Data yang dirubah : </dt>
                <?php


                echo "<dd>";
                if($tipe_perubahan != NULL)
                {
                    foreach ($tipe_perubahan as $data => $isi) {
                    # code...
                    // echo $isi->id_bkp.'-'.$value->id_bkp.'<br>';
                    if($isi->id_bkp === $value->id_bkp){

                        echo $isi->jenis_perubahan.", ";

                    }
                  }
                }else{
                  echo "-";
                }
                echo "</dd>";
                echo "<hr>";


              ?>
                <dt>Nama Kapal : </dt>
                <dd id="nama_kapal" style="margin-left: 200px;"><?=kos($value->nama_kapal)?></dd>

                <dt>Perusahaan :</dt>
                <dd id="nama_perusahaan" style="margin-left: 200px;"><?=kos($value->nama_perusahaan)?></dd>

                <dt>No Register : </dt>
                <dd id="no_register" style="margin-left: 200px;"><?=kos($value->no_register)?></dd>

                <dt>No Seri Buku Kapal :</dt>
                <dd id="wpp_info" style="margin-left: 200px;"><?=kos($value->no_seri_bkp)?></dd>

                <dt>No Tanda Pengenal :</dt>
                <dd id="no_tanda_pengenal" style="margin-left: 200px;"><?=kos($value->no_tanda_pengenal)?></dd>

                <dt>Jenis Kapal :</dt>
                <dd id="nama_jenis_kapal" style="margin-left: 200px;"><?=kos($value->nama_jenis_kapal)?></dd>
                
                <dt>Alat Tangkap :</dt>
                <dd id="nama_alat_tangkap" style="margin-left: 200px;"><?=kos($value->nama_alat_tangkap)?></dd>
                
                <dt>Bahan Kapal :</dt>
                <dd id="nama_bahan_kapal" style="margin-left: 200px;"><?=kos($value->nama_bahan_kapal)?></dd>

                <dt>Merk Mesin :</dt>
                <dd id="merek_mesin" style="margin-left: 200px;"><?=kos($value->merek_mesin)?></dd>
                
                <dt>Tipe Mesin :</dt>
                <dd id="tipe_mesin" style="margin-left: 200px;"><?=kos($value->tipe_mesin)?></dd>

                <dt>No Mesin :</dt>
                <dd id="no_mesin" style="margin-left: 200px;"><?=kos($value->no_mesin)?></dd>

                <dt>Daya Kapal :</dt>
                <dd id="daya_kapal" style="margin-left: 200px;"><?=kos($value->daya_kapal)?></dd>

                <dt>GT :</dt>
                <dd id="gt_kapal" style="margin-left: 200px;"><?=kos($value->gt_kapal)?></dd>

                <dt>NT :</dt>
                <dd id="nt_kapal" style="margin-left: 200px;"><?=kos($value->nt_kapal)?></dd>

                <dt>Panjang Kapal :</dt>
                <dd id="panjang_kapal" style="margin-left: 200px;"><?=kos($value->panjang_kapal)?> meter</dd>

                <dt>Lebar Kapal :</dt>
                <dd id="lebar_kapal" style="margin-left: 200px;"><?=kos($value->lebar_kapal)?> meter</dd>

                <dt>Dalam Kapal :</dt>
                <dd id="dalam_kapal" style="margin-left: 200px;"><?=kos($value->dalam_kapal)?> meter</dd>

                <dt>Panjang LOA Kapal :</dt>
                <dd id="panjang_loa_kapal" style="margin-left: 200px;"><?=kos($value->panjang_loa_kapal)?> meter</dd>
              </dl>
            </div>
          </div>
          <?php
          $first = false;
        }

      ?>
    </div>
    <a class="left carousel-control" href="#carousel_history_bukukapal" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel_history_bukukapal" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div>

  <script type="text/javascript">
    $( document ).ready(function() {
      $('.carousel').carousel({interval: false})
   });
  </script>