<div class="row">
  <div class="col-lg-12">
          <?php
          // var_dump($list_dokumen);
          $input_hidden  = array('id_dokumen' => kos($list_dokumen['id_dokumen']), );

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $input_hidden);

          ?>
         <?php 
          $attr_nama_dokumen = array( 'name' => $form['nama_dokumen']['name'],
                                      'label' => $form['nama_dokumen']['label'],
                                      'value' => kos($list_dokumen['nama_dokumen'])
                    );
          echo $this->mkform->input_text($attr_nama_dokumen);

          $attr_peraturan = array( 'name' => $form['peraturan']['name'],
                                      'label' => $form['peraturan']['label'],
                                      'value' => kos($list_dokumen['peraturan']),
                                      'opsi' => array('SEMUA' => 'SEMUA',
                                                      'PUSAT' => 'PUSAT',
                                                      'DAERAH' => 'DAERAH'
                                                     )
                    );
          echo $this->mkform->input_select($attr_peraturan);

          $attr_wajib = array( 'name' => $form['wajib']['name'],
                                      'label' => $form['wajib']['label'],
                                      'value' => kos($list_dokumen['wajib']),
                                      'opsi' => array('YA' => 'YA',
                                                      'TIDAK' => 'TIDAK'
                                                     )
                    );
          echo $this->mkform->input_select($attr_wajib);

          $attr_status_tampil = array( 'name' => $form['status_tampil']['name'],
                                      'label' => $form['status_tampil']['label'],
                                      'value' => kos($list_dokumen['status_tampil']),
                                      'opsi' => array('YA' => 'YA',
                                                      'TIDAK' => 'TIDAK'
                                                     )
                    );
          echo $this->mkform->input_select($attr_status_tampil);          

         ?>
  </div>
</div>
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>