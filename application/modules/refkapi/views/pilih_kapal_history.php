<!-- TAMPIL DATA -->
<div id="panel-pilih-kapal" style="margin-left:12px; padding-bottom:15px;" >


  <div class="row" style="padding-bottom: 15px;">
    <div class="col-sm-3 text-right" style=" padding-right: 25px;">
      <strong><?=$cari_noreg?></strong>
    </div>
    <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
     <input id="start_search" name="id_kapal" type="text" class="bigdrop">
     <input  id="inputNama_kapal" name="nama_kapal_register" type="hidden" class="form-control">
     <input  id="inputId_pendok_terakhir" name="id_pendok_terakhir" type="hidden" class="form-control">
     <input id="inputId_pendok" name="id_pendok" type="hidden" >
     <input id="inputId_perusahaan" name="id_perusahaan" type="hidden" >
   </div>
 </div>

 <hr>

          <style type="text/css">
          .list-group-item{
            padding: 2px 30px 0px 15px;
            background-color:transparent;
            border:0px;
          }
          </style>

          <div id="result_history" ></div>
        </div>

      </div>


      <!-- ADDITIONAL JAVASCRIPT -->
      <script>
  var search_response_time = 2000, //2 Detik
  thread = null,
  url_search_kapal = "<?php echo base_url('refkapi/mst_kapal/search_by_noreg'); ?>";
  url_set_history  = "<?php echo base_url('refkapi/mst_history/tampilkan_table'); ?>";

  function formatListkapalResult(kapal)
  {
    html = "<table class='table table-condensed table-bordered' ><tr>"
    /*html = "<table id='"+kapal.id_kapal+"' class='table table-condensed table-bordered'><tr>"
    + "<th>No Register</th>"
    + "<th>Nama Kapal</th>"
    + "<th>Nama Perusahaan</th></tr><tr>"*/
    + "<td>"+kapal.no_register+"</td>"
    + "<td>"+kapal.nama_kapal+"</td>"
    + "<td>"+kapal.nama_perusahaan+"</td>"
    + "<td>GT:"+kapal.gt_kapal+"</td>"
    + "<td>"+kapal.no_grosse_akte+"</td>"
    + "</tr></table>"
    return  html;

  }

  function set_history(id_kapal,id_bkp){

    $.ajax({
      type: 'post',
      url: url_set_history,
      data: {
        data1 : id_kapal,
        data2 : id_bkp
      },
      success: function( data ) {
        $("#result_history").html(data);
      }
    });

  }

  function set_data_kapal(kapal)
  {
    console.dir(kapal);

    //set_history(JSON.stringify(kapal));
    $("#inputId_kapal").val(kapal.id_kapal);
    $("#inputId_pendok_terakhir").val(kapal.id_pendok);
    $("#inputId_pendok_terakhir").val(kapal.id_pendok);
    $("#area_referensi_kapal").css({"display":"block"});
    $("#inputId_perusahaan").val(kapal.id_perusahaan);
    $("#inputId_kapal").val(kapal.id_kapal);
    $("#inputNama_kapal").val(kapal.nama_kapal);
    $("#viewNama_kapal").text(kapal.nama_kapal);
    $("#viewNama_perusahaan").text(kapal.nama_perusahaan);
    $("#viewNo_register").text(kapal.no_register);
    $("#viewNo_seri_bkp").text(kapal.no_seri_bkp);
    $("#viewNo_tanda_pengenal").text(kapal.no_tanda_pengenal);
    $("#viewJenisKapal").text(kapal.nama_jenis_kapal);
    $("#viewAlatTangkap").text(kapal.alat_tangkap);
    $("#viewBahanKapal").text(kapal.nama_bahan_kapal);
    $("#viewMerkMesin").text(kapal.merek_mesin);
    $("#viewTipeMesin").text(kapal.tipe_mesin);
    $("#viewNoMesin").text(kapal.no_mesin);
    $("#viewDayaKapal").text(kapal.daya_kapal);
    $("#viewGt").text(kapal.gt_kapal);
    $("#viewNt").text(kapal.nt_kapal);
    $("#viewPanjang").text(kapal.panjang_kapal + " Meter");
    $("#viewLebar").text(kapal.lebar_kapal + " Meter");
    $("#viewDalam").text(kapal.dalam_kapal + " Meter");
    $("#viewLoa").text(kapal.panjang_loa_kapal + " Meter");
    
  }

  function formatListkapalSelection(kapal)
  {
    set_history(kapal.id_kapal,kapal.id_bkp);
    set_data_kapal(kapal);
    return kapal.no_register;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
    info_filter = $("#info_filter").text();
    info_gt = $("#toggle_gt").text();
    text_info = "Pencarian kapal "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

    text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }

    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. kapal Belum Terdaftar?";
  }

  var search_no_reg = function (){
    $("#start_search").select2({
      id: function(e) { return e.id_kapal },
      allowClear: false,    
      placeholder: "Pilih kapal..",
      width: "60%",
      cache: true,
      minimumInputLength: 1,
      dropdownCssClass: "bigdrop",
      ajax: {
        url: url_search_kapal,
        dataType: "json",
        quietMillis: 2000,
        data: function(term, page){
         return {
          q: term,
                                  // i: 'data',
                                  // filter: current_filter,
                                  // limit: 100 // TODO : tentuin limit result
                                };
                              },
                              results: function(data, page){
                               return {results: data.result};
                             }
                           },
                           formatResult: formatListkapalResult,
                           formatSelection: formatListkapalSelection,
                           formatSearching: formatSearchingText,
                           formatInputTooShort: formatKurangText,
                           formatNoMatches: formatNotFound
                         });

}

function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
       $("#start_search").css({"width":"32%"});
     }
     else{
       var panjang = $("#start_search").val().length;
       $("#start_search").css({"width":(panjang/3)+32+"%"}); 
     }
   }

   s_func.push(auto_width);
   s_func.push(search_no_reg);
   </script>
