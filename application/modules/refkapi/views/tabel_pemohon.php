	<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pemohon' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	$link_entry = '<a class="btn btn-primary" href="'.base_url('refkapi/mst_pemohon/entry').'">Entry Pengurus</a>';

	if($list_pemohon){
		foreach ($list_pemohon as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('refkapi/mst_pemohon/edit/'.$item->id_pemohon).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('refkapi/mst_pemohon/delete/'.$item->id_pemohon).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								$item->nama_pemohon,
								$item->no_ktp,
								$item->nama_kabupaten_kota.', '.$item->tanggal_lahir_pemohon,
								$item->alamat_pemohon,
								$item->kode_pos,
								$item->jabatan_pemohon,
								$item->no_telp_pemohon, 
								$item->email_pemohon,
								$link_edit.' '.$link_delete
								);
			$counter++;
		}
	}

	$table_list_pemohon = $this->table->generate();
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $table_list_pemohon;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_pemohon').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>