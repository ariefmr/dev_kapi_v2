<div class="row">
  <div class="col-lg-12">
          <?php

          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');

          ?>
         <?php 
          $attr_nama_pemohon = array( 'name' => $form['nama_pemohon']['name'],
                                      'label' => $form['nama_pemohon']['label']
                    );
          echo $this->mkform->input_text($attr_nama_pemohon); 

          // TODO : Dibuat select2 ambil list dari select2
          // $test = Modules::run('refdss/mst_wilayah/list_propinsi_array');
          // var_dump($test);


          $attr_tempat_lahir_pemohon = array(
                                      'input_id' => 'id_kabupaten_kota',
                                      'input_name' => 'id_kabupaten_kota',
                                      'label_text' => 'Tempat Lahir',
                                      'array_opsi' => '', 
                                      'opsi_selected' => kos($detail_kapal_verifikasi["id_kabupaten_kota"]),  
                                      'input_width' => 'col-lg-6 manual_input', 
                                      'input_class' => 'form-control test', 
                                      'label_class' => 'col-lg-3 manual_input control-label',
                                      'from_table' => 'mst_kabupaten_kota', 
                                      'field_value' => 'id_kabupaten_kota',
                                      'field_text' => 'nama_kabupaten_kota'
                                  );
          echo $this->mkform->dropdown_dss($attr_tempat_lahir_pemohon);

          $attr_tanggal_lahir_pemohon = array( 
                          // 'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                          // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                          // 'defaultdate' => '2013-20-12', // opsi: '', tidak wajib ada
                          'placeholder' => '', // wajib ada atau '' (kosong)
                          'name' => $form['tanggal_lahir_pemohon']['name'], // wajib ada
                          'label' => $form['tanggal_lahir_pemohon']['label'] // wajib ada
                        );
          echo $this->mkform->input_date($attr_tanggal_lahir_pemohon);

          $attr_no_ktp = array( 'name' => $form['no_ktp']['name'],
                                'label' => $form['no_ktp']['label']
                    );
          echo $this->mkform->input_text($attr_no_ktp);

          $attr_email_pemohon = array(  'name' => $form['email_pemohon']['name'],
                                        'label' => $form['email_pemohon']['label']
                    );
          echo $this->mkform->input_text($attr_email_pemohon);

          $attr_alamat_pemohon = array( 'name' => $form['alamat_pemohon']['name'],
                                        'label' => $form['alamat_pemohon']['label'],
                                        'rows' => '3'
                    );
          echo $this->mkform->input_textarea($attr_alamat_pemohon);

          $attr_zipcode = array(  'name' => $form['kode_pos']['name'],
                                  'label' => $form['kode_pos']['label']
                    );
          echo $this->mkform->input_text($attr_zipcode);

          $attr_jabatan_pemohon = array( 'name' => $form['jabatan_pemohon']['name'],
                                         'label' => $form['jabatan_pemohon']['label'],
                                         'opsi' => array('Pengurus' => 'Pengurus',
                                                          'Staf Perusahaan' => 'Staf Perusahaan',
                                                          'Pemilik' => 'Pemilik',
                                                          'Penanggung Jawab' => 'Penanggung Jawab'
                                                    )
                    );
          echo $this->mkform->input_select($attr_jabatan_pemohon);

          $attr_no_telp_pemohon = array( 'name' => $form['no_telp_pemohon']['name'],
                                         'label' => $form['no_telp_pemohon']['label']
                    );
          echo $this->mkform->input_text($attr_no_telp_pemohon);


         ?>
  </div>
</div>
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>