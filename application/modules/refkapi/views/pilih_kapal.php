<!-- TAMPIL DATA -->
<div id="panel-pilih-kapal" style="margin-left:12px; padding-bottom:15px;" >


  <div class="row" style="padding-bottom: 15px;">
    <div class="col-sm-3 text-right" style=" padding-right: 25px;">
      <strong><?=$cari_noreg?></strong>
    </div>
    <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
     <input id="start_search" name="id_kapal" type="text" class="bigdrop">
     <input  id="inputNama_kapal" name="nama_kapal_register" type="hidden" class="form-control">
     <input  id="inputId_pendok_terakhir" name="id_pendok_terakhir" type="hidden" class="form-control">
     <input id="inputId_pendok" name="id_pendok" type="hidden" >
     <input id="inputId_perusahaan" name="id_perusahaan" type="hidden" >
   </div>
 </div>

 <hr>


        <!-- <div class="row" style="padding-bottom: 15px;">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Nama Kapal</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input  id="viewNama_kapal" name="nama_kapal_register" type="text" class="form-control" disabled>
            </div>
        </div>

        <div class="row" style="padding-bottom: 15px;">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Nama Perusahaan</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input  id="viewNama_perusahaan" name="nama_perusahaan" type="text" class="form-control" disabled>
            </div>
          </div> -->

          <style type="text/css">
          .list-group-item{
            padding: 2px 30px 0px 15px;
            background-color:transparent;
            border:0px;
          }
          </style>

          <div id="area_dispaly_data" class="row" >
            <div id="area_referensi_kapal" class="col-sm-8 col-sm-offset-1" style="display: none;margin-left: 90px;">
              <div class="col-sm-8">
                <dl class="dl-horizontal text-ref">
                  <dt>Nama Kapal : </dt>
                  <dd id="nama_kapal" style="margin-left: 200px;"><p  id="viewNama_kapal" ></p></dd>

                  <hr>

                  <dt>GT :</dt>
                  <dd id="gt_kapal" style="margin-left: 200px;"><p id="viewGt" ></p></dd>

                  <dt>NT :</dt>
                  <dd id="nt_kapal" style="margin-left: 200px;"><p id="viewNt" ></p></dd>

                  <hr>

                  <dt>Perusahaan :</dt>
                  <dd id="nama_perusahaan" style="margin-left: 200px;"><p id="viewNama_perusahaan" ></p></dd>

                  <dt>No Register : </dt>
                  <dd id="no_register" style="margin-left: 200px;"><p id="viewNo_register" ></p></dd>

                  <dt>No Seri Buku Kapal :</dt>
                  <dd id="wpp_info" style="margin-left: 200px;"><p id="viewNo_seri_bkp" ></p></dd>

                  <dt>No Tanda Pengenal :</dt>
                  <dd id="no_tanda_pengenal" style="margin-left: 200px;"><p id="viewNo_tanda_pengenal" ></p></dd>
                  
                  <hr>

                  <dt>Jenis Kapal :</dt>
                  <dd id="nama_jenis_kapal" style="margin-left: 200px;"><p id="viewJenisKapal" ></p></dd>
                  
                  <dt>Jenis Alat Tangkap :</dt>
                  <dd id="alat_tangkap" style="margin-left: 200px;"><p id="viewAlatTangkap" ></p></dd>
                  
                  <dt>Bahan Kapal :</dt>
                  <dd id="nama_bahan_kapal" style="margin-left: 200px;"><p id="viewBahanKapal" ></p></dd>
                  
                  <hr>

                  <dt>Merk Mesin :</dt>
                  <dd id="merek_mesin" style="margin-left: 200px;"><p id="viewMerkMesin" ></p></dd>
                  
                  <dt>Tipe Mesin :</dt>
                  <dd id="tipe_mesin" style="margin-left: 200px;"><p id="viewTipeMesin" ></p></dd>

                  <dt>No Mesin :</dt>
                  <dd id="no_mesin" style="margin-left: 200px;"><p id="viewNoMesin" ></p></dd>

                  <dt>Daya Kapal :</dt>
                  <dd id="daya_kapal" style="margin-left: 200px;"><p id="viewDayaKapal" ></p></dd>

                  <hr>

                  <dt>Panjang Kapal :</dt>
                  <dd id="panjang_kapal" style="margin-left: 200px;"><p id="viewPanjang" ></p></dd>

                  <dt>Lebar Kapal :</dt>
                  <dd id="lebar_kapal" style="margin-left: 200px;"><p id="viewLebar" ></p></dd>

                  <dt>Dalam Kapal :</dt>
                  <dd id="dalam_kapal" style="margin-left: 200px;"><p id="viewDalam" ></p></dd>

                  <dt>Panjang LOA Kapal :</dt>
                  <dd id="panjang_loa_kapal" style="margin-left: 200px;"><p id="viewLoa" ></p></dd>
                </dl>

                <!-- /.modal -->
                <div class="text-center">
                  <style type="text/css">
                  #modal-id .modal-content{
                    width:800px;
                  }
                  </style>
                  <a class="btn btn-primary" id="history" data-toggle="modal" href='#modal-id'>History Kapal</a>
                  <div class="modal fade" id="modal-id" style="left:40%!important;" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">History Kapal</h4>
                      </div>
                      <div class="modal-body" id="result_history" >
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal -->
                </div>


              </div>
              <div>
                <?php

                $checkbox_tag = array(  
                  'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array'),
                  'checked' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array_per_pendok')
                  );  

                echo $this->mkform->input_checkbox_perubahan($checkbox_tag);

                ?>
              </div>
            </div>
          </div>
        </div>



      </div>


      <!-- ADDITIONAL JAVASCRIPT -->
      <script>
  var search_response_time = 2000, //2 Detik
  thread = null,
  url_search_kapal = "<?php echo base_url('refkapi/mst_kapal/search_by_noreg'); ?>";
  url_set_history  = "<?php echo base_url('refkapi/mst_history/tampilkan_carousel'); ?>";

  function formatListkapalResult(kapal)
  {
    html = "<table class='table table-condensed table-bordered' ><tr>"
    /*html = "<table id='"+kapal.id_kapal+"' class='table table-condensed table-bordered'><tr>"
    + "<th>No Register</th>"
    + "<th>Nama Kapal</th>"
    + "<th>Nama Perusahaan</th></tr><tr>"*/
    + "<td>"+kapal.no_register+"</td>"
    + "<td>"+kapal.nama_kapal+"</td>"
    + "<td>"+kapal.nama_perusahaan+"</td>"
    + "<td>GT:"+kapal.gt_kapal+"</td>"
    + "<td>"+kapal.no_grosse_akte+"</td>"
    + "</tr></table>"
    return  html;

  }

 /* 
  function set_history(data)
  {
    if(window.XMLHttpRequest)
    {
      //Kode untuk IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
      {
        //Kode untuk IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    }
    xmlhttp.onreadystatechange = function()
    {
      if(xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        $("#display_data").html(xmlhttp.responseText).css({"display":"block"});
        console.log(xmlhttp.responseText);
      }
    }

    xmlhttp.open("GET",url_set_data+"?array="+data,true);
    xmlhttp.send();
  }
  */

  function set_history(id_kapal,id_bkp){

    $.ajax({
      type: 'post',
      url: url_set_history,
      data: {
        data1 : id_kapal,
        data2 : id_bkp
      },
      success: function( data ) {
        $("#result_history").html(data);
      }
    });

  }

  function set_data_kapal(kapal)
  {
    console.dir(kapal);

    //set_history(JSON.stringify(kapal));
    $("#inputId_kapal").val(kapal.id_kapal);
    $("#inputId_pendok_terakhir").val(kapal.id_pendok);
    $("#inputId_pendok_terakhir").val(kapal.id_pendok);
    $("#area_referensi_kapal").css({"display":"block"});
    $("#inputId_perusahaan").val(kapal.id_perusahaan);
    $("#inputId_kapal").val(kapal.id_kapal);
    $("#inputNama_kapal").val(kapal.nama_kapal);
    $("#viewNama_kapal").text(kapal.nama_kapal);
    $("#viewNama_perusahaan").text(kapal.nama_perusahaan);
    $("#viewNo_register").text(kapal.no_register);
    $("#viewNo_seri_bkp").text(kapal.no_seri_bkp);
    $("#viewNo_tanda_pengenal").text(kapal.no_tanda_pengenal);
    $("#viewJenisKapal").text(kapal.nama_jenis_kapal);
    $("#viewAlatTangkap").text(kapal.alat_tangkap);
    $("#viewBahanKapal").text(kapal.nama_bahan_kapal);
    $("#viewMerkMesin").text(kapal.merek_mesin);
    $("#viewTipeMesin").text(kapal.tipe_mesin);
    $("#viewNoMesin").text(kapal.no_mesin);
    $("#viewDayaKapal").text(kapal.daya_kapal);
    $("#viewGt").text(kapal.gt_kapal);
    $("#viewNt").text(kapal.nt_kapal);
    $("#viewPanjang").text(kapal.panjang_kapal + " Meter");
    $("#viewLebar").text(kapal.lebar_kapal + " Meter");
    $("#viewDalam").text(kapal.dalam_kapal + " Meter");
    $("#viewLoa").text(kapal.panjang_loa_kapal + " Meter");
    
  }

  function formatListkapalSelection(kapal)
  {
    set_history(kapal.id_kapal,kapal.id_bkp);
    set_data_kapal(kapal);
    return kapal.no_register;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
    info_filter = $("#info_filter").text();
    info_gt = $("#toggle_gt").text();
    text_info = "Pencarian kapal "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

    text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }

    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. kapal Belum Terdaftar?";
  }

  var search_no_reg = function (){
    $("#start_search").select2({
      id: function(e) { return e.id_kapal },
      allowClear: false,    
      placeholder: "Pilih kapal..",
      width: "60%",
      cache: true,
      minimumInputLength: 1,
      dropdownCssClass: "bigdrop",
      ajax: {
        url: url_search_kapal,
        dataType: "json",
        quietMillis: 2000,
        data: function(term, page){
         return {
          q: term,
                                  
                                };
                              },
                              results: function(data, page){
                               return {results: data.result};
                             }
                           },
                           formatResult: formatListkapalResult,
                           formatSelection: formatListkapalSelection,
                           formatSearching: formatSearchingText,
                           formatInputTooShort: formatKurangText,
                           formatNoMatches: formatNotFound
                         });

}

function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
       $("#start_search").css({"width":"32%"});
     }
     else{
       var panjang = $("#start_search").val().length;
       $("#start_search").css({"width":(panjang/3)+32+"%"}); 
     }
   }

   s_func.push(auto_width);
   s_func.push(search_no_reg);
   </script>
