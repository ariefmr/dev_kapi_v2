	<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pemohon' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	$link_entry = '<a class="btn btn-primary" href="'.base_url('refkapi/mst_dokumen/entry').'">Entry Dokumen</a>';

	if($list_dokumen){
		foreach ($list_dokumen as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('refkapi/mst_dokumen/edit/'.$item->id_dokumen).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('refkapi/mst_dokumen/delete/'.$item->id_dokumen).'">Hapus</a>';
			$this->table->add_row(
								$counter.'.',
								$item->nama_dokumen,
								$item->peraturan,
								$item->status_tampil,
								$item->wajib,
								$link_edit
								);
			$counter++;
		}
	}

	$table_list_dokumen = $this->table->generate();
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $table_list_dokumen;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_pemohon').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
	} );
</script>