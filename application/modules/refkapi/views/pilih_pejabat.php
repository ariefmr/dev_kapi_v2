<!-- TAMPIL DATA -->
    <div id="panel-pilih-pejabat" style="margin-left:12px; padding-bottom:15px;" >
        <div class="panel-body">
      <div class="row">
          <div class="col-lg-2 text-right">
             <!--  <button id="toggle_gt" type="button" class="btn btn-primary" data-current-gt="below" data-toggle="button">&#8804 30 GT</button>
 -->
             <!--  <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  Cari <text id="info_filter">Nama pejabat  </text>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#" class="ubah_filter" data-this-filter="nama_pejabat">Nama pejabat</a></li>
                  <li><a href="#" class="ubah_filter" data-this-filter="no_telp_pejabat">Nomor Telepon pejabat</a></li>
                  <li><a href="#" class="ubah_filter" data-this-filter="no_ktp">Nomor KTP</a></li>                  
                </ul>
              </div>
 -->
          <strong><?=$judul_jabatan?></strong> 
          </div>
          <div class="col-lg-10">
             <input type="hidden" id="start_search_<?=$judul_jabatan?>" data-init-text="<?=$nama_pejabat?>" value="<?=$nama_pejabat?>" class="bigdrop">
             <input type="hidden" name="id_pejabat_<?=$judul_jabatan?>" data-init-text="<?=$id_pejabat?>" value="<?=$id_pejabat?>" id="id_pejabat_<?=$judul_jabatan?>" class="bigdrop" />
             <input type="hidden" name="nama_pejabat_<?=$judul_jabatan?>" data-init-text="<?=$nama_pejabat?>" value="<?=$nama_pejabat?>" id="nama_pejabat_<?=$judul_jabatan?>" class="bigdrop" />
          </div>
      </div>
    </div>
  </div>



<!-- ADDITIONAL JAVASCRIPT -->

<script>

  var judul_jabatan = "<?php echo $judul_jabatan; ?>";
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_pejabat = "<?php echo base_url('refkapi/mst_pejabat/search_pejabat'); ?>",
    current_gt = function(){ return $("#toggle_gt"+"<?='_'.$judul_jabatan?>").data("currentGt"+"<?='_'.$judul_jabatan?>") },
    current_filter = 'nama_pejabat';

  function formatListpejabatResult(pejabat)
  {
    html = "<table id='"+pejabat.id_pejabat+"<?='_'.$judul_jabatan?>"+"' class='table table-condensed table-bordered'><tr>"
       + "<td>"+pejabat.nip+"</td>"
       + "<td>"+pejabat.nama_pejabat+"</td>"
       + "<td>"+pejabat.jabatan+"</td>"       
       + "</tr></table>"
    return  html;

  }

  function set_data_pejabat(pejabat)
  {
    console.dir(pejabat);

    document.getElementById('start_search_Kasie').onclick = function(){
      $("#id_pejabat_Kasie").val(pejabat.id_pejabat);
      $("#nama_pejabat_Kasie").val(pejabat.nama_pejabat);
    }
    document.getElementById('start_search_Kasubdit').onclick = function(){
      $("#id_pejabat_Kasubdit").val(pejabat.id_pejabat);
      $("#nama_pejabat_Kasubdit").val(pejabat.nama_pejabat);
    }
    document.getElementById('start_search_Direktur').onclick = function(){
        $("#id_nip_direktur_fake").val(pejabat.nip);
        $("#id_nip_direktur_real").val(pejabat.nip);
        $("#id_pejabat_Direktur").val(pejabat.id_pejabat);
        $("#nama_pejabat_Direktur").val(pejabat.nama_pejabat);
    }
    
  }

  function formatListpejabatSelection(pejabat)
  {
    set_data_pejabat(pejabat);
    return pejabat.nama_pejabat;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
      info_filter = $("#info_filter"+"<?='_'.$judul_jabatan?>").text();
      info_gt = $("#toggle_gt"+"<?='_'.$judul_jabatan?>").text();
      text_info = "Pencarian pejabat "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/pejabat/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/pejabat/add/" ;
    //     }
     
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. pejabat Belum Terdaftar?";
  }


  function searchURLGenerator()
  {

  }

  function formatinisial(element,callback)
  {
      var elementText = $(element).attr('data-init-text');
      callback({"nama_pejabat":elementText});
  }

  $(document).ready( function () {
    
    // $("#toggle_gt").click(function(){
    //   var currentGt = $(this).data("currentGt");
    //     if(currentGt === "below")
    //     {
    //       $(this).text("> 30 GT");
    //       $(this).data("currentGt","above");
          
    //     }else
    //     {
    //       $(this).html("&#8804 30 GT");
    //       $(this).data("currentGt","below");      
    //     }
    //     $("#start_search").select2("open");
    // });

    $('#start_search'+'<?="_".$judul_jabatan?>').select2("val",'test');

    $(".ubah_filter"+"<?='_'.$judul_jabatan?>").click(function(){
      var thisFilter = $(this).data("thisFilter"),
        thisText = $(this).text();

        $("#info_filter"+"<?='_'.$judul_jabatan?>").text(thisText);
        current_filter = thisFilter;
        $("#start_search"+"<?='_'.$judul_jabatan?>").select2("open");
    });

    $("#start_search"+"<?='_'.$judul_jabatan?>").select2({
                  id: function(e) { return e.id_pejabat },
                  allowClear: false,    
                  placeholder: "Pilih pejabat..",
                  width: "100%",
                  cache: true,
                  minimumInputLength: 3,
                  dropdownCssClass: "bigdrop",
                  ajax: {
                          url: url_search_pejabat,
                          dataType: "json",
                          quietMillis: 2000,
                          data: function(term, page){
                                         return {
                                  types: ["pejabat"],
                                  limit: -1,
                                  q: term,
                                  // i: 'data',
                                  //filter: current_filter,
                                  // limit: 100 // TODO : tentuin limit result
                                     };
                          },
                          results: function(data, page){
                                   return {results: data.result};
                          }
                  },
                  formatResult: formatListpejabatResult,
                  formatSelection: formatListpejabatSelection,
                  formatSearching: formatSearchingText,
                  formatInputTooShort: formatKurangText,
                  formatNoMatches: formatNotFound,
                  initSelection: formatinisial
                                    });

    $("#start_search").on("change",function(e) { 
                    //console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
                      // get_detail_pejabat(e.val, current_gt());
                      // var nama_pejabat_chosen = $("span.select2-chosen").text();
                      // $("#inputNama_pejabat").val(nama_pejabat_chosen);
                    });
    /*

     ajax: {
                                          url: "<?php echo base_url('mst_pejabat/json_pejabat'); ?>",
                                          dataType: "jsonp",
                                          data: function(term, page){
                                            return {
                                              q: "term",
                                              limit: 100 // TODO : tentuin limit result
                                            };
                                          },
                                          results: function(data, page){
                                            return {results: data}
                                          }
    $('#start_search').keyup(function(e){
      clearTimeout(thread);
      
      var keyword = $(this).val();

      thread = setTimeout(function(){
        update_result_pejabat(keyword);
      } ,search_response_time);
    });
    */  
  } );
</script>