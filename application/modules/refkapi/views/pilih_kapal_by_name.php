<!-- TAMPIL DATA -->
    <div id="panel-pilih-kapal2" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Nama Kapal</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="form_kapal_baru"  type="text" name="form_kapal_baru" class="bigdrop" />
               <input id="nama_kapal_baru"  type="hidden" name="nama_kapal_baru" class="bigdrop" />
            </div>
        </div>
    
    </div>


<SCRIPT type="text/javascript">
  var search_response_time = 2000, //2 Detik
  thread = null,
  url_search_kapal_by_name = "<?php echo base_url('refkapi/mst_kapal/search_by_nama'); ?>";

  function formatListkapalResultNama(kapal)
  {
    if(kapal.nama_perusahaan == ''){
      html = "<table class='table table-condensed table-bordered' ><tr><td>"+kapal.nama_kapal+"</td></tr></table>"
    }else
    {
      html = "<table class='table table-condensed table-bordered' ><tr>"
      + "<td width='30%' >"+kapal.nama_kapal+"</td>"
      + "<td width='30%' >"+kapal.nama_perusahaan+"</td>"
      + "<td width='10%' >GT:"+kapal.gt_kapal+"</td>"
      + "<td width='10%' >"+kapal.no_grosse_akte+"</td>"
      + "</tr></table>"
    }
    return  html;

  }

  function set_data_kapalNama(kapal)
  {
   
    console.dir(kapal);
    $("#nama_kapal_baru").val(kapal.nama_kapal);
    
  }

  function formatListkapalSelectionNama(kapal)
  {
    /*set_history(kapal.id_kapal,kapal.id_bkp);*/
    set_data_kapalNama(kapal);
    return kapal.nama_kapal;
  }

  function formatSearchingTextNama(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangTextNama(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
    info_filter = $("#info_filter").text();
    info_gt = $("#toggle_gt").text();
    text_info = "Pencarian kapal "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

    text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFoundNama(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }

    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. kapal Belum Terdaftar?";
  }

  var search_by_nama = function (){
    $("#form_kapal_baru").select2({
      id: function(e) { return e.id_kapal },
      allowClear: false,    
      placeholder: "Masukan nama kapal baru..",
      width: "60%",
      cache: true,
      minimumInputLength: 3,
      dropdownCssClass: "bigdrop",
      ajax: {
        url: url_search_kapal_by_name,
        dataType: "json",
        quietMillis: 2000,
        data: function(term, page){
                         return {
                          q: term
                        };
                      },
                              results: function(data, page){
                               return {results: data.result};
                             }
                           },
                           formatResult: formatListkapalResultNama,
                           formatSelection: formatListkapalSelectionNama,
                           formatSearching: formatSearchingTextNama,
                           formatInputTooShort: formatKurangTextNama,
                           formatNoMatches: formatNotFoundNama
                         });

}

/*function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
       $("#start_search").css({"width":"32%"});
     }
     else{
       var panjang = $("#start_search").val().length;
       $("#start_search").css({"width":(panjang/3)+32+"%"}); 
     }
   }*/

   s_func.push(auto_width);
   s_func.push(search_by_nama);

</SCRIPT>