<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_cleaning');
	}

	public function import_db_lama($row, $limit)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'cleaning_data';
		$views = 'view_cleaning';
		$labels = '';

		$data = $this->mdl_cleaning->list_kapal_kapi_lama($row, $limit);
		
		//HATI - HATI !!!!!!!!!!!!!!!!!!!
		//BISA MERUSAK DB YANG DI LOKAL
		vdump($data, true);
		die;
		//HATI - HATI !!!!!!!!!!!!!!!!!!!
		//BISA MERUSAK DB YANG DI LOKAL

		$data_insert = array();
		$i=0;
		$counter=0;

		foreach ($data as $key => $value) {
			//PERUSAHAAN
			//Membandingkan dengan no_siup di db_master
			$no_siup = kos($value->siup_master);
			$arrPendok = array();

			if( (empty($no_siup)) || ($no_siup==="") ){
				$data_insert['id_perusahaan'] = "0";
				$data_insert['nama_perusahaan'] = kos($value->Nama_Pemilik);
				$data_insert['nama_penanggung_jawab'] = kos($value->Nama_Penanggung_Jawab);
				$data_insert['no_identitas_penanggung_jawab'] = kos($value->No_KTP);
				$data_insert['ttl_penanggung_jawab'] = kos($value->Tempat_Tgl_Lahir);
				$data_insert['alamat_perusahaan'] = kos($value->Alamat_Pemilik);

				$arrPendok['id_perusahaan']	= "0";
				$arrPendok['nama_perusahaan'] = kos($value->Nama_Pemilik);
				$arrPendok['nama_penanggung_jawab'] = kos($value->Nama_Penanggung_Jawab);
				$arrPendok['no_identitas_penanggung_jawab'] = kos($value->No_KTP);
				$arrPendok['ttl_penanggung_jawab'] = kos($value->Tempat_Tgl_Lahir);
				$arrPendok['alamat_perusahaan'] = kos($value->Alamat_Pemilik);
			}else{
				$id = $this->mdl_cleaning->cek_perusahaan($no_siup);
				$id_perusahaan = $id->id_perusahaan;
				$nama_perusahaan = $id->nama_perusahaan;
				$nama_penanggung_jawab = $id->nama_penanggung_jawab;
				$no_identitas_penanggung_jawab = $id->no_identitas_penanggung_jawab;
				$ttl_penanggung_jawab = $id->ttl_penanggung_jawab;
				$alamat_perusahaan = $id->alamat_perusahaan;

				$data_insert['id_perusahaan'] = $id_perusahaan;
				$data_insert['nama_perusahaan'] = $nama_perusahaan;
				$data_insert['nama_penanggung_jawab'] = $nama_penanggung_jawab;
				$data_insert['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
				$data_insert['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
				$data_insert['alamat_perusahaan'] = $alamat_perusahaan;

				$arrPendok['id_perusahaan']	= $id_perusahaan;
				$arrPendok['nama_perusahaan'] = $nama_perusahaan;
				$arrPendok['nama_penanggung_jawab'] = $nama_penanggung_jawab;
				$arrPendok['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
				$arrPendok['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
				$arrPendok['alamat_perusahaan'] = $alamat_perusahaan;
			}

			

			//PEMOHON
			//Insert to DB
			//Return id_pemohon
			$arrPemohon = array();
			$arrPemohon['nama_pemohon'] = kos(str_replace("'", "", $value->Nama_Pemohon)); 
			$arrPemohon['alamat_pemohon'] = kos($value->Alamat_Pemohon); 
			$arrPemohon['jabatan_pemohon'] = kos($value->Jabatan_Pemohon);//(!=NULL)? $value->Jabatan_pemohon : ''; 
			$arrPemohon['no_telp_pemohon'] = kos($value->No_Telpon_Pemohon); 
			// vdump(kos($value->No_Telpon_Pemohon), true);
			
			$id_pemohon = $this->insert_pemohon($arrPemohon);

			//MST_KAPAL
			//Insert to DB

			$arrKapal = array();
			// ID_KAPAL
			//ini update tanggal 10 Juni 2014 di tambah utk data cleaning
			/*Permintaan Mas Deddy karena Dss saat ini id_kapal yang lama dari tr_pendok.pendok_id*/
			$id_kapal = kos($value->pendok_id);
			// ID_KAPAL NYA DIISI DARI TR_PENDOK.PENDOK_ID PERMINTAAN MAS DEDDY
			$arrKapal['id_kapal'] = $id_kapal;
			$arrKapal['no_register'] = kos($value->no_register);
			$arrKapal['id_pendok_terakhir']	= '';
			$arrKapal['id_bkp_terakhir']	= '';
			$arrKapal['nama_kapal_terbaru']	= kos($value->Nama_Kapal);

			$this->mdl_cleaning->input_kapal($arrKapal);


			//PENDOK
			//Insert to DB
			//return id_pendok

			$arrPendok['no_rekam_pendok']= kos($value->pendok_id);
			$arrPendok['id_kapal']		= $id_kapal;

			

			$arrPendok['id_pemohon']	= $id_pemohon;
			
			$arrPendok['kategori_pendaftaran']	= 'PUSAT';
			$arrPendok['id_propinsi_pendaftaran']	= '11';
			$arrPendok['id_kabkota_pendaftaran']	= '154';
			$arrPendok['propinsi_pendaftaran']	= 'DKI Jakarta';
			$arrPendok['kabkota_pendaftaran']	= 'Jakarta Pusat';

			$arrPendok['no_surat_permohonan']	= kos($value->No_Surat_Permohonan);
			$arrPendok['tanggal_surat_permohonan']	= kos($value->Tgl_Surat_Permohonan);
			$arrPendok['tipe_permohonan']	= "BARU";
			$arrPendok['nama_kapal']	= kos($value->Nama_Kapal);
			$arrPendok['status_pendok']	= "FINAL";
			$arrPendok['status_edit']	= "5";
			$arrPendok['status_verifikasi']	= "YA";
			$arrPendok['status_entry_bkp']	= "2";
			$arrPendok['status_cetak_bkp']	= "YA";
			$arrPendok['status_terima_bkp']	= "YA";
			$arrPendok['aktif']	= "YA";

			$id_pendok = $this->mdl_cleaning->input_pendok($arrPendok);


			//TRS_BKP
			//Insert to DB
			//return id_bkp

			$data_insert['id_pendok'] = $id_pendok;
			$data_insert['id_kapal'] = $id_kapal;
			$bangunan = explode("/", $value->Tempat_Pembangunan);
			$data_insert['tempat_pembangunan'] = $bangunan[0];
			$data_insert['negara_pembangunan'] = $bangunan[0];
			$data_insert['propinsi_pembangunan'] = $bangunan[0];
			$data_insert['kabkota_pembangunan'] = $bangunan[0];
			$data_insert['tahun_pembangunan'] = $bangunan[1];
			$data_insert['nama_kapal'] 		= kos($value->Nama_Kapal);
			$data_insert['nama_kapal_sblm'] = ($value->Nama_Kapal_Sebelumnya==null)? '-' : $value->Nama_Kapal_Sebelumnya ;

				if($value->Bahan_Utama_Kapal == 1)
				{
					
					$data_insert['id_bahan_kapal']  = 2;

				}else if($value->Bahan_Utama_Kapal == 2)
				{
					
					$data_insert['id_bahan_kapal']  = 1;

				}else if($value->Bahan_Utama_Kapal == 3)
				{
					
					$data_insert['id_bahan_kapal']  = 5;

				}else if(empty($value->Bahan_Utama_Kapal))
				{

					$data_insert['id_bahan_kapal']  = NULL;

				}else{

					$data_insert['id_bahan_kapal']  = NULL;

				}
				
				if($value->Type_Kapal == 1){

					$data_insert['id_jenis_kapal'] = 0;

				}else if($value->Type_Kapal == 2){

					$data_insert['id_jenis_kapal'] = 1;

				}else if($value->Type_Kapal == 3){

					$data_insert['id_jenis_kapal'] = 3;

				}else if(empty($value->Type_Kapal)){

					$data_insert['id_jenis_kapal']  = NULL;

				}else{

					$data_insert['id_jenis_kapal']  = NULL;

				}

			if(empty($value->aidi_tangkap)){
				$data_insert['id_alat_tangkap'] = NULL;
			}else{
				$data_insert['id_alat_tangkap'] = kos($value->aidi_tangkap);
				$data_insert['alat_tangkap'] = kos($value->nama_alat_tangkap);
			}
			//Perlu ditambahkan maping pada db_master

			$data_insert['merek_mesin'] = kos($value->Merek_Mesin_Utama);

			$data_insert['daya_kapal'] = kos($value->Daya_Mesin_Utama);

			$data_insert['satuan_mesin_utama_kapal'] = kos($value->Satuan_Mesin_Utama);

			$data_insert['no_mesin'] = kos($value->No_Mesin_Utama);

			$data_insert['jumlah_palka'] = kos($value->Jumlah_Palka);

			$data_insert['kapasitas_palka'] = kos($value->Kapasitas_Palka);

			//tempat pendaftaran
			if(($value->Tempat_Pendaftaran === "KKP")||($value->Tempat_Pendaftaran === "KK")||($value->Tempat_Pendaftaran === "KP")||($value->Tempat_Pendaftaran === "k")){
				$data_insert['tempat_pendaftaran_id'] = "PUSAT";
			}else{
				$data_insert['tempat_pendaftaran_id'] = kos($value->Tempat_Pendaftaran);
			}

			$data_insert['no_tanda_pengenal'] = kos($value->tanda_pengenal);

			//ini update tanggal 10 Juni 2014 di tambah utk data cleaning
			$data_insert['no_seri_bkp'] = kos($value->no_seri);

			$data_insert['panjang_kapal'] = kos($value->Panjang);

			$data_insert['lebar_kapal'] = kos($value->Lebar);

			$data_insert['dalam_kapal'] = kos($value->Dalam);

			$data_insert['gt_kapal'] = kos($value->GT);

			$data_insert['nt_kapal'] = kos($value->NT);

			$data_insert['no_grosse_akte'] = kos($value->no_gross_akte);

			$data_insert['tanggal_grosse_akte'] = kos($value->tgl_gross_akte);

			$data_insert['tempat_grosse_akte'] = kos($value->no_reg);

			$data_insert['nama_pengguna_buat'] = kos($value->Nama_Pegawai_Input);

			$data_insert['nama_kasie'] = kos($value->Kasie);

			$data_insert['nama_kasubdit'] = kos($value->Kasubdit);

			$data_insert['nama_direktur'] = kos($value->Direktur);

			$data_insert['nip_direktur'] = kos($value->Nip_Direktur);

			$data_insert['tanggal_buat'] = kos($value->insertOn);

			$data_insert['kategori_pendaftaran']	= 'PUSAT';
			$data_insert['id_propinsi_pendaftaran']	= '11';
			$data_insert['id_kabkota_pendaftaran']	= '154';
			$data_insert['propinsi_pendaftaran']	= 'DKI Jakarta';
			$data_insert['kabkota_pendaftaran']	= 'Jakarta Pusat';
			
			$data_update['id_bkp_terakhir'] = $this->mdl_cleaning->input_bkp($data_insert);

			$data_update['id_pendok_terakhir'] = $id_pendok;

			$data_update['id_kapal'] = $id_kapal;


			//vdump($arrKapal);
			$this->mdl_cleaning->update_kapal($data_update);
			// echo "~done~<br/>";
			$counter++;

		}

		echo "data yang masuk: ".$counter;

		//vdump($data_insert);
		//$data['list_data'] = $this->mdl_cleaning->list_pemohon();

		//echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function import_db_entri_kapi($row, $limit)
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'cleaning_data';
		$views = 'view_cleaning';
		$labels = '';

		$data = $this->mdl_cleaning->list_entri_kapi($row, $limit);

		$data_insert = array();
		$i=0;
		$counter=0;

		// vdump($data, true);
		$no = 1;
		$jml_ketemu = 0;
		$jml_no_ketemu = 0;
		$not_pemilik = 0;
		foreach ($data as $key => $value) {
			
			//PERUSAHAAN
			//Membandingkan dengan nama_pemilik di db_master
			$nama_kapal_entri = kos($value->nama_kapal);
			$alamat_perusahaan_entri = kos($value->alamat_pemilik);

			$nama_pemilik_entri = kos($value->nama_pemilik);
			$arrPendok = array();

			if (!empty($nama_pemilik_entri)) {
				
				/*cleaning data for high match accuracy*/
				if( ($nama_pemilik_entri === "HERY") || ($nama_pemilik_entri === "RUDI") || ($nama_pemilik_entri === "TEDY") || ($nama_pemilik_entri === "KURDI") ){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_justname($nama_pemilik_entri);
				}else if($nama_pemilik_entri === "BUYUNG"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(1751);
				}else if($nama_pemilik_entri === "PARWI"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(4843);
				}else if($nama_pemilik_entri === "TJAI"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(3291);
				}else if($nama_pemilik_entri === "GUNAWAN"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(1714);
				}else if($nama_pemilik_entri === "MUSA"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(6718);
				}else if($nama_pemilik_entri === "SUSANTO"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(2263);
				}else if($nama_pemilik_entri === "BUDIYANTO"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(3303);
				}else if($nama_pemilik_entri === "DARMAN"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(5705);
				}else if($nama_pemilik_entri === "HARTONO"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(3667);
				}else if($nama_pemilik_entri === "ANTONI"){
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik_id(59);
				}else{
					$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik($nama_pemilik_entri);
				}
				/*end cleaning data for high match accuracy*/

				if($id){
					$id_perusahaan = $id->id_perusahaan;
					$nama_perusahaan = $id->nama_perusahaan;
					$nama_penanggung_jawab = $id->nama_penanggung_jawab;
					$no_identitas_penanggung_jawab = $id->no_identitas_penanggung_jawab;
					$ttl_penanggung_jawab = $id->ttl_penanggung_jawab;
					$alamat_perusahaan = $id->alamat_perusahaan;
					
					$data_insert['id_perusahaan'] = $id_perusahaan;
					$data_insert['nama_perusahaan'] = $nama_perusahaan;
					$data_insert['nama_penanggung_jawab'] = $nama_penanggung_jawab;
					$data_insert['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
					$data_insert['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
					$data_insert['alamat_perusahaan'] = $alamat_perusahaan;

					$arrPendok['id_perusahaan']	= $id_perusahaan;
					$arrPendok['nama_perusahaan'] = $nama_perusahaan;
					$arrPendok['nama_penanggung_jawab'] = $nama_penanggung_jawab;
					$arrPendok['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
					$arrPendok['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
					$arrPendok['alamat_perusahaan'] = $alamat_perusahaan;
					$jml_ketemu++;
				}else{
					$data_insert['id_perusahaan'] = 0;
					$data_insert['nama_perusahaan'] = "";
					$data_insert['nama_penanggung_jawab'] = "";
					$data_insert['no_identitas_penanggung_jawab'] = "";
					$data_insert['ttl_penanggung_jawab'] = "";
					$data_insert['alamat_perusahaan'] = "";

					$arrPendok['id_perusahaan'] = 0;
					$arrPendok['nama_perusahaan'] = "";
					$arrPendok['nama_penanggung_jawab'] = "";
					$arrPendok['no_identitas_penanggung_jawab'] = "";
					$arrPendok['ttl_penanggung_jawab'] = "";
					$arrPendok['alamat_perusahaan'] = "";
					$jml_no_ketemu++;
				}

			}else{
				$not_pemilik++;
			}
			echo "no : ".$no."<br>";
			echo "nama kapal entri : ";
			echo $nama_kapal_entri."<br>";
			echo "nama pemilik entri : ";
			echo $nama_pemilik_entri."<br>";
			echo "nama pemilik master : ";
			echo $data_insert['nama_penanggung_jawab']."<br>";
			echo "alamat entri : ";
			echo $alamat_perusahaan_entri."<br>";
			echo "alamat master : ";
			echo $data_insert['alamat_perusahaan']."<br>";
			// echo "=================<br>";
			// vdump($data_insert);
			echo "=================<br><br><br>";
			$no++;

		/*foreach ($data as $key => $value) {
			//PERUSAHAAN
			//Membandingkan dengan nama_pemilik di db_master
			$nama_pemilik = kos($value->nama_pemilik);
			$arrPendok = array();

			if( (empty($nama_pemilik)) || ($nama_pemilik==="") ){
				$id = $this->mdl_cleaning->cek_perusahaan_by_pemilik($nama_pemilik);
				$id_perusahaan = $id->id_perusahaan;
				$nama_perusahaan = $id->nama_perusahaan;
				$nama_penanggung_jawab = $id->nama_penanggung_jawab;
				$no_identitas_penanggung_jawab = $id->no_identitas_penanggung_jawab;
				$ttl_penanggung_jawab = $id->ttl_penanggung_jawab;
				$alamat_perusahaan = $id->alamat_perusahaan;

				$data_insert['id_perusahaan'] = $id_perusahaan;
				$data_insert['nama_perusahaan'] = $nama_perusahaan;
				$data_insert['nama_penanggung_jawab'] = $nama_penanggung_jawab;
				$data_insert['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
				$data_insert['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
				$data_insert['alamat_perusahaan'] = $alamat_perusahaan;

				$arrPendok['id_perusahaan']	= $id_perusahaan;
				$arrPendok['nama_perusahaan'] = $nama_perusahaan;
				$arrPendok['nama_penanggung_jawab'] = $nama_penanggung_jawab;
				$arrPendok['no_identitas_penanggung_jawab'] = $no_identitas_penanggung_jawab;
				$arrPendok['ttl_penanggung_jawab'] = $ttl_penanggung_jawab;
				$arrPendok['alamat_perusahaan'] = $alamat_perusahaan;
			}
		}*/


			//MST_KAPAL
			//Insert to DB
			$arrKapal = array();
			$arrKapal['no_register'] = kos($value->no_reg);
			$arrKapal['id_pendok_terakhir']	= '';
			$arrKapal['id_bkp_terakhir']	= '';
			$arrKapal['nama_kapal_terbaru']	= kos($value->nama_kapal);

			$id_kapal = $this->mdl_cleaning->input_kapal($arrKapal);


			//PENDOK
			//Insert to DB
			//return id_pendok

			$arrPendok['no_rekam_pendok']	= kos($value->no_rekam_pendok);
			$arrPendok['id_kapal']		= $id_kapal;
			
			$arrPendok['kategori_pendaftaran']	= 'PUSAT';
			$arrPendok['id_propinsi_pendaftaran']	= '11';
			$arrPendok['id_kabkota_pendaftaran']	= '154';
			$arrPendok['propinsi_pendaftaran']	= 'DKI Jakarta';
			$arrPendok['kabkota_pendaftaran']	= 'Jakarta Pusat';

			$arrPendok['no_surat_permohonan']	= "-";
			$arrPendok['tanggal_surat_permohonan']	= "-";
			$arrPendok['tipe_permohonan']	= "BARU";
			$arrPendok['nama_kapal']	= kos($value->nama_kapal);
			$arrPendok['status_pendok']	= "FINAL";
			$arrPendok['status_edit']	= "5";
			$arrPendok['status_verifikasi']	= "YA";
			$arrPendok['status_entry_bkp']	= "2";
			$arrPendok['status_cetak_bkp']	= "YA";
			$arrPendok['status_terima_bkp']	= "YA";
			$arrPendok['aktif']	= "YA";

			$id_pendok = $this->mdl_cleaning->input_pendok($arrPendok);


			//TRS_BKP
			//Insert to DB
			//return id_bkp

			$data_insert['id_pendok'] = $id_pendok;
			$data_insert['id_kapal'] = $id_kapal;
			$bangunan = explode(" / ", $value->tempat_pembangunan);
			$data_insert['tempat_pembangunan'] = $bangunan[0];
			// $data_insert['negara_pembangunan'] = $bangunan[0];
			// $data_insert['propinsi_pembangunan'] = $bangunan[0];
			// $data_insert['kabkota_pembangunan'] = $bangunan[0];
			$data_insert['tahun_pembangunan'] = kos($value->tahun_pembangunan);
			$data_insert['nama_kapal'] 		= kos($value->nama_kapal);
			$data_insert['nama_kapal_sblm'] = ($value->nama_kapal_sebelumnya==null)? '-' : $value->nama_kapal_sebelumnya ;

				if($value->bahan_utama_kapal === "KAYU")
				{
					
					$data_insert['id_bahan_kapal']  = 2;

				}else if($value->bahan_utama_kapal === "BAJA")
				{
					
					$data_insert['id_bahan_kapal']  = 1;

				}else if(($value->bahan_utama_kapal === "FIBBERGLASS") || ($value->bahan_utama_kapal === "FIBERGLASS") )
				{
					
					$data_insert['id_bahan_kapal']  = 5;

				}else if(empty($value->bahan_utama_kapal))
				{

					$data_insert['id_bahan_kapal']  = NULL;

				}else{

					$data_insert['id_bahan_kapal']  = NULL;

				}
				
				if(($value->fungsi_kapal === "PENANGKAP IKAN") || ($value->fungsi_kapal === "PENANGKAPAN") ){

					$data_insert['id_jenis_kapal'] = 0;

				}else if( ($value->fungsi_kapal === "PENGANGKUT IKAN") || ($value->fungsi_kapal === "PENANGKUT IKAN") ){

					$data_insert['id_jenis_kapal'] = 1;

				}else if($value->fungsi_kapal === "PENDUKUNG OPERASIONAL"){

					$data_insert['id_jenis_kapal'] = 3;

				}else if(empty($value->fungsi_kapal)){

					$data_insert['id_jenis_kapal']  = NULL;

				}else{

					$data_insert['id_jenis_kapal']  = NULL;

				}

			$alat_tangkap = $value->jenis_alat_tangkap;
			if(empty($value->jenis_alat_tangkap)){
				$data_insert['id_alat_tangkap'] = NULL;
			}else if($value->jenis_alat_tangkap === "-"){
				$data_insert['id_alat_tangkap'] = NULL;
				$data_insert['alat_tangkap'] = "-";
			}else if($value->jenis_alat_tangkap === "HAND LINE"){
				$data_insert['id_alat_tangkap'] = NULL;
				$data_insert['alat_tangkap'] = "HAND LINE";
			}else{
				$id = $this->mdl_cleaning->cek_tangkap_by_nama($alat_tangkap);

				$data_insert['id_alat_tangkap'] = $id->id_alat_tangkap;
				$data_insert['alat_tangkap'] = kos($value->nama_alat_tangkap);
			}
			//Perlu ditambahkan maping pada db_master

			$data_insert['merek_mesin'] = kos($value->merek_mesin_utama);

			//EXPLODE UNTUK DAYA KAPAL DAN SATUAN NYA!
			$bangunan = explode(" ", $value->tempat_pembangunan);
			$data_insert['daya_kapal'] = $bangunan[0];
			$satuan_mesin = $bangunan[1];
			if($satuan_mesin === "PK")
				{
					
					$data_insert['satuan_mesin_utama_kapal']  = 1;

				}else if($satuan_mesin === "DK")
				{
					
					$data_insert['satuan_mesin_utama_kapal']  = 2;

				}else if($satuan_mesin === "HP")
				{
					
					$data_insert['satuan_mesin_utama_kapal']  = 3;

				}else{

					$data_insert['satuan_mesin_utama_kapal']  = 1;

				}

			$data_insert['no_mesin'] = kos($value->no_mesin_utama);

			$data_insert['jumlah_palka'] = kos($value->jumlah_palka);

			$data_insert['kapasitas_palka'] = kos($value->kapasitas_palka);

			//tempat pendaftaran
			if(($value->tempat_pendaftaran === "KKP")||($value->tempat_pendaftaran === "KK")||($value->tempat_pendaftaran === "KP")||($value->tempat_pendaftaran === "k")){
				$data_insert['tempat_pendaftaran_id'] = "PUSAT";
			}else{
				$data_insert['tempat_pendaftaran_id'] = kos($value->tempat_pendaftaran);
			}

			$data_insert['no_tanda_pengenal'] = kos($value->tanda_pengenal);

			//ini update tanggal 10 Juni 2014 di tambah utk data cleaning
			// $data_insert['no_seri_bkp'] = kos($value->no_seri);

			$data_insert['panjang_kapal'] = kos($value->panjang);

			$data_insert['lebar_kapal'] = kos($value->lebar);

			$data_insert['dalam_kapal'] = kos($value->dalam);

			$data_insert['gt_kapal'] = kos($value->gt);

			$data_insert['nt_kapal'] = kos($value->nt);

			$data_insert['no_grosse_akte'] = kos($value->no_grosse_akte);

			$tgl_indonesia = kos($value->tanggal_grosse_akte);
			
			$tgl_part = explode("/", $tgl_indonesia);

			echo $tgl_db_format = $tgl_part[2]."-".$tgl_part[1]."-".$tgl_part[0];

			$data_insert['tanggal_grosse_akte'] = $tgl_db_format;

			$data_insert['tempat_grosse_akte'] = kos($value->tempat_grosse_akte);

			$data_insert['nama_pengguna_buat'] = "Choko";

			$data_insert['nama_kasie'] = NULL;

			$data_insert['nama_kasubdit'] = NULL;

			$data_insert['nama_direktur'] = NULL;

			$data_insert['nip_direktur'] = NULL;

			$data_insert['tanggal_buat'] = "2014-08-15";

			$data_insert['kategori_pendaftaran']	= 'PUSAT';
			$data_insert['id_propinsi_pendaftaran']	= '11';
			$data_insert['id_kabkota_pendaftaran']	= '154';
			$data_insert['propinsi_pendaftaran']	= 'DKI Jakarta';
			$data_insert['kabkota_pendaftaran']	= 'Jakarta Pusat';
			
			$data_update['id_bkp_terakhir'] = $this->mdl_cleaning->input_bkp($data_insert);

			$data_update['id_pendok_terakhir'] = $id_pendok;

			$data_update['id_kapal'] = $id_kapal;


			//vdump($arrKapal);
			$this->mdl_cleaning->update_kapal($data_update);
			// echo "~done~<br/>";
			$counter++;

		}


		echo "jumlah ketemu = ".$jml_ketemu."<br>";
		echo "jumlah no ketemu = ".$jml_no_ketemu."<br>";
		echo "jumlah nothave pemilik = ".$not_pemilik."<br>";
	
		echo "data yang masuk: ".$counter;

		//vdump($data_insert);
		//$data['list_data'] = $this->mdl_cleaning->list_pemohon();

		//echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	private function insert_pemohon($pemohon)
	{
		$nama_pemohon = $pemohon['nama_pemohon'];
		//jika pemohon tidak ada
		$cek = $this->mdl_cleaning->cek_pemohon($nama_pemohon);
		if($cek){
			//jika
			return $cek->id_pemohon;
		}else{
			return $this->mdl_cleaning->input_pemohon($pemohon);
		}

	}

}