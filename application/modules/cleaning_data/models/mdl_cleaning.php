<?php

class Mdl_cleaning extends CI_Model
{

	private $db_kapi;
	private $db_master;
	// private $db_kapi_lama;

	function __construct()
	{
        
        $this->db_kapi = $this->load->database('default', TRUE);
        $this->db_master = $this->load->database('db_dss', TRUE);
        // $this->db_kapi_lama = $this->load->database('db_kapi_lama', TRUE);
        // $this->db_kapi_lama = $this->load->database('db_entri_kapi', TRUE);
	}

	/*public function list_kapal_kapi_lama($row, $limit)
    {
    	$query = "  SELECT distinct
                        tbk.no_reg as no_register,
                        tbk.no_seri,
                        tbk.tanda_pengenal,
                        tbk.insertOn,
                        mat.id_alat_tangkap as aidi_tangkap,
                        mat.nama_alat_tangkap,
                        tbk.Kasie,
                        tbk.Kasubdit,
                        tbk.Direktur,
                        tbk.Nip_Direktur,
                        tbk.Nama_Pegawai_Input,
                        mpr.nama_perusahaan as nm_perusahaan_master,
                        mpr.no_siup as siup_master,
                        mpr.id_perusahaan,
                        trp . *
                    from
                        kapi_pendaftaran.tr_pendok trp
                            left join
                        db_master.mst_perusahaan mpr ON trp.no_siup = mpr.no_siup
                            inner join
                        kapi_pendaftaran.tr_buku_kapal tbk ON trp.pendok_id = tbk.pendok_id
                            left join
                        kapi_pendaftaran.tab_jenis_alat tab ON tab.Jenis_Alat_ID = trp.Jenis_Alat
                            left join
                        db_master.mst_alat_tangkap mat ON tab.Kode = mat.kode_alat_tangkap
                    limit $row , $limit ";

    	$run_query = $this->db_kapi_lama->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }*/

    public function list_entri_kapi($row, $limit)
    {
        $query = "  SELECT *
                    from
                        db_pendaftaran_kapal.entri_manual em
                    limit $row , $limit ";

        $run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_pemohon($nama_pemohon)
    {
    	$query = "  SELECT mp.id_pemohon
					from db_pendaftaran_kapal.mst_pemohon mp
					where mp.nama_pemohon = '$nama_pemohon' ";

    	$run_query = $this->db_kapi->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input_bkp($data)
    {
    	$this->db_kapi->insert('trs_bkp', $data);                         
        
        $aff_rows = $this->db_kapi->affected_rows();
        if($aff_rows > 0)
        {
            $id_bkp = $this->db_kapi->insert_id();    
        }else{
            $id_bkp = false;
        }

        return $id_bkp;
    }

    public function update_kapal($data)
    {
    	$this->db_kapi->where('id_kapal', $data['id_kapal']);
    	$this->db_kapi->update('mst_kapal', $data);

    	$aff_rows = $this->db_kapi->affected_rows();
    	if($aff_rows > 0)
    	{
    		$affected = true;	
    	}else{
    		$affected = false;
    	}

    	return $affected;
    }

    public function input_pemohon($pemohon)
    {
    	$this->db_kapi->insert('mst_pemohon', $pemohon);                         
        
        $aff_rows = $this->db_kapi->affected_rows();
        if($aff_rows > 0)
        {
            $id_pemohon = $this->db_kapi->insert_id();    
        }else{
            $id_pemohon = false;
        }

        return $id_pemohon;
    }

    public function input_pendok($pendok)
    {
    	$this->db_kapi->insert('trs_pendok', $pendok);                        
        
        $aff_rows = $this->db_kapi->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db_kapi->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function input_kapal($kapal)
    {
    	$this->db_kapi->insert('mst_kapal', $kapal);                           
        
        $aff_rows = $this->db_kapi->affected_rows();
        if($aff_rows > 0)
        {
            $id_kapal = $this->db_kapi->insert_id();    
        }else{
            $id_kapal = false;
        }

        return $id_kapal;
    }

    public function cek_perusahaan($no_siup)
    {
        $query = "  SELECT  mp.id_perusahaan, 
                            mp.nama_perusahaan, 
                            mp.nama_penanggung_jawab, 
                            mp.no_identitas_penanggung_jawab, 
                            mp.ttl_penanggung_jawab, 
                            mp.alamat_perusahaan
                    from db_master.mst_perusahaan mp
                    where mp.no_siup = '$no_siup' ";

        $run_query = $this->db_master->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_perusahaan_by_pemilik_id($id)
    {
        $query = "  SELECT  mp.id_perusahaan, 
                            mp.nama_perusahaan, 
                            mp.nama_penanggung_jawab, 
                            mp.no_identitas_penanggung_jawab, 
                            mp.ttl_penanggung_jawab, 
                            mp.alamat_perusahaan
                    from db_master.mst_perusahaan mp
                    where mp.id_perusahaan = '$id' 
                    order by mp.id_perusahaan DESC
                    limit 1";

        $run_query = $this->db_master->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_perusahaan_by_pemilik_justname($search_like)
    {
        $query = "  SELECT  mp.id_perusahaan, 
                            mp.nama_perusahaan, 
                            mp.nama_penanggung_jawab, 
                            mp.no_identitas_penanggung_jawab, 
                            mp.ttl_penanggung_jawab, 
                            mp.alamat_perusahaan
                    from db_master.mst_perusahaan mp
                    where mp.nama_penanggung_jawab like '".$search_like."' 
                    order by mp.id_perusahaan DESC
                    limit 1";

        $run_query = $this->db_master->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_perusahaan_by_pemilik($search_like)
    {
        $query = "  SELECT  mp.id_perusahaan, 
                            mp.nama_perusahaan, 
                            mp.nama_penanggung_jawab, 
                            mp.no_identitas_penanggung_jawab, 
                            mp.ttl_penanggung_jawab, 
                            mp.alamat_perusahaan
                    from db_master.mst_perusahaan mp
                    where mp.nama_penanggung_jawab like '%".$search_like."%' 
                    order by mp.id_perusahaan DESC
                    limit 1";

        $run_query = $this->db_master->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cek_tangkap_by_nama($search_like)
    {
    	$query = "  SELECT  mp.id_alat_tangkap, 
                            mp.nama_alat_tangkap
					from db_master.mst_alat_tangkap mp
					where mp.nama_alat_tangkap like '%".$search_like."%' 
                    and mp.aktif = 'Ya' ";

    	$run_query = $this->db_master->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    

}

?>