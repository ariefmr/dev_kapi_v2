<?php
	//OLAH DATA TAMPIL
	// $template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
	// $this->table->set_template($template);
	// $this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_data){
		vdump($list_data);
		$i = 1;
		$jml = count($list_data);
		echo $jml."<br>";
		foreach ($list_data as $item) {
			echo "[".$i."]--->";
			foreach ($item as $key => $value) {
				echo $value.", ";
			}
			echo "<br>";
			$i++;
		}

		// foreach ($list_data as $item) {
		// 	//$link_edit = '<a class="btn btn-warning" href="'.base_url('pendok/mst_pemohon/edit/'.$item->id_pendok).'">Edit</a>';
		// 	//$link_delete = '<a class="btn btn-danger" href="'.base_url('refkapi/mst_pemohon/delete/'.$item->id_pendok).'">Hapus</a>';
		// 	$this->table->add_row(
		// 						$counter.'.',
		// 						$item->no_rekam_pendok,
		// 						'<a href="'.base_url('pendok/main/view/'.$item->id_pendok).'">'.$item->nama_kapal.'</a>',
		// 						$item->nama_perusahaan,
		// 						$item->tipe_permohonan,
		// 						$item->status_pendok
		// 						);
		// 	$counter++;
		// }
	}

	// $table_list_data = $this->table->generate();
?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_pendok').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>