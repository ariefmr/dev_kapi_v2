<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dev extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $id_aplikasi_kapi = "4";

	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
	}
	
	public function index()
	{

		$this->load->model('mdl_dev_progress');
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_moni';
		$modules = 'progress';
		$views = 'view_table_progres';
		$labels = 'label_view_table_index';

		$data['list_progress'] = $this->mdl_dev_progress->get_list_progress();
		
		$this->_fakesession();

		//echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
		echo Modules::run($template, //tipe template
							$modules, //nama module
							$views, //nama file view
							$labels, //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

	public function _fakesession()
	{
			$user_data = array();
			$isLoggedIn = $this->session->userdata('logged_in');
      $whitelist = array('127.0.0.1', '::1');

			if(!$isLoggedIn) {
				if( in_array($_SERVER['REMOTE_ADDR'], $whitelist ) )
				{
					$user_data = array (
									'username' => 'fulan',
									'kode_pengguna' => 'fulan',
									'id_pengguna' => 0,
									'is_super_admin' => 0,
									'is_admin' => 1,
									'nama_pengguna' => 'fulan',
									'id_divisi' => 1,
									'id_lokasi' =>  1,
									'id_grup_pengguna' => 2,
									'fullname' => 'fulan',
									'email' => '',
									'app_access' => 0,
									'arr_access' => 0,
									'logged_in' => TRUE,
									'login_id' => 'c03de1de99c32cc2aadd160b30dba450',
									'id_aplikasi' => '72,6,9,4,13,75',
									'firstpage' => 'administrator/masterpengguna_views',
									'data_otoritas_array' => array(
													0 => array(
														'id_aplikasi_otoritas'	 	=> "4",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> ""
													),
													1 => array(
														'id_aplikasi_otoritas'	 	=> "75",
														'id_grup_otoritas' 		=> "8",
														'id_pelabuhan_otoritas' 	=> "1291"
													)
												)
								);
					$this->session->set_userdata($user_data);
				}
			}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */