<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_dev_progress extends CI_Model
{

    function __construct()
    {
        $this->load->database();

    }

	public function get_list_progress()
    {
        
    	$query =   "SELECT  kapal.no_register,
                            buku.nama_kapal,
                            pemilik.nama_penanggung_jawab,
                            pendok.no_rekam_pendok,
                            pendok.status_pendok,
                            pendok.status_edit,
                            pendok.status_verifikasi,
                            pendok.status_entry_bkp,
                            pendok.status_cetak_bkp,
                            pendok.status_terima_bkp
                    FROM    db_pendaftaran_kapal.mst_kapal as kapal,
                            db_pendaftaran_kapal.trs_bkp as buku,
                            db_pendaftaran_kapal.trs_pendok as pendok,
                            db_master.mst_perusahaan as pemilik
                    WHERE   pendok.id_perusahaan = pemilik.id_perusahaan
                    AND     pendok.id_kapal = kapal.id_kapal
                    AND     kapal.id_bkp_terakhir = buku.id_bkp
                   ";
                   /*
                    SELECT  kapal.no_register,
                            buku.nama_kapal,
                            pemilik.nama_penanggung_jawab,
                            pendok.status_pendok,
                            pendok.status_edit,
                            pendok.status_verifikasi,
                            pendok.status_entry_bkp,
                            pendok.status_cetak_bkp
                    FROM    db_pendaftaran_kapal.mst_kapal as kapal
                            JOIN db_pendaftaran_kapal.trs_bkp as buku
                            ON kapal.id_bkp = buku.id_bkp
                            JOIN db_pendaftaran_kapal.trs_pendok as pendok
                            ON pendok.id_kapal = kapal.id_kapal
                            LEFT JOIN db_master.mst_perusahaan as pemilik
                            ON pendok.id_perusahaan = pemilik.id_perusahaan
                   */
    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}