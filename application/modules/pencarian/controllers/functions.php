<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MX_Controller {

	function __construct()
		{
			parent::__construct();
		}

	public function pencarian()
	{
		$this->load->model('mdl_pencarian_kapal');
		$this->load->model('kapal/mdl_kapal');
		$this->load->model('buku_kapal/mdl_buku_kapal');

		$array_input = $this->input->post(NULL, TRUE);

		$temp_alat_tangkap = explode("|", $array_input['id_alat_tangkap']);

		if( $array_input['nama_kapal'] === '' ) {
			$array_input['nama_kapal'] = FALSE;
		}
        if( $array_input['nama_pemilik'] === '' ) {
        	$array_input['nama_pemilik'] = FALSE;
        }
        if( $array_input['alamat_pemilik'] === '' ) {
        	$array_input['alamat_pemilik'] = FALSE;
        }
        if( $array_input['nomor_mesin'] === '' ) {
        	$array_input['nomor_mesin'] = FALSE;
        }
        if( $array_input['merek_mesin'] === '' ) {
        	$array_input['merek_mesin'] = FALSE;
        }
        if( $array_input['jumlah_palka'] === '' ) {
        	$array_input['jumlah_palka'] = FALSE;
        }
        if( $array_input['nomor_ga'] === '' ) {
        	$array_input['nomor_ga'] = FALSE;
        }
        if( $array_input['tempat_ga'] === '' ) {
        	$array_input['tempat_ga'] = FALSE;
        }
        if( $array_input['tempat_pembangunan'] === '' ) {
        	$array_input['tempat_pembangunan'] = FALSE;	
        }
        if( $array_input['id_bahan_kapal'] === 'semua' ) {
        	$array_input['id_bahan_kapal'] = FALSE;
        }
        if( $array_input['id_jenis_kapal'] === 'semua' ) {
        	$array_input['id_jenis_kapal'] = FALSE;
        }
        if( $temp_alat_tangkap[0] === "0" ) {
        	$temp_alat_tangkap[0] = FALSE;
        }
        if( $array_input['nama_perusahaan'] === '' ) {
        	$array_input['nama_perusahaan'] = FALSE;
        }


        if($array_input['search_gt'] !== '' ){
			$gt = TRUE;
	        $temp_gt_kapal = explode(",", $array_input['search_gt'] );
			$gt_from = $temp_gt_kapal[0];
			$gt_to = $temp_gt_kapal[1];
        }else{
        	$gt = FALSE;
        	$gt_from = '';
			$gt_to = '';
        }
        if($array_input['search_ploa'] !== '' ){
			$panjang_loa = TRUE;
			$temp_panjang_loa = explode(",", $array_input['search_ploa']);
			$panjang_loa_from = $temp_panjang_loa[0];
			$panjang_loa_to = $temp_panjang_loa[1];
        }else{
        	$panjang_loa = FALSE;
        	$panjang_loa_from = '';
			$panjang_loa_to = '';
        }
        if($array_input['search_l'] !== '' ){
			$panjang = TRUE;
			$temp_panjang = explode(",", $array_input['search_l']);
			$panjang_from = $temp_panjang[0];
			$panjang_to = $temp_panjang[1];
        }else{
        	$panjang = FALSE;
        	$panjang_from = '';
			$panjang_to = '';
        }
        if($array_input['search_thnpm'] !== '' ){
			$tahun = TRUE;
			$temp_tahun_pembangunan = explode(",", $array_input['search_thnpm']);
			$tahun_from = $temp_tahun_pembangunan[0];
			$tahun_to = $temp_tahun_pembangunan[1];
        }else{
        	$tahun = FALSE;
        	$tahun_from = '';
			$tahun_to = '';
        }
        if($array_input['search_daya'] !== '' ){
			$daya = TRUE;
			$temp_daya = explode(",", $array_input['search_daya']);
			$daya_from = $temp_daya[0];
			$daya_to = $temp_daya[1];
        }else{
        	$daya = FALSE;
        	$daya_from = '';
			$daya_to = '';
        }
        if($array_input['search_palka'] !== '' ){
			$palka = TRUE;
			$temp_palka = explode(",", $array_input['search_palka']);
			$palka_from = $temp_palka[0];
			$palka_to = $temp_palka[1];
        }else{
        	$palka = FALSE;
        	$palka_from = '';
			$palka_to = '';
        }

		$data = array(
						'nama_kapal' => $array_input['nama_kapal'],
				        'nama_penanggung_jawab' => $array_input['nama_pemilik'],
				        'alamat_perusahaan' => $array_input['alamat_pemilik'],
				        'no_mesin' => $array_input['nomor_mesin'],
				        'merek_mesin' => $array_input['merek_mesin'],
				        'jumlah_palka' => $array_input['jumlah_palka'],
				        'no_grosse_akte' => $array_input['nomor_ga'],
				        'tempat_grosse_akte'  => $array_input['tempat_ga'],
				        'tempat_pembangunan' => $array_input['tempat_pembangunan'],
				        'id_alat_tangkap' => $temp_alat_tangkap[0],
				        'id_bahan_kapal' => $array_input['id_bahan_kapal'],
				        'id_jenis_kapal' => $array_input['id_jenis_kapal'],
				        'gt' => $gt,
				        'gt_from' => $gt_from,
				        'gt_to' => $gt_to,
				        'panjang_loa' => $panjang_loa,
				        'panjang_loa_from' => $panjang_loa_from,
				        'panjang_loa_to' => $panjang_loa_to,
				        'panjang' => $panjang,
				        'panjang_from' => $panjang_from,
				        'panjang_to' => $panjang_to,
				        'tahun' => $tahun,
				        'tahun_from' => $tahun_from,
				        'tahun_to' => $tahun_to,
				        'daya' => $daya,
				        'daya_from' => $daya_from,
				        'daya_to' => $daya_to,
				        'palka' => $palka,
				        'palka_from' => $palka_from,
				        'palka_to' => $palka_to,
				        'nama_perusahaan' => $array_input['nama_perusahaan']
						
				);

		$get_pencarian = $this->mdl_pencarian_kapal->get_list_kapal_kapi($data);


		vdump($data);
		vdump($get_pencarian);

	}

}
