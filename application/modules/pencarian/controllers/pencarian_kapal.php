<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pencarian_kapal extends MX_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('mdl_pencarian_kapal');
  }

  public function views()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js','bootstrap-slider.js');
    $add_css = array('select2.css', 'jquery.dataTables.css','slider.css');

    $template = 'templates/page/v_form';
    $modules = 'pencarian';
    $views = 'view_pencarian_kapal';
    $labels = 'label_view_pencarian_kapal';
    $data['submit_form'] = 'pencarian/pencarian_kapal/pencarian';

    // $data['get_pencarian'] = $this->mdl_pencarian_kapal->list_kapal_kapi();
    $data['hasil_pencarian'] = FALSE;
    // vdump($data, true);

    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function pencarian()
  {
    $this->load->library('excel');
    $this->load->model('mdl_pencarian_kapal');
    $this->load->model('kapal/mdl_kapal');
    $this->load->model('buku_kapal/mdl_buku_kapal');

    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js','bootstrap-slider.js');
    $add_css = array('select2.css', 'jquery.dataTables.css','slider.css');

    $template = 'templates/page/v_form';
    $modules = 'pencarian';
    $views = 'view_hasil';
    $labels = 'label_view_hasil_pencarian';
    $data['submit_form'] = '';

    $array_input = $this->input->post(NULL, TRUE);

    $temp_alat_tangkap = explode("|", $array_input['id_alat_tangkap']);

    if( $array_input['nama_kapal'] === '' ) {
      $array_input['nama_kapal'] = FALSE;
    }
    if( $array_input['nama_pemilik'] === '' ) {
      $array_input['nama_pemilik'] = FALSE;
    }
    if( $array_input['alamat_pemilik'] === '' ) {
      $array_input['alamat_pemilik'] = FALSE;
    }
    if( $array_input['nomor_mesin'] === '' ) {
      $array_input['nomor_mesin'] = FALSE;
    }
    if( $array_input['merek_mesin'] === '' ) {
      $array_input['merek_mesin'] = FALSE;
    }
    if( $array_input['jumlah_palka'] === '' ) {
      $array_input['jumlah_palka'] = FALSE;
    }
    if( $array_input['nomor_ga'] === '' ) {
      $array_input['nomor_ga'] = FALSE;
    }
    if( $array_input['tempat_ga'] === '' ) {
      $array_input['tempat_ga'] = FALSE;
    }
    if( $array_input['tempat_pembangunan'] === '' ) {
      $array_input['tempat_pembangunan'] = FALSE; 
    }
    if( $array_input['id_bahan_kapal'] === 'semua' ) {
      $array_input['id_bahan_kapal'] = FALSE;
    }
    if( $array_input['id_jenis_kapal'] === 'semua' ) {
      $array_input['id_jenis_kapal'] = FALSE;
    }
    if( $temp_alat_tangkap[0] === "0" ) {
      $temp_alat_tangkap[0] = FALSE;
    }
    if( $array_input['nama_perusahaan'] === '' ) {
      $array_input['nama_perusahaan'] = FALSE;
    }

    if($array_input['search_gt'] !== '' ){
      $gt = TRUE;
      $temp_gt_kapal = explode(",", $array_input['search_gt'] );
      $gt_from = $temp_gt_kapal[0];
      $gt_to = $temp_gt_kapal[1];
    }else{
      $gt = FALSE;
      $gt_from = '';
      $gt_to = '';
    }
    if($array_input['search_ploa'] !== '' ){
      $panjang_loa = TRUE;
      $temp_panjang_loa = explode(",", $array_input['search_ploa']);
      $panjang_loa_from = $temp_panjang_loa[0];
      $panjang_loa_to = $temp_panjang_loa[1];
    }else{
      $panjang_loa = FALSE;
      $panjang_loa_from = '';
      $panjang_loa_to = '';
    }
    if($array_input['search_l'] !== '' ){
      $panjang = TRUE;
      $temp_panjang = explode(",", $array_input['search_l']);
      $panjang_from = $temp_panjang[0];
      $panjang_to = $temp_panjang[1];
    }else{
      $panjang = FALSE;
      $panjang_from = '';
      $panjang_to = '';
    }
    if($array_input['search_thnpm'] !== '' ){
      $tahun = TRUE;
      $temp_tahun_pembangunan = explode(",", $array_input['search_thnpm']);
      $tahun_from = $temp_tahun_pembangunan[0];
      $tahun_to = $temp_tahun_pembangunan[1];
    }else{
      $tahun = FALSE;
      $tahun_from = '';
      $tahun_to = '';
    }
    if($array_input['search_daya'] !== '' ){
      $daya = TRUE;
      $temp_daya = explode(",", $array_input['search_daya']);
      $daya_from = $temp_daya[0];
      $daya_to = $temp_daya[1];
    }else{
      $daya = FALSE;
      $daya_from = '';
      $daya_to = '';
    }
    if($array_input['search_palka'] !== '' ){
      $palka = TRUE;
      $temp_palka = explode(",", $array_input['search_palka']);
      $palka_from = $temp_palka[0];
      $palka_to = $temp_palka[1];
    }else{
      $palka = FALSE;
      $palka_from = '';
      $palka_to = '';
    }
    if($array_input['search_noreg'] !== '' ){
      $noreg = TRUE;
      $temp_noreg = explode(",", $array_input['search_noreg']);
      $noreg_from = $temp_noreg[0];
      $noreg_to = $temp_noreg[1];
    }else{
      $noreg = FALSE;
      $noreg_from = '';
      $noreg_to = '';
    }

    $search_keyword = array(
      'nama_kapal' => $array_input['nama_kapal'],
      'nama_penanggung_jawab' => $array_input['nama_pemilik'],
      'alamat_perusahaan' => $array_input['alamat_pemilik'],
      'no_mesin' => $array_input['nomor_mesin'],
      'merek_mesin' => $array_input['merek_mesin'],
      'jumlah_palka' => $array_input['jumlah_palka'],
      'no_grosse_akte' => $array_input['nomor_ga'],
      'tempat_grosse_akte'  => $array_input['tempat_ga'],
      'tempat_pembangunan' => $array_input['tempat_pembangunan'],
      'id_alat_tangkap' => $temp_alat_tangkap[0],
      'id_bahan_kapal' => $array_input['id_bahan_kapal'],
      'id_jenis_kapal' => $array_input['id_jenis_kapal'],
      'gt' => $gt,
      'gt_from' => $gt_from,
      'gt_to' => $gt_to,
      'panjang_loa' => $panjang_loa,
      'panjang_loa_from' => $panjang_loa_from,
      'panjang_loa_to' => $panjang_loa_to,
      'panjang' => $panjang,
      'panjang_from' => $panjang_from,
      'panjang_to' => $panjang_to,
      'tahun' => $tahun,
      'tahun_from' => $tahun_from,
      'tahun_to' => $tahun_to,
      'daya' => $daya,
      'daya_from' => $daya_from,
      'daya_to' => $daya_to,
      'palka' => $palka,
      'palka_from' => $palka_from,
      'palka_to' => $palka_to,
      'noreg' => $noreg,
      'noreg_from' => $noreg_from,
      'noreg_to' => $noreg_to,
      'nama_perusahaan' => $array_input['nama_perusahaan']

      );


$get_pencarian = $this->mdl_pencarian_kapal->get_list_kapal_kapi($search_keyword,$kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

$data['search_keyword'] = $search_keyword;
$data['hasil_pencarian'] = $get_pencarian;
$data['submit_form'] = 'pencarian/pencarian_kapal/xls';


echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
}

public function xls()
{
  $this->load->library('excel');
  $this->load->model('mdl_pencarian_kapal');

  //info session userdata
  $kategori_pengguna = $this->mksess->id_lokasi();
  $id_propinsi_pengguna = $this->mksess->id_propinsi();
  $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();

  $add_js = array('select2.min.js', 'jquery.dataTables.min.js','bootstrap-slider.js');
  $add_css = array('select2.css', 'jquery.dataTables.css','slider.css');

  $array_input = $this->input->post(NULL, TRUE);

  $search_keyword = $array_input;

  $get_pencarian = $this->mdl_pencarian_kapal->get_list_kapal_kapi($search_keyword,$kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

  $filename = "HASIL_PENCARIAN.xlsx";
      ////
  $this->excel->setActiveSheetIndex(0);
  $this->excel->getActiveSheet()->setTitle('Daftar Hasil Pencarian');

  $sheet = $this->excel->getActiveSheet();
  $sheet      -> setCellValue('A1', 'Daftar Hasil Pencarian');
  $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
  $sheet->getStyle('A1')->applyFromArray($styleArray);
  $sheet->mergeCells('A1:G1');
      ////
  $array_thead = array( 'No. ',
    'No Register',
    'Tanda Pengenal',
    'Nama Kapal',
    'Nama Kapal Sebelumnya',
    'Tempat Pembangunan',
    'Bahan Utama Kapal',
    'Fungsi Kapal',
    'Jenis Alat Tangkap',
    'Merek Mesin Utama',
    'Daya Mesin Utama',
    'No Mesin Utama',
    'Jumlah Palka',
    'Kapasitas Palka',
    'Tempat Pendaftaran',
    'Tempat Gross Akte',
    'No Gross Akte',
    'Tanggal Gross Akte',
    'Panjang',
    'Lebar',
    'Dalam',
    'LOA',
    'GT',
    'NT',
    'Nama Perusahaan',
    'Nama Penanggung Jawab',
    'Alamat Pemilik',
    'Keterangan',
    'Tanggal Terima Dokumen',
    'Tanggal Tanda Tangan Direktur',
    'Petugas'
    );

  $column_letter = 'A';

  foreach ($array_thead as $key => $value) {
   $sheet      -> setCellValue($column_letter.'3', $value);
   $column_letter++;
 }                  
 $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
 $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
          // MASUKKAN DATA
 $cell_number = 4;

 $number = 1;
 foreach ($get_pencarian as $key => $item) {

   $tempat_pembangunan = $item->tempat_pembangunan;
   switch ($item->kategori_pendaftaran) {
    case 'PUSAT':
    $tempat_pendaftaran = "KKP";
    break;

    case 'PROPINSI':
    $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
    break;

    case 'KAB/KOTA':
    $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
    break;

    default:
    $tempat_pendaftaran = "KKP";
    break;
  }

  $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));

  $sheet->setCellValue('A'.$cell_number,  $number);
  $sheet->getStyle('B')->getNumberFormat()->setFormatCode('000000'); //format untuk noregister supaya formated
  $sheet->setCellValue('B'.$cell_number,  $item->no_register);
  $sheet->setCellValue('C'.$cell_number,  $item->no_tanda_pengenal);
  $sheet->setCellValue('D'.$cell_number,  $item->nama_kapal);
  $sheet->setCellValue('E'.$cell_number,  $item->nama_kapal_sblm );
  $sheet->setCellValue('F'.$cell_number,  $tempat_pembangunan);
  $sheet->setCellValue('G'.$cell_number,  $item->nama_bahan_kapal);
  $sheet->setCellValue('H'.$cell_number,  $item->nama_jenis_kapal );
  $sheet->setCellValue('I'.$cell_number,  $item->alat_tangkap);
  $sheet->setCellValue('J'.$cell_number,  $item->merek_mesin);
  $sheet->setCellValue('K'.$cell_number,  $item->daya_kapal);
  $sheet->setCellValue('L'.$cell_number,  $item->no_mesin);
  $sheet->setCellValue('M'.$cell_number,  $item->jumlah_palka);
  $sheet->setCellValue('N'.$cell_number,  $item->kapasitas_palka);
  $sheet->setCellValue('O'.$cell_number,  $tempat_pendaftaran);
  $sheet->setCellValue('P'.$cell_number,  $item->tempat_grosse_akte);
  $sheet->setCellValue('Q'.$cell_number,  $item->no_grosse_akte);
  $sheet->setCellValue('R'.$cell_number,  fmt_tgl($item->tanggal_grosse_akte));
  $sheet->setCellValue('S'.$cell_number,  $item->panjang_kapal);
  $sheet->setCellValue('T'.$cell_number,  $item->lebar_kapal);
  $sheet->setCellValue('U'.$cell_number,  $item->dalam_kapal);
  $sheet->setCellValue('V'.$cell_number,  $item->panjang_loa_kapal);
  $sheet->setCellValue('W'.$cell_number,  $item->gt_kapal);
  $sheet->setCellValue('X'.$cell_number,  $item->nt_kapal);
  $sheet->setCellValue('Y'.$cell_number,  $item->nama_perusahaan);
  $sheet->setCellValue('Z'.$cell_number,  $item->nama_penanggung_jawab);
  $sheet->setCellValue('AA'.$cell_number,  $item->alamat_perusahaan);
  $sheet->setCellValue('AB'.$cell_number,  $item->keterangan_pendok);
  $sheet->setCellValue('AC'.$cell_number, fmt_tgl($item->tanggal_surat_permohonan));
  $sheet->setCellValue('AD'.$cell_number, fmt_tgl($item->tgl_ttd_direktur));
  $sheet->setCellValue('AE'.$cell_number, $item->nama_pengguna);
  $cell_number++;           
  $number++;           
}

          //MENGATUR UKURAN KOLOM
$array_thwidth = array( 'A' => 5,
  'B' => 20,
  'C' => 50,
  'D' => 40,
  'E' => 40,
  'F' => 40,
  'G' => 40,
  'H' => 25,
  'I' => 50,
  'J' => 50,
  'K' => 20,
  'L' => 20,
  'M' => 20,
  'N' => 20,
  'O' => 20,
  'P' => 30,
  'Q' => 15,
  'R' => 20,
  'S' => 15,
  'T' => 15,
  'U' => 15,
  'V' => 15,
  'W' => 15,
  'X' => 15,
  'Y' => 50,
  'Z' => 100,
  'AA' => 10,
  'AB' => 40,
  'AC' => 40,
  'AD' => 50,
  'AE' => 50

  );
foreach ($array_thwidth as $column => $width) {
  $sheet->getColumnDimension($column)->setWidth($width);
}

$styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

          // Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Daftar Kapal Terdaftar.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
$objWriter->save('php://output');

      // echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */