<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_pencarian_kapal extends CI_Model
{
  
    function __construct()
    {

    }

  public function get_list_kapal_kapi($data, $kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $nama_kapal = $data['nama_kapal'];
        $nama_penanggung_jawab = $data['nama_penanggung_jawab'];
        $alamat_perusahaan = $data['alamat_perusahaan'];
        $no_mesin = $data['no_mesin'];
        $merek_mesin = $data['merek_mesin'];
        $jumlah_palka = $data['jumlah_palka'];
        $no_grosse_akte = $data['no_grosse_akte'];
        $tempat_grosse_akte  = $data['tempat_grosse_akte'];
        $tempat_pembangunan = $data['tempat_pembangunan'];
        $id_alat_tangkap = $data['id_alat_tangkap'];
        $id_bahan_kapal = $data['id_bahan_kapal'];
        $id_jenis_kapal = $data['id_jenis_kapal'];
        $gt = $data['gt'];
        $gt_from = $data['gt_from'];
        $gt_to = $data['gt_to'];
        $panjang_loa = $data['panjang_loa'];
        $panjang_loa_from = $data['panjang_loa_from'];
        $panjang_loa_to = $data['panjang_loa_to'];
        $panjang = $data['panjang'];
        $panjang_from = $data['panjang_from'];
        $panjang_to = $data['panjang_to'];
        $tahun = $data['tahun'];
        $tahun_from = $data['tahun_from'];
        $tahun_to = $data['tahun_to'];
        $daya = $data['daya'];
        $daya_from = $data['daya_from'];
        $daya_to = $data['daya_to'];
        $palka = $data['palka'];
        $palka_from = $data['palka_from'];
        $palka_to = $data['palka_to'];
        $noreg = $data['noreg'];
        $noreg_from = $data['noreg_from'];
        $noreg_to = $data['noreg_to'];
        $nama_perusahaan = $data['nama_perusahaan'];

        $query =   "SELECT  kapal.no_register,
                            jenis_kapal.nama_jenis_kapal,
                            bahan_kapal.nama_bahan_kapal,
                            pendok.tipe_permohonan,
                            pendok.nama_perusahaan,
                            pendok.alamat_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.no_identitas_penanggung_jawab,
                            pendok.ttl_penanggung_jawab,
                            pendok.tanggal_surat_permohonan,
                            pendok.kategori_pendaftaran,
                            pendok.keterangan_pendok,
                            buku.id_bkp,
                            buku.id_kapal,
                            buku.id_pendok,
                            buku.id_perusahaan,
                            buku.id_jenis_kapal,
                            buku.id_alat_tangkap,
                            buku.alat_tangkap,
                            buku.id_bahan_kapal,
                            buku.no_seri_bkp,
                            buku.nama_kapal,
                            buku.nama_kapal_sblm,
                            buku.no_mesin,
                            buku.merek_mesin,
                            buku.tipe_mesin,
                            buku.daya_kapal,
                            buku.satuan_mesin_utama_kapal,
                            buku.jumlah_palka,
                            buku.kapasitas_palka,
                            buku.tempat_pendaftaran_id,
                            buku.tempat_grosse_akte,
                            buku.no_grosse_akte,
                            buku.tanggal_grosse_akte,
                            buku.panjang_kapal,
                            buku.lebar_kapal,
                            buku.dalam_kapal,
                            buku.panjang_loa_kapal,
                            buku.gt_kapal,
                            buku.nt_kapal,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.nama_kasie,
                            buku.nama_kasubdit,
                            buku.nama_direktur,
                            buku.nip_direktur,
                            buku.tgl_ttd_direktur,
                            buku.tahun_pembangunan,
                            buku.tempat_pembangunan,
                            buku.no_telp_perusahaan,
                            buku.perusahaan_sblm,
                            buku.kabkota_pendaftaran,
                            pengguna.nama_pengguna
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                       )
                            ON          (
                                        buku.id_bkp = kapal.id_bkp_terakhir
                                        and buku.id_pendok = pendok.id_pendok
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        )
                            LEFT JOIN db_master.mst_pengguna pengguna ON pengguna.id_pengguna = buku.id_pengguna_buat
                    WHERE ";
                // field pencarian no.1
                if($nama_kapal){
                    $query .= " buku.nama_kapal like '%".$nama_kapal."%' ";
                    if($nama_penanggung_jawab){
                        $query .= " and pendok.nama_penanggung_jawab like '%".$nama_penanggung_jawab."%' ";
                    }
                    if($alamat_perusahaan){
                        $query .= " and pendok.alamat_perusahaan like '%".$alamat_perusahaan."%' ";
                    }
                    if($no_mesin){
                        $query .= " and buku.no_mesin = '".$no_mesin."' ";
                    }
                    if($merek_mesin){
                        $query .= " and buku.merek_mesin = '".$merek_mesin."' ";
                    }
                    if($jumlah_palka){
                        $query .= " and buku.jumlah_palka = '".$jumlah_palka."' ";
                    }
                    if($no_grosse_akte){
                        $query .= " and buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= " and buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= " and buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.2
                else if($nama_penanggung_jawab){
                    $query .= " pendok.nama_penanggung_jawab like '%".$nama_penanggung_jawab."%' ";
                    if($alamat_perusahaan){
                        $query .= " and pendok.alamat_perusahaan like '%".$alamat_perusahaan."%' ";
                    }
                    if($no_mesin){
                        $query .= " and buku.no_mesin = '".$no_mesin."' ";
                    }
                    if($merek_mesin){
                        $query .= " and buku.merek_mesin = '".$merek_mesin."' ";
                    }
                    if($jumlah_palka){
                        $query .= " and buku.jumlah_palka = '".$jumlah_palka."' ";
                    }
                    if($no_grosse_akte){
                        $query .= " and buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= " and buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= " and buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.3
                else if($alamat_perusahaan){
                    $query .= " pendok.alamat_perusahaan like '%".$alamat_perusahaan."%' ";
                    if($no_mesin){
                        $query .= " and buku.no_mesin = '".$no_mesin."' ";
                    }
                    if($merek_mesin){
                        $query .= " and buku.merek_mesin = '".$merek_mesin."' ";
                    }
                    if($jumlah_palka){
                        $query .= " and buku.jumlah_palka = '".$jumlah_palka."' ";
                    }
                    if($no_grosse_akte){
                        $query .= " and buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= " and buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= " and buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.4
                else if($no_mesin){
                    $query .= " buku.no_mesin = '".$no_mesin."' ";
                    if($merek_mesin){
                        $query .= " and buku.merek_mesin = '".$merek_mesin."' ";
                    }
                    if($jumlah_palka){
                        $query .= "and  buku.jumlah_palka = '".$jumlah_palka."' ";
                    }
                    if($no_grosse_akte){
                        $query .= "and  buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= "and  buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= "and  buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.5
                else if($merek_mesin){
                    $query .= " buku.merek_mesin = '".$merek_mesin."' ";
                    if($jumlah_palka){
                        $query .= "and  buku.jumlah_palka = '".$jumlah_palka."' ";
                    }
                    if($no_grosse_akte){
                        $query .= "and  buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= "and  buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= " buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.6
                else if($jumlah_palka){
                    $query .= " buku.jumlah_palka = '".$jumlah_palka."' ";
                    if($no_grosse_akte){
                        $query .= "and  buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    }
                    if($tempat_grosse_akte){
                        $query .= "and  buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= "and  buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.7
                else if($no_grosse_akte){
                    $query .= " buku.no_grosse_akte = '".$no_grosse_akte."' ";
                    if($tempat_grosse_akte){
                        $query .= "and  buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    }
                    if($tempat_pembangunan){
                        $query .= "and  buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.8
                else if($tempat_grosse_akte){
                    $query .= " buku.tempat_grosse_akte = '".$tempat_grosse_akte."' ";
                    if($tempat_pembangunan){
                        $query .= "and  buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    }
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.9
                else if($tempat_pembangunan){
                    $query .= " buku.tempat_pembangunan = '".$tempat_pembangunan."' ";
                    if($gt){
                        $query .= " and buku.gt_kapal > $gt_from ";
                        $query .= " and buku.gt_kapal < $gt_to ";
                    }
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.10
                else if($gt){
                    $query .= " buku.gt_kapal > $gt_from ";
                    $query .= " and  buku.gt_kapal < $gt_to ";
                    if($id_bahan_kapal){
                        $query .= " and buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    }
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.11
                else if($id_bahan_kapal){
                    $query .= " buku.id_bahan_kapal = '".$id_bahan_kapal."' ";
                    if($id_jenis_kapal){
                        $query .= " and buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    }
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.12
                else if($id_jenis_kapal){
                    $query .= " buku.id_jenis_kapal = '".$id_jenis_kapal."' ";
                    if($id_alat_tangkap){
                        $query .= " and buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    }
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.13
                else if($id_alat_tangkap){
                    $query .= " buku.id_alat_tangkap = '".$id_alat_tangkap."' ";
                    if($panjang_loa){
                        $query .= " and buku.panjang_loa_kapal > $panjang_loa_from ";
                        $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    }
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.14
                else if($panjang_loa){
                    $query .= " buku.panjang_loa_kapal > $panjang_loa_from ";
                    $query .= " and buku.panjang_loa_kapal < $panjang_loa_to ";
                    if($panjang){
                        $query .= " and buku.panjang_kapal > $panjang_from ";
                        $query .= " and buku.panjang_kapal < $panjang_to ";
                    }
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }                
                // field pencarian no.15
                else if($panjang){
                    $query .= " buku.panjang_kapal > $panjang_from ";
                    $query .= " and buku.panjang_kapal < $panjang_to ";
                    if($tahun){
                        $query .= " and buku.tahun_pembangunan > $tahun_from ";
                        $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    }
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.16
                else if($tahun){
                    $query .= " buku.tahun_pembangunan > $tahun_from ";
                    $query .= " and buku.tahun_pembangunan < $tahun_to ";
                    if($daya){
                        $query .= " and buku.daya_kapal > $daya_from ";
                        $query .= " and buku.daya_kapal < $daya_to ";
                    }
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.17
                else if($daya){
                    $query .= " buku.daya_kapal > $daya_from ";
                    $query .= " and buku.daya_kapal < $daya_to ";
                    if($palka){
                        $query .= " and buku.kapasitas_palka > $palka_from ";
                        $query .= " and buku.kapasitas_palka < $palka_to ";
                    }
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.18
                else if($palka){
                    $query .= " buku.kapasitas_palka > $palka_from ";
                    $query .= " and buku.kapasitas_palka < $palka_to ";
                    if($noreg){
                        $query .= " and kapal.no_register > $noreg_from ";
                        $query .= " and kapal.no_register < $noreg_to ";
                    }
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.19
                else if($noreg){
                    $query .= " kapal.no_register > $noreg_from ";
                    $query .= " and kapal.no_register < $noreg_to ";
                    if($nama_perusahaan){
                        $query .= " and pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }
                // field pencarian no.20
                else if($nama_perusahaan){
                    if($nama_perusahaan){
                        $query .= " pendok.nama_perusahaan like '%".$nama_perusahaan."%' ";
                    }
                }

        switch ($kategori_pengguna) {
            case '1':
                $query .= "";
                break;
            
            case '2':
                $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                break;

            case '3':
                $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                break;

            default:
                $query .= "";
                break;
        } 
        $query .=  "ORDER BY kapal.no_register ASC
                    LIMIT 0, 9999
                    ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        // echo $query;
        return $result;
    }

    public function from_kapi()
    {
      
    }
}