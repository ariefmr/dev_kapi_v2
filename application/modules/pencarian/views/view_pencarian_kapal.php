<div class="row">
  <div class="col-lg-12">
    <?php 
    echo form_open($submit_form, 'id="form_entry" role="form"');      
    ?>
    <div class="row"><!-- ROW UTAMA -->

      <!-- colom 1 -->
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="nama_kapal">Nama Kapal :</label>
            <input type="text" class="form-control" id="nama_kapal" name="nama_kapal">
          </div>

          <div class="form-group">
            <label for="nama_perusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan">
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="nomor_ga">Nomor (Gross Akte) :</label>
            <input type="number" class="form-control" id="nomor_ga" name="nomor_ga">          
          </div> 
          <div class="form-group">
            <label for="id_bahan_kapal">Bahan Kapal : </label>
            <select name="id_bahan_kapal" class="form-control">
              <option value="semua" selected>Semua</option>
              <option value="1">Besi/Baja</option>
              <option value="2">Kayu</option>
              <option value="3">Composite</option>
              <option value="4">Kayu Laminasi</option>
              <option value="5">Fiber glass</option>
              <option value="6">Ferro Cement</option>
              <option value="7">Alumunium</option>
            </select>
          </div> 
        </div>
      </div>
      <!-- end colom 1 -->

      <!-- colom 2 -->
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="nama_pemilik">Nama Pemilik :</label>
            <input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik">
          </div> 
          
          <div class="form-group">
            <label for="merek_mesin">Merk Mesin Kapal :</label>
            <select name="merek_mesin" class="form-control">           
            </select>
          </div> 
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="tempat_ga">Tempat (Gross Akte) :</label>
            <input type="text" class="form-control" id="tempat_ga" name="tempat_ga">
          </div>  
          <div class="form-group">
            <label for="id_jenis_kapal">Jenis Kapal : </label>
            <select name="id_jenis_kapal" class="form-control">
              <option value="semua" selected>Semua</option>
              <option value="0">Penangkap</option>
              <option value="1">Pengangkut/Pengumpul</option>
              <option value="2">Kapal Lampu</option>
              <option value="3">Kapal Penggiring</option>
            </select>
          </div>
        </div>
      </div>
      <!-- end colom 2 -->

      <!-- colom 3 -->
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="alamat_pemilik">Alamat Pemilik :</label>
            <input type="text" class="form-control" id="alamat_pemilik" name="alamat_pemilik">          
          </div>
          
          <div class="form-group">
            <label for="nomor_mesin">Nomor Mesin Utama :</label>
            <input type="number" class="form-control" id="nomor_mesin" name="nomor_mesin">
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="tanggal_ga">Tanggal (Gross Akte) :</label>
            <select name="tanggal_ga" class="form-control">           
            </select>
          </div> 
          <div class="form-group">
            <label for="jumlah_palka">Jumlah Palka :</label>
            <input type="number" class="form-control" id="jumlah_palka" name="jumlah_palka">
          </div> 
        </div>
      </div>
      <!-- end colom 3 -->

      <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <br>
          <br>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div class="form-group">
            <?php
            $nama_tempat = '';
            echo Modules::run('refdss/mst_wilayah/pilih_tempat_pembangunan',0,$nama_tempat);
            ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div class="form-group" style=" margin-left: 10px; padding-right: 70px;">
            <?php 
            $attr_jenis_alat_tangkap = array( 'name' => 'id_alat_tangkap',
              'label' => 'Jenis Alat Tangkap',
              'opsi' => Modules::run('refdss/mst_alat_tangkap/list_alat_tangkap_array'),
              'value' => ''
              );
            echo $this->mkform->input_select2($attr_jenis_alat_tangkap);
            ?>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
              <?php 
              $attr_jenis_alat_tangkap = array( 'name' => 'id_alat_tangkap',
                'label' => 'Jenis Alat Tangkap',
                'opsi' => Modules::run('refdss/mst_alat_tangkap/list_alat_tangkap_array'),
                'value' => ''
                );
              // echo $this->mkform->input_select2($attr_jenis_alat_tangkap);
                ?>

              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <!-- <div class="form-group"> -->
              <?php
              // $attr_jenis_kapal = array(
              //   'input_id' => 'id_jenis_kapal', 
              //   'input_name' => 'id_jenis_kapal',
              //   'label_text' => 'Jenis Kapal <em>*</em>',
              //   'array_opsi' => '', 
              //   'opsi_selected' => '2',  
              //   'input_width' => 'col-lg-6 manual_input', 
              //   'input_class' => 'form-control test', 
              //   'label_class' => 'col-lg-3 manual_input control-label',
              //   'from_table' => 'mst_jenis_kapal', 
              //   'field_value' => 'id_jenis_kapal',
              //   'field_text' => 'nama_jenis_kapal',
              //   'pencarian' => 'TRUE'
              //   );
          // $attr_jenis_kapal['enable'] = ($aksi === "view")? "false" : "true";
              // echo $this->mkform->dropdown_dss($attr_jenis_kapal);
              ?>
              <!-- </div> -->

            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <!-- <div class="form-group"> -->
              <?
              // $attr_bahan_kapal = array(
              //   'input_id' => 'id_bahan_kapal',
              //   'input_name' => 'id_bahan_kapal',
              //   'label_text' => 'Bahan Kapal',
              //   'array_opsi' => '', 
              //   'opsi_selected' => 'semua',  
              //   'input_width' => 'col-lg-6 manual_input', 
              //   'input_class' => 'form-control test', 
              //   'label_class' => 'col-lg-3 manual_input control-label',
              //   'from_table' => 'mst_bahan_kapal', 
              //   'field_value' => 'id_bahan_kapal',
              //   'field_text' => 'nama_bahan_kapal',
              //   'pencarian' => 'TRUE'
              //   );
          // $attr_bahan_kapal['enable'] = ($aksi === "view")? "false" : "true";
              // echo $this->mkform->dropdown_dss($attr_bahan_kapal);
              ?>
              <!-- </div> -->

            </div>
          </div>

        <!-- <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
           <div class="form-group">
            <label for="nomor_ga">Nomor (Gross Akte) :</label>
            <input type="number" class="form-control" id="nomor_ga" name="nomor_ga">          
          </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div class="form-group">
            <label for="tempat_ga">Tempat (Gross Akte) :</label>
            <select name="tempat_ga" class="form-control">
              <option>...</option>           
            </select>
          </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <div class="form-group">
            <label for="tanggal_ga">Tanggal (Gross Akte) :</label>
            <select name="tanggal_ga" class="form-control">           
            </select>
          </div>
        </div>
      </div> -->

    </div>

    <hr>
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 100px; margin-bottom: 25px;">
            <label>GT : <text id="info-gt">... s/d ...</text> </label>            
            <input name="search_gt" type="text" class="span2 slider-it" data-infotag="#info-gt" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,100]">               
          </div> 
        </div>
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 100px; margin-bottom: 25px;">
            <label>Panjang LOA : <text id="info-ploa">... s/d ...</text> </label>              
            <input name="search_ploa" type="text" class="span2 slider-it" data-infotag="#info-ploa" value="" data-slider-min="0" data-slider-max="500" data-slider-step="5" data-slider-value="[0,100]">               
          </div> 
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 10px; margin-bottom: 25px;">
            <label>Panjang L : <text id="info-l">... s/d ...</text> </label>           
            <input name="search_l" type="text" class="span2 slider-it" data-infotag="#info-l" value="" data-slider-min="0" data-slider-max="500" data-slider-step="5" data-slider-value="[0,100]">               
          </div> 
        </div>
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 10px; margin-bottom: 25px;">
            <label>Tahun Pembangunan : <text id="info-thnpm">... s/d ...</text> </label>             
            <input name="search_thnpm" type="text" class="span2 slider-it" data-infotag="#info-thnpm" value="" data-slider-min="1970" data-slider-max="2020" data-slider-step="1" data-slider-value="[2013,2014]">               
          </div> 
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 100px; margin-bottom: 25px;">
            <label>Daya Mesin Utama : <text id="info-daya">... s/d ...</text> </label>           
            <input name="search_daya" type="text" class="span2 slider-it" data-infotag="#info-daya" value="" data-slider-min="0" data-slider-max="500" data-slider-step="5" data-slider-value="[0,100]">               
          </div> 
        </div>     
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 10px; margin-bottom: 25px;">
            <label>Kapasitas Palka : <text id="info-palka">... s/d ...</text> </label>             
            <input name="search_palka" type="text" class="span2 slider-it" data-infotag="#info-palka" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="1" data-slider-value="[0,100]">               
          </div> 
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row slider-container">          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" margin-left: 100px; margin-bottom: 25px;">
            <label>No Register : <text id="info-noreg">... s/d ...</text> </label>             
            <input name="search_noreg" type="text" class="span2 slider-it" data-infotag="#info-noreg" value="" data-slider-min="0" data-slider-max="8000" data-slider-step="1" data-slider-value="[0,8000]">               
          </div> 
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button type="submit" class="btn btn-large btn-block btn-primary">Search</button>    
      </div>  
    </div>


    <?php
    echo form_close();
    ?>
  </div>
</div>
<hr>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
var merk_mesin = ['','AKASAKA','BAZAN MAN B&amp;W 8 L 20/27','BIG CAM','BLACKSTONE EZSL 12','CAIYOUJI','CARTERPILLAR','CHAYOJI','CHIDONG','CUMMINS',
'DAEWOO','DAIHATSU','DAIYA','DALIAN','DELETE','DETROIT','DEUTZ TD','DFD','DONG FENG','FUJI','FUSO','GM-DIESEL','HANSHIN','HINO',
'HYUNDAI','GUANDONG','GUANZHOU','GUASCOR','ISUZU','JDEC','JINAN','KOMATSU','KONG CHIU','LU TAI DY','M.W.M. DIESEL','MAGDEBURG',
'MAK','MAKITA','MATSUI','MERCEDES','MITSUBISHI','MITSUI','MTU','MWM','NANTONG','NIIGATA','NINGBO','NISSAN','OTSUKA','PH 300',
'SANG YONG','SDEC','SHANDONG','SHANGHAI','TONG XIN','TUNG TECH','VOLVO','WEICHAI','WEIFANG','WUXI DIESEL','YANMAR','YUASA',
'YUCHAI','ZIBO','ZICHAI','ZINTO'];

$(document).ready( function () {
		// $("#table_container").hide();
    merk_mesin.forEach(function(d){
      $("select[name=merek_mesin]").append('<option>'+d+'</option>');
    })

    //listener update slider pas selesai di slide
    $('.slider-it').slider().on('slideStop', function(ev){
      var info = $(this).val().split(',');
      info = info.join(' s/d ');
      var info_tag = $(this).data('infotag');
      $(info_tag).html(info);
    });
     //Set default
     $('.slider-it').each(function(){
      var info = $(this).data('sliderValue').join(' s/d ');

      var info_tag = $(this).data('infotag');
      $(info_tag).html(info);
    });
     $('#table_daftar_pendok').dataTable( {
       "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
       "aoColumns":  [
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"}
       ],
       "bFilter": true,
       "bAutoWidth": false,
       "bInfo": false,
       "bPaginate": true,
       "bSort": true,
       "fnInitComplete": function(oSettings, json) {
         $("#loading_info").hide();
         $("#table_container").removeClass('hidden');
       }
     } );
} );
</script>