<?php
	//OLAH DATA TAMPIL
$template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
$this->table->set_template($template);
$this->table->set_heading('No. ',
  'No Register',
  'Tanda Pengenal',
  'Nama Kapal',
  'Nama Kapal Sebelumnya',
  'Tempat Pembangunan',
  'Bahan Utama Kapal',
  'Fungsi Kapal',
  'Jenis Alat Tangkap',
  'Merek Mesin Utama',
  'Daya Mesin Utama',
  'No Mesin Utama',
  'Jumlah Palka',
  'Kapasitas Palka',
  'Tempat Pendaftaran',
  'Tempat Gross Akte',
  'No Gross Akte',
  'Tanggal Gross Akte',
  'Panjang',
  'Lebar',
  'Dalam',
  'LOA',
  'GT',
  'NT',
  'Nama Perusahaan',
  'Nama Penanggung Jawab',
  'Alamat Pemilik',
  'Keterangan',
  'Tanggal Terima Dokumen',
  'Tanggal Tanda Tangan Direktur',
  'Petugas'
  );

if($hasil_pencarian)
{
  // vdump($hasil_pencarian);
  $number = 1;
  foreach ($hasil_pencarian as $key => $item) {
        /*if($item->negara_pembangunan === 'Indonesia' ){
              $tempat_pembangunan = $item->kabkota_pembangunan;
         }else{
         }*/
         $tempat_pembangunan = $item->tempat_pembangunan;
         switch ($item->kategori_pendaftaran) {
          case 'PUSAT':
          $tempat_pendaftaran = "KKP";
          break;

          case 'PROPINSI':
          $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
          break;

          case 'KAB/KOTA':
          $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
          break;

          default:
          $tempat_pendaftaran = "KKP";
          break;
        }
        $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));
        $this->table->add_row( 
          $number ,
          noreg($item->no_register) ,
          $item->no_tanda_pengenal ,
          $item->nama_kapal,
          $item->nama_kapal_sblm ,
          $tempat_pembangunan ,
          $item->nama_bahan_kapal,
          $item->nama_jenis_kapal ,
          $item->alat_tangkap ,
          $item->merek_mesin ,
          $item->daya_kapal ,
          $item->no_mesin,
          $item->jumlah_palka,
          $item->kapasitas_palka,
          $tempat_pendaftaran,
          $item->tempat_grosse_akte ,
          $item->no_grosse_akte ,
          fmt_tgl($item->tanggal_grosse_akte) ,
          $item->panjang_kapal ,
          $item->lebar_kapal ,
          $item->dalam_kapal ,
          $item->panjang_loa_kapal ,
          $item->gt_kapal ,
          $item->nt_kapal ,
          kos($item->nama_perusahaan,'-' ),
          kos($item->nama_penanggung_jawab,'-' ),
          kos($item->alamat_perusahaan,'-' ),
          kos($item->keterangan_pendok,'-' ),
          fmt_tgl($item->tanggal_surat_permohonan),
          fmt_tgl($item->tgl_ttd_direktur),
          $item->nama_pengguna
          );
        $number++;
      }
    }

    $table_report_kapal = $this->table->generate();

    $this->table->clear();
    $link_entry = '<a href="xls" class="btn btn-primary">Export to XLS</a>';
    ?>

<!-- TOMBOL EXPORT XLS -->
<?php
echo form_open($submit_form, 'id="form_entry" role="form"', $search_keyword);
/*
  keterangan:
  tombol export melemparkan hasil pencarian berdasarkan $search_keyword
*/
?>
    <div class="row">
      <div class="col-lg-12"> 
        <div class="form-group">
          <div class="col-sm-3">
           <button type="submit" class="btn btn-large btn-block btn-primary">Export to XLS</button> 
         </div>
       </div>
     </div>
    </div>
<?php
echo form_close();
?>
<!-- END TOMBOL EXPORT XLS -->

    <p></p>
    <!-- TAMPIL DATA -->
    <div id="loading_info" class="panel panel-default">
      <div class="panel-body">
       <p class="text-center">
         Memuat Data Kapal. Harap Tunggu...
       </p>
     </div>
   </div>

   <div class="row">
     <div class="col-lg-12" style="overflow-x: auto;">
       <?php echo $table_report_kapal; ?>    
     </div>
   </div>

 <!-- ADDITIONAL JAVASCRIPT -->
 <script>
 var merk_mesin = ['','AKASAKA','BAZAN MAN B&amp;W 8 L 20/27','BIG CAM','BLACKSTONE EZSL 12','CAIYOUJI','CARTERPILLAR','CHAYOJI','CHIDONG','CUMMINS',
 'DAEWOO','DAIHATSU','DAIYA','DALIAN','DELETE','DETROIT','DEUTZ TD','DFD','DONG FENG','FUJI','FUSO','GM-DIESEL','HANSHIN','HINO',
 'HYUNDAI','GUANDONG','GUANZHOU','GUASCOR','ISUZU','JDEC','JINAN','KOMATSU','KONG CHIU','LU TAI DY','M.W.M. DIESEL','MAGDEBURG',
 'MAK','MAKITA','MATSUI','MERCEDES','MITSUBISHI','MITSUI','MTU','MWM','NANTONG','NIIGATA','NINGBO','NISSAN','OTSUKA','PH 300',
 'SANG YONG','SDEC','SHANDONG','SHANGHAI','TONG XIN','TUNG TECH','VOLVO','WEICHAI','WEIFANG','WUXI DIESEL','YANMAR','YUASA',
 'YUCHAI','ZIBO','ZICHAI','ZINTO'];

 $(document).ready( function () {
		// $("#table_container").hide();
    merk_mesin.forEach(function(d){
      $("select[name=merek_mesin]").append('<option>'+d+'</option>');
    })

    //listener update slider pas selesai di slide
    $('.slider-it').slider().on('slideStop', function(ev){
      var info = $(this).val().split(',');
      info = info.join(' s/d ');
      var info_tag = $(this).data('infotag');
      $(info_tag).html(info);
    });
     //Set default
     $('.slider-it').each(function(){
      var info = $(this).data('sliderValue').join(' s/d ');

      var info_tag = $(this).data('infotag');
      $(info_tag).html(info);
    });
     $('#table_daftar_pendok').dataTable( {
       "sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
       "aoColumns":  [
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"},
       {"sClass": "text-center"}
       ],
       "bFilter": true,
       "bAutoWidth": false,
       "bInfo": false,
       "bPaginate": true,
       "bSort": true,
       "fnInitComplete": function(oSettings, json) {
         $("#loading_info").hide();
         $("#table_container").removeClass('hidden');
       }
     } );
} );
</script>