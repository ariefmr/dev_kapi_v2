<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class C_verifyLogin extends CI_Controller {
    function __construct() {
        parent::__construct();
        //load session and connect to database
        $this->load->model('mdl_login','login',TRUE);
        $this->load->helper(array('form', 'url','html'));
        $this->load->library(array('form_validation','session'));
    }
 
    function index() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
 
        if($this->form_validation->run() == FALSE) {
            
            $this->load->config('labels');

            $data['database_timeline'] = $this->config->item('database_timeline');

            $data['labels'] =  Array(
                                        'form_kapal' => $this->config->item('form_kapal')
                                    );
            echo Modules::run('templates/page/v_view', //tipe template
                                'login', //nama module
                                'v_login', //nama file view
                                'login/c_verifylogin', //nama file form
                                '', //dari labels.php
                                '', //plugin javascript khusus untuk page yang akan di load
                                '', //file css khusus untuk page yang akan di load
                                $data); //array data yang akan digunakan di file view
            
            
            } else {
                //Go to private area
                redirect(base_url('#'), 'refresh');
            }       
     }
    
     /*
     buat session set_userdata "logged_in"
     untuk session 
        - id_pengguna
        - nama_pengguna
        - id_gurp_pengguna
        - id_perusahaan
     */
     function check_database($password) {
         //Field validation succeeded.  Validate against database
         $username = $this->input->post('username');
         //query the database
         $result = $this->login->login($username, $password);
         if($result) {
             $sess_array = array();
             foreach($result as $row) {
                 //create the session
                 $sess_array = array('id_pengguna' => $row->id_pengguna,
                     'nama_pengguna' => $row->nama_pengguna,
                     'id_grup_pengguna' => $row->id_grup_pengguna,
                     'id_perusahaan' => $row->id_perusahaan,
                     'nama_lokasi' => $row->nama_lokasi,
                     );
                 //set session with value from database
                 $this->session->set_userdata('logged_in', $sess_array);
                 }
                 // var_dump($sess_array);
          return TRUE;
          } else {
              //if form validate false
              $this->form_validation->set_message('check_database', 'Invalid username or password');
              return FALSE;
          }
      }
}