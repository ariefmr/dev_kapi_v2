<?php
  echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>
<div class="row">
  <div class="col-md-6">
    <?php

      $attr_kategori_pengguna = array( 
                      'name'    => $form['kategori_pengguna']['name'],
                      'label'   => $form['kategori_pengguna']['label'],
                      'opsi'    => array(
                          '2' => 'PROPINSI',
                          '3' => 'KAB/KOTA',),
                      'value'   => 2,
                      'OnChange' => 'cek_kategori(this.value)'
                          );
       echo $this->mkform->input_select($attr_kategori_pengguna);

    ?>
      
  </div>

  <div class="col-md-6"> 
          <?php

          //AREA FORM KABKOTA
          echo "<div id='area_kab_kota' style='margin-bottom:15px; display:block;' >";
          $attr_prop_kota  = array( 
                                'input_id' => 'id_propinsi',
                                'input_id_kota' => 'id_kabupaten_kota',
                                'input_name' => 'id_propinsi',
                                'input_name_kota' => 'id_kabupaten_kota',
                                'label_text' => 'Propinsi',
                                'label_text_kota' => 'Kabupaten/Kota',
                                'array_opsi' => '', 
                                'opsi_selected' => kos($detail_pengguna_kapi["id_propinsi"]),  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label'
                          );
          echo $this->mkform->dropdown_propkota_kode($attr_prop_kota);
          echo "</div>";

          ?>
        </div>
</div>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Log in</button>
            </div>
          </div>
  </div>
</div>
</form>

<script>
  
    function cek_kategori(jenis_kategori)
    {
      if(jenis_kategori==='PUSAT')
      {
        document.getElementById("area_pusat").style.display = "block";
        // document.getElementById("area_propinsi").style.display = "none";
        document.getElementById("area_kab_kota").style.display = "none";

      }
      else if(jenis_kategori==='2')
      {
        document.getElementById("area_kab_kota").style.display = "block";
        document.getElementById("area_pusat").style.display = "none";
        // document.getElementById("area_propinsi").style.display = "none";
      }
      else if(jenis_kategori==='3')
      {
        document.getElementById("area_kab_kota").style.display = "block";
        document.getElementById("area_pusat").style.display = "none";
        // document.getElementById("area_propinsi").style.display = "none";
      }
    }

</script>