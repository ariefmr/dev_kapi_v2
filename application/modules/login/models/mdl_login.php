<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
  
class Mdl_login extends CI_Model {
 
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function login($username) {
        //create query to connect user login database
        $this->db->select('mst_pengguna.id_pengguna,
                           mst_pengguna.nama_pengguna,
                           mst_pengguna.kode_pengguna AS username,
                           mst_pengguna.kata_kunci AS password,
                           mst_pengguna.id_perusahaan,
                           mst_pengguna.id_grup_pengguna,
                           mst_lokasi.nama_lokasi,
                           mst_grup_pengguna.nama_grup_pengguna,
                           mst_perusahaan.company_name');
        $this->db->from('mst_pengguna');
        $this->db->join('mst_lokasi','mst_lokasi.id_lokasi = mst_pengguna.id_lokasi');
        $this->db->join('mst_perusahaan','mst_perusahaan.id_perusahaan = mst_pengguna.id_perusahaan','left');
        $this->db->join('mst_grup_pengguna','mst_grup_pengguna.id_grup_pengguna = mst_pengguna.id_grup_pengguna','left');
        $this->db->where('nama_pengguna', $username);
        $this->db->limit(1);
         
        //get query and processing
        $query = $this->db->get();
        if($query->num_rows() == 1) { 
            return $query->result_array(); //if data is true
        } else {
            return false; //if data is wrong
        }
    }
}