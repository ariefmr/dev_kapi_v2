var link_search_kapal_dss = "<?php echo base_url('refdss/mst_kapal/search_kapal');?>",
	link_detail_kapal_dss = "<?php echo base_url('refdss/mst_kapal/detail_kapal');?>",
  link_search_kapal_kapi = "<?php echo base_url('refkapi/mst_kapal/search_kapal');?>",
  link_detail_kapal_kapi = "<?php echo base_url('refkapi/mst_kapal/detail_kapal');?>";	

// Stored function: semua fungsi di push ke s_func lalu di jalanin setelah document ready
var s_func = [];

var nama_bulan = new Array("Januari", "Pebruari", "Maret", 
"April", "Mei", "Juni", "Juli", "Agustus", "September", 
"Oktober", "November", "Desember");

var nama_bulan_short = ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agst','Sept','Okt','Nov','Des'];

var site_url = '<?php echo site_url(); ?>';

function format_date_to_str(objectDate)
{
  // Javasript date use month number start from 0. So January is 0, December is 11;
  return objectDate.getDate()+"/"+ (objectDate.getMonth() +1 )+"/"+objectDate.getFullYear();
}

function format_thnbln_to_str(objectDate)
{
  var bulan = nama_bulan[objectDate.getMonth()];
  return  bulan+", "+objectDate.getFullYear();
}



$(document).ready(function(){
	while(s_func.length > 0)
	{
		(s_func.shift())();
	}
});