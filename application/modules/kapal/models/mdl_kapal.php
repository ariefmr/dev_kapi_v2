<?php

class Mdl_kapal extends CI_Model
{   
	private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    public function list_kapal_pendok($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $query = "SELECT 
                    pendok.id_pendok, 
                    pendok.no_rekam_pendok, 
                    kapal.no_register, 
                    bkp.nama_kapal as nama_kapal_bkp,
                    bkp.gt_kapal as gt, 
                    pendok.nama_kapal as nama_kapal_pendok,
                    pendok.nama_perusahaan, 
                    pendok.tipe_permohonan,
                    pendok.status_entry_bkp,
                    pendok.kategori_pendaftaran,
                    pendok.propinsi_pendaftaran,
                    pendok.kabkota_pendaftaran
                    -- FROM db_pendaftaran_kapal.trs_pendok pendok
                    FROM (select * from db_pendaftaran_kapal.trs_pendok limit 5800, 5000) pendok
                    
                    LEFT JOIN db_pendaftaran_kapal.trs_bkp bkp
                    ON bkp.id_pendok = pendok.id_pendok
                    LEFT JOIN db_pendaftaran_kapal.mst_kapal kapal
                    ON kapal.id_pendok_terakhir = pendok.id_pendok
                    WHERE pendok.status_pendok = 'FINAL' 
                            and pendok.aktif = 'YA' ";
            /*$query =   "SELECT  kapal.id_pendok_terakhir as id_pendok,
                                pendok.no_rekam_pendok,
                                kapal.no_register,
                                buku.nama_kapal as nama_kapal_bkp,
                                pendok.id_perusahaan,
                                pendok.nama_penanggung_jawab,
                                pendok.nama_perusahaan,
                                buku.id_pengguna_buat,
                                buku.gt_kapal as gt,
                                pendok.tipe_permohonan,
                                pendok.status_entry_bkp,
                                pendok.status_verifikasi,
                                pendok.kategori_pendaftaran
                        FROM    db_pendaftaran_kapal.mst_kapal as kapal
                                JOIN        db_pendaftaran_kapal.trs_bkp as buku
                                ON          kapal.id_bkp_terakhir = buku.id_bkp
                                JOIN        db_pendaftaran_kapal.trs_pendok as pendok
                                ON          pendok.id_pendok = kapal.id_pendok_terakhir 
                        WHERE pendok.status_pendok = 'FINAL' 
                                and buku.aktif = 'YA' ";*/
            switch ($kategori_pengguna) {
                case '1':
                    $query .= "";
                    break;
                
                case '2':
                    $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                    break;

                case '3':
                    $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                    break;

                default:
                    $query .= "";
                    break;
            } 
        $query .=  "ORDER BY pendok.no_rekam_pendok
                    DESC
                   ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;

    	$this->db->insert('trs_bkp', $data);

    	$aff_rows = $this->db->affected_rows();
    	if($aff_rows > 0)
    	{
    		$id_kapal = $this->db->insert_id();	
    	}else{
    		$id_kapal = false;
    	}

    	return $id_kapal;
    }

	public function input_gambar($data)
    {
        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;

        $this->db->insert('trs_foto_kapal', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $status_upload = true;
        }
        else{
            $status_upload = false;
        }

        return $status_upload;
    }
	
    public function update_kapal($id_kapal,$data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_kapal', $id_kapal);

        if($this->db->update('mst_kapal', $data))
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }

    public function update_nama_kapal($id_pendok,$data,$data2,$data3)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok_terakhir', $id_pendok);
        $this->db->update('mst_kapal', $data);

        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data2);

        $this->db->where('id_pendok', $id_pendok);
        if($this->db->update('trs_bkp', $data3))
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }
    
    public function update($id_pendok, $data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_bkp', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }
    
    public function update_perusahaan($id_pendok, $data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

    	$this->db->where('id_pendok', $id_pendok);
    	$this->db->update('trs_bkp', $data);

        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

    	$aff_rows = $this->db->affected_rows();
    	if($aff_rows > 0)
    	{
    		$affected = true;	
    	}else{
    		$affected = false;
    	}

    	return $affected;
    }
    

    public function set_final($id_kapal)
    {
        $date_terima = date('Y-m-d');
        $id_pengguna = $this->mksess->id_pengguna();
        $data = array('status_dokumen' => 'final'
                        ,'tanggal_terima_dokumen' => $date_terima
                        ,'tanggal_ubah' => 'NOW()'
                       ,'id_pengguna_ubah' => $id_pengguna
                    );
        $this->db->where('id_kapal', $id_kapal);
        $this->db->update('trs_bkp', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function get_detail($id_pendok)
    {
        $query = $this->db->get_where('trs_bkp', array('id_pendok' => $id_pendok));

        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_mst_kapal_detail($id_kapal)
    {
        $query = $this->db->get_where('mst_kapal',array('id_kapal' => $id_kapal));

        if($query->num_rows() > 0 )
        {
            $result = $query->result();
        }
        else
        {
            $result = false;
        }

        return $result;
    }

    public function get_trs_bkp_detail($id_bkp)
    {
        $query = $this->db->get_where('trs_bkp',array('id_bkp' => $id_bkp));

        if($query->num_rows() > 0 )
        {
            $result = $query->result();
        }
        else
        {
            $result = false;
        }

        return $result;
    }
    
    public function get_foto_kapal($id_bkp)
    {
        $sql = "SELECT foto_kapal.id_foto_kapal,foto_kapal.path_foto
                  FROM trs_bkp bkp
                  LEFT JOIN trs_foto_kapal foto_kapal
                  ON bkp.id_bkp = foto_kapal.id_bkp
                  WHERE bkp.id_bkp = '$id_bkp'";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0 )
        {
            $result = $query->result();
        }
        else
        {
            $result = false;
        }

        return $result;   
    }

     public function get_trs_perubahan($id_pendok)
    {
        $query = "
                    SELECT  tipe.kolom_db
                    FROM    trs_perubahan trs, 
                            mst_tipe_perubahan tipe
                    WHERE   trs.id_pendok = ".$id_pendok."
                    AND     trs.id_tipe_perubahan = tipe.id_tipe_perubahan
                    AND     trs.aktif = 'ya'

                 ";

        $run_query = $this->db->query($query);

        if($run_query->num_rows() > 0)
        {
            $result = $run_query->result_array();
        }
        else{
            $result = false;
        }

        return $result;

    }

    public function get_detail_cetak($id_pendok)
    {
        $query = "  SELECT  kapal.no_register,
                            perusahaan.nama_perusahaan,
                            perusahaan.alamat_perusahaan,
                            perusahaan.nama_penanggung_jawab,
                            perusahaan.no_siup,
                            jenis_kapal.nama_jenis_kapal,
                            alat_tangkap.nama_alat_tangkap,
                            bahan_kapal.nama_bahan_kapal,
                            kabkota.nama_kabupaten_kota,
                            buku.*
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        db_master.mst_perusahaan as perusahaan,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal,
                                        db_master.mst_kabupaten_kota as kabkota
                                       )
                            ON          (
                                        buku.id_bkp = kapal.id_bkp_terakhir
                                        and buku.id_pendok = pendok.id_pendok
                                        and buku.id_perusahaan = perusahaan.id_perusahaan
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        and buku.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        and buku.tempat_pembangunan_id = kabkota.id_kabupaten_kota
                                        )
                    WHERE   buku.id_pendok = '$id_pendok'
                 ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function set_verifikasi($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $data = array('status_verifikasi' => 'YA'
                        ,'tanggal_ubah' => $date_terima
                        ,'id_pengguna_ubah' => '12'
                    );
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function set_status_entry($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $data = array('status_entry_bkp' => '1');
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function get_trs_foto()
    {
        $query = " SELECT * FROM trs_foto_kapal, mst_kapal where trs_foto_kapal.id_kapal = mst_kapal.id_kapal";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kode_prop($id_propinsi)
    {
        $query = " SELECT propinsi.kode 
                        FROM db_pendaftaran_kapal.mst_propinsi propinsi
                        WHERE propinsi.id_propinsi = '".$id_propinsi."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kode_kabkota($id_kabupaten_kota)
    {
        $query = " SELECT kabkota.kode 
                        FROM db_pendaftaran_kapal.mst_kabupaten_kota kabkota
                        WHERE kabkota.id_kabupaten_kota = '".$id_kabupaten_kota."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_nama_kabkota($id_kabupaten_kota)
    {
        $query = " SELECT kabkota.nama_kabupaten_kota 
                        FROM db_pendaftaran_kapal.mst_kabupaten_kota kabkota
                        WHERE kabkota.id_kabupaten_kota = '".$id_kabupaten_kota."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kode_jns_kapal($id_jns_kapal)
    {
        $query = " SELECT jns_kapal.kode_penamaan 
                        FROM db_pendaftaran_kapal.mst_jenis_kapal jns_kapal
                        WHERE jns_kapal.id_jenis_kapal = '".$id_jns_kapal."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kode_alat_tangkap($id_jns_alat)
    {
        $query = " SELECT alat_tangkap.kode_penandaan, alat_tangkap.nama_alat_tangkap
                        FROM db_pendaftaran_kapal.mst_alat_tangkap alat_tangkap
                        WHERE alat_tangkap.id_alat_tangkap = '".$id_jns_alat."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_id_kapal($id_pendok)
    {
        $query = " SELECT bkp.id_kapal 
                        FROM db_pendaftaran_kapal.trs_bkp bkp
                        WHERE bkp.id_pendok = '".$id_pendok."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_id_pendok_sebelum($id_kapal)
    {
        $query = " SELECT trs_bkp.id_pendok 
                    from db_pendaftaran_kapal.trs_bkp 
                    where trs_bkp.id_kapal = '".$id_kapal."' 
                        and trs_bkp.aktif = 'ya'
                        order by id_bkp 
                        desc limit 1,1
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

}

?>
