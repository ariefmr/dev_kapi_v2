<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 21cm;
	        min-height: 29.7cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
        .table {
            border: 5px black dotted;
        }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>
<?php 
 	$tmpl = array ( 'table_open'  => '<table id="table_detail_kapal" class="table">' );
    $this->table->set_template($tmpl);

    $this->table->add_row('1.','Nama Kapal ',': '.kos($detail_kapal['nama_kapal']));
    $this->table->add_row('2.','Nama Kapal Sebelumnya ',': '.kos($detail_kapal['nama_kapal_sebelumnya']));
    $this->table->add_row('3.','Tempat/Tahun Pembangunan', ': '.kos($detail_kapal['nama_kabupaten_kota']).'/'
                            .kos($detail_kapal['tahun_pembangunan']));
    $this->table->add_row('4.','Type/Jenis Kapal', ': '.kos($detail_kapal['nama_jenis_kapal']));
    $this->table->add_row('5.','Alat Penangkap Ikan', ': '.kos($detail_kapal['nama_alat_tangkap']));
    $this->table->add_row('6.','Merek dan Type Mesin Utama Kapal', ': '.kos($detail_kapal['merek_mesin']).' '
                            .kos($detail_kapal['tipe_mesin']));
    $this->table->add_row('7.','Daya Mesin Utama Kapal', ': '.kos($detail_kapal['daya_kapal']));
    $this->table->add_row('8.','Nomor Mesin Utama Kapal', ': '.kos($detail_kapal['no_mesin']));
    $this->table->add_row('9.','Jumlah Palka', ': '.kos($detail_kapal['jumlah_palka']));
    $this->table->add_row('10.','Kapasitas Palka', ': '.kos($detail_kapal['kapasitas_palka']));
    $this->table->add_row('11.','Tempat Pendaftaran', ': '.kos($detail_kapal['tempat_pendaftaran']));
    $this->table->add_row('12.','Tempat, No, Tgl Gross Akte', ': '.kos($detail_kapal['tempat_pendaftaran']).', '
                            .kos($detail_kapal['no_grosse_akte']).', '
                            .kos($detail_kapal['tgl_grosse_akte']));
    $this->table->add_row('13.','Tanda Pegenal', ': '.kos($detail_kapal['no_tanda_pengenal']));
    $this->table->add_row('14.','Lebar dan Panjang Kapal (m)', ': '.kos($detail_kapal['lebar_kapal']).' & '
                            .kos($detail_kapal['panjang_kapal']));
    $this->table->add_row('15.','GT dan NT Kapal', ': '.kos($detail_kapal['gt_kapal']).' & '
                            .kos($detail_kapal['nt_kapal']));

   
    $html = $this->table->generate();
    $this->table->clear();

    $html .= '<div class="datagrid">';
    $html .= '</div>';

    $tmpl = array ( 'table_open'  => '<table id="table_detail_kapal">' );
    $this->table->set_template($tmpl);

    $this->table->add_row('1.','Nama Pemilik ',': '.kos($detail_kapal['nama_perusahaan']));
    $this->table->add_row('2.','Alamat Pemilik ',': '.kos($detail_kapal['alamat_perusahaan']));
    $this->table->add_row('3.','Nama Penanggung Jawab', ': '.kos($detail_kapal['nama_penanggung_jawab']));
    $this->table->add_row('4.','No SIUP', ': '.kos($detail_kapal['no_siup']));

    $html2 = $this->table->generate();
    $this->table->clear();

    $html2 .= '<div class="datagrid">';
    $html2 .= '</div>';

 ?>
 <div class="book">
    <div class="page">
        <div class="subpage">
            <p>
                <h3 align="center">IDENTITAS KAPAL PERIKANAN</h3>
            </p>
                <h4 align="right">No Reg: <?=kos($detail_kapal['no_register'])?><br>No Seri: <?=kos($detail_kapal['no_seri_bkp'])?></h4>
                
                

        	<?php echo $html; ?>
        	
            <p><h3 align="center">IDENTITAS PEMILIK KAPAL PERIKANAN</h3></p>

            <?php echo $html2; ?>
            <?php 
                $i = 0;
                while($i < 1){
                    echo '<br>';
                    $i++;               
                }
            ?>
            <p><h3 align="center">VERIFIKATOR</h3></p>

            <table class="table table-striped table-bordered table-hover">
                            
                <tbody>
                    <tr>
                        <th width="350" style="text-align: center;"><h3>Kasie</h3></th>
                        <th width="350" style="text-align: center;"><h3>Kasubdit</h3></th>
                    </tr>
                    <tr>
                        <td>
            <?php 
                $i = 0;
                while($i < 5){
                    echo '<br>';
                    $i++;               
                }
            ?>          </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="350" style="text-align: center;"><h3><?=$detail_kapal['nama_kasie']?></h3></td>
                        <td width="350" style="text-align: center;"><h3><?=$detail_kapal['nama_kasubdit']?></h3></td>
                    </tr>
                </tbody>
            </table>

            <?php 
                $i = 0;
                while($i < 1){
                    echo '<br>';
                    $i++;               
                }
            ?>
            <!-- <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p> -->
        </div>    
    </div>
</div>

