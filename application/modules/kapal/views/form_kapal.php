<?php

/*  LIST DATA KAPAL YANG TERSEDIA dan YANG DIPAKAI:
    1. $detail_pendok = $this->mdl_pendok->get_detail($id_pendok); //untuk inisialisasi entry data kapal
    2. $detail_kapal = $this->mdl_kapal->get_mst_kapal_detail($data['detail_pendok']['id_kapal']);
    3. $detail_bkp = $this->mdl_kapal->get_trs_bkp_detail($data['detail_kapal']['id_bkp_terakhir']);
    4. $detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($data['detail_kapal']['id_pendok_terakhir']);
*/

	$hidden_input = array('id_pendok' => $detail_pendok['id_pendok'],
                        'id_perusahaan'  =>  $detail_pendok['id_perusahaan'],
                        'nama_kapal' => $detail_pendok['nama_kapal'],
                        'id_bkp' => kos($detail_bkp['id_bkp'])
                        );

    if(isset($detail_kapal))
    {
        $hidden_input['id_kapal']  =  $detail_kapal['id_kapal'];
        $hidden_input['no_register'] = kos($detail_kapal['no_register']);
    }
    // vdump($hidden_input);
// -------------------------------------------------------------------------------------------------------
//FILTER MEMBEDAKAN AKSI VIEW ATAU ENTRY, UNTUK AKSI FORM_OPEN NYA...$submit_form
// -------------------------------------------------------------------------------------------------------
if($aksi === 'view'){
?>
    <div class="row">
      <div class="col-lg-12"> 
              <div class="form-group col-lg-10">
              </div>
              <div class="form-group col-lg-2">
                  <div class="col-sm-offset-2 col-sm-4">
<?php 
                  $link_aksi = '<a class="btn btn-primary" href="'.base_url('kapal/main/entry/'.$detail_pendok['id_pendok']).'">'.$button.'</a>';
                  echo $link_aksi; 
?>
                </div>
              </div>
      </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

<?php
        echo form_open_multipart('id="form_entry" class="form-horizontal" role="form"', $hidden_input);
}else{
?>

    <div class="row" id="form_perubahan" >
        <div class="col-lg-12">

    <?php
        echo form_open_multipart($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
}
// -------------------------------------------------------------------------------------------------------
//END FILTER MEMBEDAKAN AKSI VIEW ATAU ENTRY, UNTUK AKSI FORM_OPEN NYA...$submit_form
// -------------------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------------
//FILTER MEMBEDAKAN TIPE PERMOHONAN,
// -------------------------------------------------------------------------------------------------------

//JIKA TIPE PERMOHONAN = PERUBAHAN ADALAH DATA INPUTAN DARI APLIKASI BARU MAKA PERUBAHAN DI MUNCULKAN REFERENSI SEBELUM PERUBAHAN
if(($detail_pendok['tipe_permohonan'] === 'PERUBAHAN') && ($detail_pendok['id_kapal'] > 6415)){
?>
<!-- TAMPILAN REFERENSI UNTUK TIPE PERMOHONAN PERUBAHAN, ADA REFERENSI DATA KAPAL SEBELUM PERUBAHAN AGAR PETUGAS DAPAT 
MELIHAT HISTORI KAPAL YANG AKAN MENGALAMI PERUBAHAN -->
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <dl class="dl-horizontal text-ref">
              <dt>Nama Kapal : </dt>
                <dd><?=$detail_buku['nama_kapal']?></dd>

              <hr>

              <dt>GT :</dt>
              <dd><?=$detail_buku['gt_kapal']?></dd>

              <dt>NT :</dt>
              <dd><?=$detail_buku['nt_kapal']?></dd>

              <hr>

              <dt>Perusahaan :</dt>
              <dd><?=$detail_buku['nama_perusahaan']?></dd>

              <dt>No Register : </dt>
              <dd><?=$detail_buku['no_register']?></dd>

              <dt>No Seri Buku Kapal :</dt>
              <dd><?=$detail_buku['no_seri_bkp']?></dd>

              <dt>No Tanda Pengenal :</dt>
              <dd><?=$detail_buku['no_tanda_pengenal']?></dd>
              
              <hr>

              <dt>Jenis Kapal :</dt>
              <dd><?=$detail_buku['nama_jenis_kapal']?></dd>
              
              <dt>Jenis Alat Tangkap :</dt>
              <dd><?=$detail_buku['alat_tangkap']?></dd>
              
              <dt>Bahan Kapal :</dt>
              <dd><?=$detail_buku['nama_bahan_kapal']?></dd>
              
              <hr>
            </dl>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <dl class="dl-horizontal text-ref">
            <dt>Merk Mesin :</dt>
              <dd><?=$detail_buku['merek_mesin']?></dd>
              
              <dt>Tipe Mesin :</dt>
              <dd><?=$detail_buku['tipe_mesin']?></dd>

              <dt>No Mesin :</dt>
              <dd><?=$detail_buku['no_mesin']?></dd>

              <dt>Daya Kapal :</dt>
              <dd><?=$detail_buku['daya_kapal']?></dd>

              <hr>

              <dt>Panjang Kapal :</dt>
              <dd><?=$detail_buku['panjang_kapal']?></dd>

              <dt>Lebar Kapal :</dt>
              <dd><?=$detail_buku['lebar_kapal']?></dd>

              <dt>Dalam Kapal :</dt>
              <dd><?=$detail_buku['dalam_kapal']?></dd>

              <dt>Panjang LOA Kapal :</dt>
              <dd><?=$detail_buku['panjang_loa_kapal']?></dd>
        </dl>
    </div>
<!-- END TAMPILAN REFERENSI UNTUK TIPE PERMOHONAN PERUBAHAN, ADA REFERENSI DATA KAPAL SEBELUM PERUBAHAN AGAR PETUGAS DAPAT 
MELIHAT HISTORI KAPAL YANG AKAN MENGALAMI PERUBAHAN -->
            
    <div class="clearfix"></div>
    <?php      
    $attr_foto  = array(   'name'  =>  $form['path_foto']['name'],
                           'label' =>  $form['path_foto']['label'],
                           'value' =>  kos($detail_bkp['path_foto'])
                    );

    echo $this->mkform->input_image($attr_foto);

    $attr_no_register = array(   'name' => 'no_reg_view',
                                'label' => 'No Register',
                                'value' => noreg($detail_kapal['no_register']),
                                'disabled' => true
                    );
    echo $this->mkform->input_text($attr_no_register);
    
    $attr_nama_kapal = array(   'name' => $form['nama_kapal']['name'],
                                'label' => $form['nama_kapal']['label'],
                                'value' => kos($detail_bkp['nama_kapal'], ' ')
                );
    echo $this->mkform->input_text($attr_nama_kapal);

    if($aksi === 'entry'){
      
        //$hidden_input['nama_kapal_sblm'] = kos($detail_bkp['nama_kapal']);

        $array_check = json_decode($json_id_perubahan);
        // vdump($array_check);
        if(in_array('id_nama_kapal', $array_check))
        {
            $attr_hidden_nama_sblm = array( 
                                  'name'    =>'nama_kapal_sblm',
                                  'label'   => 'nama_kapal_sblm',
                                  'value'   => kos($detail_bkp['nama_kapal'])
                              );
            echo $this->mkform->input_hidden($attr_hidden_nama_sblm);

            echo "<div id='area_cari_namakapal' style='margin-bottom:15px; display:block;' >";
            echo Modules::run('refkapi/mst_kapal/pilih_nama_kapal');
            echo "</div>";
            echo "<script>";
            echo '$("label[for=\'nama_kapal\']").text("Nama Kapal Sekarang")';
            echo "  
                    $('#id_nama_kapal').prop('disabled', true)
                    $('#panel-pilih-kapal2 .row strong').first().text('Nama Kapal Baru');
                    //getAllResponseHeaders();
                  </script>";
        }

        if(in_array('id_no_tanda_pengenal', $array_check)){
            /* no_tanda_pengenal */
            //sementara bisa di edit karena belum dihandle jika kapal jenis kapal angkut
            $no_tanda_pengenal = array(   'name' => 'no_tanda_pengenal',
                                        'label' => 'No Tanda Pengenal',
                                        'value' => kos($detail_buku['no_tanda_pengenal']),
                                        'disabled' => false
                            );
            echo $this->mkform->input_text($no_tanda_pengenal);
        }
    }

    if(kos($detail_bkp['nama_kapal_sblm'])!= "")
    {

        $attr_nama_kapal_sblm = array(    'name' => '',
                                            'label' => $form['nama_kapal_sblm']['label'],
                                            'value' => kos($detail_bkp['nama_kapal_sblm']),
                                            'disabled' => true
                    );
        echo $this->mkform->input_text($attr_nama_kapal_sblm); 

    }

//END JIKA TIPE PERMOHONAN = PERUBAHAN ADALAH DATA INPUTAN DARI APLIKASI BARU MAKA PERUBAHAN DI MUNCULKAN REFERENSI SEBELUM PERUBAHAN
}else{
//JIKA TIPE PERMOHONAN = BARU MAKA TIDAK DI MUNCULKAN TAMPILAN REFERENSI
    $attr_foto       = array(   'name'  =>  $form['path_foto']['name'],
                                'label' =>  $form['path_foto']['label'],
                                'value' =>  kos($foto_kapal)
                                );

    echo $this->mkform->input_image($attr_foto);

    echo $this->mkform->dupe_form($attr_foto);

    $is_admin = $this->mksess->info_is_admin();
    if( $is_admin ){
      $disabled = FALSE;
    }else{
      $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
    }

    $attr_no_register = array(   'name' => 'no_reg_view',
                                'label' => 'No Register',
                                'value' => noreg($detail_kapal['no_register']),
                                'disabled' => true
                    );
    echo $this->mkform->input_text($attr_no_register);

    $attr_nama_kapal = array(   'name' => $form['nama_kapal']['name'],
                                'label' => $form['nama_kapal']['label'],
                                'value' => kos($detail_bkp['nama_kapal']),
                                'disabled' => $disabled
                    );
    echo $this->mkform->input_text($attr_nama_kapal);

//-----------------------------------------------------------------------------------------
/*
JIKA TIPE PERMOHONAN = PERUBAHAN dan Ceklis perubahan Nama Kapal, HANDLE DATA KAPAL LAMA,
13 JUNI 2014, MASALAH NYA PADA SAAT PAK KOKO MENELPON
*/
//-----------------------------------------------------------------------------------------
if($detail_pendok['tipe_permohonan'] === 'PERUBAHAN'){

  if($aksi === 'entry'){
      
      $array_check = json_decode($json_id_perubahan);

      // vdump($array_check);
      if(in_array('id_nama_kapal', $array_check))
      {
          $attr_hidden_nama_sblm = array( 
                                  'name'    =>'nama_kapal_sblm',
                                  'label'   => 'nama_kapal_sblm',
                                  'value'   => kos($detail_bkp['nama_kapal'])
                              );
          echo $this->mkform->input_hidden($attr_hidden_nama_sblm);
          
          echo "<div id='area_cari_namakapal' style='margin-bottom:15px; display:block;' >";
          echo Modules::run('refkapi/mst_kapal/pilih_nama_kapal');
          echo "</div>";
          echo "<script>";
          echo '$("label[for=\'nama_kapal\']").text("Nama Kapal Sekarang")';
          echo "  
                  $('#id_nama_kapal').prop('disabled', true)
                  $('#panel-pilih-kapal2 .row strong').first().text('Nama Kapal Baru');
                  //getAllResponseHeaders();
                </script>";
      }

      if(in_array('id_no_tanda_pengenal', $array_check)){
          /* no_tanda_pengenal */
          //sementara bisa di edit karena belum dihandle jika kapal jenis kapal angkut
          $no_tanda_pengenal = array(   'name' => 'no_tanda_pengenal',
                                      'label' => 'No Tanda Pengenal',
                                      'value' => kos($detail_buku['no_tanda_pengenal']),
                                      'disabled' => false
                          );
          echo $this->mkform->input_text($no_tanda_pengenal);
      }
  }
}

    $attr_nama_kapal_sblm = array(    'name' => $form['nama_kapal_sblm']['name'],
                                            'label' => $form['nama_kapal_sblm']['label'],
                                            'value' => kos($detail_bkp['nama_kapal_sblm']),
                                            'disabled' => false
                    );
    echo $this->mkform->input_text($attr_nama_kapal_sblm);
//END JIKA TIPE PERMOHONAN = BARU MAKA TIDAK DI MUNCULKAN TAMPILAN REFERENSI
}
// -------------------------------------------------------------------------------------------------------
//END FILTER MEMBEDAKAN TIPE PERMOHONAN,
// -------------------------------------------------------------------------------------------------------

    $attr_tempat_pendaftaran = array(   'opsi' => array('PUSAT' => 'PUSAT',
                                                        'PROPINSI' => 'PROPINSI',
                                                        'KAB/KOTA' => 'KAB/KOTA'
                                                        ),
                                        'name' => $form['tempat_pendaftaran']['name'],
                                        'label' => $form['tempat_pendaftaran']['label'],
                                        'value' => $this->mksess->kategori_pengguna(),
                                        'disabled' => true
                    );
    $attr_tempat_pendaftaran['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->input_select($attr_tempat_pendaftaran);

    $attr_tempat_pembangunan  = array( 
                        'name' => 'tempat_pembangunan',
                        'label' => 'Tempat Pembangunan',
                        'value' => kos($detail_bkp['tempat_pembangunan'])
                  );
    $attr_tempat_pembangunan['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->input_text($attr_tempat_pembangunan);

    $attr_negara = array(   'opsi' => Modules::run('refkapi/mst_kapal/list_tempat_array'),
                            'name' => 'tempat_pembangunan',
                            'label' => 'Tempat Pembangunan',
                            'value' => $detail_bkp['tempat_pembangunan']
                );
    $attr_negara['enable'] = ($aksi === "view")? "false" : "true";
    // echo $this->mkform->input_select2($attr_negara);
    
/*SEMENTARA KARENA QUERY AMBIL TEMPAT PEMBANGUNAN NYA OVER MEMORY JADI DI COMMENT DULU YA
SAMBIL NUNGGU MYSQL DI SERVER INTEGRASI DI RESTART OLEH PIHAK YG BERWENANG..*/
/*=====================================================================================..*/
    // $nama_tempat = $detail_bkp['tempat_pembangunan'];
    // echo Modules::run('refdss/mst_wilayah/pilih_tempat_pembangunan',0,$nama_tempat);  
/*=====================================================================================..*/

// -------------------------------------------------------------------------------------------------------
//UNTUK HANDLE DATA KAPAL DARI DATABASE KAPI YANG LAMA, BAGIAN FIELD TEMPAT_PEMBANGUNAN
// -------------------------------------------------------------------------------------------------------
/*
    //JIKA ID_KAPAL DI BAWAH 6416 MAKA DI ASUMSIKAN NEGARA PEMBANGUNAN NYA DI AMBIL DARI FIELD negara_pembangunan
    if($detail_bkp['id_kapal'] < 6416){
        $attr_tempat_pembangunan  = array( 
                            'name' => 'negara_pembangunan_dblama',
                            'label' => 'Tempat Pembangunan',
                            'value' => kos($detail_bkp['negara_pembangunan'])
                      );
        $attr_tempat_pembangunan['enable'] = ($aksi === "view")? "false" : "true";
        echo $this->mkform->input_text($attr_tempat_pembangunan);

    }else{
    //JIKA KAPAL YANG DI ENTRY DARI APLIKASI BARU (ID_KAPAL > 6415), ADA PILIHAN NEGARA, PROPINSI DAN KABUPATEN KOTA
        $attr_negara = array(   'opsi' => Modules::run('refdss/mst_wilayah/list_negara_array'),
                                'name' => 'neg_pem',
                                'label' => 'Negara Pembangunan',
                                'value' => $detail_bkp['id_negara_pembangunan'].'|'.$detail_bkp['negara_pembangunan']
                    );
        $attr_negara['enable'] = ($aksi === "view")? "false" : "true";
        echo $this->mkform->input_select2($attr_negara);

        $attr_tempat_pembangunan  = array( 
                            'input_width' => 'col-lg-6 manual_input', 
                            'label_text' => 'Tempat Pembangunan',
                            'label_class' => 'col-lg-3 manual_input control-label',
                            'value' => $detail_bkp['id_propinsi_pembangunan'].'|'.$detail_bkp['id_kabkota_pembangunan'].'|'.$detail_bkp['propinsi_pembangunan'].'|'.$detail_bkp['kabkota_pembangunan']
                      );
        $attr_tempat_pembangunan['enable'] = ($aksi === "view")? "false" : "true";
        echo $this->mkform->dropdown_kabkota_prop($attr_tempat_pembangunan);
        
    }
*/
// -------------------------------------------------------------------------------------------------------
//END UNTUK HANDLE DATA KAPAL DARI DATABASE KAPI YANG LAMA, BAGIAN FIELD TEMPAT_PEMBANGUNAN
// -------------------------------------------------------------------------------------------------------

    $attr_tahun_pembangunan = array(   'name' => $form['tahun_pembangunan']['name'],
                                        'label' => $form['tahun_pembangunan']['label'],
                                        'value' => kos($detail_bkp['tahun_pembangunan'])
                                );
    echo $this->mkform->input_text($attr_tahun_pembangunan);

    $attr_bahan_kapal = array(
                                'input_id' => $form['bahan_kapal']['name'],
                                'input_name' => $form['bahan_kapal']['name'],
                                'label_text' => $form['bahan_kapal']['label'],
                                'array_opsi' => '', 
                                'opsi_selected' => kos($detail_bkp["id_bahan_kapal"]),  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label',
                                'from_table' => 'mst_bahan_kapal', 
                                'field_value' => 'id_bahan_kapal',
                                'field_text' => 'nama_bahan_kapal'
                            );
    $attr_bahan_kapal['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->dropdown_dss($attr_bahan_kapal);

    $attr_jenis_kapal = array(
                                'input_id' => 'id_jenis_kapal', 
                                'input_name' => 'id_jenis_kapal',
                                'label_text' => 'Jenis Kapal <em>*</em>',
                                'array_opsi' => '', 
                                'opsi_selected' => $detail_bkp["id_jenis_kapal"],  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label',
                                'from_table' => 'mst_jenis_kapal', 
                                'field_value' => 'id_jenis_kapal',
                                'field_text' => 'nama_jenis_kapal'
                            );
    $attr_jenis_kapal['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->dropdown_dss($attr_jenis_kapal);

    $attr_jenis_alat_tangkap = array( 'name' => 'id_alat_tangkap',
                                      'label' => $form['alat_tangkap']['label'],
                                      'opsi' => Modules::run('refdss/mst_alat_tangkap/list_alat_tangkap_array'),
                                      'value' => $detail_bkp['id_alat_tangkap'].'|'.$detail_bkp['alat_tangkap']
                    );

    $attr_jenis_alat_tangkap['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->input_select2($attr_jenis_alat_tangkap);

    $attr_merk_mesin_kapal = array( 'name' => $form['merek_mesin']['name'],
                                    'label' => $form['merek_mesin']['label'],
                                    'value' => kos($detail_bkp['merek_mesin'])
                    );
    echo $this->mkform->input_text($attr_merk_mesin_kapal);

    $attr_tipe_mesin_kapal = array( 'name' => $form['tipe_mesin']['name'],
                                    'label' => $form['tipe_mesin']['label'],
                                    'value' => kos($detail_bkp['tipe_mesin'])
                    );
    echo $this->mkform->input_text($attr_tipe_mesin_kapal);

    $attr_daya_kapal = array(   'name' => $form['daya_kapal']['name'],
                                'label' => $form['daya_kapal']['label'],
                                'value' => kos_value($detail_bkp['daya_kapal']),
                                'tipe' => 'text'
                    );
    // echo $this->mkform->input_text($attr_daya_kapal);

    $attr_satuan_mesin_utama_kapal = array( 'name' => $form['satuan_mesin_utama_kapal']['name'],
                                            'label' => $form['satuan_mesin_utama_kapal']['label'],
                                            'opsi' => array('1' => 'PK', 
                                                            '2' => 'DK',
                                                            '3' => 'HP'),
                                            'value' => kos($detail_bkp['satuan_mesin_utama_kapal']),
                                            'tipe' => 'select'
                    );
    $attr_satuan_mesin_utama_kapal['enable'] = ($aksi === "view")? "false" : "true";
    // echo $this->mkform->input_select($attr_satuan_mesin_utama_kapal);

    $attr_multi_input_daya_mesin = array (
                            'name'  => "multi_input",
                            'label'  => "Daya Mesin Kapal",
                            'arr'   => array(
                                        $attr_daya_kapal,
                                        $attr_satuan_mesin_utama_kapal
                                        ),
                            'tipe' => 'true'
                        );
    echo $this->mkform->one_row($attr_multi_input_daya_mesin);

    $attr_no_mesin = array( 'name' => $form['no_mesin']['name'],
                            'label' => $form['no_mesin']['label'],
                            'value' => kos($detail_bkp['no_mesin'])
                    );
    echo $this->mkform->input_text($attr_no_mesin);

    $attr_jumlah_palka = array( 'name' => $form['jumlah_palka']['name'],
                                'label' => $form['jumlah_palka']['label'],
                                'value' => kos_value($detail_bkp['jumlah_palka'])
                    );
    echo $this->mkform->input_text($attr_jumlah_palka);

    $attr_kapasitas_palka = array(  'name' => $form['kapasitas_palka']['name'],
                                    'label' => $form['kapasitas_palka']['label'],
                                    'value' => kos_value($detail_bkp['kapasitas_palka'])
                    );
    echo $this->mkform->input_text($attr_kapasitas_palka);

    $attr_tempat_grosse_akte = array(   'name' => $form['tempat_grosse_akte']['name'],
                                        'label' => $form['tempat_grosse_akte']['label'],
                                        'value' => kos($detail_bkp['tempat_grosse_akte'])
                    );
    echo $this->mkform->input_text($attr_tempat_grosse_akte);

    $attr_no_grosse_akte = array(   'name' => $form['no_grosse_akte']['name'],
                                    'label' => $form['no_grosse_akte']['label'],
                                    'value' => kos($detail_bkp['no_grosse_akte'])
                    );
    echo $this->mkform->input_text($attr_no_grosse_akte);

    $attr_path_file_gross = array(
                                    'name'  =>  $form['path_file_gross']['name'],
                                    'label' =>  $form['path_file_gross']['label'],
                                    'view_file' => false,
                                    'value' =>  kos($detail_bkp['path_file_gross'])
                                );
    echo $this->mkform->input_image($attr_path_file_gross);

    $attr_imo_number = array(   'name' => $form['imo_number']['name'],
                                    'label' => $form['imo_number']['label'],
                                    'value' => kos($detail_bkp['imo_number'])
                    );
    echo $this->mkform->input_text($attr_imo_number);

    $attr_tanggal_grosse_akte = array(  
                                    // 'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                                    // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                                    'default_date' => kos($detail_bkp['tanggal_grosse_akte']),
                                    'placeholder' => '', // wajib ada atau '' (kosong)
                                    'name' => $form['tanggal_grosse_akte']['name'], // wajib ada
                                    'label' => $form['tanggal_grosse_akte']['label'], // wajib ada
                                    'curdate' => false
                    );
    echo $this->mkform->input_date($attr_tanggal_grosse_akte);

/*  
//-------------------------------------------------------------------------------
FORM UNTUK PANJANG, LEBAR, DALAM KAPAL SEBELUM MENGGUNAKAN MKFORM ONE_ROW()
//-------------------------------------------------------------------------------

    echo $this->mkform->input_text($attr_panjang_kapal);

    echo $this->mkform->input_text($attr_lebar_kapal);

    echo $this->mkform->input_text($attr_dalam_kapal);

//-------------------------------------------------------------------------------
END FORM UNTUK PANJANG, LEBAR, DALAM KAPAL SEBELUM MENGGUNAKAN MKFORM ONE_ROW()
//-------------------------------------------------------------------------------
*/

// FORM INPUT UNTUK PANJANG, LEBAR, DAN DALAM KAPAL MENGGUNAKAN FUNGSI MKFORM ONE_ROW()
    $attr_panjang_kapal = array(    'name' => $form['panjang_kapal']['name'],
                                    'label' => $form['panjang_kapal']['label'],
                                    'value' => kos_value($detail_bkp['panjang_kapal'])
                    );
    $attr_lebar_kapal = array( 'name' => $form['lebar_kapal']['name'],
                                'label' => $form['lebar_kapal']['label'],
                                'value' => kos_value($detail_bkp['lebar_kapal'])
                    );
    $attr_dalam_kapal = array( 'name' => $form['dalam_kapal']['name'],
                                'label' => $form['dalam_kapal']['label'],
                                'value' => kos_value($detail_bkp['dalam_kapal'])
                    );

    $attr_multi_input = array (
                            'name'  => "multi_input",
                            'label'  => "Panjang x Lebar x Dalam",
                            'arr'   => array(
                                        $attr_panjang_kapal,
                                        $attr_lebar_kapal,
                                        $attr_dalam_kapal
                                        )
                        );
    echo $this->mkform->one_row($attr_multi_input);
// END FORM INPUT UNTUK PANJANG, LEBAR, DAN DALAM KAPAL MENGGUNAKAN FUNGSI MKFORM ONE_ROW()

    $attr_panjang_loa_kapal = array(    'name' => $form['panjang_loa_kapal']['name'],
                                        'label' => $form['panjang_loa_kapal']['label'],
                                        'value' => kos_value($detail_bkp['panjang_loa_kapal'])
                    );
    echo $this->mkform->input_text($attr_panjang_loa_kapal);

    $attr_gt_kapal = array( 'name' => $form['gt_kapal']['name'],
                            'label' => $form['gt_kapal']['label'],
                            'value' => kos($detail_bkp['gt_kapal']),
                            'limit_gt' => $limit_gt,
                            'min_gt' => $min_gt
                    );
    echo $this->mkform->input_range($attr_gt_kapal);

    $attr_nt_kapal = array( 'name' => $form['nt_kapal']['name'],
                            'label' => $form['nt_kapal']['label'],
                            'value' => kos($detail_bkp['nt_kapal'])
                    );
    echo $this->mkform->input_text($attr_nt_kapal);

// ---------------------------------------------------------------------------------------------------------------------
//SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO TELP PERUSAHAAN. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN
// ---------------------------------------------------------------------------------------------------------------------
    $attr_telp_perusahaan = array( 'name' => 'no_telp_perusahaan',
                            'label' => 'No Telp Perusahaan',
                            'value' => kos($detail_bkp['no_telp_perusahaan'])
                    );
    echo $this->mkform->input_text($attr_telp_perusahaan);    

    $attr_nt_kapal = array( 'name' => 'perusahaan_sblm',
                            'label' => 'Perusahaan Sebelum',
                            'value' => kos($detail_bkp['perusahaan_sblm'])
                    );
    echo $this->mkform->input_text($attr_nt_kapal);
// --------------------------------------------------------------------------------------------------------------------------
// END SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO TELP PERUSAHAAN. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN
// --------------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA TEMPAT TANGGAL LAHIR. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 8 april 2015
// ---------------------------------------------------------------------------------------------------------------------
    $attr_ttl = array( 'name' => 'ttl_penanggung_jawab',
                            'label' => 'Tempat Tanggal Lahir',
                            'value' => kos($detail_bkp['ttl_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($attr_ttl);
// --------------------------------------------------------------------------------------------------------------------------
// END SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA TEMPAT TANGGAL LAHIR. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 8 april 2015
// --------------------------------------------------------------------------------------------------------------------------

    //SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO. KTP JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 19 Juni 2015
// ---------------------------------------------------------------------------------------------------------------------
    $attr_ktp = array( 'name' => 'no_identitas_penanggung_jawab',
                            'label' => 'No. KTP Penanggung Jawab',
                            'value' => kos($detail_bkp['no_identitas_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($attr_ktp);
// --------------------------------------------------------------------------------------------------------------------------
// END SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO. KTP JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 19 Juni 2015
// --------------------------------------------------------------------------------------------------------------------------

?>
        <div id="result" ></div>
	</div>
</div>

<?php

    if($aksi === 'view'){
        $link_aksi = '<a  class="btn btn-primary" href="'.base_url('kapal/main/entry/'.$detail_pendok['id_pendok']).'">'.$button.'</a>';
    }else{
        $link_aksi = '<button type="submit" class="btn btn-primary btn-submit" >SIMPAN</button>';
    }/*else{
        $link_aksi = '<a class="btn btn-primary" href="'.base_url('kapal/functions/update/'.$detail_pendok['id_pendok']).'">SIMPAN</a>';
    }*/
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group col-lg-6">
          </div>
          <div class="form-group col-lg-4">
              <div class="col-sm-offset-2 col-sm-4">
<?php
              if(isset($cetak)){
                $link_cetak = '<a class="btn btn-warning" href="'.base_url('kapal/cetak/preview/'.$detail_pendok['id_pendok']).'">CETAK DRAFT DATA KAPAL</a>';
              }
?>
              </div>
          </div>
          <div class="form-group col-lg-2">
              <div class="col-sm-offset-2 col-sm-4">

<?php 
              echo $link_aksi; 
?>

            </div>
        </div>

    </div>
</div>

</form>

<script>
  var is_aksi = "<?php echo $aksi ?>";
  var tipe_permohonan = "<?php echo $detail_pendok['tipe_permohonan']; ?>";
  var id_kapal = "<?php echo $detail_pendok['id_kapal']; ?>";
  var list_id_to_show = <?php if( isset($json_id_perubahan) ){ echo $json_id_perubahan; }else{ echo '[]'; } ?>;

  var cek_aksi = function(){
        if(is_aksi==='view'){
          $("input").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
          $(".col-sm-8").prepend(": ");
          $("select").prop("disabled", true);
          $("textarea").prop("disabled", true);

          $(".select2-choice").css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});    
          $(".select2-arrow").remove();

          //$('.select2-container').hide();
          // $("input").prop("disabled", true);
          // $("select").prop("disabled", true);
          $("textarea").prop("disabled", true);
        }else{

// -------------------------------------------------------------------------------------------------------
//aksi Javascript() JIKA PERUBAHAN ADALAH DATA INPUTAN DARI APLIKASI BARU (ID_KAPAL > 6415) MAKA PERUBAHAN DI MUNCULKAN REFERENSI SEBELUM PERUBAHAN
// -------------------------------------------------------------------------------------------------------
            if((tipe_permohonan === 'PERUBAHAN') && (id_kapal > 6415))
            {
                $('.form-group').hide();
                list_id_to_show.forEach(function(id){
                    if($("#"+id).parent().parent().show()){
                        $("#"+id).parent().parent().parent().show();
                    }
                    if(id=="id_panjang_loa_kapal"){
                        $('#id_path_foto').parent().parent().parent().show();
                    }

                });

            }
// -------------------------------------------------------------------------------------------------------
//aksi Javascript() JIKA PERUBAHAN ADALAH DATA INPUTAN DARI APLIKASI MAKA PERUBAHAN DI MUNCULKAN REFERENSI SEBELUM PERUBAHAN
// -------------------------------------------------------------------------------------------------------
            $('.btn-primary').parent().parent().show();
            
        }
    };

  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };

  s_func.push(submit_listener);
  s_func.push(cek_aksi);
</script>
