<?php
	//OLAH DATA TAMPIL
$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-bordered'>");
$this->table->set_template($template);
$this->table->set_heading($constants['th_table']);
$counter = 1;
if($list_kapal_pendok){
	foreach ($list_kapal_pendok as $item) {
		if($item->nama_kapal_bkp !== NULL)
		{
			$link_nama = '<a href="'.base_url('kapal/main/view/'.$item->id_pendok).'">'.$item->nama_kapal_bkp.'</a>';
		}else{
			$link_nama = '<a href="'.base_url('kapal/main/view/'.$item->id_pendok).'">'.$item->nama_kapal_pendok.'</a>';
		}
		$btn_blm_entry = '<a class="btn-small btn-warning" href="'.base_url('kapal/main/entry/'.$item->id_pendok).'">PROSES</a>';
		$btn_sdh_entry = '<a class="btn-small btn-primary" href="'.base_url('kapal/main/view/'.$item->id_pendok).'">VALID</a>';

		if(($item->status_entry_bkp === '1') || ($item->status_entry_bkp === '2')){
			$status_entry = $btn_sdh_entry;
		}else{
			$status_entry = $btn_blm_entry;
		}
			/*$btn_blm_ver = '<a class="btn btn-danger" href="'.base_url('kapal/functions/verifikasi/'.$item->id_pendok).'">BELUM VERIFIKASI</a>';
			$btn_sdh_ver = '<a class="btn btn-primary" href="'.base_url('kapal/main/view/'.$item->id_pendok).'">SUDAH VERIFIKASI</a>';

			if($item->status_verifikasi === "YA"){
				$btn_ver = $btn_sdh_ver;
			}else{
				$btn_ver = $btn_blm_ver;
			}*/

			if($item->kategori_pendaftaran === 'PUSAT'){
				$asal_lokasi = "";
			}elseif ($item->kategori_pendaftaran === 'PROPINSI') {
				$asal_lokasi = " | ".$item->propinsi_pendaftaran;
			}elseif ($item->kategori_pendaftaran === 'KAB/KOTA') {
				$asal_lokasi = " | ".$item->kabkota_pendaftaran;
			}

			$this->table->add_row(
				$counter.'.',
				noreg($item->no_rekam_pendok),
				noreg($item->no_register),
				$link_nama,
				$item->nama_perusahaan,
				$item->gt,
				$item->tipe_permohonan,
				$item->kategori_pendaftaran.$asal_lokasi,
				$status_entry
				);
			$counter++;
		}
	}

	$table_list_kapal_pendok = $this->table->generate();
	?>

<p></p>
<!-- TAMPIL DATA -->
<div id="loading_info" class="panel panel-default">
	<div class="panel-body">
	   <p class="text-center">
	   		Memuat Data Kapal Pendok. Harap Tunggu...
	   </p>
	</div>
</div>

<div id="table_container" class="panel-body overflowed hidden">
	<?php
		echo $table_list_kapal_pendok;
	?>
</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- ADTTIONAL JAVASCRIPT -->
	<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "iDisplayLength": 30,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
			}
		} );
	} );
	</script>
