<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MX_Controller {

	function __construct()
		{
			parent::__construct();
		}

	public function input()
	{
		$this->load->model('mdl_kapal');
		$this->load->model('buku_kapal/mdl_buku_kapal');
		$this->load->model('pendok/mdl_pendok');

		$array_input = $this->input->post(NULL, TRUE);

		// vdump($array_input, true);

		//explode untuk array input alat_tangkap
		$arr_alt_tgk = explode("|", $array_input['id_alat_tangkap']);

		/*if(isset($array_input['negara_pembangunan_dblama'])){
			$array_input['negara_pembangunan'] = $array_input['negara_pembangunan_dblama'];
			$array_input['propinsi_pembangunan'] = $array_input['negara_pembangunan_dblama'];
			$array_input['kabkota_pembangunan'] = $array_input['negara_pembangunan_dblama'];
			unset($array_input['negara_pembangunan_dblama']);
		}else{
			//explode untuk array input kabkota propinsi dan kabupaten kota
			$arr_info_lokasi = explode("|", $array_input['kabkota_prop']);

			//explode untuk array input negara_pembangunan
			$arr_neg_pem = explode("|", $array_input['neg_pem']);
		
			$array_input['id_propinsi_pembangunan'] = $arr_info_lokasi[0];
			$array_input['id_kabkota_pembangunan'] = $arr_info_lokasi[1];
			$array_input['propinsi_pembangunan'] = $arr_info_lokasi[2];
			$array_input['kabkota_pembangunan'] = $arr_info_lokasi[3];

			$array_input['id_negara_pembangunan'] = $arr_neg_pem[0];
			$array_input['negara_pembangunan'] = $arr_neg_pem[1];
		}*/

		$array_input['id_alat_tangkap'] = $arr_alt_tgk[0];
		$array_input['alat_tangkap'] = $arr_alt_tgk[1];

		//=============BEGIN- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$id_lokasi = $this->mksess->id_lokasi();
		
		$info_lokasi_pengguna = (array)$this->mdl_pendok->get_info_lokasi($id_propinsi_pengguna, $id_kabkota_pengguna);

		$array_input['id_propinsi_pendaftaran'] = $id_propinsi_pengguna;
		$array_input['id_kabkota_pendaftaran'] = $id_kabkota_pengguna;
		$array_input['propinsi_pendaftaran'] = $info_lokasi_pengguna['nama_propinsi'];
		$array_input['kabkota_pendaftaran'] = $info_lokasi_pengguna['nama_kabupaten_kota'];

		//=============END- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		switch ($id_lokasi) {
			case '2':
				$array_input['kategori_pendaftaran'] = 'PROPINSI';
				break;

			case '3':
				$array_input['kategori_pendaftaran'] = 'KAB/KOTA';
				break;

			default:
				$array_input['kategori_pendaftaran'] = 'PUSAT';
				break;
		}

		unset($array_input['kabkota_prop']);
		unset($array_input['neg_pem']);

		// vdump($array_input, true);

		$array_input['id_pengguna_buat'] = $this->mksess->id_pengguna();
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');
		$kondisi = false;

		$id_pendok = $array_input['id_pendok'];
		$id_kapal  = $array_input['id_kapal'];
		$no_register  = noreg($array_input['no_register']);
		unset($array_input['no_register']);

	// Proses untuk GENERATE NO TANDA PENGENAL
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 3);
		if($get_tipe_perubahan){

			/*tipe perubahan tanda pengenal nya jadi manual saja,
			 di form entry kapal akan muncul field input tanda pengenal*/

		}else{


			$jenis_kapal_id = $array_input['id_jenis_kapal'];
			$alat_tangkap_id = $array_input['id_alat_tangkap'];
			$jns_kapal = (array)$this->mdl_kapal->get_kode_jns_kapal($jenis_kapal_id);		
			$alt_tangkap = (array)$this->mdl_kapal->get_kode_alat_tangkap($alat_tangkap_id);

			// if($array_input['tempat_pendaftaran_id'] === "Pusat"){
			// 	$kode_lokasi_pendaftaran = "A";
			// }else if($array_input['tempat_pendaftaran_id'] === "Provinsi"){
			// 	$kode_lokasi_pendaftaran = "B";
			// }else{
			// 	$kode_lokasi_pendaftaran = "C";
			// }

			$id_propinsi = $this->mksess->id_propinsi();
			$id_kabupaten_kota = $this->mksess->id_kabupaten_kota();

			$kod_prop = $this->mdl_kapal->get_kode_prop($id_propinsi);
			$kod_kab = $this->mdl_kapal->get_kode_kabkota($id_kabupaten_kota);

			// vdump($kod_prop->kode, true);

			$id_lokasi = $this->mksess->id_lokasi();

			switch ($id_lokasi) {
				case '1':
					$kode_lokasi_pendaftaran = "A";
					break;

				case '2':
					$kode_lokasi_pendaftaran = "B-".$kod_prop->kode;
					break;

				case '3':
					$kode_lokasi_pendaftaran = "C-".$kod_kab->kode.".".$kod_prop->kode;
					break;

				default:
					$kode_lokasi_pendaftaran = "A";
					break;
			}

			/*if($array_input['kategori_pendaftaran'] === "PUSAT"){
				$kode_lokasi_pendaftaran = "A";
			}else if($array_input['kategori_pendaftaran'] === "PROPINSI"){
				$kode_lokasi_pendaftaran = "B-".$kod_prop;
			}else if($array_input['kategori_pendaftaran'] === "KAB/KOTA"){
				$kode_lokasi_pendaftaran = "C-".$kod_kab.".".$kod_prop;
			}else{
				$kode_lokasi_pendaftaran = "A";
			}*/

			$temp_nama_tangkap = $alt_tangkap['nama_alat_tangkap'];
			if(preg_match('/ dua /', $temp_nama_tangkap)){
				$armada = true;
			}else if(preg_match('/ grup /', $temp_nama_tangkap)){
				$armada = true;
			}else{
				$armada = false;
			}

			//jika termasuk satuan armada, Menggunakan A-
			// if($alat_tangkap_id === "5" || $alat_tangkap_id === "18" || $alat_tangkap_id === "22" || $alat_tangkap_id === "76" || $alat_tangkap_id === "77" ){
			if($armada){
				//jika kapal pengangkut tidak pakai WPP-NRI
				if($jns_kapal['kode_penamaan'] === 'KA' || $jns_kapal['kode_penamaan'] === 'KL'){
					$array_input['no_tanda_pengenal'] = $kode_lokasi_pendaftaran."/"."A-".$jns_kapal['kode_penamaan']."-".$alt_tangkap['kode_penandaan']."/".$no_register ;
				}else{
					$array_input['no_tanda_pengenal'] = $kode_lokasi_pendaftaran."/"."WPP-NRI"."/"."A-".$jns_kapal['kode_penamaan']."-".$alt_tangkap['kode_penandaan']."/".$no_register ;
				}
			}else{
				//jika kapal pengangkut tidak pakai WPP-NRI
				if($jns_kapal['kode_penamaan'] === 'KA' || $jns_kapal['kode_penamaan'] === 'KL'){
					$array_input['no_tanda_pengenal'] = $kode_lokasi_pendaftaran."/".$jns_kapal['kode_penamaan']."-".$alt_tangkap['kode_penandaan']."/".$no_register ;
				}else{
					$array_input['no_tanda_pengenal'] = $kode_lokasi_pendaftaran."/"."WPP-NRI"."/".$jns_kapal['kode_penamaan']."-".$alt_tangkap['kode_penandaan']."/".$no_register ;
				}
					
			}

		}
	//END PROSES GENERATE

		//wadah untuk array_input['nama_kapal']
		$nama_kapal_edit = $array_input['nama_kapal'];
		$nama_kapal_sblm_edit = $array_input['nama_kapal_sblm'];

		if($this->mdl_kapal->get_detail($id_pendok))
		{
			/*
			bagian logik jika ada perubahan nama_kapal di form_kapal oleh petugas entry
			yang di update adalah ketiga tabel yaitu mst_kapal, trs_bkp, dan trs_pendok
			berdasarkan id_pendok nya.
			*/
			$is_admin = $this->mksess->info_is_admin();
			if($is_admin){
				$data_update_mst_kapal = array(
									'nama_kapal_terbaru' => $nama_kapal_edit
									);
				$data_update_trs_bkp = array(
									'nama_kapal' => $nama_kapal_edit,
									'nama_kapal_sblm' => $nama_kapal_sblm_edit
									);
				$data_update_trs_pendok = array(
									'nama_kapal' => $nama_kapal_edit
									);

				$this->mdl_kapal->update_nama_kapal($id_pendok, $data_update_mst_kapal, $data_update_trs_pendok, $data_update_trs_bkp);
			}

			//update ke trs_bkp..
			$id_bkp_terakhir = $array_input['id_bkp'];
			$id_bkp = $array_input['id_bkp'];
			
			if(isset($array_input['nama_kapal_baru'])){
				//jika permohonan PERUBAHAN dan melakukan UPDATE
				$nama_kapal_terbaru = $array_input['nama_kapal_baru'];
				$array_input['nama_kapal'] = $array_input['nama_kapal_baru'];
				
			}else{
				//jika permohonan BARU dan melakukan Update
				$nama_kapal_terbaru = $array_input['nama_kapal'];
				// unset($array_input['nama_kapal_sblm']);
			}

			unset($array_input['id_bkp']);
			unset($array_input['form_kapal_baru']);
			unset($array_input['nama_kapal_baru']);

			if($this->mdl_kapal->update($id_pendok,$array_input)){

				/*Update ke mst_kapal*/
				$data_update = array(
								'nama_kapal_terbaru' => $nama_kapal_terbaru
							);

				if($this->mdl_kapal->update_kapal($id_kapal, $data_update) ){

					$this->mdl_kapal->set_status_entry($id_pendok);

					$url = base_url('kapal/main/view/'.$id_pendok);

					$kondisi = true;

				}else{

					$url = base_url('kapal/main/entry/'.$id_pendok);
					
				}
			}

		}else{
			//insert ke trs_bkp
			// vdump($array_input, true);

			$id_bkp_terakhir    = $array_input['id_bkp'];
			
			if(isset($array_input['nama_kapal_baru'])){
				$nama_kapal_terbaru = $array_input['nama_kapal_baru'];
				$array_input['nama_kapal'] = $array_input['nama_kapal_baru'];
			}else{
				$nama_kapal_terbaru = $array_input['nama_kapal'];
			}

			unset($array_input['id_bkp']);
			unset($array_input['form_kapal_baru']);
			unset($array_input['nama_kapal_baru']);

			if($id_bkp = $this->mdl_kapal->input($array_input)){

				/*Update ke mst_kapal*/
				$data_update = array(

								'id_pendok_terakhir'=> $id_pendok,

								'id_bkp_terakhir'	=> $id_bkp,

								'nama_kapal_terbaru'=> $nama_kapal_terbaru

							);

				if($this->mdl_kapal->update_kapal($id_kapal, $data_update)){

					$this->mdl_kapal->set_status_entry($id_pendok);
					$url = base_url('kapal/main/view/'.$id_pendok);
					$kondisi = true;

				}else{

					$url = base_url('kapal/main/entry/'.$id_pendok);

				}

			}else{

				$url = base_url('kapal/main/entry/'.$id_pendok);

			}

		}




		//config upload
		$config['overwrite']     = TRUE;
		$config['allowed_types'] = 'gif|jpeg|jpg|png';
		$config['file_name']	 = $id_kapal."-".date("Y-m-d");
		$this->load->library('upload', $config);

		//config upload
		$config['upload_path']   = 'uploads/file_gross/';

		$this->upload->initialize($config);
		if($this->upload->do_upload("path_file_gross")){
			$upload_gross = $this->upload->data();
			$array_input['path_file_gross'] = $upload_gross['file_name'];
		}

		$config['upload_path']   = 'uploads/foto_kapal/';

		$this->upload->initialize($config);
		
		$posisi	 = 0;
		while($kondisi)
		{
			if($posisi == 0){
				
				$form_upload = "path_foto";
				$config['file_name'] = $id_kapal."-".date("Y-m-d");

			}else{

				$temp = $posisi;
				$posisi -= 1;
				$form_upload = "path_foto_".$posisi;
				$config['file_name'] = $id_kapal."-".date("Y-m-d")."-".$posisi;
				$posisi = $temp;
			}

			$this->upload->initialize($config);

			if($this->upload->do_upload($form_upload))
			{
				//do_upload_foto
				$upload_foto = $this->upload->data();
				$data['path_foto'] = $upload_foto['file_name'];

				$data['id_bkp'] 	= $id_bkp;
				$data['id_kapal'] 	= $id_kapal;
				$this->mdl_kapal->input_gambar($data);
				$this->thumb_generate($data['path_foto']);
				//$array_input['path_foto'] = $config['upload_path'].$upload_foto['file_name'];
			}
			else{
				$kondisi = false;
			}
			$posisi++;
		}

		redirect($url);

		/*
		if( $this->mdl_kapal->input($array_input) ){

			$this->mdl_kapal->set_status_entry($id_pendok);
			$this->mdl_kapal->update_kapal($array_input['id_kapal'], $data);
			$url = base_url('buku_kapal/main/entry/'.$id_pendok);
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
		*/
	}

	/*
	public function update()
	{
		$this->load->model('mdl_kapal');

		$array_input = $this->input->post(NULL, TRUE);
		$id_pendok = $array_input['id_pendok'];

		//vdump($array_input,true);

		if( $this->mdl_kapal->update($id_pendok, $array_input) ){
			$this->mdl_kapal->set_status_entry($id_pendok);
			$url = base_url('kapal/main/view/'.$id_pendok);
			redirect($url);
		}else{
			$url = base_url('kapal/main/view/'.$id_pendok);
			redirect($url);
		}

	}
	*/


	public function edit_perusahaan()
	{
		$this->load->model('mdl_kapal');

		$array_input = $this->input->post(NULL, TRUE);
		$id_pendok = $array_input['id_pendok'];

		// vdump($array_input, true);

		if( $this->mdl_kapal->update_perusahaan($id_pendok, $array_input) ){
			$this->mdl_kapal->set_status_entry($id_pendok);
			$url = base_url('kapal/main/view/'.$id_pendok);
			redirect($url);
		}else{
			$url = base_url('kapal/main/view/'.$id_pendok);
			redirect($url);
		}

	}
	

	public function verifikasi($id_pendok)
	{
		$this->load->model('mdl_kapal');

		if( $this->mdl_kapal->set_verifikasi($id_pendok) ){
			$url = base_url('buku_kapal/main/views');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

	public function status_entry($id_pendok)
	{
		$this->load->model('mdl_kapal');

		if( $this->mdl_kapal->set_status_entry($id_pendok) ){
			$url = base_url('kapal/main/views');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

	public function thumb_generate($foto_name){
		$config['image_library'] = 'gd2';
		$config['source_image']	= 'uploads/foto_kapal/'.$foto_name;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= 200;
		$config['height']	= 175;

		$this->load->library('image_lib', $config); 

		if($this->image_lib->resize()){
			$data['success'] = TRUE;
			// echo "sukses generate thumbnail";
		}else{
			$data['success'] = FALSE;
		}
	}

}
