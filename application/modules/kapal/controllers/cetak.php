<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends MX_Controller {


	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_kapal');
			$this->load->config('globals');
			$this->load->config('labels');
		}

	public function index($id_pendok, $is_final = '')
	{	
		if($is_final === 'final')
		{
			$this->mdl_kapal->set_final($id_pendok);
		}
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('form_kapal_label');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_cetak_kapal = $this->mdl_kapal->get_detail_cetak($id_pendok);
		$data['detail_kapal'] = (array) $get_detail_cetak_kapal[0];
		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_draft_kapal', $data, TRUE);
		//$dokumen = '<p>Hallo</p>';
		// var_dump($css);
		// var_dump($dokumen);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);
		$this->mpdf->SetHeader('Draft Buku Kapal | |'.$data['detail_kapal']['nama_kapal'].'	');
		$this->mpdf->SetFooter('Direktorat Kapal Perikanan dan Alat Penangkapan Ikan ||'.$timestamp);
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('Tanda Terima Permohonan Dokumen.pdf','I');

	}

	public function preview_before($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('form_kapal_label');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');

		$get_detail_cetak_kapal = $this->mdl_kapal->get_detail_cetak($id_pendok);
		$data['id_pendok'] = $id_pendok;
		$data['detail_kapal'] = (array) $get_detail_cetak_kapal[0];
		$data['is_preview'] = TRUE;
		$data['forms'] = $forms['form'];
		$data['css'] = $css;
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$this->load->view('print_draft_kapal', $data);
	}

	public function preview($id_pendok)
	{	
		$get_detail_kapal = $this->mdl_kapal->get_detail($id_pendok);
		$data['detail_kapal'] = (array) $get_detail_kapal[0];

		// $data['edit_link'] = base_url('pendok/main/edit/index');
		// $data['cetak_link'] = base_url('pendok/cetak/index');
		$data['id_pendok'] = $id_pendok;

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'kapal', //nama module
							'preview_print', //nama file view
							'label_print_draft', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

}
?>