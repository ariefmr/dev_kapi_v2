<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_kapal');
			$this->load->model('pendok/mdl_pendok');
			$this->load->model('buku_kapal/mdl_buku_kapal');
		}

	public function entry($id_pendok)
	{	

		/*
			-jika dia baru maka form yang akan tampil adalah semua
			-jika dia perubahan maka form yang akan tampil merupakan form yang sudah dichecklist
		*/

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');

		$data['limit_gt'] = 0;
		$data['min_gt'] = 0;

		$kategori_pengguna = $this->mksess->kategori_pengguna();
		switch ($kategori_pengguna) {
			case 'PUSAT':
				$data['limit_gt'] = 99999;
				$data['min_gt'] = 30;
				break;

			case 'PROPINSI':
				$data['limit_gt'] = 30;
				$data['min_gt'] = 10;
				break;

			case 'KAB/KOTA':
				$data['limit_gt'] = 10;
				$data['min_gt'] = 5;
				break;
		}

		//inisialisasi untuk menentukan tipe_permohonan PERUBAHAN atau BARU
		$array_perubahan	= (array) $this->mdl_pendok->get_permohonan($id_pendok);

		$get_detail_pendok	   = $this->mdl_pendok->get_detail($id_pendok); //untuk inisialisasi entry data kapal
		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		$get_detail_kapal 		= $this->mdl_kapal->get_mst_kapal_detail($data['detail_pendok']['id_kapal']);
		$data['detail_kapal'] 	= (array) $get_detail_kapal[0];
		
		//detail kapal bkp
		$get_detail_bkp 	= $this->mdl_kapal->get_trs_bkp_detail($data['detail_kapal']['id_bkp_terakhir']);
		$data['detail_bkp'] = (array) $get_detail_bkp[0];

		//detail buku kapal
		$get_detail_buku 	= $this->mdl_buku_kapal->detail_cetak_buku($data['detail_kapal']['id_pendok_terakhir']);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		if($array_perubahan['tipe_permohonan'] == "PERUBAHAN")
		{

			$get_detail_perubahan = $this->mdl_kapal->get_trs_perubahan($id_pendok);
			$detail_perubahan = array();
				foreach ($get_detail_perubahan as $key => $value) {
					$tmp_value = str_replace('trs_bkp.', '', $value['kolom_db']);
						array_push($detail_perubahan, $tmp_value);
				}

			$tmp_detail_perubahan = implode(",", $detail_perubahan);
			$detail_perubahan = explode(",", $tmp_detail_perubahan);

			$list_detail_perubahan = array();
			foreach ($detail_perubahan as $index => $n_value) {
				if(strrpos($n_value, 'id') === FALSE)
				{
					$n_value = 'id_'.$n_value;
				}
				array_push($list_detail_perubahan, $n_value);
			}
			$data['json_id_perubahan'] = json_encode($list_detail_perubahan);

		}

		$data['aksi'] = 'entry';

		$data['perubahan'] = true;

		$data['submit_form'] = 'kapal/functions/input';

		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$file_views = 'form_kapal';
		$labels = 'form_kapal_label';

		echo Modules::run(	$template, //tipe template
							$modules, //nama module
							$file_views, //nama file view
							$labels, //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function views($page = 1)
	{
		
		if($page > 0)
		{
			$up_to = $page * 100;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*100;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$file_views = 'tabel_kapal';
		$labels = 'label_view_kapal_pendok';

		// $data['list_kapal_pendok'] = $this->mdl_kapal->list_kapal_pendok($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna,$down_to,$up_to);
		$data['list_kapal_pendok'] = $this->mdl_kapal->list_kapal_pendok($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
		// vdump($data['list_kapal_pendok'], true);
		echo Modules::run($template, 
							$modules, 
							$file_views, 
							$labels, 
							$add_js, 
							$add_css, 
							$data);

	}

	public function view($id_pendok)
	{	
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');

		$data['limit_gt'] = 0;
		$data['min_gt'] = 0;

		$kategori_pengguna = $this->mksess->kategori_pengguna();
		switch ($kategori_pengguna) {
			case 'PUSAT':
				$data['limit_gt'] = 99999;
				$data['min_gt'] = 31;
				break;

			case 'PROPINSI':
				$data['limit_gt'] = 30;
				$data['min_gt'] = 11;
				break;

			case 'KAB/KOTA':
				$data['limit_gt'] = 10;
				$data['min_gt'] = 5;
				break;
		}

		$array_perubahan = (array) $this->mdl_pendok->get_permohonan($id_pendok);

		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok); //untuk inisialisasi entry data kapal

		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		$get_detail_kapal = $this->mdl_kapal->get_mst_kapal_detail($data['detail_pendok']['id_kapal']);
		$data['detail_kapal'] = (array) $get_detail_kapal[0];

		$get_detail_bkp = $this->mdl_kapal->get_trs_bkp_detail($data['detail_kapal']['id_bkp_terakhir']);
		$data['detail_bkp'] = (array) $get_detail_bkp[0];

		$get_foto_kapal		= $this->mdl_kapal->get_foto_kapal($data['detail_kapal']['id_bkp_terakhir']);
		$data['foto_kapal'] = (array) $get_foto_kapal;

		//detail buku kapal
		$get_detail_buku 	= $this->mdl_buku_kapal->detail_cetak_buku($data['detail_kapal']['id_pendok_terakhir']);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		if($array_perubahan['tipe_permohonan'] == "PERUBAHAN")
		{

			$data['get_detail_perubahan'] = (array) $this->mdl_kapal->get_trs_perubahan($id_pendok);

		}

		//vdump($data,true);
		

		$data['aksi'] = 'view';
		$data['perubahan'] = false;
		
		if(!$get_detail_kapal){
			$data['button'] = 'entry';
		}else{
			$data['button'] = 'edit';
			$data['cetak'] = TRUE;
		}

		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$file_views = 'form_kapal';
		$labels = 'form_kapal_label';

		echo Modules::run(	$template, //tipe template
							$modules, //nama module
							$file_views, //nama file view
							$labels, //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function view_for_compare($id_pendok)
	{	
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');

		$data['limit_gt'] = 0;
		$data['min_gt'] = 0;

		$kategori_pengguna = $this->mksess->kategori_pengguna();
		switch ($kategori_pengguna) {
			case 'PUSAT':
				$data['limit_gt'] = 99999;
				$data['min_gt'] = 31;
				break;

			case 'PROPINSI':
				$data['limit_gt'] = 30;
				$data['min_gt'] = 11;
				break;

			case 'KAB/KOTA':
				$data['limit_gt'] = 10;
				$data['min_gt'] = 5;
				break;
		}

		$array_perubahan = (array) $this->mdl_pendok->get_permohonan($id_pendok);

		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok); //untuk inisialisasi entry data kapal

		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		$get_detail_kapal = $this->mdl_kapal->get_mst_kapal_detail($data['detail_pendok']['id_kapal']);
		$data['detail_kapal'] = (array) $get_detail_kapal[0];

		$get_detail_bkp = $this->mdl_kapal->get_trs_bkp_detail($data['detail_kapal']['id_bkp_terakhir']);
		$data['detail_bkp'] = (array) $get_detail_bkp[0];

		$get_foto_kapal		= $this->mdl_kapal->get_foto_kapal($data['detail_kapal']['id_bkp_terakhir']);
		$data['foto_kapal'] = (array) $get_foto_kapal;

		//detail buku kapal
		$get_detail_buku 	= $this->mdl_buku_kapal->detail_cetak_buku($data['detail_kapal']['id_pendok_terakhir']);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		if($array_perubahan['tipe_permohonan'] == "PERUBAHAN")
		{

			$data['get_detail_perubahan'] = (array) $this->mdl_kapal->get_trs_perubahan($id_pendok);

		}

		//vdump($data,true);
		

		$data['aksi'] = 'view';
		$data['perubahan'] = false;
		
		if(!$get_detail_kapal){
			$data['button'] = 'entry';
		}else{
			$data['button'] = 'edit';
			$data['cetak'] = TRUE;
		}

		$template = 'templates/page/v_form_c';
		$modules = 'kapal';
		$file_views = 'form_kapal_c';
		$labels = 'form_kapal_label';

		echo Modules::run(	$template, //tipe template
							$modules, //nama module
							$file_views, //nama file view
							$labels, //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function edit_perusahaan($id_pendok)
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'jquery.dataTables.css', 'validationEngine.jquery.css');

		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$file_views = 'form_edit_perusahaan';
		$labels = 'form_edit_perusahaan_label';

		$data['submit_form'] = 'kapal/functions/edit_perusahaan';

		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok); //untuk inisialisasi entry data kapal

		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		$get_detail_kapal = $this->mdl_kapal->get_mst_kapal_detail($data['detail_pendok']['id_kapal']);
		$data['detail_kapal'] = (array) $get_detail_kapal[0];

		$get_detail_bkp = $this->mdl_kapal->get_trs_bkp_detail($data['detail_kapal']['id_bkp_terakhir']);
		$data['detail_bkp'] = (array) $get_detail_bkp[0];

		$get_foto_kapal		= $this->mdl_kapal->get_foto_kapal($data['detail_kapal']['id_bkp_terakhir']);
		$data['foto_kapal'] = (array) $get_foto_kapal;

		//detail buku kapal
		$get_detail_buku 	= $this->mdl_buku_kapal->detail_cetak_buku($data['detail_kapal']['id_pendok_terakhir']);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		echo Modules::run($template, 
							$modules, 
							$file_views, 
							$labels, 
							$add_js, 
							$add_css, 
							$data);
		
	}

	public function get_foto_data()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'jquery.dataTables.css', 'validationEngine.jquery.css');

		$template = 'templates/page/v_form';
		$modules = 'kapal';
		$file_views = 'form_edit_perusahaan';
		$labels = 'form_edit_perusahaan_label';

		$get_trs_foto		= $this->mdl_kapal->get_trs_foto();
		$data['foto_kapal'] = (array) $get_trs_foto;

		vdump($data);

		/*echo Modules::run($template, 
							$modules, 
							$file_views, 
							$labels, 
							$add_js, 
							$add_css, 
							$data);*/
		
	}

}
