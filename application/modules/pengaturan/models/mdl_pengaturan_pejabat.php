<?php 

class Mdl_pengaturan_pejabat extends CI_Model
{
	function __construct()
    {
        $this->load->database();
    }

    function list_pejabat()
    {
    	$query 		= "
    				   SELECT *
    				   FROM   db_pendaftaran_kapal.mst_pejabat
    				  ";
    	$run_query 	= $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    function input($data)
    {
        $this->db->insert('mst_pejabat', $data);
    }

    function detil_pejabat($id_pejabat)
    {
    	$sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_pejabat
                    WHERE mst_pejabat.id_pejabat = $id_pejabat ";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($data)
    {
        $this->db->where('id_pejabat', $data['id_pejabat']);
        $query = $this->db->update('mst_pejabat',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {
        $this->db->where('id_pejabat',$id);
        $query = $this->db->delete('mst_pejabat');

        if($query)
        {
            $result = true;
        }
        else{
            $result = false;
        }
        return $result;
    }

}

?>