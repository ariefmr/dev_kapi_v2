<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {

	function __construct()
		{
			parent::__construct();

			$this->load->model('mdl_pengaturan_pejabat');
		}

	public function index()
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'tabel_pengaturan_pejabat';
		$labels = 'label_view_pengaturan_pejabat';

		$data['list_pejabat'] = $this->mdl_pengaturan_pejabat->list_pejabat();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function entry()
	{
		$data['submit_form'] = 'pengaturan/main/input';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_pengaturan_pejabat';
		$labels = 'form_pengaturan_pejabat';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function input()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_input['nip'] = $array_input['nip_real'];

		unset($array_input['nip_real']);

		print_r($array_input);

		if( $this->mdl_pengaturan_pejabat->input($array_input) ){
			$url = base_url('pengaturan/main');
			redirect($url);
		}else{
			$url = base_url('pengaturan/main');
			redirect($url);
		}
	}

	public function edit($id_record)
	{

		$get_detail = $this->mdl_pengaturan_pejabat->detil_pejabat($id_record);

		if( !$get_detail )
		{
			$data['detail_pejabat'] = 'Data tidak ditemukan';
		}else{
			$data['detail_pejabat'] = (array)$get_detail;
			$data['detail_pejabat']['nip_fake'] = $data['detail_pejabat']['nip'];
			$data['detail_pejabat']['nip_real'] = $data['detail_pejabat']['nip'];
		}

		$data['submit_form'] = 'pengaturan/main/update';
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'pengaturan';
		$views = 'form_pengaturan_pejabat';
		$labels = 'form_pengaturan_pejabat';
		 
		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function update()
	{
		$array_input = $this->input->post(NULL, TRUE);

		$array_to_edit = $array_input;
		$array_to_edit['nip'] = $array_to_edit['nip_real'];
		unset($array_to_edit['nip_real']);
		//print_r($array_to_edit);
		
		if( $this->mdl_pengaturan_pejabat->update($array_to_edit) ){
			$url = base_url('pengaturan/main');
			redirect($url);
		}else{
			$url = base_url('pengaturan/edit/');
			redirect($url);
		}
	}

	public function delete($id)
    {
        $this->mdl_pengaturan_pejabat->delete($id);

        $url = base_url('pengaturan/main');
        redirect($url);
    }

}
?>