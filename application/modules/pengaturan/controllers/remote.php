<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remote extends MX_Controller {

	function __construct()
		{
			parent::__construct();

			$this->load->model('mdl_remote_pejabat');
		}

	public function input_form()
	{

		$this->load->view('remote_mst_pejabat');
	}

	public function search_pejabat(){

		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_remote_pejabat->search_by_q($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);

	}

}