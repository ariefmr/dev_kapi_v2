	<?php
	/*OLAH DATA TAMPIL*/
	$template = array( "table_open" => "<table id='table_pejabat' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	$link_entry = '<a class="btn btn-primary" href="'.base_url('pengaturan/main/entry/input').'">Entry Pejabat</a>';

	if($list_pejabat){
		foreach ($list_pejabat as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('pengaturan/main/edit/'.$item->id_pejabat).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('pengaturan/main/delete/'.$item->id_pejabat).'">Hapus</a>';

			if($item->jabatan === "1"){
				$jabatan = "Direktur";
			}elseif($item->jabatan === "2")
			{
				
				$jabatan = "Kepala Bagian";

			}elseif ($item->jabatan === "3") {
				
				$jabatan = "Kepala Seksi";

			}

			$this->table->add_row(
								$counter.'.',
								$item->nip,
								$item->nama_pejabat,
								$jabatan,
								$link_edit."<br/>".$link_delete
								);
			$counter++;
		}
	}

	$table_list_dokumen = $this->table->generate();
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $table_list_dokumen;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_pejabat').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>