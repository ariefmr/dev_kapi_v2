<?php

  if(!isset($detail_pejabat['nama_pejabat']))
  {
   $detail_pejabat['nama_pejabat'] = "" ;
  }
  if(!isset($detail_pejabat['id_pengguna']))
  {
   $detail_pejabat['id_pengguna'] = "" ;
  }

?>
<!-- TAMPIL DATA -->
    <div id="panel-pilih-pejabat" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row" style="padding-bottom: 15px;">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Nama Pejabat</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <input id="start_search" name="nama_pejabat" data-init-text="<?php echo $detail_pejabat['nama_pejabat']; ?>" value="<?php echo $detail_pejabat['nama_pejabat']; ?>" type="text" class="bigdrop">
               <input  id="id_pengguna" name="id_pengguna" data-init-text="<?php echo $detail_pejabat['nama_pejabat']; ?>" value="<?php echo $detail_pejabat['nama_pejabat']; ?>"  type="hidden" class="form-control">
            </div>
        </div>

  </div>


<!-- ADDITIONAL JAVASCRIPT -->
<script>

  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_pejabat = "<?php echo base_url('pengaturan/remote/search_pejabat'); ?>";

  function formatListpejabatResult(pejabat)
  {
    html = "<table id='"+pejabat.id_pengguna+"' class='table table-condensed table-bordered'><tr>"
       + "<th>NIP</th>"
       + "<th>Nama Pejabat</th>"
       + "<th>Nama Jabatan</th></tr><tr>"
       + "<td>"+pejabat.nip+"</td>"
       + "<td>"+pejabat.nama_pengguna+"</td>"
       + "<td>"+pejabat.nama_jabatan+"</td>"
       + "</tr></table>"
    return  html;

  }

  function set_data_pejabat(pejabat)
  {
    console.dir(pejabat);

    <?php if(!isset($detail_pejabat['nip_fake'])||!isset($detail_pejabat['nip_real'])): ?>
    $("#inputNamaPejabat").val(pejabat.nama_pengguna);
    $("#id_pengguna").val(pejabat.id_pengguna);
    $("#id_nip_real").val(pejabat.nip);
    $("#id_nip_fake").val(pejabat.nip);
    <?php endif; ?>
    //set_history(JSON.stringify(kapal));
    /*
    $("#area_dispaly_data").css({"display":"block"});
    $("#inputId_kapal").val(kapal.id_kapal);
    $("#inputNama_kapal").val(kapal.nama_kapal);
    $("#inputId_pendok_terakhir").val(kapal.id_pendok);
    $("#viewNama_kapal").text(kapal.nama_kapal).append("<input align='right' type='checkbox' />");
    $("#viewNama_perusahaan").text(kapal.nama_perusahaan);
    $("#viewNo_register").text(kapal.no_register);
    $("#viewNo_seri_bkp").text(kapal.no_seri_bkp);
    $("#viewNo_tanda_pengenal").text(kapal.no_tanda_pengenal);
    $("#viewGt").text(kapal.gt_kapal);
    $("#viewNt").text(kapal.nt_kapal);
    $("#viewPanjang").text(kapal.panjang_kapal + " Meter");
    $("#viewLebar").text(kapal.lebar_kapal + " Meter");
    $("#viewDalam").text(kapal.dalam_kapal + " Meter");
    $("#viewLoa").text(kapal.panjang_loa_kapal + " Meter");
    */
  }

  function formatListpejabatSelection(pejabat)
  {
    set_data_pejabat(pejabat);
    return pejabat.nama_pengguna;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
      info_filter = $("#info_filter").text();
      info_gt = $("#toggle_gt").text();
      text_info = "Pencarian pejabat "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/kapal/add/" ;
    //     }
     
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. Pejabat Belum Terdaftar?";
  }


  function searchURLGenerator()
  {

  }

  function formatinisial(element,callback)
  {
      var elementText = $(element).attr('data-init-text');
      callback({"nama_pengguna":elementText});
      return elementText;
  }

  $(document).ready( function () {
    
    // $("#toggle_gt").click(function(){
    //   var currentGt = $(this).data("currentGt");
    //     if(currentGt === "below")
    //     {
    //       $(this).text("> 30 GT");
    //       $(this).data("currentGt","above");
          
    //     }else
    //     {
    //       $(this).html("&#8804 30 GT");
    //       $(this).data("currentGt","below");      
    //     }
    //     $("#start_search").select2("open");
    // });

    $("#start_search").select2({
                  id: function(e) { return e.nama_pengguna },
                  allowClear: false,    
                  placeholder: "Pilih pejabat..",
                  width: "50%",
                  cache: true,
                  minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
                          url: url_search_pejabat,
                          dataType: "json",
                          quietMillis: 2000,
                          data: function(term, page){
                                         return {
                                  types: ["pejabat"],
                                  limit: -1,
                                  q: term
                                  // i: 'data',
                                  // filter: current_filter,
                                  // limit: 100 // TODO : tentuin limit result
                                     };
                      },
                      results: function(data, page){
                                   return {results: data.result};
                          }
                  },
                                    formatResult: formatListpejabatResult,
                                    formatSelection: formatListpejabatSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound,
                                    initSelection: formatinisial
                                    });

    $("#start_search").on("change",function(e) { 
                    //console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
                      // get_detail_kapal(e.val, current_gt());
                      // var nama_kapal_chosen = $("span.select2-chosen").text();
                      // $("#inputNama_kapal").val(nama_kapal_chosen);
                    });
    /*

     ajax: {
                                          url: "<?php echo base_url('mst_kapal/json_kapal'); ?>",
                                          dataType: "jsonp",
                                          data: function(term, page){
                                            return {
                                              q: "term",
                                              limit: 100 // TODO : tentuin limit result
                                            };
                                          },
                                          results: function(data, page){
                                            return {results: data}
                                          }
    $('#start_search').keyup(function(e){
      clearTimeout(thread);
      
      var keyword = $(this).val();

      thread = setTimeout(function(){
        update_result_kapal(keyword);
      } ,search_response_time);
    });
    */  
  } );
  
  function auto_width(){
      //jQuery.fn.exists = function(){return this.length>0;}
      if($("#start_search").val().length < 30  )
      {
         $("#start_search").css({"width":"32%"});
      }
      else{
         var panjang = $("#start_search").val().length;
         $("#start_search").css({"width":(panjang/3)+32+"%"}); 
      }
    }

    s_func.push(auto_width);
</script>