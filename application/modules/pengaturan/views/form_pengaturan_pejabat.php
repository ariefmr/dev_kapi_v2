<?php
	echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"');
?>
<div class="row">
  <div class="col-lg-12">
  	<?php
  		echo Modules::run('pengaturan/remote/input_form');

  		/*nama pejabat
  		$attr_nama_pejabat	=	array( 
  										'name'	=> $form['nama_pejabat']['name'],
                                        'label'	=> $form['nama_pejabat']['label'],
                                        'value'	=> kos($detail_pendok['nama_pejabat'])
                    			);
       	echo $this->mkform->input_text($attr_nama_pejabat);*/
        /*echo "<pre>";
        print_r($detail_pejabat);
        echo "</pre>";*/
        // echo kos($detail_pejabat['nip_fake']);
       	/*nip pejabat*/
  		$attr_nip_pejabat	=	array( 
  										'name'		=> $form['nip_real']['name'],
                                        'label'		=> $form['nip_real']['label'],
                                        'value'		=> kos($detail_pejabat['nip_real']),
                                        'disabled'=> false
                    			);
       	echo $this->mkform->input_text($attr_nip_pejabat);

       	/*nip pejabat*/
  		$attr_hidden_nip	=	array( 
  										'name'		=> $form['nip_real']['name'],
                                        'label'		=> $form['nip_real']['label'],
                                        'value'		=> kos($detail_pejabat['nip_real'])
                    			);
       	// echo $this->mkform->input_hidden($attr_hidden_nip);

      $attr_hidden_id = array( 
                                        'name'    =>'id_pejabat',
                                        'label'   => 'id_pejabat',
                                        'value'   => kos($detail_pejabat['id_pejabat'])
                          );
        echo $this->mkform->input_hidden($attr_hidden_id);

       	/*jabatan pejabat*/
  		$attr_jabatan	=	array( 
  										'name'		=> $form['jabatan']['name'],
                                        'label'		=> $form['jabatan']['label'],
	                                    'opsi'		=> array('1' => 'Direktur', 
					                            			 '2' => 'Kepala Bagian', 
					                            			 '3' => 'Kepala Seksi',),
                                      'value'   => kos($detail_pejabat['jabatan'])
                    			);
       	echo $this->mkform->input_select($attr_jabatan);

  	?>
  		
  </div>
</div>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>