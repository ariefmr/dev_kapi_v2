<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MX_Controller {

	function __construct()
		{
			parent::__construct();

		}

	public function input()
	{
		$this->load->model('mdl_pendok');
		$array_input = $this->input->post(NULL, TRUE);

		$id_pendok = $this->mdl_pendok->input($array_input);

		if($id_pendok !== false)
		{
			$url_redir = base_url('pendok/main/views');
		}else{
			$url_redir = base_url('pendok/main');
		}

		redirect($url_redir);

	}

	public function input_init()
	{
		$this->load->model('mdl_pendok');
		$this->load->model('tables/mdl_tables');
		$array_input = $this->input->post(NULL, TRUE);

		//=============BEGIN- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$id_lokasi = $this->mksess->id_lokasi();
		
		$info_lokasi_pengguna = (array)$this->mdl_pendok->get_info_lokasi($id_propinsi_pengguna, $id_kabkota_pengguna);

		$array_input['id_propinsi_pendaftaran'] = $id_propinsi_pengguna;
		$array_input['id_kabkota_pendaftaran'] = $id_kabkota_pengguna;
		$array_input['propinsi_pendaftaran'] = $info_lokasi_pengguna['nama_propinsi'];
		$array_input['kabkota_pendaftaran'] = $info_lokasi_pengguna['nama_kabupaten_kota'];

		//=============END- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		switch ($id_lokasi) {
			case '2':
				$array_input['kategori_pendaftaran'] = 'PROPINSI';
				break;

			case '3':
				$array_input['kategori_pendaftaran'] = 'KAB/KOTA';
				break;

			default:
				$array_input['kategori_pendaftaran'] = 'PUSAT';
				break;
		}

		if($array_input['tipe_permohonan'] === 'PERUBAHAN'){
			$cekbox['isi'] = $array_input['cekbox'];
			$array_input['nama_kapal'] = $array_input['nama_kapal_register'];
		}else{
			$array_input['nama_kapal'] = $array_input['nama_kapal_baru'];
		}
		
		unset($array_input['cekbox']);
		unset($array_input['no_register']);
		unset($array_input['nama_kapal_baru']);
		unset($array_input['nama_kapal_register']);
		unset($array_input['form_kapal_baru']);

		//id_pendok_lama dipakai ketika tipe perubahan adalah perubahan, ini untuk mengambil data dari trs bkp
		$id_pendok_lama = $array_input['id_pendok_terakhir'];
		unset($array_input['id_pendok_terakhir']);

		$id_kapal = $array_input['id_kapal'];

		$id_lokasi = $this->mksess->id_lokasi();

		switch ($id_lokasi) {
			case '2':
				$id_tempat = $this->mksess->id_propinsi();
				break;

			case '3':
				$id_tempat = $this->mksess->id_kabupaten_kota();
				break;

			default:
				$id_tempat = 0;
				break;
		}

		//================BEGIN- SET UNTUK NO REKAM PENDOK DAN UPDATE COUNTER NO REKAM PENDOK====================
		// if($array_input['tipe_permohonan'] === 'BARU'){
			$array_input['no_rekam_pendok'] = $this->mdl_tables->next_no_rekam_pendok($id_lokasi, $id_tempat);
			$this->mdl_tables->update_counter_pendok($id_lokasi, $id_tempat);
		// }
		//================END- SET UNTUK NO REKAM PENDOK DAN UPDATE COUNTER NO REKAM PENDOK======================

		//=======BEGIN- EXECUTE INPUT INIT TO TABLE TRS_PENDOK=====
		$id_pendok = $this->mdl_pendok->input($array_input);
		//=======END- EXECUTE INPUT INIT TO TABLE TRS_PENDOK=======

		$perubahan = array();

		$ganti_pemilik = FALSE;
			
		if($array_input['tipe_permohonan'] === 'PERUBAHAN'){
			foreach ($cekbox['isi'] as $key => $value) {
				$perubahan['id_pendok'] = $id_pendok;
				$perubahan['id_tipe_perubahan'] = $key;
				$this->mdl_pendok->input_trs_perubahan($perubahan);
				$value_perubahan = explode('|',$value);
				if( ($value_perubahan[1] === "identitas_pemilik") && ($ganti_pemilik===FALSE) )
				{
					$ganti_pemilik = TRUE;
				}

			}
		}

		if($id_pendok !== false)	
		{
			
			if(($ganti_pemilik == TRUE)||($array_input['tipe_permohonan'] === 'BARU'))
			{
				$url_redir = base_url('pendok/main/entry/prsh/'.$id_pendok);
			}
			else
			{
				$identitas_kapal['id_kapal'] = $id_kapal;
				//$status_ik adalah status dari hasil update pendok, untuk edit Identitas Kapal
				$status_ik = $this->mdl_pendok->edit($id_pendok,$identitas_kapal);
				$url_redir = base_url('pendok/main/entry/pmhn/'.$id_pendok);
			}

		}else{
			$url_redir = base_url('pendok/entry');
		}

		redirect($url_redir);
	}

	public function update($origin = '')
	{
		$this->load->model('mdl_pendok');
		$this->load->model('kapal/mdl_kapal');

		$config['upload_path'] = 'uploads/document';
		$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';

		$this->load->library('upload',$config);

		$array_input = $this->input->post(NULL, TRUE);

		// vdump($array_input, true);

		$id_pendok = $array_input['id_pendok'];

		if($array_input['tipe_permohonan'] === 'PERUBAHAN'){
			$cekbox_perubahan['isi'] = $array_input['cekbox_perubahan'];
		}
		
		unset($array_input['cekbox_perubahan']);

		if($array_input['tipe_permohonan'] === 'PERUBAHAN'){

			$get_from_db = $this->mdl_pendok->get_trs_perubahan_2($id_pendok);
			$convert_result = $this->toArray($get_from_db);
			// vdump($convert_result);

			$array_trs_perubahan_sblm = array();
			foreach ($convert_result as $item) {
				foreach ($item as $key => $value) {
					$array_trs_perubahan_sblm[] = (string)$value;
				}
			}

			$array_trs_perubahan_ssdh = array();
			foreach ($cekbox_perubahan['isi'] as $key => $value) {
				$array_trs_perubahan_ssdh[] = (string)$key;
			}

			$array_sblm = $array_trs_perubahan_sblm;
			$array_ssdh = $array_trs_perubahan_ssdh;

			// vdump($array_sblm);
			// vdump($array_ssdh);
	
			$arr_to_delete = array();
			$arr_to_input = array();
				
			$arr_to_delete = array_diff($array_sblm, $array_ssdh);
			$arr_to_input = array_diff($array_ssdh, $array_sblm);
			
			// vdump($arr_to_delete);
			// vdump($arr_to_input);

			$arr_data_delete = array();
			foreach ($arr_to_delete as $key => $value) {
				$data_delete['id_pendok'] = $id_pendok;
				$data_delete['id_tipe_perubahan'] = $value;
				$data_delete['aktif'] = 'tidak';
				$arr_data_delete[] = $data_delete;
				$this->mdl_pendok->delete_trs_perubahan($data_delete);
			}

			// vdump($arr_data_delete);

			$arr_data_input = array();
			foreach ($arr_to_input as $key => $value) {
				$data_input['id_pendok'] = $id_pendok;
				$data_input['id_tipe_perubahan'] = $value;
				$arr_data_input[] = $data_input;
				$this->mdl_pendok->input_trs_perubahan($data_input);
			}
			// vdump($arr_data_input, true);
		}
		
		if($origin === ''){ //handle untuk button simpan/preview ketika di form_edit_pendok
			//jika button simpan yang di klik, dan kelengkapan dokumen di ubah
			$submit_to = $array_input['submit_to'];
			// var_dump($submit_to);
			// if($submit_to === 'edit'){
				$cekbox['isi'] = $array_input['cekbox'];
				foreach ($cekbox['isi'] as $key => $value) {
					$perubahan['id_pendok'] = $id_pendok;
					$perubahan['id_dokumen'] = $key;
					$perubahan['isi'] = $value;
					$this->mdl_pendok->update_trs_pendok_dokumen($perubahan);
					// echo "<pre>";
					// var_dump($perubahan);
					// echo "</pre>";
				}
			// }
		}else if($origin === 'final'){
			/*
			origin = final disini maksud nya adalah untuk menghandle data2 pendok dari database kapi lama
			yang sudah set final namun ingin di perbaiki data2nya.
			*/

			// vdump($array_input, false);

			$data_perusahaan['id_perusahaan'] = $array_input['id_perusahaan'];
			$data_perusahaan['nama_perusahaan'] = $array_input['nama_perusahaan'];
			$data_perusahaan['nama_penanggung_jawab'] = $array_input['nama_penanggung_jawab'];
			$data_perusahaan['no_identitas_penanggung_jawab'] = $array_input['no_identitas_penanggung_jawab'];
			$data_perusahaan['ttl_penanggung_jawab'] = $array_input['ttl_penanggung_jawab'];
			$data_perusahaan['alamat_perusahaan'] = $array_input['alamat_perusahaan'];

			if( !($data_perusahaan['id_perusahaan']==='') ){
				// vdump($data_perusahaan, false);
				$this->mdl_kapal->update_perusahaan($id_pendok, $data_perusahaan);
			}

			$sudah_ada_dokumen = $this->mdl_pendok->is_trs_pendok_dokumen($array_input['id_pendok']);
			if( !($sudah_ada_dokumen) ){
				$cekbox['isi'] = $array_input['cekbox'];
				foreach ($cekbox['isi'] as $key => $value) {
					$perubahan['path_dok'] = '';

					if($value === "ADA")
					{
						if($this->upload->do_upload('file_'.$key))
						{
							$upload = $this->upload->data();
							$perubahan['path_dok'] = 'uploads/document/'.$upload['file_name'];
						}
					}

					$perubahan['id_pendok'] = $id_pendok;
					$perubahan['id_dokumen'] = $key;
					$perubahan['isi'] = $value;

					// vdump($cekbox['isi'], true);
					$this->mdl_pendok->input_trs_pendok_dokumen($perubahan);
				}			
			}else{
				$cekbox['isi'] = $array_input['cekbox'];
				foreach ($cekbox['isi'] as $key => $value) {
					$perubahan['path_dok'] = '';

					if($value === "ADA")
					{
						if($this->upload->do_upload('file_'.$key))
						{
							$upload = $this->upload->data();
							$perubahan['path_dok'] = 'uploads/document/'.$upload['file_name'];
						}
					}

					$perubahan['id_pendok'] = $id_pendok;
					$perubahan['id_dokumen'] = $key;
					$perubahan['isi'] = $value;

					// vdump($cekbox['isi'], true);
					$this->mdl_pendok->update_trs_pendok_dokumen($perubahan);
				}
			}

		}

		//dipakai ketika pendok sedang dalam step dkmn_entry
		if($origin === 'dkmn' ){
			$cekbox['isi'] = $array_input['cekbox'];
			foreach ($cekbox['isi'] as $key => $value) {
				$perubahan['path_dok'] = '';

				if($value === "ADA")
				{
					if($this->upload->do_upload('file_'.$key))
					{
						$upload = $this->upload->data();
						$perubahan['path_dok'] = 'uploads/document/'.$upload['file_name'];
					}
				}

				$perubahan['id_pendok'] = $id_pendok;
				$perubahan['id_dokumen'] = $key;
				$perubahan['isi'] = $value;
				$this->mdl_pendok->input_trs_pendok_dokumen($perubahan);
			}
		}

		//unset beberapa field input yang tidak masuk ke dalam tabel pendok di database
		unset($array_input['cekbox']);
		unset($array_input['submit_to']);
		unset($array_input['id_pendok']);
		// unset($array_input['nama_perusahaan']);
		unset($array_input['nama_pemohon']);
		unset($array_input['no_telp_pemohon']);

		/*print_r($array_input);
		print_r($id_pendok);*/

		$affected = $this->mdl_pendok->edit($id_pendok, $array_input);	

		echo ($affected === true)? "TRUE" : "FALSE";

		switch ($origin) {
			case 'init':
				$url = 'pendok/main/entry/prsh/'.$id_pendok;
				break;
			case 'prsh':
				$url = 'pendok/main/entry/pmhn/'.$id_pendok;
				break;
			case 'pmhn':
				$url = 'pendok/main/entry/dkmn/'.$id_pendok;
				break;
			case 'dkmn':
				$url = 'pendok/main/edit/'.$id_pendok;
				break;
			case 'final':
				$url = 'pendok/main/view/'.$id_pendok;
				break;
			default:
				if($submit_to === 'edit')
				{
					$url = 'pendok/cetak/preview/'.$id_pendok;
				}elseif($submit_to === 'detail')
				{
					$url = 'pendok/cetak/preview/'.$id_pendok;
				}

				break;
		}
			$url_redir = base_url($url);
	

		redirect($url_redir);
	}

	public function set_pendok_final($id_pendok, $is_final = '')
	{
		$this->load->model('mdl_pendok');
		$this->load->model('tables/mdl_tables');

		//session
		$id_pengguna = $this->mksess->id_pengguna();

		//mengambil tipe permohonan di table pendok
		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);
		$get_status_pendok = (array)$this->mdl_pendok->get_status_pendok($id_pendok);

		if($is_final === 'final')
		{
			//menjalankan fungsi set_final untuk merubah status_pendok menjadi FINAL
			$this->mdl_pendok->set_final($id_pendok);
			//jika tipe permohonan = baru dan status_pendok masih EDIT, ini untuk mencegah jika url di refresh

			$id_lokasi = $this->mksess->id_lokasi();

			switch ($id_lokasi) {
				case '2':
					$id_tempat = $this->mksess->id_propinsi();
					break;

				case '3':
					$id_tempat = $this->mksess->id_kabupaten_kota();
					break;

				default:
					$id_tempat = 0;
					break;
			}
			
			//================BEGIN- SET UNTUK NO REGISTER DAN UPDATE COUNTER NO REGISTER====================
			if(($get_permohonan['tipe_permohonan'] === 'BARU') && ($get_status_pendok['status_pendok'] === 'EDIT')){
				$no_register = $this->mdl_tables->next_no_register($id_lokasi, $id_tempat);
				$affected = $this->mdl_pendok->trigger_kapal_trs_bkp($id_pendok, $no_register, $id_pengguna);
				$this->mdl_tables->update_counter_register($id_lokasi, $id_tempat);
			}
			//================END- SET UNTUK NO REGISTER DAN UPDATE COUNTER NO REGISTER====================
		}

		if($affected !== false)
		{
			$url_redir = base_url('pendok/main/views');
		}else{
			$url = 'pendok/main/view/'.$id_pendok;
			$url_redir = base_url($url);
		}

		redirect($url_redir);

	}

	public function toArray($obj)
	{
	    if (is_object($obj)) $obj = (array)$obj;
	    if (is_array($obj)) {
	        $new = array();
	        foreach ($obj as $key => $val) {
	            $new[$key] = $this->toArray($val);
	        }
	    } else {
	        $new = $obj;
	    }

	    return $new;
	}

	public function update_no_rekam_pendok()
	{
		$this->load->model('mdl_pendok');

		$data_update = (array)$this->mdl_pendok->get_update_no_rekam_pendok();
		// vdump($data);
		$norek_update = 7027;
		foreach ($data_update as $item) {
			$data['id_pendok'] = $item->id_pendok;
			echo $item->no_rekam_pendok;
			echo " ----- ";
			echo $data['no_rekam_pendok'] = $norek_update;
			if( $this->mdl_pendok->update_no_rekam_pendok($data) ){
				echo "----- berhasil";
			}
			echo "<br>";
			$norek_update++;
		}

	}

}