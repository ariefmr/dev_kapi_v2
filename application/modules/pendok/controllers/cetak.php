<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends MX_Controller {


	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_pendok');
			$this->load->config('globals');
			$this->load->config('labels');
		}

	public function index($id_pendok, $is_final = '')
	{
		//mengambil tipe permohonan di table pendok
		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);
		$get_status_pendok = (array)$this->mdl_pendok->get_status_pendok($id_pendok);

		if($is_final === 'final')
		{
			//menjalankan fungsi set_final untuk merubah status_pendok menjadi FINAL
			$this->mdl_pendok->set_final($id_pendok);
			//jika tipe permohonan = baru dan status_pendok masih EDIT, ini untuk mencegah jika url di refresh
			if(($get_permohonan['tipe_permohonan'] === 'BARU') && ($get_status_pendok['status_pendok'] === 'EDIT')){
				$this->mdl_pendok->trigger_kapal_trs_bkp($id_pendok);
				
			}
		}

		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('form_pendok_label');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
		$data['detail_pendok'] = (array) $get_detail_pendok[0];
		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;
		$data['checkbox_tag'] = Modules::run('refkapi/mst_dokumen/list_trs_pendok_dokumen_per_id',$id_pendok);
		if(!$data['checkbox_tag']){
			$data['checkbox_kosong'] = Modules::run('refkapi/mst_dokumen/list_dokumen_array');

		}

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_tanda_terima_pendok', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);
		$this->mpdf->SetHeader('Tanda Terima Permohonan Pendaftaran Kapal | |NO. PENDOK ('.$data['detail_pendok']['no_rekam_pendok'].')	');
		$this->mpdf->SetFooter('Direktorat Kapal Perikanan dan Alat Penangkapan Ikan ||'.$timestamp);
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		if($is_final === 'final')
		{
			$this->mpdf->SetJS('this.print();');
		}
		$this->mpdf->Output('Tanda Terima Permohonan Dokumen.pdf','I');

		$this->preview($id_pendok);

	}

	public function preview_pendok($id_pendok)
	{	
		$data['id_pendok'] = $id_pendok;

		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);
		$tipe_permohonan = $get_permohonan['tipe_permohonan'];
			// vdump($tipe_permohonan['tipe_permohonan'], true);
		if($tipe_permohonan == 'BARU'){
			$data['tipe_draft'] = 'baru';
		}else{
			$data['tipe_draft'] = 'perubahan';
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							'preview_print_draft', //nama file view
							'label_print_draft', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function preview_before($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('form_pendok_label');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');

		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
		$data['id_pendok'] = $id_pendok;
		$data['detail_pendok'] = (array) $get_detail_pendok[0];
		$data['is_preview'] = TRUE;
		$data['forms'] = $forms['form'];
		$data['css'] = $css;
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$this->load->view('print_tanda_terima_pendok', $data);
	}

	public function preview($id_pendok)
	{	
		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		// $data['edit_link'] = base_url('pendok/main/edit/index');
		// $data['cetak_link'] = base_url('pendok/cetak/index');
		$data['id_pendok'] = $id_pendok;

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							'preview_print', //nama file view
							'label_print_pendok', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}



}
?>