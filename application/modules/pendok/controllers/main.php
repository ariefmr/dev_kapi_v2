<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
		{
			parent::__construct();
			$this->load->model('mdl_pendok');
		}

	public function index(){
		$this->views();
	}

	public function entry($part = 'init',$id_pendok = '')
	{	
		if($id_pendok !== '')
		{
			$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
			$data['detail_pendok'] = (array) $get_detail_pendok[0];
		}

		switch ($part) {
			case 'init':
				$data['submit_form'] = 'pendok/functions/input_init';
				$file_view = 'entry_init';
				break;
			case 'prsh':
				$data['submit_form'] = 'pendok/functions/update/prsh';
				$file_view = 'entry_perusahaan';
				break;
			case 'pmhn':
				$data['submit_form'] = 'pendok/functions/update/pmhn';
				$file_view = 'entry_pemohon';
				break;
			case 'dkmn':
				$data['submit_form'] = 'pendok/functions/update/dkmn';
				$file_view = 'entry_dokumen';				
				break;
		}

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok_label', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function edit($id_pendok = '')
	{	
		//jika id_pendok tidak kosong
		if($id_pendok !== '')
		{
			//maka ambil semua data pendok dengan id_pendok tsb lalu akan digunakan untuk referensi validasi/melengkapi kekurangan
			$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
			$data['detail_pendok'] = (array) $get_detail_pendok[0];
			$status_edit = $data['detail_pendok']['status_edit'];
			$status_final = $data['detail_pendok']['status_pendok'];
		}

		switch ($status_edit) {
			//filter status_edit untuk melanjutkan proses pengisian form pendok
			case '1':
				$data['submit_form'] = 'pendok/functions/input_init';
				$file_view = 'entry_init';
				break;
			case '2':
				$data['submit_form'] = 'pendok/functions/update/prsh';
				$file_view = 'entry_perusahaan';
				break;
			case '3':
				$data['submit_form'] = 'pendok/functions/update/pmhn';
				$file_view = 'entry_perusahaan';
				break;
			case '4':
				$data['submit_form'] = 'pendok/functions/update/dkmn';
				$file_view = 'entry_dokumen';				
				break;
			case '5':
				if($status_final === 'FINAL'){
					$data['submit_form'] = 'pendok/functions/update/final';
					$tipe_permohonan = (array) $this->mdl_pendok->get_permohonan($id_pendok);
					if($tipe_permohonan['tipe_permohonan'] == "PERUBAHAN"){
						$array_trs_perubahan = (array) $this->mdl_pendok->get_trs_perubahan($id_pendok);
						$data['trs_perubahan'] = $array_trs_perubahan;
						$file_view = 'form_pendok_perubahan';
					}else{
						$file_view = 'form_pendok';
					}

				}else{
					$data['submit_form'] = 'pendok/functions/update';
					$tipe_permohonan = (array) $this->mdl_pendok->get_permohonan($id_pendok);
					if($tipe_permohonan['tipe_permohonan'] == "PERUBAHAN"){
						$file_view = 'form_pendok_perubahan';
					}else{
						$file_view = 'form_pendok';
					}
				}
				break;
		}

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
			/*echo "<pre>";
			print_r($data);
			echo "</pre>";*/
		
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok_label', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function views()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'pendok';
		$views = 'tabel_pendok';
		$labels = 'label_view_pendok';

		$data['list_pendok'] = $this->mdl_pendok->list_pendok($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);

	}

	public function view($id_pendok)
	{	
		$get_detail_pendok = $this->mdl_pendok->get_detail($id_pendok);
		$data['detail_pendok'] = (array) $get_detail_pendok[0];

		$status_dokumen = $data['detail_pendok']['status_pendok'];

		$tipe_permohonan = (array) $this->mdl_pendok->get_permohonan($id_pendok);
		if($tipe_permohonan['tipe_permohonan'] == "PERUBAHAN"){
			$array_trs_perubahan = (array) $this->mdl_pendok->get_trs_perubahan($id_pendok);
			$data['trs_perubahan'] = $array_trs_perubahan;
			$file_view = 'view_pendok_perubahan';
		}else{
			$file_view = 'view_pendok';
		}

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok_label', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

}