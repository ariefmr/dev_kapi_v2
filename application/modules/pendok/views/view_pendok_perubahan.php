<?php 
//WALAUPUN PENDOK SUDAH FINAL TETAPI DATA PENDOK MERUPAKAN INPUTAN DARI APLIKASI LAMA AKAN TETAP BISA EDIT
if(($detail_pendok['status_pendok'] === 'FINAL') && ($detail_pendok['id_pendok'] <= 5859) ) : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-2"> 
              <?php 
              $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
              // if($detail_pendok['status_edit']==5){
                echo $link_cetak;
              // }
              ?>
  </div>
  <div class="col-lg-1"> 
              <?php 
              $link_edit = '<a class="btn btn-primary" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
              echo $link_edit; 
              ?>
  </div>
</div>
<?php elseif($detail_pendok['status_pendok'] === 'EDIT') : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-2"> 
              <?php 
              $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
              if($detail_pendok['status_edit']==5){
                echo $link_cetak;
              }
              ?>
  </div>
  <div class="col-lg-1"> 
              <?php 
              $link_edit = '<a class="btn btn-primary" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
              echo $link_edit; 
              ?>
  </div>
</div>
<?php elseif($detail_pendok['status_pendok'] === 'FINAL') : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-1"> 
    <?php 
    $is_admin = $this->mksess->info_is_admin();
    if( $is_admin ){
      $link_edit = '<a class="btn btn-danger" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
      echo $link_edit; 
    }
    ?>
  </div>
  <div class="col-lg-2"> 
    <?php 
    $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
    echo $link_cetak; 
    ?>
  </div>
</div>
<?php endif; ?>

<div class="row">
  <div class="col-lg-12">
          <?php

            //var_dump($detail_pendok['status_pendok']);
            $hidden_input = array('id_pendok' => $detail_pendok['id_pendok'],
                                  'submit_to' => 'edit');
            echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);

            ?>
           <?php 
            $attr_no_permohonan = array( 'name' => $form['no_surat_permohonan']['name'],
                                          'label' => $form['no_surat_permohonan']['label'],
                                          'value' => $detail_pendok['no_surat_permohonan']
                      );
            echo $this->mkform->input_text($attr_no_permohonan);

            $attr_tanggal_surat_permohonan = array( 
                             'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                            // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                            'default_date' => $detail_pendok['tanggal_surat_permohonan'], // opsi: '', tidak wajib ada
                            'placeholder' => '', // wajib ada atau '' (kosong)
                            'name' => $form['tanggal_surat_permohonan']['name'], // wajib ada
                            'label' => $form['tanggal_surat_permohonan']['label'] // wajib ada
                          );
            echo $this->mkform->input_date($attr_tanggal_surat_permohonan);

            $attr_tipe_permohonan = array( 'name' => $form['tipe_permohonan']['name'],
                                          'label' => $form['tipe_permohonan']['label'],
                                          'opsi' => array('baru' => 'Baru', 'lama' => 'Lama'),
                                          'value' => $detail_pendok['tipe_permohonan']
                      );
            echo $this->mkform->input_select($attr_tipe_permohonan);


            $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                                          'label' => $form['nama_kapal']['label'],
                                          'value' => $detail_pendok['nama_kapal']
                      );
            echo $this->mkform->input_text($attr_nama_kapal);  

            $attr_nama_perusahaan = array( 'name' => $form['nama_perusahaan']['name'],
                                          'label' => $form['nama_perusahaan']['label'],
                                          'value' => $detail_pendok['nama_perusahaan']
                      );
            echo $this->mkform->input_text($attr_nama_perusahaan);

            $attr_nama_perusahaan = array( 'name' => 'npj',
                                          'label' => 'Nama Penanggung Jawab',
                                          'value' => $detail_pendok['nama_penanggung_jawab']
                      );
            echo $this->mkform->input_text($attr_nama_perusahaan); 

            $attr_nama_perusahaan = array( 'name' => 'nipj',
                                          'label' => 'No Identitas Penanggung Jawab',
                                          'value' => $detail_pendok['no_identitas_penanggung_jawab']
                      );
            echo $this->mkform->input_text($attr_nama_perusahaan); 

            $attr_nama_perusahaan = array( 'name' => 'ap',
                                          'label' => 'Alamat Perusahaan',
                                          'value' => $detail_pendok['alamat_perusahaan']
                      );
            echo $this->mkform->input_text($attr_nama_perusahaan); 

            $attr_nama_pemohon = array( 'name' => 'nm_pemohon',
                                          'label' => $form['nama_pemohon']['label'],
                                          'value' => $detail_pendok['nama_pemohon']
                      );
            echo $this->mkform->input_text($attr_nama_pemohon); 

            $attr_no_telp_pemohon = array( 'name' => 'notelp_pmohon',
                                          'label' => 'No Telepon Pemohon',
                                          'value' => $detail_pendok['no_telp_pemohon']
                      );
            echo $this->mkform->input_text($attr_no_telp_pemohon);
            
            $attr_keterangan_pendok = array('name' => $form['keterangan_pendok']['name'],
                                            'label' => $form['keterangan_pendok']['label'],
                                            'value' => $detail_pendok['keterangan_pendok'],
                                            'rows' => '3'
                      );
            echo $this->mkform->input_textarea($attr_keterangan_pendok);

            // TODO : Dibuat select2 ambil list dari select2
            // $test = Modules::run('refdss/mst_wilayah/list_propinsi_array');
            // var_dump($test);
          
         ?>
  </div>
</div>
<div class="row">
  
  <div class="col-lg-3 col-lg-offset-1">
          <div class="panel">
              <div class="panel-body"><strong>Daftar Perubahan Data Kapal</strong></div>
               <div class="panel">
<?php 
                $checkbox_tag = array(  
                  'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array'),
                  'cheked' => Modules::run('refkapi/mst_tipe_perubahan/get_checked_perubahan',$detail_pendok['id_pendok'])
                  );  

                echo $this->mkform->input_checkbox_perubahan_2($checkbox_tag);
?>
                </div>
          </div>
      </div>
  <div class="col-lg-8 col-lg-offset-0">
     <div class="panel"><!-- 
          <div class="panel-heading">
            Kelengkapan Dokumen
          </div> -->
          <div class="panel-body">
           <strong>Daftar kelengkapan Dokumen</strong>
          </div>
          <!-- <ul class="list-group"> -->
              <?php 
                $id_pendok = $detail_pendok['id_pendok'];

                //handle jika ketika sedang entry pendok lalu tidak mengisi form dokumen entry .
                if(($detail_pendok['status_edit'] < 4) || ($detail_pendok['status_edit'] == 4)){
                  $checkbox_tag = array(
                                    'opsi' => Modules::run('refkapi/mst_dokumen/list_dokumen_array'),
                                    'value'=> 'ADA'
                                  );
                }else{
                  // vdump($detail_pendok['status_edit']);
                  
                  $checkbox_tag = array(
                                    'opsi' => Modules::run('refkapi/mst_dokumen/list_trs_pendok_dokumen_per_id',$id_pendok),
                                    'trs' => 'true',
                                    'value' => 'ADA'
                                  );
                }
                echo $this->mkform->radio_dokumen($checkbox_tag);
                ?>
              <!-- </ul> -->
         </div>
  </div>
  <div class="col-lg-1">
  </div>
</div>

<?php 
//WALAUPUN PENDOK SUDAH FINAL TETAPI DATA PENDOK MERUPAKAN INPUTAN DARI APLIKASI LAMA AKAN TETAP BISA EDIT
if(($detail_pendok['status_pendok'] === 'FINAL') && ($detail_pendok['id_pendok'] <= 5859) ) : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-2"> 
              <?php 
              $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
              // if($detail_pendok['status_edit']==5){
                echo $link_cetak;
              // }
              ?>
  </div>
  <div class="col-lg-1"> 
              <?php 
              $link_edit = '<a class="btn btn-primary" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
              echo $link_edit; 
              ?>
  </div>
</div>
<?php elseif($detail_pendok['status_pendok'] === 'EDIT') : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-2"> 
              <?php 
              $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
              if($detail_pendok['status_edit']==5){
                echo $link_cetak;
              }
              ?>
  </div>
  <div class="col-lg-1"> 
              <?php 
              $link_edit = '<a class="btn btn-primary" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
              echo $link_edit; 
              ?>
  </div>
</div>
<?php elseif($detail_pendok['status_pendok'] === 'FINAL') : ?>
<div class="row">
  <div class="col-lg-9"> 
  </div> 
  <div class="col-lg-1"> 
    <?php 
    $is_admin = $this->mksess->info_is_admin();
    if( $is_admin ){
      $link_edit = '<a class="btn btn-danger" href="'.base_url('pendok/main/edit/'.$detail_pendok['id_pendok']).'">Edit</a>';
      echo $link_edit; 
    }
    ?>
  </div>
  <div class="col-lg-2"> 
    <?php 
    $link_cetak = '<a class="btn btn-primary" href="'.base_url('pendok/cetak/preview/'.$detail_pendok['id_pendok']).'">Cetak Tanda Terima</a>';
    echo $link_cetak; 
    ?>
  </div>
</div>
<?php endif; ?>

</form>

<script>
  var is_final = "<?php echo $detail_pendok['status_pendok'] ?>";

  var cek_final = function(){
    if(is_final==='FINAL'){
      $("a").prop("disabled", true);
    }
  }

  $("input").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
  $("select").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
  $("textarea").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
  // $("input").prop("disabled", true);
  // $("select").prop("disabled", true);
  // $("textarea").prop("disabled", true);

  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  // s_func.push(cek_final);
</script>