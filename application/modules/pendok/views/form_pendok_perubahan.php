<div class="row">
  <div class="col-lg-12">
          <?php          
          

          $hidden_input = array('id_pendok' => kos($detail_pendok['id_pendok']),
                                'submit_to' => 'edit');
          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          
          
          $is_admin = $this->mksess->info_is_admin();
          /*if( $is_admin ){
            $disabled = TRUE;
          }else{
            $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
          }*/
          if($detail_pendok['tipe_permohonan'] === 'PERUBAHAN'){
            $disabled = TRUE;
          }else{
            $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
          }
          
          $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                                        'label' => $form['nama_kapal']['label'],
                                        'value' => kos($detail_pendok['nama_kapal']),
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_kapal);  

          $attr_no_permohonan = array( 'name' => $form['no_surat_permohonan']['name'],
                                        'label' => $form['no_surat_permohonan']['label'],
                                        'value' => kos($detail_pendok['no_surat_permohonan']),
                                        'disabled' => FALSE
                    );
          echo $this->mkform->input_text($attr_no_permohonan);

          $attr_tanggal_surat_permohonan = array( 
                           'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                          // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                          'default_date' => $detail_pendok['tanggal_surat_permohonan'], // opsi: '', tidak wajib ada
                          'placeholder' => '', // wajib ada atau '' (kosong)
                          'name' => $form['tanggal_surat_permohonan']['name'], // wajib ada
                          'label' => $form['tanggal_surat_permohonan']['label'] // wajib ada
                        );
          echo $this->mkform->input_date($attr_tanggal_surat_permohonan);

          $attr_tipe_permohonan = array( 'name' => $form['tipe_permohonan']['name'],
                                        'label' => $form['tipe_permohonan']['label'],
                                        'opsi' => array('BARU' => 'BARU', 'PERUBAHAN' => 'PERUBAHAN'),
                                        'value' => kos($detail_pendok['tipe_permohonan'])
                    );
          echo $this->mkform->input_select($attr_tipe_permohonan); 

          ?>
          
          <div class="col-md-6">
            <div id="area_cari_pemohon" style="margin-bottom:15px; display:block;">
                      <?php 
                      $nama_pemohon = kos($detail_pendok['nama_pemohon']);
                      $id_pemohon   = kos($detail_pendok['id_pemohon']);
                      echo Modules::run('refkapi/mst_pemohon/pilih_pemohon',$nama_pemohon,$id_pemohon);
                      ?> 
            </div>           
          </div>

          <div class="col-md-6">
                <div id="area_cari_perusahaan" style="margin-bottom:15px; display:block;">
                  <?php 
                  $np = kos($detail_pendok['nama_perusahaan']);
                  $ip   = kos($detail_pendok['id_perusahaan']);
                  $npj   = kos($detail_pendok['nama_penanggung_jawab']);
                  $nipj   = kos($detail_pendok['no_identitas_penanggung_jawab']);
                  $ttl   = kos($detail_pendok['ttl_penanggung_jawab']);
                  $ap   = kos($detail_pendok['alamat_perusahaan']);
                  echo Modules::run('refdss/mst_perusahaan/pilih_perusahaan',$np,$ip,$npj,$nipj,$ttl,$ap);
                  ?> 
                </div>
          </div>

          <?php
          $attr_keterangan_pendok = array('name' => $form['keterangan_pendok']['name'],
                                          'label' => $form['keterangan_pendok']['label'],
                                          'value' => kos($detail_pendok['keterangan_pendok']),
                                          'rows' => '3'
                    );
          echo $this->mkform->input_textarea($attr_keterangan_pendok);


         ?>
  </div>
</div>
<?php
  //WALAUPUN PENDOK SUDAH FINAL TETAPI DATA PENDOK MERUPAKAN INPUTAN DARI APLIKASI LAMA AKAN TETAP BISA EDIT
  if(($detail_pendok['status_pendok'] === 'final')||($detail_pendok['id_pendok'] <= 5859)){
    ?>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
           <div class="panel"><!-- 
                <div class="panel-heading">
                  Kelengkapan Dokumen
                </div> -->
                <div class="panel-body" style="margin-bottom:15px;">
                 <strong>Daftar kelengkapan Dokumen</strong>
                </div>
                      <?php
                      $checkbox_tag = array(
                                        'opsi' => Modules::run('refkapi/mst_dokumen/list_dokumen_array')
                                      );
                      // echo $this->mkform->input_checkbox_perubahan($checkbox_tag);
                      echo $this->mkform->radio_dokumen($checkbox_tag);
                     ?>
               </div>
        </div>
        <div class="col-lg-1">
        </div>
      </div>
    <?php
  }else{
    ?>
    <div class="row">
      <div class="col-lg-3 col-lg-offset-1">
          <div class="panel">
              <div class="panel-body"><strong>Daftar Perubahan Data Kapal</strong></div>
               <div class="panel">
<?php 
                $checkbox_tag = array(  
                  'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array'),
                  'cheked' => Modules::run('refkapi/mst_tipe_perubahan/get_checked_perubahan',$detail_pendok['id_pendok'])
                  );  

                echo $this->mkform->input_checkbox_perubahan_2($checkbox_tag);
?>
                </div>
          </div>
      </div>
      <div class="col-lg-8 col-lg-offset-0">
          <div class="panel">
            <div class="panel-body"><strong>Daftar kelengkapan Dokumen</strong></div>
<?php 
             $id_pendok = $detail_pendok['id_pendok'];

             $checkbox_tag = array(
              'opsi' => Modules::run('refkapi/mst_dokumen/list_trs_pendok_dokumen_per_id',$id_pendok),
              'trs' => 'true',
              'value' => 'ADA'
              );
             echo $this->mkform->radio_dokumen($checkbox_tag);
?>
          </div>
     </div>
<?php } ?>
   </div>
        
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
              <button type="submit" class="btn btn-primary btn-submit" data-submit-to="edit">Simpan</button>
            </div>
          </div>
  </div>
</div>
</form>

<script>
  var is_final = "<?php echo $detail_pendok['status_pendok'] ?>";


  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  // s_func.push(cek_final);
</script>