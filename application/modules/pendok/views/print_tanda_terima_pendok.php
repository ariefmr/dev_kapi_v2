<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 21cm;
	        min-height: 29.7cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>
<?php 
 	$tmpl = array ( 'table_open'  => '<table id="table_detail_pendok">' );
    $this->table->set_template($tmpl);

    $this->table->add_row('1.','No. Surat Permohonan ',': '.$detail_pendok['no_surat_permohonan']);
    $this->table->add_row('2.','Pemilik (Perorangan / Perusahaan) ',': '.$detail_pendok['nama_perusahaan']);
    $this->table->add_row('3.','Pengurus', ': '.$detail_pendok['nama_pemohon']);
    $this->table->add_row('4.','No. Telepon', ': '.$detail_pendok['no_telp_pemohon']);
    $this->table->add_row('5.','Nama Kapal', ': '.$detail_pendok['nama_kapal']);
    $this->table->add_row('6.','Jenis Permohonan', ': '.$detail_pendok['tipe_permohonan']);

   
    $html = $this->table->generate();
    $this->table->clear();

    $tmpl = array ( 'table_open'  => '<table id="table_detail_dokumen">' );
    $this->table->set_template($tmpl);

    $this->table->set_heading('No.', 'Uraian','Kelengkapan');
    /*$checkbox_tag = array(  'formulir_permohonan',
                                      'ftcp_siup',
                                      'ftcp_sipi',
                                      'ftcp_gross',
                                      'ftcp_ktp',
                                      'formulir_periksa_fisik',
                                      'ftcp_hasil_periksa_fisik',
                                      'ftcp_ukur_kapal',
                                      'ftcp_laut_pas',
                                      'ftcp_kelaikan_sertifikat_keselamatan',
                                      'penghapusan_kapal',
                                      'foto_kapal',
                                      'pernyataan'
                                    );*/
    
    $counter = 1;
    if($checkbox_tag){
        foreach ($checkbox_tag as $key) {
            $ceklis = $key->isi === 'ADA' ? '&radic;' : '-';
            $this->table->add_row($counter.'. ', $key->text, $ceklis);
            $counter++;
        }
    }else{
        //handle tampil dokumen jika dari data kapi yang lama, di anggap akan ceklis manual kelengkapan dokumen.
        foreach ($checkbox_kosong as $key) {
            $ceklis = '';
            $this->table->add_row($counter.'. ', $key->text, $ceklis);
            $counter++;
        }
    }
    $html .= "<hr>";
    $html .= '<div class="datagrid">';
    $html .= $this->table->generate();
    $html .= '</div>';

 ?>
 <div class="book">
    <div class="page">
        <div class="subpage">
            <p><h3 align="center">Tanda Terima Permohonan Pendaftaran Kapal</h3></p>

        	<?php echo $html; ?>
        	<?php 
        		$i = 0;
	        	while($i < 5){
	        		echo '<br>';
	        		$i++;        		
	        	}
        	?>
        	<table style="width: 300px; margin-left: 65%;">
        	<tbody>
        	<tr>
        		<td align="center"><p><?php 
        		$dow = intval( date('w', now()) );
        		$dt = date('d', now() );
        		$yr = date('Y', now() );
        		$indm = intval( date('n', now() ));
        		$month = $nama_bulan[$indm-1]; 
        		$day = $nama_hari[$dow];
        		echo $day.', '.$dt.' '.$month.' '.$yr;
        		//echo date('l, d F Y ',now())
        		 ?></p></td>
        	</tr>
        	<tr>
        		<td align="center"><p>Petugas Penerima Dokumen</p></td>
        	</tr>
            <tr>
                <td align="center"><p>ttd</p></td>
            </tr>
        	<tr>
        		<td align="center">
        		<?php 
        		$i = 0;
	        	while($i < 3){
	        		echo '<br>';
	        		$i++;        		
	        	}
        		?>
        		</td>
        	</tr>
        	<tr>
        		<td align="center"><?php echo $this->mksess->nama_pengguna(); ?></td>
        	</tr>
        	</tbody>
        	</table>
           <!--  <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p> -->
        </div>    
    </div>
</div>

