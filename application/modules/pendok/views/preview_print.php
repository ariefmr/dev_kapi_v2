<!-- frame menampilkan halaman tanda terima pendok -->
<iframe src="<?php echo base_url('pendok/cetak/index/'.$id_pendok) ?>" width="100%" height="960">
  <p>Your browser does not support iframes.</p>
</iframe>

<!-- tombol cetak tanda terima -->
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
         <div class="well" style="max-width: 400px; margin: 0 auto 10px;">
          <?php if ($detail_pendok['status_pendok'] === 'FINAL'): ?>
            <button type="button" class="btn btn-default btn-lg btn-block">
              Sudah dicetak pada <?php echo tgl($detail_pendok['tanggal_terima_dokumen']); ?>.
              </button>
          <?php else: ?>
            <button id="btn-cetak-final" type="button" class="btn btn-primary btn-lg btn-block">Cetak Tanda Terima</button>
          <?php endif ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // var url_cetak_final = "<?php echo base_url('pendok/cetak/index/'.$id_pendok) ?>"+"/final",
  //     pesan = "Peringatan: Cetak Dokumen akan merubah status dokumen menjadi final.";
  var url_cetak_final = "<?php echo base_url('pendok/functions/set_pendok_final/'.$id_pendok) ?>"+"/final",
      pesan = "Peringatan: Cetak Dokumen akan merubah status dokumen menjadi final.";
  $("#btn-cetak-final").click(function(){
    if( confirm(pesan) )
    {
      window.open(url_cetak_final, '_self');
    }
  });
</script>