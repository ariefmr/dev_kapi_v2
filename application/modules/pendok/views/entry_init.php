<div class="row">
  <div class="col-md-6">
    <?php
    $hidden_input = array('id_pendok' => kos($detail_pendok['id_pendok']),
      'status_edit' => '2',
                                'status_pendok' => 'EDIT' // status edit jika sudah di isi maka lanjut edit step selanjutnya
                                );
    echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);

    $attr_tipe_permohonan = array( 'name' => $form['tipe_permohonan']['name'],
      'label' => $form['tipe_permohonan']['label'],
      'opsi' => array('BARU' => 'BARU', 'PERUBAHAN' => 'PERUBAHAN'),
      'value' => kos($detail_pendok['tipe_permohonan']),
      'OnChange' => 'cek_permohonan(this.value)' 
      );
    echo $this->mkform->input_select($attr_tipe_permohonan);

    $attr_no_permohonan = array( 'name' => $form['no_surat_permohonan']['name'],
      'label' => $form['no_surat_permohonan']['label'],
      'value' => kos($detail_pendok['no_surat_permohonan'])
      );
    echo $this->mkform->input_text($attr_no_permohonan);

    $attr_tanggal_surat_permohonan = array( 
                          //'mindate' => array('time' => '9 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                          // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                          'default_date' => kos($detail_pendok['tanggal_surat_permohonan']),
                          'placeholder' => '', // wajib ada atau '' (kosong)
                          'name' => $form['tanggal_surat_permohonan']['name'], // wajib ada
                          'label' => $form['tanggal_surat_permohonan']['label'] // wajib ada
                          );
    echo $this->mkform->input_date($attr_tanggal_surat_permohonan);

    echo $detail_pendok['tanggal_surat_permohonan'];

          /*$checkbox_tag = array(  
                                  'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array')
                                );
          //var_dump($checkbox_tag);
          //AREA CEKBOX PERUBAHAN
          echo "<div id='area_cekbox_perubahan' class='col-sm-2 col-sm-offset-9' style=' display:none;' >";
            echo $this->mkform->input_checkbox_perubahan($checkbox_tag);
          echo "</div>";

          //CLEAR FIX
          echo "<div class='clearfix'></div>";*/
          ?>
        </div>

        <div class="col-md-6"> 
          <?php

          //AREA FORM CARI NOREG
          echo "<div id='area_cari_noreg' style='margin-bottom:15px; display:none;' >";
          echo Modules::run('refkapi/mst_kapal/pilih_no_register');  
          echo "</div>";

          //AREA FORM CARI NAMA KAPAL
          echo "<div id='area_cari_namakapal' style='margin-bottom:15px; display:block;' >";
          echo Modules::run('refkapi/mst_kapal/pilih_nama_kapal');
          echo "</div>";

          /*$checkbox_tag = array(  
                                  'opsi' => Modules::run('refkapi/mst_tipe_perubahan/list_tipe_perubahan_array')
                                );
          //var_dump($checkbox_tag);
          //AREA CEKBOX PERUBAHAN
          echo "<div id='area_cekbox_perubahan' class='col-sm-2 col-sm-offset-9' style=' display:none;' >";
            echo $this->mkform->input_checkbox_perubahan($checkbox_tag);
          echo "</div>";

          //CLEAR FIX
          echo "<div class='clearfix'></div>";*/

          ?>
        </div>
      </div>


      <div class="row">
        <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" id="next" class="btn btn-primary">Next</button>
            </div>
          </div>
        </div>
      </div>
    </form>


    <script>
      //$('#next').prop("disabled", true);
      var set_validation = function() 
      {
        $("#id_no_surat_permohonan").addClass('validate[required]');
        $("#id_nama_kapal").addClass('validate[required]');

        $('#form_entry').validationEngine();
    }//end set_validation

    //tipe permohonan
    function cek_permohonan(jenis_permohonan)
    {
      if(jenis_permohonan==='BARU')
      {
        //document.getElementById("area_cekbox_perubahan").style.display = "none";
        document.getElementById("area_cari_namakapal").style.display = "block";
        //document.getElementById("area_cekbox_perubahan").style.display = "none";
        document.getElementById("area_cari_noreg").style.display = "none";

      }
      else if(jenis_permohonan==='PERUBAHAN')
      {
        //document.getElementById("area_cekbox_perubahan").style.display = "block";
        document.getElementById("area_cari_noreg").style.display = "block";
        document.getElementById("area_cari_namakapal").style.display = "none";
        $('#next').prop("disabled", true);
      }
    }



    var typingTimer;                //timer identifier
    var doneTypingInterval = 1500;
    var search_kapal_listener = function () {

      function doneTyping(query){
          // var query = $('#id_nama_kapal').val(),
          //     filter = $(this).data('filter'),
          //     link_search_kapal = filter === 'kapi' ? link_search_kapal_kapi : link_search_kapal_dss;
          
          var filter = 'count';

          $.ajax({
            dataType: "json",
                url: link_search_kapal_kapi, //
                data: { i: filter, q: query}, // 
                success: update_result // 
              });
        }

        $("#id_nama_kapal").keydown(function(){
          clearTimeout(typingTimer);
        });

        $("#id_nama_kapal").keyup(function(){
          var query = $(this).val();
          typingTimer = setTimeout(function(){ doneTyping(query); }, doneTypingInterval);
        });

        $("#area_referensi_kapal [type=checkbox]").on("click",function(){
          $('#next').prop("disabled", false);
        });

        function update_result(data)
        {
          console.dir(data);
          $("#id_nama_kapal").popover('destroy');
          if(data !== '')
          {
            var button_content = 'Ditemukan '+data.jumlah_result+' kapal dengan nama '+data.search_like;
          //$("#id_nama_kapal").popover({ content: button_content , html: true, placement: 'top'});
          //$("#id_nama_kapal").popover('show');
        }
      }

      // $(".form-group").on("click", ".btn-cari-kapal");
    }//end search_kapal_listener

    s_func.push(set_validation);
    s_func.push(search_kapal_listener);

    </script>