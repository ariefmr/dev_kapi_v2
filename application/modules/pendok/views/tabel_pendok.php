<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_pendok){
		foreach ($list_pendok as $item) {
			//$link_edit = '<a class="btn btn-warning" href="'.base_url('pendok/mst_pemohon/edit/'.$item->id_pendok).'">Edit</a>';
			//$link_delete = '<a class="btn btn-danger" href="'.base_url('refkapi/mst_pemohon/delete/'.$item->id_pendok).'">Hapus</a>';
			if($item->kategori_pendaftaran === 'PUSAT'){
				$asal_lokasi = "";
			}elseif ($item->kategori_pendaftaran === 'PROPINSI') {
				$asal_lokasi = " | ".$item->propinsi_pendaftaran;
			}elseif ($item->kategori_pendaftaran === 'KAB/KOTA') {
				$asal_lokasi = " | ".$item->kabkota_pendaftaran;
			}
			$this->table->add_row(
								$counter.'.',
								'<a style="text-decoration:none" href="'.base_url('pendok/main/view/'.$item->id_pendok).'"><font color="">'.noreg($item->no_rekam_pendok).'</font></a>',
								'<a href="'.base_url('pendok/main/view/'.$item->id_pendok).'">'.$item->nama_kapal.'</a>',
								$item->nama_perusahaan,
								$item->tipe_permohonan,
								$item->kategori_pendaftaran.$asal_lokasi,
								tgl($item->tanggal_buat)." | -".kos($item->nama_pengguna, '-'),
								$item->status_pendok
								);
			$counter++;
		}
	}

	$table_list_pendok = $this->table->generate();

	$link_entry = '<a class="btn btn-primary" href="'.base_url('pendok/main/entry').'">Entri Pendok</a>';
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<p></p>
<!-- TAMPIL DATA -->
	<div id="loading_info" class="panel panel-default">
		<div class="panel-body">
		   <p class="text-center">
		   		Memuat Data Pendok. Harap Tunggu...
		   </p>
		</div>
	</div>

	<div id="table_container" class="panel-body overflowed hidden">
		<?php
			echo $table_list_pendok;

			$link_entry = '<a class="btn btn-primary" href="'.base_url('refkapi/mst_pemohon/entry').'">Entri Pemohon</a>';
		?>
	</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		// $("#table_container").hide();

		$('#table_daftar_pendok').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
        	"iDisplayLength": 30,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
		    }
		} );
	} );
</script>