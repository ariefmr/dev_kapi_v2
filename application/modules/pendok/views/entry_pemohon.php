<div class="row">
  <div class="col-md-6">
          <?php
          $hidden_input = array('id_pendok' => kos($detail_pendok['id_pendok']),
                                'status_edit' => '4' // status edit jika sudah di isi maka lanjut edit step selanjutnya
                                );
          echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);

          $is_admin = $this->mksess->info_is_admin();
          /*if( $is_admin ){
            $disabled = TRUE;
          }else{
            $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
          }*/
          if($detail_pendok['tipe_permohonan'] === 'PERUBAHAN'){
            $disabled = TRUE;
          }else{
            $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
          }
          
          $attr_nama_kapal = array( 'name' => $form['nama_kapal']['name'],
                                        'label' => $form['nama_kapal']['label'],
                                        'value' => kos($detail_pendok['nama_kapal']),
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_kapal);
          
          $attr_no_permohonan = array( 'name' => $form['no_surat_permohonan']['name'],
                                        'label' => $form['no_surat_permohonan']['label'],
                                        'value' => kos($detail_pendok['no_surat_permohonan']),
                                        'disabled' => FALSE
                    );
          echo $this->mkform->input_text($attr_no_permohonan);
   
         ?>
  </div>
  <div class="col-md-6">
    <div id="area_cari_pemohon" style="margin-bottom:15px; display:block;">
              <?php 
                $nama_pemohon = kos($detail_pendok['nama_pemohon']);
                $id_pemohon   = kos($detail_pendok['id_pemohon']);
                echo Modules::run('refkapi/mst_pemohon/pilih_pemohon',$nama_pemohon,$id_pemohon);
              ?> 
    </div>           
  </div>
</div>  
         
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <button type="submit" class="btn btn-primary">Next</button>
            </div>
          </div>
  </div>
</div>
</form>

<div id="modal-search-kapal" class="modal fade" data-width="760">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        <thead> <th>No.</th> <th>Nama Kapal</th><th>No. SIPI</th><th>Tanggal SIPI</th></thead>
        <tbody></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>  
</div><!-- /.modal -->


<script>
    var set_validation = function() 
    {
      $("#id_nama_pemilik").addClass('validate[required]');
      $('#form_entry').validationEngine();
    }//end set_validation

    var typingTimer;                //timer identifier
    var doneTypingInterval = 1500;
    var search_kapal_listener = function () {
   
      function doneTyping(query){
        $("#id_nama_kapal").popover('destroy');
        if(query !== '')
        {
          var button_content = 'Cari Kapal '+query+' :'+
                                '<button type="button" data-filter="pusat" class="btn btn-info btn-cari-kapal">DSS Pusat</button> '+
                                '<button type="button" data-filter="daerah" class="btn btn-info btn-cari-kapal">DSS Daerah</button>'+
                                '<button type="button" data-filter="kapi" class="btn btn-info btn-cari-kapal">KAPI</button>';
          $("#id_nama_kapal").popover({ content: button_content , html: true, placement: 'top'});
          $("#id_nama_kapal").popover('show');
        }
      }

      $("#id_nama_kapal").keydown(function(){
        clearTimeout(typingTimer);
      });

      $("#id_nama_kapal").keyup(function(){
        var query = $(this).val();
          typingTimer = setTimeout(function(){ doneTyping(query); }, doneTypingInterval);
      });

      function update_result(data)
      {
        // console.log(data);
        $("#modal-search-kapal .modal-title").html('');
        $("#modal-search-kapal .modal-title").text('Pencarian Kapal "'+data.search_like+'" ('+data.filter+'). Ditemukan '+data.jumlah_result+' kapal.');
        $("#modal-search-kapal tbody").html(''); // Kosongin
          if(data.jumlah_result > 0)
          {
            data.result.forEach(function(d, i){
              $("#modal-search-kapal tbody").append('<tr><td>'+(i+1)+'.</td> <td>'+d.nama_kapal+'</td><td>'+d.no_sipi+'</td><td>'+d.tanggal_sipi+' s/d '+d.tanggal_akhir_sipi+'</td>  </tr>');
            });
          }

        $("#modal-search-kapal").modal('show');
      }

      $(".form-group").on("click", ".btn-cari-kapal",function(){
          var query = $('#id_nama_kapal').val(),
              filter = $(this).data('filter'),
              link_search_kapal = filter === 'kapi' ? link_search_kapal_kapi : link_search_kapal_dss;

          // $("#modal-search-kapal .modal-title").text("Pencarin Kapal : "+query);
              $.ajax({
                dataType: "json",
                url: link_search_kapal, //
                data: { i: filter, q: query}, // 
                success: update_result // 
              });
          
      });
    }//end search_kapal_listener

  s_func.push(set_validation);
  //s_func.push(search_kapal_listener);
</script>