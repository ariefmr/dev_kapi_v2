<?php

class Mdl_pendok extends CI_Model
{
	function __construct()
    {
        $this->load->database();
    }

    public function list_pendok($id_lokasi, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        /*$query = "SELECT *
                    FROM db_pendaftaran_kapal.trs_pendok
                    WHERE db_pendaftaran_kapal.trs_pendok.aktif = 'ya'
                    ";*/
        $query = "  SELECT 
                        pendok.*,
                        pemohon.*,
                        pengguna.nama_pengguna
                    FROM 
                        db_pendaftaran_kapal.trs_pendok pendok
                    LEFT JOIN (
                        db_pendaftaran_kapal.mst_pemohon pemohon
                            )
                    ON (
                        pendok.id_pemohon = pemohon.id_pemohon
                            )
                    LEFT JOIN db_master.mst_pengguna pengguna ON pengguna.id_pengguna = pendok.id_pengguna_buat

                    WHERE pendok.aktif = 'YA' ";
                    switch ($id_lokasi) {
                        case '1':
                            $query .= "";
                            break;
                        
                        case '2':
                            $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                            break;

                        case '3':
                            $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                            break;
                            
                        default:
                            $query .= "";
                        break;
                    }
        $query .=  "ORDER BY pendok.no_rekam_pendok
                    DESC

                    ";

        // vdump($query, true);

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;


        $this->db->insert('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function input_trs_pendok_dokumen($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;

    	$this->db->insert('trs_pendok_dokumen', $data);

    	$aff_rows = $this->db->affected_rows();
    	if($aff_rows > 0)
    	{
    		$id_pendok = $this->db->insert_id();	
    	}else{
    		$id_pendok = false;
    	}

    	return $id_pendok;
    }

    public function edit($id_pendok, $data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }

    public function update_trs_pendok_dokumen($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok', $data['id_pendok']);
    	$this->db->where('id_dokumen', $data['id_dokumen']);
    	$this->db->update('trs_pendok_dokumen', $data);

    	$aff_rows = $this->db->affected_rows();
    	if($aff_rows > 0)
    	{
    		$affected = true;	
    	}else{
    		$affected = false;
    	}

    	return $affected;
    }

    public function set_final($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $id_pengguna = $this->mksess->id_pengguna();
        $data = array('status_pendok' => 'FINAL'
                        ,'tanggal_terima_dokumen' => $date_terima
                        ,'tanggal_ubah' => $date_terima
                       ,'id_pengguna_ubah' => $id_pengguna
                    );
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function get_detail($id_pendok)
    {
        $query = "  SELECT 
                        pendok.*,
                        pemohon.*
                    FROM 
                        db_pendaftaran_kapal.trs_pendok pendok
                    LEFT JOIN (
                                db_pendaftaran_kapal.mst_pemohon pemohon
                                )
                    ON (
                        pendok.id_pemohon = pemohon.id_pemohon
                        )
                    WHERE 
                        pendok.id_pendok = '".$id_pendok."'
                        ";

        $run_query = $this->db->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_trs_pd($id_pendok)
    {
        $query = "  SELECT 
                        mst.nama_dokumen as text,
                        trs.id_dokumen as id,
                        trs.isi
                    FROM
                        db_pendaftaran_kapal.trs_pendok_dokumen trs
                    JOIN
                        (
                        db_pendaftaran_kapal.mst_dokumen mst
                        )
                    ON (
                        trs.id_dokumen = mst.id_dokumen
                        )
                    WHERE mst.peraturan = 'all'
                        and mst.status_tampil = 'ya'
                        and mst.aktif = 'ya'
                        and trs.id_pendok = '".$id_pendok."'
                        ";

        $run_query = $this->db->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_opsi_perubahan()
    {
        $query = "SELECT id_tipe_perubahan AS id, jenis_perubahan as text FROM mst_tipe_perubahan";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function trigger_kapal_trs_bkp($id_pendok, $no_register, $id_pengguna)
    {
        //INISIALISASI DI TABEL MST_KAPAL
        $query = "  INSERT INTO db_pendaftaran_kapal.mst_kapal (nama_kapal_terbaru, id_pendok_terakhir, no_register, id_pengguna_buat) 
                        SELECT nama_kapal, id_pendok, '".$no_register."' as no_register, '".$id_pengguna."' as id_pengguna_buat 
                            FROM db_pendaftaran_kapal.trs_pendok pendok
                            WHERE pendok.id_pendok = '".$id_pendok."'
                ";
        $run_query = $this->db->query($query);

        //UPDATE ID_KAPAL DI TABEL TRS_PENDOK UNTUK KAPAL BARU
        $query2 = "  UPDATE db_pendaftaran_kapal.trs_pendok pendok INNER JOIN db_pendaftaran_kapal.mst_kapal kapal
                                                        ON pendok.id_pendok = kapal.id_pendok_terakhir
                        SET pendok.id_kapal = kapal.id_kapal
                ";
        $run_query = $this->db->query($query2);

        //INISIALISASI DI TABEL TRS_BKP
        $query3 = "  INSERT INTO db_pendaftaran_kapal.trs_bkp 
                        (id_kapal, id_pendok, nama_kapal, id_perusahaan, 
                            nama_perusahaan, nama_penanggung_jawab, no_identitas_penanggung_jawab,
                            ttl_penanggung_jawab, alamat_perusahaan)
                        SELECT  pendok.id_kapal, 
                                pendok.id_pendok, 
                                pendok.nama_kapal, 
                                pendok.id_perusahaan,
                                pendok.nama_perusahaan,
                                pendok.nama_penanggung_jawab,
                                pendok.no_identitas_penanggung_jawab,
                                pendok.ttl_penanggung_jawab,
                                pendok.alamat_perusahaan
                        FROM db_pendaftaran_kapal.trs_pendok pendok
                        WHERE pendok.id_pendok = '".$id_pendok."';
                    ";
        $run_query = $this->db->query($query3);

        //UPDATE ID_KAPAL DI TABEL TRS_PENDOK UNTUK KAPAL BARU
        $query4 = "  UPDATE db_pendaftaran_kapal.mst_kapal kapal INNER JOIN db_pendaftaran_kapal.trs_bkp bkp
                                                        ON kapal.id_pendok_terakhir = bkp.id_pendok
                        SET kapal.id_bkp_terakhir = bkp.id_bkp
                ";
        $run_query = $this->db->query($query4);
        
        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }

    public function get_permohonan($id_pendok)
    {
        $query = " SELECT pendok.tipe_permohonan 
                        FROM db_pendaftaran_kapal.trs_pendok pendok
                        WHERE pendok.id_pendok = '".$id_pendok."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input_trs_perubahan($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;

        $this->db->insert('trs_perubahan', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function delete_trs_perubahan($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;
        
        // $this->db->delete('trs_perubahan', $data);

        $this->db->where('id_pendok', $data['id_pendok']);
        $this->db->where('id_tipe_perubahan', $data['id_tipe_perubahan']);
        $this->db->where('aktif', 'ya');
        $this->db->update('trs_perubahan', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function delete_trs_perubahan_where($id_pendok)
    {
        $query = " UPDATE db_pendaftaran_kapal.trs_perubahan SET aktif = 'tidak' 
                        WHERE db_pendaftaran_kapal.trs_perubahan.id_pendok = '".$id_pendok."'
                        and db_pendaftaran_kapal.trs_perubahan.aktif = 'ya'
                ";

        $result = $this->db->query($query);

        return $result;
    }

    public function get_trs_perubahan_2($id_pendok)
    {
        $query = " SELECT 
                        tp.id_tipe_perubahan
                        FROM db_pendaftaran_kapal.trs_perubahan tp,
                            db_pendaftaran_kapal.mst_tipe_perubahan mtp
                        WHERE tp.id_pendok = '".$id_pendok."'
                            and tp.id_tipe_perubahan = mtp.id_tipe_perubahan
                            and tp.aktif = 'ya'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_trs_perubahan($id_pendok)
    {
        $query = " SELECT 
                        tp.id_perubahan,
                        tp.id_pendok,
                        tp.id_tipe_perubahan,
                        mtp.jenis_perubahan
                        FROM db_pendaftaran_kapal.trs_perubahan tp,
                            db_pendaftaran_kapal.mst_tipe_perubahan mtp
                        WHERE tp.id_pendok = '".$id_pendok."'
                            and tp.id_tipe_perubahan = mtp.id_tipe_perubahan
                            and tp.aktif = 'ya'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_status_pendok($id_pendok)
    {
        $query = " SELECT pendok.status_pendok 
                        FROM db_pendaftaran_kapal.trs_pendok pendok
                        WHERE pendok.id_pendok = '".$id_pendok."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function is_trs_pendok_dokumen($id_pendok)
    {
        $query = " SELECT pd.id_pendok_dokumen 
                        FROM db_pendaftaran_kapal.trs_pendok_dokumen pd
                        WHERE pd.id_pendok = '".$id_pendok."'
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_info_lokasi($id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $query = " SELECT prop.nama_propinsi,
                            prop.id_propinsi,
                            kabkota.nama_kabupaten_kota,
                            kabkota.id_kabupaten_kota 
                        FROM db_master.mst_propinsi prop, 
                                db_master.mst_kabupaten_kota kabkota
                        WHERE   prop.id_propinsi = kabkota.id_propinsi
                                and prop.id_propinsi = '".$id_propinsi_pengguna."'
                                and kabkota.id_kabupaten_kota = '".$id_kabkota_pengguna."'
                ";
        // HANDLE JIKA informasi user tidak lengkap, terutama USER daerah.
        /*$query = "SELECT 
                        prop.nama_propinsi,
                        prop.id_propinsi,
                        kabkota.nama_kabupaten_kota,
                        kabkota.id_kabupaten_kota
                    FROM
                        db_master.mst_propinsi prop,
                        db_master.mst_kabupaten_kota kabkota
                    WHERE
                        prop.id_propinsi = kabkota.id_propinsi
                            and prop.id_propinsi = '".$id_propinsi_pengguna."'
                    limit 0,1
                    ";*/

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_update_no_rekam_pendok()
    {
        $query = " SELECT * FROM trs_pendok 
                        where no_rekam_pendok >= 7027 
                        and no_rekam_pendok <= 7264
                        and aktif = 'YA'
                        ORDER BY no_rekam_pendok ASC
                ";

        $run_query = $this->db->query($query);  

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_no_rekam_pendok($data)
    {

        $this->db->where('id_pendok', $data['id_pendok']);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;
    }



}

?>