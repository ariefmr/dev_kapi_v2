<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_refdss_kapal extends CI_Model
{
	private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

	public function list_kapal($limit = '100')
    {
        $query = '  SELECT 
                        db_master.mst_kapal.id_kapal, 
                        db_master.mst_kapal.nama_kapal, 
                        db_master.mst_kapal.nama_kapal_sebelumnya,
                        db_master.mst_kapal.tahun_pembangunan,
                        db_master.mst_kapal.gt_kapal,
                        db_master.mst_kapal.nt_kapal,
                        db_master.mst_kapal.panjang_kapal,
                        db_master.mst_kapal.lebar_kapal,
                        db_master.mst_kapal.dalam_kapal,
                        db_master.mst_kapal.no_mesin,
                        db_master.mst_kapal.merek_mesin,
                        db_master.mst_kapal.tipe_mesin,
                        db_master.mst_kapal.daya_kapal,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap, 
                        db_master.mst_izin.no_sipi, 
                        db_master.mst_izin.tanggal_sipi, 
                        db_master.mst_izin.tanggal_akhir_sipi,
                        db_master.mst_perusahaan.nama_penanggung_jawab, 
                        db_master.mst_perusahaan.nama_perusahaan
                    FROM db_master.mst_kapal
                    LEFT JOIN (db_master.mst_perusahaan, db_master.mst_alat_tangkap, db_master.mst_izin, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal.id_alat_tangkap_perizinan = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan 
                            AND db_master.mst_izin.id_kapal = db_master.mst_kapal.id_kapal
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal.id_bahan_kapal
                        )
                    LIMIT 0, '.$limit.' ';
        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_kapal_daerah($limit = '100')
    {
        $query = '  SELECT 
                        db_master.mst_kapal_daerah.id_kapal_daerah, 
                        db_master.mst_kapal_daerah.nama_kapal, 
                        db_master.mst_kapal_daerah.nama_kapal_sebelumnya,
                        db_master.mst_kapal_daerah.tahun_pembangunan,
                        db_master.mst_kapal_daerah.gt_kapal,
                        db_master.mst_kapal_daerah.nt_kapal,
                        db_master.mst_kapal_daerah.panjang_kapal,
                        db_master.mst_kapal_daerah.lebar_kapal,
                        db_master.mst_kapal_daerah.dalam_kapal,
                        db_master.mst_kapal_daerah.no_mesin,
                        db_master.mst_kapal_daerah.merek_mesin,
                        db_master.mst_kapal_daerah.tipe_mesin,
                        db_master.mst_kapal_daerah.daya_kapal,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap, 
                        db_master.mst_izin_daerah.no_sipi, 
                        db_master.mst_izin_daerah.tanggal_sipi, 
                        db_master.mst_izin_daerah.tanggal_akhir_sipi,
                        db_master.mst_perusahaan_daerah.nama_penanggung_jawab, 
                        db_master.mst_perusahaan_daerah.nama_perusahaan
                    FROM db_master.mst_kapal_daerah
                    LEFT JOIN (db_master.mst_perusahaan_daerah, db_master.mst_alat_tangkap, db_master.mst_izin_daerah, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal_daerah.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal_daerah.id_perusahaan_daerah = db_master.mst_perusahaan_daerah.id_perusahaan_daerah 
                            AND db_master.mst_izin_daerah.id_kapal_daerah = db_master.mst_kapal_daerah.id_kapal_daerah
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal_daerah.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal_daerah.id_bahan_kapal
                        )
                    LIMIT 0, '.$limit.' ';
        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_kapal($filter, $id_kapal, $id_izin)
    {

        switch ($filter) {
            
            case 'pusat':
                $sql = "SELECT *,
                        db_master.mst_izin.no_sipi, 
                        db_master.mst_izin.tanggal_sipi,
                        db_master.mst_izin.tanggal_akhir_sipi,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap, 
                        db_master.mst_perusahaan.nama_penanggung_jawab, 
                        db_master.mst_perusahaan.nama_perusahaan
                    FROM db_master.mst_kapal
                    LEFT OUTER JOIN (db_master.mst_perusahaan, db_master.mst_alat_tangkap, db_master.mst_izin, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal.id_alat_tangkap_perizinan = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan 
                            AND db_master.mst_izin.id_kapal = db_master.mst_kapal.id_kapal
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal.id_bahan_kapal
                            AND db_master.mst_izin.id_izin = $id_izin
                        )
                    WHERE mst_kapal.id_kapal = $id_kapal
                        OR mst_izin.id_izin = $id_izin ";
                break;
                
            case 'daerah':
                $sql = "SELECT *,
                        db_master.mst_izin_daerah.no_sipi, 
                        db_master.mst_izin_daerah.tanggal_sipi,
                        db_master.mst_izin_daerah.tanggal_akhir_sipi,
                        db_master.mst_jenis_kapal.nama_jenis_kapal,
                        db_master.mst_bahan_kapal.nama_bahan_kapal,
                        db_master.mst_alat_tangkap.nama_alat_tangkap, 
                        db_master.mst_perusahaan_daerah.nama_penanggung_jawab, 
                        db_master.mst_perusahaan_daerah.nama_perusahaan
                    FROM db_master.mst_kapal_daerah
                    LEFT OUTER JOIN (db_master.mst_perusahaan_daerah, db_master.mst_alat_tangkap, db_master.mst_izin_daerah, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal_daerah.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal_daerah.id_perusahaan_daerah = db_master.mst_perusahaan_daerah.id_perusahaan_daerah 
                            AND db_master.mst_izin_daerah.id_kapal_daerah = db_master.mst_kapal_daerah.id_kapal_daerah
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal_daerah.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal_daerah.id_bahan_kapal
                            AND db_master.mst_izin_daerah.id_izin_daerah = $id_izin
                        ) 
                    WHERE mst_kapal_daerah.id_kapal_daerah = $id_kapal";
                break;
        }



        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }  

    public function cari_kapal($filter, $search_like)
    {

        switch ($filter) {
            
            case 'pusat':
                $sql = "SELECT 
                        db_master.mst_kapal.id_kapal AS id, 
                        db_master.mst_kapal.nama_kapal,
                        db_master.mst_izin.id_izin,
                        db_master.mst_izin.no_sipi, 
                        db_master.mst_izin.tanggal_sipi,
                        db_master.mst_izin.tanggal_akhir_sipi,
                        db_master.mst_perusahaan.id_perusahaan
                    FROM db_master.mst_kapal
                    LEFT JOIN (db_master.mst_perusahaan, db_master.mst_alat_tangkap, db_master.mst_izin, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal.id_alat_tangkap_perizinan = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal.id_perusahaan = db_master.mst_perusahaan.id_perusahaan 
                            AND db_master.mst_izin.id_kapal = db_master.mst_kapal.id_kapal
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal.id_bahan_kapal
                        ) 
                    WHERE mst_kapal.nama_kapal LIKE '%".$search_like."%' ";
                break;
                
            case 'daerah':
                $sql = "SELECT 
                        db_master.mst_kapal_daerah.id_kapal_daerah AS id, 
                        db_master.mst_kapal_daerah.nama_kapal,
                        db_master.mst_izin_daerah.id_izin_daerah, 
                        db_master.mst_izin_daerah.no_sipi, 
                        db_master.mst_izin_daerah.tanggal_sipi,
                        db_master.mst_izin_daerah.tanggal_akhir_sipi,
                        db_master.mst_perusahaan_daerah.id_perusahaan_daerah
                    FROM db_master.mst_kapal_daerah
                    LEFT JOIN (db_master.mst_perusahaan_daerah, db_master.mst_alat_tangkap, db_master.mst_izin_daerah, db_master.mst_jenis_kapal, db_master.mst_bahan_kapal)
                    ON (    db_master.mst_kapal_daerah.id_alat_tangkap = db_master.mst_alat_tangkap.id_alat_tangkap
                            AND db_master.mst_kapal_daerah.id_perusahaan_daerah = db_master.mst_perusahaan_daerah.id_perusahaan_daerah 
                            AND db_master.mst_izin_daerah.id_kapal_daerah = db_master.mst_kapal_daerah.id_kapal_daerah
                            AND db_master.mst_jenis_kapal.id_jenis_kapal = db_master.mst_kapal_daerah.id_jenis_kapal
                            AND db_master.mst_bahan_kapal.id_bahan_kapal = db_master.mst_kapal_daerah.id_bahan_kapal
                        ) 
                    WHERE mst_kapal_daerah.nama_kapal LIKE '%".$search_like."%' ";
                    
                break;
        }

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($filter, $data)
    {
        switch ($filter) {
            case 'pusat':
                $this->db_kapi->insert('mst_kapal', $data);
                break;
            
            case 'daerah':
                $this->db_kapi->insert('mst_kapal_daerah', $data);
                break;
        }
    }

}