<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_wilayah extends CI_Model
{
    private $db_kapi;
	private $db_dss;

    function __construct()
    {
        $this->db_kapi = $this->load->database('default', TRUE);
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

    public function list_negara()
    {
        $query = "SELECT id_negara AS id, nama_negara as text FROM mst_negara";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_propinsi()
    {
        $query = "SELECT id_propinsi AS id, nama_propinsi as text FROM mst_propinsi";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }


    public function list_kab_kota()
    {
        $query = "SELECT id_kabupaten_kota AS id, nama_kabupaten_kota as text FROM mst_kabupaten_kota";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_tempat_pembangunan()
    {
        $query = "SELECT 
                        uni.nama AS ID, CONCAT(uni.nama, ' , ', uni.tingkat) AS TEXT
                    FROM
                        (SELECT 
                            nama_desa AS nama, 'desa' AS tingkat
                        FROM
                            mst_desa_pembangunan union
                        SELECT 
                            nama_kecamatan AS nama, 'kecamatan' AS tingkat
                        FROM
                            mst_kecamatan_pembangunan union
                        SELECT 
                            nama_kabupaten_kota AS nama, 'kabkota' AS tingkat
                        FROM
                            mst_kabupaten_kota union 
                        SELECT 
                            nama_negara AS nama, 'negara' AS tingkat
                        FROM
                            db_master.mst_negara) uni
                    WHERE
                        uni.nama LIKE '%pasir%'";

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function cari_tempat_pembangunan($search_like)
    {
        $sql = "SELECT 
                    uni.nama AS id_tempat, CONCAT(uni.nama, ' , ', uni.tingkat) AS nama_tempat
                FROM
                    (SELECT 
                        nama_desa AS nama, 'desa' AS tingkat
                    FROM
                        mst_desa_pembangunan union
                    SELECT 
                        nama_kecamatan AS nama, 'kecamatan' AS tingkat
                    FROM
                        mst_kecamatan_pembangunan union
                    SELECT 
                        nama_kabupaten_kota AS nama, 'kabkota' AS tingkat
                    FROM
                        db_master.mst_kabupaten_kota union 
                    SELECT 
                        nama_negara AS nama, 'negara' AS tingkat
                    FROM
                        db_master.mst_negara) uni
                WHERE
                    uni.nama LIKE '%".$search_like."%' ";

        $run_query = $this->db_kapi->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;

    }

}