<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_alat_tangkap extends CI_Model
{
	private $db_dss;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
    }

	public function list_opsi()
    {
    	$query = "SELECT id_alat_tangkap AS id, nama_alat_tangkap as text FROM mst_alat_tangkap where aktif = 'YA' ";
    	$run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}