<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_perusahaan extends CI_Model
{
    private $db_dss;
	private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

	public function list_perusahaan($limit = '100')
    {
        $query = '  SELECT *
                    FROM db_master.mst_perusahaan
                    LIMIT 0, '.$limit.' ';
        
        $run_query = $this->db_dss->query($query);

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_perusahaan_daerah($limit = '100')
    {
        $query = '  SELECT *
                    FROM db_master.mst_perusahaan_daerah
                    LIMIT 0, '.$limit.' ';

        $run_query = $this->db_dss->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detil_perusahaan($filter, $id)
    {
        switch ($filter) {
            
            case 'pusat':
                $sql = "SELECT *
                            FROM db_master.mst_perusahaan
                            WHERE mst_perusahaan.id_perusahaan = $id ";
                break;
                
            case 'daerah':
                $sql = "SELECT *
                            FROM db_master.mst_perusahaan_daerah
                            WHERE mst_perusahaan_daerah.id_perusahaan_daerah = $id ";
                    
                break;
        }

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    /*public function cari_perusahaan($filter, $search_like)
    {
        switch ($filter) {
            
            case 'pusat':
                $sql = "SELECT *
                            FROM db_master.mst_perusahaan
                            WHERE mst_perusahaan.nama_perusahaan LIKE '%".$search_like."%' ";
                break;
                
            case 'daerah':
                $sql = "SELECT *
                            FROM db_master.mst_perusahaan_daerah
                            WHERE mst_perusahaan_daerah.nama_perusahaan LIKE '%".$search_like."%' ";
                    
                break;
        }

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }*/

    public function cari_perusahaan($filter, $search_like, $search_filter)
    {

        switch ($filter) {
            case 'data':
                $sql = "SELECT *
                            FROM db_master.mst_perusahaan
                            WHERE mst_perusahaan.".$search_filter." LIKE '%".$search_like."%' ";
                break;
                
            case 'count':
                $sql = "SELECT count(db_master.mst_perusahaan.nama_perusahaan) as jumlah
                            FROM db_master.mst_perusahaan
                            WHERE mst_perusahaan.".$search_filter." LIKE '%".$search_like."%' ";
                    
                break;
        }

        $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function input($filter, $data)
    {
        switch ($filter) {
            case 'pusat':
                $this->db_kapi->insert('mst_perusahaan', $data);
                break;
            
            case 'daerah':
                $this->db_kapi->insert('mst_perusahaan_daerah', $data);
                break;
        }
    }

}