<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_wilayah extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			

		}

	public function list_negara_json()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_negara();

		echo json_encode($list_opsi);
	}

	public function list_negara_array()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_negara();

		// vdump($list_opsi, false);

		return $list_opsi;
	}

	public function list_propinsi_json()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_propinsi();

		echo json_encode($list_opsi);
	}

	public function list_propinsi_array()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_propinsi();

		return $list_opsi;
	}

	public function list_kab_kota_json()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_kab_kota();

		echo json_encode($list_opsi);
	}


	public function list_kab_kota_array()
	{
		$this->load->model('mdl_wilayah');

		$list_opsi = $this->mdl_wilayah->list_kab_kota();

		return $list_opsi;
	}

	public function search_tempat_pembangunan()
	{
		$this->load->model('mdl_wilayah');
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_wilayah->cari_tempat_pembangunan($get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);
		
		echo json_encode($json);
	}

	public function pilih_tempat_pembangunan($marking = 0,$nama_tempat)
	{
		$data['nama_tempat'] = $nama_tempat;
		$this->load->view('pilih_tempat_pembangunan', $data);
	}

}
?>