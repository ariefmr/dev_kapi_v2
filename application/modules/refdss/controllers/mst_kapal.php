<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mst_kapal extends MX_Controller {

	function __construct()
		{
			parent::__construct();
			
			$this->load->model('mdl_refdss_kapal');
		}

	public function index()
	{
		$this->kapal();

	}

	public function kapal( $filter = '' )
	{
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		if(($filter === 'daerah')){
			$labels = 'label_view_kapal_daerah';
			$data['list_kapal'] = $this->mdl_refdss_kapal->list_kapal_daerah();
		}else{
			$labels = 'label_view_kapal';
			$data['list_kapal'] = $this->mdl_refdss_kapal->list_kapal();
		}
			echo Modules::run('templates/page/v_form', 'refdss', 'tabel_kapal_refdss', $labels, $add_js, $add_css, $data);

	}

	public function detail_kapal()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_id_kapal = $this->input->get('ik', FALSE);
		$get_id_izin = $this->input->get('iz', FALSE);
		$detail_result = $this->mdl_refdss_kapal->detil_kapal($get_filter, $get_id_kapal, $get_id_izin);

		echo json_encode($detail_result);

	}

	public function search_kapal()
	{
		$get_filter = $this->input->get('i', FALSE);
		$get_search_like = $this->input->get('q', FALSE);
		$search_result = $this->mdl_refdss_kapal->cari_kapal($get_filter, $get_search_like);
		$jml_search_result = count($search_result);
		if(!$search_result){
			$jml_search_result = 0;
		}
		$json = array( 
					'filter' => $get_filter, 
					'search_like' => $get_search_like,
					'jumlah_result' => $jml_search_result,
					'result' => $search_result
					);

		echo json_encode($json);
	}



}