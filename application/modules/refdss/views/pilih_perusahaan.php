<!-- TAMPIL DATA -->
<!-- <div id="panel-pilih-perusahaan" class="panel">
  <div class="panel-body"> -->
    

    <div class="row">
      <div class="col-sm-3 text-right" style=" padding-right: 15px;">
          <strong>Perusahaan</strong> 
      </div>  

      <div class="col-lg-2">
         <!--  <button id="toggle_gt" type="button" class="btn btn-primary" data-current-gt="below" data-toggle="button">&#8804 30 GT</button>-->
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              Cari <text id="info_filter">Nama Perusahaan  </text>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="#" class="ubah_filter" data-this-filter="nama_perusahaan">Nama Perusahaan</a></li>
              <li><a href="#" class="ubah_filter" data-this-filter="nama_penanggung_jawab">Penanggung Jawab</a></li>
              <li><a href="#" class="ubah_filter" data-this-filter="no_siup">Nomor SIUP</a></li>
            </ul>
          </div>

      </div>
      <div class="col-lg-6">
         <!-- <input id="start_search_perusahaan" name="id_perusahaan" type="hidden" class="bigdrop"> -->
         <!-- <input  id="inputNama_perusahaan" name="nama_perusahaan" type="hidden" class="form-control"> -->
         <input type="hidden" id="start_search_perusahaan" data-init-text="<?=$nama_perusahaan?>" value="<?=$nama_perusahaan?>" class="bigdrop">
         <input type="hidden" id="id_perusahaan" name="id_perusahaan" data-init-text="<?=$id_perusahaan?>" value="<?=$id_perusahaan?>" class="bigdrop" />
         <input type="hidden" id="inputNama_perusahaan" name="nama_perusahaan" data-init-text="<?=$nama_perusahaan?>" value="<?=$nama_perusahaan?>" class="bigdrop" />
         <input type="hidden" id="inputNama_penanggung" name="nama_penanggung_jawab" value="<?=$nama_penanggung_jawab?>" class="bigdrop" />
         <input type="hidden" id="inputKtp_penanggung" name="no_identitas_penanggung_jawab" value="<?=$no_identitas_penanggung_jawab?>" class="bigdrop" />
         <input type="hidden" id="inputTtl_penanggung" name="ttl_penanggung_jawab" value="<?=$ttl_penanggung_jawab?>" class="bigdrop" />
         <input type="hidden" id="inputAlamat_perusahaan" name="alamat_perusahaan" value="<?=$alamat_perusahaan?>" class="bigdrop" />
      </div>

          
    </div>
    <br>

    <?php
    $is_admin = $this->mksess->info_is_admin();
    if( $is_admin ){
          $disabled = FALSE;

          $attr_nama_perusahaan = array( 'name' => 'nama_perusahaan',
                                        'label' => 'Nama Perusahaan',
                                        'value' => $nama_perusahaan,
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);

          $attr_nama_perusahaan = array( 'name' => 'nama_penanggung_jawab',
                                        'label' => 'Nama Penanggung Jawab',
                                        'value' => $nama_penanggung_jawab,
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);
    
          $attr_nama_perusahaan = array( 'name' => 'no_identitas_penanggung_jawab',
                                        'label' => 'No Identitas Penanggung Jawab',
                                        'value' => $no_identitas_penanggung_jawab,
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);

          $attr_nama_perusahaan = array( 'name' => 'ttl_penanggung_jawab',
                                        'label' => 'TTL Penanggung Jawab',
                                        'value' => $ttl_penanggung_jawab,
                                        'disabled' => $disabled
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);

          $attr_nama_perusahaan = array( 'name' => 'alamat_perusahaan',
                                        'label' => 'Alamat Perusahaan',
                                        'value' => $alamat_perusahaan,
                                        'disabled' => $disabled,
                                        'rows' => 3
                    );
          echo $this->mkform->input_textarea($attr_nama_perusahaan);
    }else{
          $disabled = TRUE;
          // $disabled = ($detail_pendok['status_pendok'] === 'FINAL') ? TRUE : FALSE;
          $attr_nama_perusahaan = array( 'name' => 'inputNama_penanggung',
                                      'label' => 'Nama Penanggung Jawab',
                                      'value' => $nama_penanggung_jawab,
                                      'disabled' => true
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);
    
          $attr_nama_perusahaan = array( 'name' => 'inputKtp_penanggung',
                                        'label' => 'No Identitas Penanggung Jawab',
                                        'value' => $no_identitas_penanggung_jawab,
                                        'disabled' => true
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);
    
          $attr_nama_perusahaan = array( 'name' => 'inputTtl_penanggung',
                                        'label' => 'TTL Penanggung Jawab',
                                        'value' => $ttl_penanggung_jawab,
                                        'disabled' => true
                    );
          echo $this->mkform->input_text($attr_nama_perusahaan);

          $attr_nama_perusahaan = array( 'name' => 'inputAlamat_perusahaan',
                                        'label' => 'Alamat Perusahaan',
                                        'value' => $alamat_perusahaan,
                                        'disabled' => true,
                                        'rows' => 3
                    );
          echo $this->mkform->input_textarea($attr_nama_perusahaan);
          }
    ?>


  <!-- </div>
</div> -->


<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var search_response_time = 2000, //2 Detik
    thread = null,
    url_search_perusahaan = "<?php echo base_url('refdss/mst_perusahaan/search_perusahaan'); ?>",
    current_gt = function(){ return $("#toggle_gt").data("currentGt") },
    current_filter = 'nama_perusahaan';

  function formatListperusahaanResult(perusahaan)
  {
    html = "<table id='"+perusahaan.id_perusahaan+"' class='table table-condensed table-bordered'><tr>"
       + "<td>"+perusahaan.nama_perusahaan+"</td>"
       + "<td title='"+perusahaan.no_identitas_penanggung_jawab+"'>"+perusahaan.nama_penanggung_jawab+"</td>"
       + "<td>"+perusahaan.alamat_perusahaan+"</td>"
       + "<td title='"+perusahaan.tanggal_siup+"'>"+perusahaan.no_siup+"</td>"
       + "</tr></table>"
    return  html;

  }

  function set_data_perusahaan(perusahaan)
  {
    var is_admin = "<?php echo $this->mksess->info_is_admin(); ?>";
    console.log(is_admin);
    console.dir(perusahaan);

    document.getElementById('start_search_perusahaan').onclick = function(){

      if(is_admin == 1){

        $("#id_perusahaan").val(perusahaan.id_perusahaan);
        $("#nama_perusahaan").val(perusahaan.nama_perusahaan);
        $("#nama_penanggung_jawab").val(perusahaan.nama_penanggung_jawab);
        $("#no_identitas_penanggung_jawab").val(perusahaan.no_identitas_penanggung_jawab);
        $("#ttl_penanggung_jawab").val(perusahaan.ttl_penanggung_jawab);
        $("#alamat_perusahaan").val(perusahaan.alamat_perusahaan);

        $("#id_nama_perusahaan").val(perusahaan.nama_perusahaan);
        $("#id_nama_penanggung_jawab").val(perusahaan.nama_penanggung_jawab);
        $("#id_no_identitas_penanggung_jawab").val(perusahaan.no_identitas_penanggung_jawab);
        $("#id_ttl_penanggung_jawab").val(perusahaan.ttl_penanggung_jawab);
        $("#id_alamat_perusahaan").val(perusahaan.alamat_perusahaan);

      }else{

        $("#id_perusahaan").val(perusahaan.id_perusahaan);
        $("#inputNama_perusahaan").val(perusahaan.nama_perusahaan);
        $("#inputNama_penanggung").val(perusahaan.nama_penanggung_jawab);
        $("#inputKtp_penanggung").val(perusahaan.no_identitas_penanggung_jawab);
        $("#inputTtl_penanggung").val(perusahaan.ttl_penanggung_jawab);
        $("#inputAlamat_perusahaan").val(perusahaan.alamat_perusahaan);

        $("#id_inputNama_penanggung").val(perusahaan.nama_penanggung_jawab);
        $("#id_inputKtp_penanggung").val(perusahaan.no_identitas_penanggung_jawab);
        $("#id_inputTtl_penanggung").val(perusahaan.ttl_penanggung_jawab);
        $("#id_inputAlamat_perusahaan").val(perusahaan.alamat_perusahaan);

      }

    }
    
    // $("#inputNama_perusahaan").val(perusahaan.nama_perusahaan);
    // $("#inputNama_penanggung").val(perusahaan.nama_penanggung_jawab);
    // $("#inputKtp_penanggung").val(perusahaan.no_identitas_penanggung_jawab);
    // $("#inputTtl_penanggung").val(perusahaan.ttl_penanggung_jawab);
    // $("#inputAlamat_perusahaan").val(perusahaan.alamat_perusahaan);
    
  }

  function formatListperusahaanSelection(perusahaan)
  {
    set_data_perusahaan(perusahaan);
    return perusahaan.nama_perusahaan;
  }

  function formatSearchingText(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangText(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
      info_filter = $("#info_filter").text();
      info_gt = $("#toggle_gt").text();
      text_info = "Pencarian perusahaan "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

      text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFound(term)
  {
    // var link_daftar = "";
    //   if(current_gt()  === "below")
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/perusahaan/add/" ;
    //     }else
    //     {
    //       link_daftar = "http://integrasi.djpt.kkp.go.id/izin-daerah/perusahaan/add/" ;
    //     }
     
    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. Perusahaan Belum Terdaftar?";
  }


  function searchURLGenerator()
  {

  }

  function formatinisial(element,callback)
  {
      var elementText = $(element).attr('data-init-text');
      callback({"nama_perusahaan":elementText});
  }

  $(document).ready( function () {
    
    // $("#toggle_gt").click(function(){
    //   var currentGt = $(this).data("currentGt");
    //     if(currentGt === "below")
    //     {
    //       $(this).text("> 30 GT");
    //       $(this).data("currentGt","above");
          
    //     }else
    //     {
    //       $(this).html("&#8804 30 GT");
    //       $(this).data("currentGt","below");      
    //     }
    //     $("#start_search_perusahaan").select2("open");
    // });

    $(".ubah_filter").click(function(){
      var thisFilter = $(this).data("thisFilter"),
        thisText = $(this).text();

        $("#info_filter").text(thisText);
        current_filter = thisFilter;
        $("#start_search_perusahaan").select2("open");
    });

    $("#start_search_perusahaan").select2({
                  id: function(e) { return e.id_perusahaan },
                  allowClear: false,    
                  placeholder: "Pilih perusahaan..",
                  width: "100%",
                  cache: true,
                  minimumInputLength: 3,
                                    dropdownCssClass: "bigdrop",
                                    ajax: {
                          url: url_search_perusahaan,
                          dataType: "json",
                          quietMillis: 2000,
                          data: function(term, page){
                                         return {
                                  q: term,
                                  i: 'data',
                                  filter: current_filter,
                                  limit: 100 // TODO : tentuin limit result
                                     };
                      },
                      results: function(data, page){
                                   return {results: data.result};
                          }
                  },
                                    formatResult: formatListperusahaanResult,
                                    formatSelection: formatListperusahaanSelection,
                                    formatSearching: formatSearchingText,
                                    formatInputTooShort: formatKurangText,
                                    formatNoMatches: formatNotFound,
                                    initSelection: formatinisial
                                    });

    $("#start_search_perusahaan").on("change",function(e) { 
                    //console.log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));
                      // get_detail_perusahaan(e.val, current_gt());
                      var nama_perusahaan_chosen = $("span.select2-chosen").text();
                      $("#inputNama_pemilik").val(nama_perusahaan_chosen);
                    });
    /*

     ajax: {
                                          url: "<?php echo base_url('mst_perusahaan/json_perusahaan'); ?>",
                                          dataType: "jsonp",
                                          data: function(term, page){
                                            return {
                                              q: "term",
                                              limit: 100 // TODO : tentuin limit result
                                            };
                                          },
                                          results: function(data, page){
                                            return {results: data}
                                          }
    $('#start_search_perusahaan').keyup(function(e){
      clearTimeout(thread);
      
      var keyword = $(this).val();

      thread = setTimeout(function(){
        update_result_perusahaan(keyword);
      } ,search_response_time);
    });
    */  
  } );
</script>