<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_perusahaan' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_perusahaan){
		foreach ($list_perusahaan as $item) {
			$this->table->add_row(
								$counter.'.',
								$item->nama_perusahaan,
								$item->no_siup,
								$item->tanggal_siup,
								$item->no_npwp,
								$item->nama_penanggung_jawab,
								$item->no_identitas_penanggung_jawab,
								$item->alamat_perusahaan
								);
			$counter++;
		}
	}

	$table_list_kapal = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_perusahaan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>