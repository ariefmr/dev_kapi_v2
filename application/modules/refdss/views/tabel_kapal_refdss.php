<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_kapal' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_kapal){
		foreach ($list_kapal as $item) {
			$this->table->add_row(
								$counter.'.',
								$item->nama_kapal,
								$item->nama_perusahaan.'<br>/<br>'.$item->nama_penanggung_jawab,
								$item->nama_kapal_sebelumnya,
								$item->no_sipi,
								$item->tanggal_sipi.'<br>s/d<br>'.$item->tanggal_akhir_sipi,
								$item->gt_kapal.'<br>&<br>'.$item->nt_kapal, 
								'P: '.$item->panjang_kapal.'<br>L: '.$item->lebar_kapal.'<br>D: '.$item->dalam_kapal,
								$item->nama_jenis_kapal,
								$item->nama_bahan_kapal,
								$item->no_mesin.' <br> '.$item->merek_mesin,
								$item->nama_alat_tangkap
								);
			$counter++;
		}
	}

	$table_list_kapal = $this->table->generate();
?>

<!-- TAMPIL DATA -->
		<?php
			echo $table_list_kapal;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_daftar_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>