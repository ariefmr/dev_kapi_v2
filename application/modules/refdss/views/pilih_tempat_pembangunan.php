<!-- TAMPIL DATA -->
    <div id="panel-pilih-tempat2" style="margin-left:12px; padding-bottom:15px;" >
        
        
        <div class="row">
            <div class="col-sm-3 text-right" style=" padding-right: 25px;">
              <strong>Tempat Pembangunan</strong> 
            </div>
            <div class="col-sm-8" style="margin-left: 5px;padding-left: 0px;padding-right: 20px;">
               <!-- <input id="form_tempat_pembangunan"  type="text" name="form_tempat_pembangunan" class="bigdrop" /> -->
               <input type="hidden" id="form_tempat_pembangunan" data-init-text="<?=$nama_tempat?>" value="<?=$nama_tempat?>" class="bigdrop">
               <input type="hidden" name="tempat_pembangunan" data-init-text="<?=$nama_tempat?>" value="<?=$nama_tempat?>" id="id_tempat_pembangunan" class="bigdrop" />
               <input id="tempat_pembangunan"  type="hidden" name="tempat_pembangunan" class="bigdrop" />
            </div>
        </div>
    
    </div>


<SCRIPT type="text/javascript">
  var search_response_time = 2000, //2 Detik
  thread = null,
  url_search_tempat_by_name = "<?php echo base_url('refdss/mst_wilayah/search_tempat_pembangunan'); ?>";

  function formatListTempatResultNama(tempat)
  {
    if(tempat.nama_tempat == ''){
      html = "<table class='table table-condensed table-bordered' ><tr><td>"+tempat.nama_tempat+"</td></tr></table>"
    }else
    {
      html = "<table class='table table-condensed table-bordered' ><tr>"
      + "<td width='30%' >"+tempat.id_tempat+"</td>"
      + "<td width='30%' >"+tempat.nama_tempat+"</td>"
      + "</tr></table>"
    }
    return  html;

  }

  function set_data_Tempat(tempat)
  {
   
    console.dir(tempat);
    
    $("#tempat_pembangunan").val(tempat.id_tempat);

    document.getElementById('form_tempat_pembangunan').onclick = function(){
      $("#id_tempat_pembangunan").val(tempat.id_tempat);
    }
    
  }

  function formatListTempatSelectionNama(tempat)
  {
    /*set_history(tempat.id_tempat,tempat.id_bkp);*/
    set_data_Tempat(tempat);
    return tempat.nama_tempat;
  }

  function formatSearchingTextTempat(term)
  {
    return "Sedang mencari..";
  }

  function formatKurangTextTempat(term, minLength)
  {
    var char_yang_kurang = minLength - term.length,
    info_filter = $("#info_filter").text();
    info_gt = $("#toggle_gt").text();
    text_info = "Pencarian tempat "+info_gt+" berdasarkan <strong>"+info_filter+"</strong>. ";

    text_info += "Input minimal "+char_yang_kurang+" karakter";  
    return text_info;
  }

  function formatNotFoundTempat(term)
  {

    return "Pencarian <strong>"+term+"</strong> tidak menemukan hasil. tempat Belum Terdaftar?";
  }

  function formatinisialTempat(element,callback)
  {
      var elementText = $(element).attr('data-init-text');
      callback({"nama_tempat":elementText});
  }

  var search_by_tempat = function (){
    $("#form_tempat_pembangunan").select2({
      id: function(e) { return e.id_tempat },
      allowClear: false,    
      placeholder: "Pilih Tempat Pembangunan..",
      width: "60%",
      cache: true,
      minimumInputLength: 3,
      dropdownCssClass: "bigdrop",
      ajax: {
        url: url_search_tempat_by_name,
        dataType: "json",
        quietMillis: 2000,
        data: function(term, page){
                         return {
                          q: term
                        };
                      },
                              results: function(data, page){
                               return {results: data.result};
                             }
                           },
                           formatResult: formatListTempatResultNama,
                           formatSelection: formatListTempatSelectionNama,
                           formatSearching: formatSearchingTextTempat,
                           formatInputTooShort: formatKurangTextTempat,
                           formatNoMatches: formatNotFoundTempat,
                           initSelection: formatinisialTempat
                         });

}

   s_func.push(search_by_tempat);

</SCRIPT>