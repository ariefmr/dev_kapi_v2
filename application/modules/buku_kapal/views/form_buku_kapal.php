<?php
// vdump($detail_buku_kapal, true);
    $hidden_input = array(
                          'id_pendok'    => $detail_buku_kapal['id_pendok']);
    if($aksi === 'view'){
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group col-lg-10">
          </div>
          <div class="form-group col-lg-2">
              <div class="col-sm-offset-2 col-sm-4">
<?php 
             $link_aksi = '<a class="btn btn-primary" href="'.base_url('buku_kapal/main/entry/'.$detail_buku_kapal['id_pendok']).'">'.$button.'</a>';
              echo $link_aksi; 
?>
            </div>
          </div>
  </div>
</div>
<div class="row">
    <div class="col-lg-12">

<?php
        echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);
    }else{
?>

<div class="row">
    <div class="col-lg-12">

<?php
        echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
    }

    /* nama_kapal */
    $attr_nama_kapal = array(   'name' => $form['nama_kapal']['name'],
                                'label' => $form['nama_kapal']['label'],
                                'value' => kos($detail_buku_kapal['nama_kapal']),
                                'disabled' => true
                    );
    echo $this->mkform->input_text($attr_nama_kapal);

    /* no_register */
    $no_register = array(   'name' => $form['no_register']['name'],
                            'label' => $form['no_register']['label'],
                            'value' => noreg($detail_buku_kapal['no_register']),
                            'disabled' => true
                    );
    echo $this->mkform->input_text($no_register);

    /* no_seri_bkp */
    $no_seri_bkp = array(   'name' => $form['no_seri_bkp']['name'],
                                'label' => $form['no_seri_bkp']['label'],
                                'value' => kos($detail_buku_kapal['no_seri_bkp'])
                    );
    echo $this->mkform->input_text($no_seri_bkp);

    /* no_tanda_pengenal */
    //sementara bisa di edit karena belum dihandle jika kapal jenis kapal angkut
    $no_tanda_pengenal = array(   'name' => $form['no_tanda_pengenal']['name'],
                                'label' => $form['no_tanda_pengenal']['label'],
                                'value' => kos($detail_buku_kapal['no_tanda_pengenal']),
                                'disabled' => false
                    );
    echo $this->mkform->input_text($no_tanda_pengenal);

    /* keterangan_bkp */
    $keterangan_bkp = array(   'name' => $form['keterangan_bkp']['name'],
                                'label' => $form['keterangan_bkp']['label'],
                                'value' => kos($detail_buku_kapal['keterangan_bkp'])
                    );
    echo $this->mkform->input_text($keterangan_bkp);

    /* id_pengguna_buat */
    $id_pengguna_buat = array(   'name' => $form['id_pengguna_buat']['name'],
                                'label' => $form['id_pengguna_buat']['label'],
                                'value' => kos($detail_buku_kapal['id_pengguna_buat']),
                                'disabled' => true
                    );
    // echo $this->mkform->input_text($id_pengguna_buat);
    /* nama_kasie */
    if(($aksi == "entry")||($aksi == "edit"))
    {
      ?>
      <div class="col-lg-10" style="margin-left:110px;" >
        <?php
          $nama_pejabat = $detail_buku_kapal['nama_kasie'];
          $id_pejabat   = $detail_buku_kapal['kasie_id'];
          echo Modules::run('refkapi/mst_pejabat/pilih_pejabat',"Kasie",0,$nama_pejabat,$id_pejabat);
        ?>
      </div>
      <?php
    }
    else
    {

      $attr_nama_kapal = array(   'name' => $form['nama_kasie']['name'],
                                'label' => $form['nama_kasie']['label'],
                                'value' => kos($detail_buku_kapal['nama_kasie'])
                    );

      echo $this->mkform->input_text($attr_nama_kapal);

    }

    /* nama_kasubdit */
    if(($aksi == "entry")||($aksi == "edit"))
    {
      ?>
      <div class="col-lg-10" style="margin-left:110px;" >
        <?php
          $nama_pejabat = $detail_buku_kapal['nama_kasubdit'];
          $id_pejabat   = $detail_buku_kapal['kasubdit_id'];
          echo Modules::run('refkapi/mst_pejabat/pilih_pejabat',"Kasubdit",0,$nama_pejabat,$id_pejabat);
        ?>
      </div>
      <?php
    }
    else
    {

      $attr_nama_kapal = array(   'name' => $form['nama_kasubdit']['name'],
                                'label' => $form['nama_kasubdit']['label'],
                                'value' => kos($detail_buku_kapal['nama_kasubdit'])
                    );
      echo $this->mkform->input_text($attr_nama_kapal);

    }
    
    /* nama_direktur */
    if(($aksi == "entry")||($aksi == "edit"))
    {
      ?>
      <div class="col-lg-10" style="margin-left:110px;" >
        <?php
          $nama_pejabat = $detail_buku_kapal['nama_direktur'];
          $id_pejabat   = $detail_buku_kapal['direktur_id'];
          echo Modules::run('refkapi/mst_pejabat/pilih_pejabat',"Direktur",1,$nama_pejabat,$id_pejabat);
        ?>
      </div>
      <?php
    }
    else
    {

      $attr_nama_kapal = array(   'name' => $form['nama_direktur']['name'],
                                'label' => $form['nama_direktur']['label'],
                                'value' => kos($detail_buku_kapal['nama_direktur'])
                    );
      echo $this->mkform->input_text($attr_nama_kapal);

    }

    /* nip_direktur */
    $nip_direktur = array(   'name'     => $form['nip_direktur_fake']['name'],
                                'label'    => $form['nip_direktur_fake']['label'],
                                'value'    => kos($detail_buku_kapal['nip_direktur']),
                                'disabled' => true
                    );
    // echo $this->mkform->input_text($nip_direktur);

    $nip_direktur = array(   'name'     => $form['nip_direktur_real']['name'],
                                'label'    => $form['nip_direktur_real']['label'],
                                'value'    => kos($detail_buku_kapal['nip_direktur'])
                    );
    echo $this->mkform->input_text($nip_direktur);


    $lokasi_pengesahan_id = array(  'opsi' => Modules::run('refdss/mst_wilayah/list_kab_kota_array'),
                                    'name' => 'lok_peng',
                                    'label' => $form['lokasi_pengesahan_id']['label'],
                                    'value' => $detail_buku_kapal['lokasi_pengesahan_id'].'|'.$detail_buku_kapal['lokasi_pengesahan']
                                    );
    echo $this->mkform->input_select2($lokasi_pengesahan_id);

    /* tgl_ttd_direktur */
    $tgl_ttd_direktur = array( 
                          // 'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                          // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                          'default_date' => kos($detail_buku_kapal['tgl_ttd_direktur']),
                          'placeholder' => '', // wajib ada atau '' (kosong)
                          'name' => $form['tgl_ttd_direktur']['name'], // wajib ada
                          'label' => $form['tgl_ttd_direktur']['label'] // wajib ada
                        );
    echo $this->mkform->input_date($tgl_ttd_direktur);

	?>
	</div>
</div>

<?php

if($aksi === 'view'){
        $link_aksi = '<a class="btn btn-primary" href="'.base_url('buku_kapal/main/entry/'.$detail_buku_kapal['id_pendok']).'">'.$button.'</a>';
    }else{
        $link_aksi = '<button type="submit" class="btn btn-primary btn-submit" >SIMPAN</button>';
    }/*else{
        $link_aksi = '<a class="btn btn-primary" href="'.base_url('kapal/functions/update/'.$detail_pendok['id_pendok']).'">SIMPAN</a>';
    }*/
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group col-lg-6">
          </div>
          <div class="form-group col-lg-4">
              <div class="col-sm-offset-2 col-sm-4">
<?php
              if(isset($cetak)){
                $link_cetak = '<a class="btn btn-warning" href="'.base_url('kapal/cetak/preview/'.$detail_buku_kapal['id_pendok']).'">CETAK DRAFT DATA KAPAL</a>';
                // echo $link_cetak; 
              }
?>
              </div>
          </div>
          <div class="form-group col-lg-2">
              <div class="col-sm-offset-2 col-sm-4">

<?php 
              echo $link_aksi; 
?>

            </div>
        </div>

    </div>
</div>

</form>
<script>
  var is_aksi = "<?php echo $aksi ?>";

  var cek_aksi = function(){
    if(is_aksi==='view'){
      $("input").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
      $(".col-sm-8").prepend(": ");
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }
  }
  $("input[name=nama_kapal]").prop("disabled", true);
  $("input[name=nama_kapal_sebelumnya]").prop("disabled", true);

  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  s_func.push(cek_aksi);
</script>