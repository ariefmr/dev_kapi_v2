
<div class="panel-heading">
  <h3 class="panel-title">Nama Kapal: <?php echo $detail_buku['nama_kapal']; ?></h3>
</div>
<div class="panel-heading">
  <h3 class="panel-title">No. Register: <?php echo $detail_buku['no_register']; ?></h3>
</div>
<div class="panel-heading">
  <h3 class="panel-title">Tipe Permohonan: <?php echo $detail_buku['tipe_permohonan']; ?></h3>
</div>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#halaman-1" data-toggle="tab">Perubahan Pemilik</a></li>
  <li><a href="#halaman-2" data-toggle="tab">Perubahan Fisik Kapal</a></li>
  <li><a href="#halaman-3" data-toggle="tab">Perubahan Tanda Pengenal</a></li>
  <li><a href="#halaman-6" data-toggle="tab">Perubahan Pemilik (Hal. Genap)</a></li>
  <li><a href="#halaman-10" data-toggle="tab">Fisik Kapal (Hal. Genap)</a></li>
  <li><a href="#halaman-14" data-toggle="tab">Tanda Pengenal (Hal. Genap)</a></li>
</ul>


<!-- Tab panes -->
<div class="tab-content">

  <div class="tab-pane active" id="halaman-1">
      <!-- frame menampilkan halaman tanda terima cetak_bkp -->
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_pemilik/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-2">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_fisik/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-3">
      <!-- frame menampilkan halaman tanda terima cetak_bkp -->
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_tanda_pengenal/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>

  <div class="tab-pane" id="halaman-6">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_pemilik_genap/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-10">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_fisik_genap/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-14">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal_tanda_pengenal_genap/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>

</div>