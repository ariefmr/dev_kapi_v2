<div class="panel-heading">
  <h3 class="panel-title">Nama Kapal: <?php echo $detail_buku['nama_kapal']; ?></h3>
</div>
<div class="panel-heading">
  <h3 class="panel-title">No. Register: <?php echo $detail_buku['no_register']; ?></h3>
</div>
<div class="panel-heading">
  <h3 class="panel-title">Tipe Permohonan: <?php echo $detail_buku['tipe_permohonan']; ?></h3>
</div>
<?php
  // vdump($detail_buku,true);
?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#halaman-1" data-toggle="tab">Halaman 1</a></li>
  <!-- <li><a href="#halaman-2" data-toggle="tab">Halaman 2</a></li> -->
  <li><a href="#halaman-2r" data-toggle="tab">Halaman 2</a></li>
  <li><a href="#halaman-3" data-toggle="tab">Halaman 3</a></li>
  <!-- <li><a href="#halaman-4" data-toggle="tab">Halaman 4</a></li> -->
  <li><a href="#halaman-4r" data-toggle="tab">Halaman 4</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="halaman-1">
      <!-- frame menampilkan halaman tanda terima cetak_bkp -->
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal1/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-2">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal2/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-2r">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal2_test/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-3">
      <!-- frame menampilkan halaman tanda terima cetak_bkp -->
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal3/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-4">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal4_test/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
  <div class="tab-pane" id="halaman-4r">
      <iframe src="<?php echo base_url('buku_kapal/cetak/hal4_potong/'.$id_pendok) ?>" width="100%" height="960">
        <p>Your browser does not support iframes.</p>
      </iframe>
  </div>
</div>