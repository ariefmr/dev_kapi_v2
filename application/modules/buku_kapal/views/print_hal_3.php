<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 19.5cm;
            min-height: 19.51cm;
            padding: 2cm;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
        .table {
            border: 5px black dotted;
        }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>

 <div class="book">
    <div class="page">
        <div class="subpage">

            <?php 
                $i = 0;
                while($i < 1){
                    echo '<br>';
                    $i++;               
                }
            ?>

            
            <table width="1000" class="table table-hover"    >
                
                <tbody>
                    <tr>
                        <tr>
                        <td width="32%" height="150"></td><!-- jarak -->
                        <td ></td><!-- jarak -->
                    </tr>
                    
                </tbody>
            </table>
            <table width="1000" class="table table-hover" border="0">
                <tbody>
                    <tr>
                        <td colspan="2" style="padding-left:350px"><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['tempat_grosse_akte'], '-'); ?></font></td>
                        <td><font style="font-size:0.9em;" face="arial"></font></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left:310px"><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['no_grosse_akte'], '-')."    "; ?><?php echo fmt_tgl(kos($detail_buku['tanggal_grosse_akte'], '-')); ?></font></td>
                        <td><font style="font-size:0.9em;" face="arial"></font></td>
                    </tr>
                    <tr>
                        <td width="57%"></td><!-- spasi -->
                        <td width="20%"><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['panjang_kapal'], '-'); ?></font></td>
                        <td><font style="font-size:0.9em;" face="arial">LoA = <?php echo kos($detail_buku['panjang_loa_kapal'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td><!-- spasi -->
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['lebar_kapal'], '-'); ?></font></td>
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['lebar_kapal'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td><!-- spasi -->
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['dalam_kapal'], '-'); ?></font></td>
                        
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['dalam_kapal'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td><!-- spasi -->
                        <td height="4"></td>
                    </tr>
                    <tr>
                        <td></td><!-- spasi -->
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['gt_kapal'], '-'); ?></font></td>                      
                    </tr>
                    <tr>
                        <td></td><!-- spasi -->
                        <td><font style="font-size:0.9em;" face="arial"><?php echo kos($detail_buku['nt_kapal'], '-'); ?></font></td>                      
                    </tr>
                </tbody>
            </table>
            
        </div>    
    </div>
</div>

