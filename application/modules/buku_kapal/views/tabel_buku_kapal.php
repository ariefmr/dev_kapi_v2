<?php

	//Olah data tampil untuk buku kapal
	$template = array( "table_open" 	=> 	"<table id='table_progres' class='table table-hover table-bordered'>"
					);
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_bkp_pendok){
		// var_dump($list_bkp_pendok);
		foreach ($list_bkp_pendok as $item) {
			$link_entry = '<a href="'.base_url('kapal/main/view/'.$item->id_pendok).'">'.$item->nama_kapal.'</a>';
			
			$btn_blm_entry = '<a class="btn btn-warning" href="'.base_url('kapal/main/entry/'.$item->id_pendok).'">PROSES ENTRY</a>';
			$btn_blm_bkp = '<a class="btn btn-warning" href="'.base_url('buku_kapal/main/entry/'.$item->id_pendok).'">PROSES BKP</a>';
			$btn_blm_ver = '<button OnClick="warning_validasi('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">PROSES VERIFIKASI</button>';
			$btn_sdh_ver = '<a class="btn btn-primary" href="'.base_url('buku_kapal/main/view/'.$item->id_pendok).'">VERIFIKASI</a>';
			$link_cetak = '<a class="btn btn-success" href="'.base_url('buku_kapal/cetak/preview_draft/'.$item->id_pendok).'">DRAFT BUKU</a>';
			
			// $btn_blm_terima = '<button OnClick="warning_validasi('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">BELUM</button>';
			// $btn_sdh_terima = '<a class="btn btn-primary" href="'.base_url('kapal/cetak/preview/'.$item->id_pendok).'">SUDAH</a>';

			switch ($item->status_entry_bkp) {
				case '0':
					$btn_verifikasi = $btn_blm_entry;
					break;

				case '1':
					$btn_verifikasi = $btn_blm_bkp;
					break;
				
				case '2':
					if($item->status_verifikasi === "YA"){
						$btn_verifikasi = $btn_sdh_ver;
					}else{
						$btn_verifikasi = $btn_blm_ver;
					}
					break;

				default:
					$btn_verifikasi = $btn_blm_entry;
					break;
			}

			if($item->kategori_pendaftaran === 'PUSAT'){
				$asal_lokasi = "";
			}elseif ($item->kategori_pendaftaran === 'PROPINSI') {
				$asal_lokasi = " | ".$item->propinsi_pendaftaran;
			}elseif ($item->kategori_pendaftaran === 'KAB/KOTA') {
				$asal_lokasi = " | ".$item->kabkota_pendaftaran;
			}

			$this->table->add_row(
								$counter.'.',
								noreg($item->no_rekam_pendok),
								noreg($item->no_register),
								$link_entry,
								$item->nama_penanggung_jawab,
								$item->gt,
								$item->tipe_permohonan,
								$item->kategori_pendaftaran.$asal_lokasi,
								$link_cetak,
								$btn_verifikasi
								/*$link_cetak,
								$btn_verifikasi*/
								/*,
								'<a href="" >Check</a>',
								'<a href="" >Check</a>'*/
								);
			$counter++;
		}
	}

	$table_buku_kapal = $this->table->generate();

?>

<p></p>
<!-- TAMPIL DATA -->
<div id="loading_info" class="panel panel-default">
	<div class="panel-body">
	   <p class="text-center">
	   		Memuat Data Buku Kapal. Harap Tunggu...
	   </p>
	</div>
</div>

<div id="table_container" class="panel-body overflowed hidden">
	<?php
		echo $table_buku_kapal;
	?>
</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- ADTTIONAL JAVASCRIPT -->
<script>
	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {



		$('#table_progres').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "iDisplayLength": 30,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
			}
		} );
	} );
</script>