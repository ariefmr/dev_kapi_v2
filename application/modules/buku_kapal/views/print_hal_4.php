<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 19.5cm;
            min-height: 19.51cm;
            padding: 2cm;
            margin: 1cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
        .table {
            border: 5px black dotted;
        }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>

 <div class="book">
    <div class="page">
        <div class="subpage">

            <?php 
                $i = 0;
                while($i < 1){
                    echo '<br>';
                    $i++;               
                }
            ?>

            
            <table width="1000" class="table table-hover" border="0">
                <tbody>
                    <tr>
                        <td height="100"></td>
                    </tr>
                    <tr>
                        <td width="40%" height="35"></td>
                        <td ><font face="arial"><?php echo kos($detail_buku['nama_perusahaan'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td height="35"><font face="arial"><?php echo kos($detail_buku['alamat_perusahaan'], '-'); ?></font></td>
                    </tr>
                </tbody>
            </table>
                    
            <table width="1000" class="table table-hover" >
                <tbody>
                    <tr>
                        <td width="54%" height="83"></td>
                        <td><font face="arial"><?php echo kos($detail_buku['no_telp_perusahaan'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td height="10"><font face="arial"><!-- JAKARTA 10160 --></font></td>
                    </tr>
                </tbody>
            </table>

            <table width="1000" class="table table-hover">
                <tbody>
                    <tr>
                        <td width="40%" height="20"></td>
                        <td ><font face="arial"><?php echo kos($detail_buku['nama_penanggung_jawab'], '-'); ?></font></td>
                    </tr>        
                    <tr>
                        <td></td>
                        <td  height="36"><font face="arial"><?php echo kos($detail_buku['ttl_penanggung_jawab'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td height="24"><font face="arial"><?php echo kos($detail_buku['no_identitas_penanggung_jawab'], '-'); ?></font></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td height="35"><font face="arial"><?php echo kos($detail_buku['perusahaan_sblm'], '-'); ?></font></td>
                    </tr>
                </tbody>
            </table>
                    
            

            <!-- <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p> -->
        </div>    
    </div>
</div>

