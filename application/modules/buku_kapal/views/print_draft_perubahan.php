<?php if ($is_preview): ?>
	<style type="text/css">
   body {
       margin: 0;
       padding: 0;
       background-color: #FAFAFA;
   }
   * {
       box-sizing: border-box;
       -moz-box-sizing: border-box;
   }
   .page {
       width: 21cm;
       min-height: 29.7cm;
       padding: 2cm;
       margin: 1cm auto;
       border: 1px #D3D3D3 solid;
       border-radius: 5px;
       background: white;
       box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
   }
   .subpage {
       padding: 1cm;
       border: 5px black dotted;
       height: 237mm;
       outline: 2cm #FFEAEA solid;
   }
   .table {
    border: 5px black dotted;
}

@page {
   size: A4;
   margin: 0;
}
</style>
<?php endif ?>

<br>
<table height="1000" width="100%" class="table table-hover" border="0">
	<tbody>
		<tr>
			<td colspan="4">
				<center><h4>Data-data kapal yang mengalami perubahan:</h4></center>
			</td>

		</tr>
	</tbody>
</table>

<hr>
<b>NAMA KAPAL     : <?=kos($verifikator['nama_kapal'])?></b>
<br>
<b>NO REGISTER    : <?=noreg($verifikator['no_register'])?></b>
<hr>
<br>

PERUBAHAN PEMILIK
<br>
--------------------------------
<table width="1000" class="table table-hover" border="0"   >

	<tbody>
		<tr>
			<td width="5%"></td>
			<td width="40%" ><font style="font-size:1em;"><?php echo $semula_pemilik.$perubahan_pemilik; ?></font></td>
		</tr>

	</tbody>
</table>
<br>
PERUBAHAN FISIK
<br>
---------------------------
<table width="1000" class="table table-hover" border="0"   >

	<tbody>
		<tr>
			<!-- <td ><font style="font-size:1.5em; letter-spacing:0.5cm;" face="britannic"><?php //echo "TES PEMILIK"; ?></font></td> -->
			<td width="5%"></td>
			<td width="40%" ><font style="font-size:1em;"><?php echo $semula_fisik.$perubahan_fisik; ?></font></td>
		</tr>

	</tbody>
</table>
<br>
PERUBAHAN TANDA PENGENAL
<br>
-----------------------------------------------
<table width="1000" class="table table-hover" border="0"   >

	<tbody>
		<tr>
			<!-- <td ><font style="font-size:1.5em; letter-spacing:0.5cm;" face="britannic"><?php //echo "TES PEMILIK"; ?></font></td> -->
			<td width="5%"></td>
			<td width="40%" ><font style="font-size:1em;"><?php echo $semula_tanda.$perubahan_tanda; ?></font></td>
		</tr>

	</tbody>
</table>
<br>
<table height="1000" width="100%" class="table table-hover" border="0">
	<tbody>
		<tr>
			<td colspan="4">
				<center><h4>VERIFIKATOR</h4></center>
			</td>

		</tr>
		<tr>
			<td style="text-align: right;">Kasie</td>
			<td></td>
			<td></td>
			<td>Kasubdit</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<?php 
				$i = 0;
				while($i < 1){
					echo '<br>';
					$i++;               
				}
				?>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="text-align: right;"><?=kos($verifikator['nama_kasie'])?></td>
			<td></td>
			<td></td>
			<td><?=kos($verifikator['nama_kasubdit'])?></td>
		</tr>
	</tbody>
</table>
