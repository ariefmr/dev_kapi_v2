<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 21cm;
	        min-height: 29.7cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>
<?php 
 	$tmpl = array ( 'table_open'  => '<table id="table_detail_buku">' );
    $this->table->set_template($tmpl);

    $this->table->add_row('Nama Kapal ',': '.kos($detail_buku['nama_kapal']));
    $this->table->add_row('Nama Kapal Sebelum ',': '.kos($detail_buku['nama_kapal_sblm']));
    $this->table->add_row('Tempat / Tahun Pembangunan ',': '.kos($detail_buku['tempat_pembangunan']).'/'.kos($detail_buku['tahun_pembangunan']));
    $this->table->add_row('Bahan Utama Kapal', ': '.kos($detail_buku['nama_bahan_kapal']));
    $this->table->add_row('Type/Jenis Kapal', ': '.kos($detail_buku['nama_jenis_kapal']));
    $this->table->add_row('Jenis Alat Penangkap Ikan', ': '.kos($detail_buku['nama_alat_tangkap']));
    $this->table->add_row('Merk dan Tipe Mesin Utama', ': '.kos($detail_buku['merek_mesin']).' '.kos($detail_buku['tipe_mesin']));
    $this->table->add_row('Daya Mesin Utama', ': '.kos($detail_buku['daya_kapal']).' '.kos($detail_buku['satuan_mesin_utama_kapal']));
    $this->table->add_row('No.Seri Mesin Utama', ': '.kos($detail_buku['no_mesin']));
    $this->table->add_row('Jumlah dan Kapasitas Palka', ': '.kos($detail_buku['jumlah_palka']).' '.kos($detail_buku['jumlah_palka']));

   
    $html = $this->table->generate();
    $this->table->clear();
    

 ?>
 <div class="book">
    <div class="page">
        <div class="subpage">
            <p><h3 align="center">No.Register <?=kos($detail_buku['no_register'])?></h3></p>

        	<?php echo $html; ?>
        	<?php 
        		$i = 0;
	        	while($i < 5){
	        		echo '<br>';
	        		$i++;        		
	        	}
        	?>
        </div>    
    </div>
</div>

