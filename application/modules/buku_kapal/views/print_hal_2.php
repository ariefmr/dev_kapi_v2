<!DOCTYPE html>
<head>
<?php if ($is_preview): ?>
    <style type="text/css">
        body{
            transform: rotate(0.5turn);
            -ms-transform:rotate(7deg); 
            -webkit-transform:rotate(0.5turn);
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;
        }
         
    </style>
<?php endif ?>
</head>
<body>
<div id="rotate">
    <div class="book">
        <div class="page">
            <div class="subpage" >

                <?php 
                $i = 0;
                while($i < 1){
                    echo '<br>';
                    $i++;               
                }
                ?>

                
                <table width="1000" class="table table-hover"    >
                    <tbody>
                        <tr>
                            <td height="44"></td><!-- kosong -->
                        </tr>
                        <tr>
                            <td width="40%"></td>
                            <?php

                                switch ($detail_buku['kategori_pendaftaran']) {
                                    case 'PUSAT':
                                        $tempat_pendaftaran = "KKP";
                                        break;

                                    case 'PROPINSI':
                                        $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
                                        break;
                                    
                                    case 'KAB/KOTA':
                                        $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                        break;

                                    default:
                                        $tempat_pendaftaran = "KKP";
                                        break;
                                }
                            ?>
                            <td ><font face="arial"><?php echo kos($tempat_pendaftaran, '-'); ?></font></td>
                        </tr>
                        
                    </tbody>
                </table>
                <table width="1000" class="table table-hover"  >
                    <tbody>
                        <tr>
                            <td width="40%"></td><!-- no tanda pengenal -->
                            <td><font face="arial"><?php echo kos($detail_buku['no_tanda_pengenal'], '-'); ?></font></td>
                        </tr>
                        <tr>
                            <td height="60"></td><!-- jarak -->
                            <td></td><!-- jarak -->
                        </tr>
                    </tbody>
                </table>
                <table class="table table-hover" width="1000">
                    <thead>
                        <tbody>
                            <tr>
                                <td width="56%"></td>
                                <?php

                                switch ($detail_buku['kategori_pendaftaran']) {
                                    case 'PUSAT':
                                        $tempat_pendaftaran = "JAKARTA";
                                        break;

                                    case 'PROPINSI':
                                        $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                        break;
                                    
                                    case 'KAB/KOTA':
                                        $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                        break;
                                }
                            ?>
                                <td><font face="arial"><?php echo kos($tempat_pendaftaran, '-') ?></font></td>
                            </tr>
                            <tr>
                                <td height="55"></td><!-- jarak -->
                                <td></td><!-- jarak -->
                            </tr>
                        </tbody>
                    </table>

                    <table width="1000" class="table table-hover" >
                        <tbody>
                            <tr>
                                <td width="48%"></td><!--  -->
                                <td><font face="arial"><?php echo kos($detail_buku['nama_direktur'], '-'); ?></font></td>
                            </tr>



                        </tbody>
                    </table>
                    <table width="1000" class="table table-hover" >
                        <tbody>
                            <tr>
                                <td width="52%"></td>
                                <td><font face="arial"><?php echo "NIP. ".kos($detail_buku['nip_direktur'], '-'); ?></font></td>
                            </tr>
                        </tbody>
                    </table>

                <!-- <p>
                    *) Lembar putih : Petugas KAPI
                    <br>
                    *) Lembar merah : Pemohon
                </p> -->
            </div>    
        </div>
    </div>
</div>
</body>
</html>