<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 19.5cm;
	        min-height: 19.51cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
        .table {
            border: 5px black dotted;
        }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>

 <div class="book">
    <div class="page">
        <div class="subpage">
            
            <table width="1000" class="table table-hover" border="0"   >
                
                <tbody>
                    <tr>
                        <td height="90"></td>
                    </tr>
                    <tr>
                        <td style="padding-left:120px" width="24%" valign="top"><font style="font-size:0.9em;" face="arial">1.</font></td>
                        <td width="30%" ><font style="font-size:0.7em;" face="arial"><?php echo $semula.$perubahan; ?></font></td>
                        <td style="padding-left:10px" width="44%" valign="top" ><font style="font-size:0.9em;" face="arial"><?php echo fmt_tgl($tgl_pengesahan); ?></font></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>    
    </div>
</div>

