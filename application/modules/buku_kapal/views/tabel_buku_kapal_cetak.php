<?php

	//Olah data tampil untuk buku kapal
	$template = array( "table_open" 	=> 	"<table id='table_progres' class='table table-hover table-bordered'>"
					);
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_bkp_pendok){
		// var_dump($list_bkp_pendok);
		foreach ($list_bkp_pendok as $item) {
			$link_entry = '<a href="'.base_url('buku_kapal/main/view/'.$item->id_pendok).'">'.$item->nama_kapal.'</a>';
			
			$btn_sdh_cetak = '<a class="btn btn-primary" href="'.base_url('buku_kapal/cetak/preview/'.$item->id_pendok).'">SUDAH CETAK</a>';
			$link_cetak = '<a class="btn btn-warning" href="'.base_url('buku_kapal/cetak/preview/'.$item->id_pendok).'">CETAK BUKU</a>';
			$btn_blm_cetak = '<button OnClick="warning_cetak('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">BELUM CETAK</button>';
			$btn_sdh_terima = '<a class="btn btn-primary" href="'.base_url('kapal/cetak/preview/'.$item->id_pendok).'">SUDAH</a>';


			if($item->status_cetak_bkp === "YA"){
				$btn_blm_terima = '<button OnClick="warning_terima('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">BELUM</button>';
				
				$status = $btn_sdh_cetak;

				if($item->status_terima_bkp === "YA"){
					$status_terima = $btn_sdh_terima;
				}else{
					$status_terima = $btn_blm_terima;
				}

			}else{
				$status = $btn_blm_cetak;

				$btn_blm_terima = '<button OnClick="warning_blm_cetak('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">BELUM</button>';

				$status_terima = $btn_blm_terima;
			}

			$this->table->add_row(
								$counter,
								noreg($item->no_rekam_pendok),
								noreg($item->no_register),
								$item->tipe_permohonan,
								$link_entry,
								$item->nama_penanggung_jawab,
								$item->kategori_pendaftaran,
								$link_cetak,
								$status,
								$status_terima
								/*$link_cetak,
								$btn_verifikasi*/
								/*,
								'<a href="" >Check</a>',
								'<a href="" >Check</a>'*/
								);
			$counter++;
		}
	}

	$table_buku_kapal = $this->table->generate();

?>
<p></p>
<!-- TAMPIL DATA -->
<div id="loading_info" class="panel panel-default">
	<div class="panel-body">
	   <p class="text-center">
	   		Memuat Data Buku Kapal Cetak. Harap Tunggu...
	   </p>
	</div>
</div>

<div id="table_container" class="panel-body overflowed hidden">
	<?php
		echo $table_buku_kapal;
	?>
</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- ADTTIONAL JAVASCRIPT -->
<script>
	function warning_cetak(id_pendok)
	{
		var url_set_status_cetak_bkp = "<?php echo base_url('buku_kapal/functions/status_cetak_bkp/') ?>",
		  	pesan = "Yakin sudah dicetak?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('buku_kapal/functions/status_cetak_bkp/"+id_pendok+"') ?>", '_self');

		}
	}

	function warning_blm_cetak(id_pendok)
	{
		var pesan = "Buku belum dicetak";
		  	// id_pendok = $(this).data('pendok');
		alert(pesan);
	}

	function warning_terima(id_pendok)
	{
		var url_set_status_cetak_bkp = "<?php echo base_url('buku_kapal/functions/status_terima_bkp/') ?>",
		  	pesan = "Yakin sudah serah terima?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('buku_kapal/functions/status_terima_bkp/"+id_pendok+"') ?>", '_self');

		}
	}

	$(document).ready( function () {
		$('#table_progres').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "iDisplayLength": 30,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
			}
		} );
	} );
</script>