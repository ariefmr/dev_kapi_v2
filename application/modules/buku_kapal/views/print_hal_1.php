<?php if ($is_preview): ?>
	<style type="text/css">
   		body {
	        margin: 0;
	        padding: 0;
	        background-color: #FAFAFA;
		}
	    * {
	        box-sizing: border-box;
	        -moz-box-sizing: border-box;
	    }
	    .page {
	        width: 19.5cm;
	        min-height: 19.51cm;
	        padding: 2cm;
	        margin: 1cm auto;
	        border: 1px #D3D3D3 solid;
	        border-radius: 5px;
	        background: white;
	        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	    }
	    .subpage {
	        padding: 1cm;
	        border: 5px black dotted;
	        height: 237mm;
	        outline: 2cm #FFEAEA solid;
	    }
        .table {
            border: 5px black dotted;
        }
	    
	    @page {
	        size: A4;
	        margin: 0;
	    }
	</style>
<?php endif ?>

 <div id="rotate" class="book">
    <div class="page">
        <div class="subpage">
            
            <table width="1000" class="table table-hover" border="0"   >
                
                <tbody>
                    <tr>
                        <tr>
                        <td width="45%" ></td><!-- no register -->
                        <td ><font style="font-size:1.5em; letter-spacing:0.5cm;" face="arial"><?php echo noreg($detail_buku['no_register'], '-'); ?></font></td>
                    </tr>
                    
                </tbody>
            </table>
            <table width="1000" class="table table-hover" border="0" >
                    <tr>
                        <td width="48%" height="49"></td><!-- nama kapal  -->
                        <td  height="38"><font face="arial"><?php echo kos($detail_buku['nama_kapal'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- nama kapal sebelum -->
                        <td height="18"><font face="arial"><?php echo kos($detail_buku['nama_kapal_sblm'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- tempat dan tahun pembangunan -->
                        <?php
                           /*if($detail_buku['tempat_pembangunan'] === 'Indonesia' ){
                                $tempat_pembangunan = $detail_buku['tempat_pembangunan'];
                           }else{
                           }*/
                            $tempat_pembangunan = $detail_buku['tempat_pembangunan'];
                        ?>
                        <td height="30"><font face="arial"><?php echo kos($tempat_pembangunan).' / '.kos($detail_buku['tahun_pembangunan'], '-') ; ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- nama bahan kapal -->
                        <td height="30"><font face="arial"><?php echo kos($detail_buku['nama_bahan_kapal'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- nama jenis kapal (harus nya pakai garis coret) -->
                        <td height="40"><font face="arial"><?php //echo kos($detail_buku['nama_jenis_kapal'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- nama alat tangkap ikan -->
                        <td height="43"><font face="arial"><?php echo kos($detail_buku['alat_tangkap'], '-')." [".kos($detail_buku['kode_penandaan'], '-')."]"  ; ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- merk dan type mesin utam -->
                        <td><font face="arial"><?php echo kos($detail_buku['merek_mesin'], '-').' '.kos($detail_buku['tipe_mesin'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- daya mesin utama kapal -->
                        <td><font face="arial"><?php echo kos($detail_buku['daya_kapal'], '-'); ?></font></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td></td><!-- no mesin kapal  -->
                        <td height="34" ><font face="arial"><?php echo kos($detail_buku['no_mesin'], '-'); ?></font></td>
                    </tr>
                </tbody>
               
            </table>

            <table width="1000" class="table table-hover" >
                <tbody>
                    <tr>
                        <td width="48%"></td>
                        <td width="23%" height="31"><font face="arial"><?php echo kos($detail_buku['jumlah_palka'], '-'); ?></font></td><!-- jumlah palka -->
                        <td><font face="arial"> <?php echo kos($detail_buku['kapasitas_palka'], '-'); ?></font></td><!-- kapasitas palka -->
                    </tr>
                </tbody>
            </table>

            <!-- <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p> -->
        </div>    
    </div>
</div>

