<?php if ($is_preview): ?>
	<style type="text/css">
   body {
       margin: 0;
       padding: 0;
       background-color: #FAFAFA;
   }
   * {
       box-sizing: border-box;
       -moz-box-sizing: border-box;
   }
   .page {
       width: 21cm;
       min-height: 29.7cm;
       padding: 2cm;
       margin: 1cm auto;
       border: 1px #D3D3D3 solid;
       border-radius: 5px;
       background: white;
       box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
   }
   .subpage {
       padding: 1cm;
       border: 5px black dotted;
       height: 237mm;
       outline: 2cm #FFEAEA solid;
   }
   .table {
    border: 5px black dotted;
}

@page {
   size: A4;
   margin: 0;
}
</style>
<?php endif ?>

<div class="book">
    <div class="page">
        <div class="subpage">

            <?php 
            $i = 0;
            while($i < 1){
                echo '<br>';
                $i++;               
            }
            ?>

            <table height="1000" width="100%" class="table table-hover"  border="1" >
                <tbody>
                    <tr>
                        <td colspan="2">
                            <center><h3>Draft Buku Kapal</h3></center>
                        </td>
                    </tr>
                    <tr> <!-- row 1  --> 
                        <td> <!-- halaman 1 -->
                            <table width="500" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="4" bgcolor="grey">IDENTITAS KAPAL PERIKANAN <br/>
                                            No. Reg. : <?php echo noreg($detail_buku['no_register'], '-'); ?><br/>
                                            No. Seri. : <?php echo kos($detail_buku['no_seri_bkp'], '-'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td width="50%">Nama Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['nama_kapal'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td width="50%">Nama Kapal Sebelumnya</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['nama_kapal_sblm'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td width="50%">Tempat / Tahun Pembangunan</td>
                                        <td>:</td>
                                        <?php
                                           /*if($detail_buku['tempat_pembangunan'] === 'Indonesia' ){
                                                $tempat_pembangunan = $detail_buku['tempat_pembangunan'];
                                           }else{
                                           }*/
                                                $tempat_pembangunan = $detail_buku['tempat_pembangunan'];
                                        ?>
                                        <td width="50%"><?php echo kos($tempat_pembangunan, '-').' / '.kos($detail_buku['tahun_pembangunan'], '-') ; ?></td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td width="50%">Bahan Utama Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['nama_bahan_kapal'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td width="50%">Type/Jenis Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['nama_jenis_kapal'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td width="50%">Jenis Alat Penangkap Ikan</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['alat_tangkap'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>7.</td>
                                        <td width="50%">Merek dan Type Mesin Utama Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['merek_mesin'], '-').' '.kos($detail_buku['tipe_mesin'], '-'); ?> </td>
                                    </tr>
                                    <tr>
                                        <td>8.</td>
                                        <td width="50%">Daya Mesin Utama Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos_value($detail_buku['daya_kapal'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>9.</td>
                                        <td width="50%">Nomor Mesin Utama Kapal</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos($detail_buku['no_mesin'], '-'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>10.</td>
                                        <td width="50%">Jumlah Palka</td>
                                        <td>:</td>
                                        <td width="50%"><?php echo kos_value($detail_buku['jumlah_palka'], '-'); ?> & Kapasitas Palka: <?php echo kos_value($detail_buku['kapasitas_palka'], '-'); ?></td>
                                    </tr>
                                </tbody>
                            </table> 
                        </td>
                        <td> <!-- halaman 2 -->
                        <table width="500" class="table table-hover" >
                            <tbody>
                                <tr>
                                    <td>11.</td>
                                    <td  width="50%">Tempat Pendaftaran</td>
                                    <td>:</td>
                                    <?php

                                        switch ($detail_buku['kategori_pendaftaran']) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }
                                    ?>
                                    <td  width="50%"><?php echo kos($tempat_pendaftaran, '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>12.</td>
                                    <td width="50%">Tanda Pengenal Kapal </td><!-- no tanda pengenal -->
                                    <td >:</td><!-- no tanda pengenal -->
                                    <td width="50%"><?php echo kos($detail_buku['no_tanda_pengenal'], '-'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="500" class="table table-hover">
                            <tbody>
                                <tr>
                                    <?php

                                        switch ($detail_buku['kategori_pendaftaran']) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "Jakarta";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $detail_buku['propinsi_pendaftaran'];
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }
                                    ?>
                                    <td width="80%"></td><!--  -->
                                    <td><?php echo kos($tempat_pendaftaran, '-'); ?></td>
                                </tr>
                                <tr>
                                    <td height="53"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="500" class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="70%"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="500" class="table table-hover" >
                            <tbody>
                                <tr>
                                    <td width="80%"></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr height="50%"> <!-- row 2 -->
                    <td> <!-- halaman 3 -->
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table table-hover" border="1">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th> Keterangan</th>
                                                    <th>Gross Akta</th>
                                                    <th> Hasil Pemeriksaan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>I</td>
                                                    <td>Dimensi Utama Kapal (meter)</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>- Panjang (L)</td>
                                                    <td><?php echo kos_value($detail_buku['panjang_kapal'], '-'); ?></td>
                                                    <td>LOA : <?php echo kos_value($detail_buku['panjang_loa_kapal'], '-'); ?></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>- Lebar (B)</td>
                                                    <td><?php echo kos_value($detail_buku['lebar_kapal'], '-'); ?></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>- Dalam (D)</td>
                                                    <td><?php echo kos_value($detail_buku['dalam_kapal'], '-'); ?></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>II</td>
                                                    <td>Tonnase</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>- GT</td>
                                                    <td><?php echo kos($detail_buku['gt_kapal'], '-'); ?></td>
                                                    <td bgcolor="black"></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>- NT</td>
                                                    <td><?php echo kos($detail_buku['nt_kapal'], '-'); ?></td>
                                                    <td bgcolor="black"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table> <!-- halaman 3 -->
                    </td>
                    <td> <!-- halaman 4 -->
                        <center><u>IDENTITAS PEMILIK KAPAL PERIKANAN</u></center>
                        <table width="500" class="table table-hover" >
                            <tbody>
                                <tr>
                                    <td width="50%">Nama (Perusahaan / Perorangan) </td>
                                    <td>:</td>
                                    <td width="50%"><?php echo kos($detail_buku['nama_perusahaan'], '-'); ?></td>
                                </tr>
                                <tr>
                                    <td width="50%">Alamat (Perusahaan / Perorangan) </td>
                                    <td>:</td>
                                    <td width="50%"><?php echo kos($detail_buku['alamat_perusahaan'], '-'); ?></td>
                                </tr>
                                <tr>
                                    <td width="50%">Nama Penanggung Jawab Perusahaan / Perorangan </td>
                                    <td>:</td>
                                    <td width="50%"><?php echo kos($detail_buku['nama_penanggung_jawab'], '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Tempat/Tanggal Lahir</td>
                                    <td>:</td>
                                    <td><?php echo kos($detail_buku['ttl_penanggung_jawab'], '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>No KTP</td>
                                    <td>:</td>
                                    <td><?php echo kos($detail_buku['no_identitas_penanggung_jawab'], '-'); ?></td>
                                </tr> 
                                <tr>
                                    <td>Nama Pemilik Kapal Perikaan Sebelumnya</td>
                                    <td>:</td>
                                    <td><?php echo kos($detail_buku['perusahaan_sblm'], '-'); ?></td>
                                </tr>   
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>

        </table>
        <table height="1000" width="100%" class="table table-hover" border="0">
            <tbody>
                <tr>
                    <td colspan="4">
                        <center><h3>VERIFIKATOR</h3></center>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right;">Kasie</td>
                    <td></td>
                    <td></td>
                    <td>Kasubdit</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <?php 
                        $i = 0;
                        while($i < 1){
                            echo '<br>';
                            $i++;               
                        }
                        ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: right;"><?=kos($detail_buku['nama_kasie'])?></td>
                    <td></td>
                    <td></td>
                    <td><?=kos($detail_buku['nama_kasubdit'])?></td>
                </tr>
            </tbody>
        </table>




            <!-- <p>
                *) Lembar putih : Petugas KAPI
                <br>
                *) Lembar merah : Pemohon
            </p> -->
        </div>    
    </div>
</div>

