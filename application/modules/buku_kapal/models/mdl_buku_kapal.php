<?php

class Mdl_buku_kapal extends CI_Model
{
	function __construct()
    {
        $this->load->database();
    }

    public function list_buku_kapal($kategori_pengguna='', $id_propinsi_pengguna='', $id_kabkota_pengguna='')
    {
    
        $query =   "SELECT  kapal.id_pendok_terakhir as id_pendok,
                            pendok.no_rekam_pendok,
                            kapal.no_register,
                            buku.nama_kapal,
                            pendok.id_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.nama_perusahaan,
                            buku.id_pengguna_buat,
                            buku.gt_kapal as gt,
                            pendok.tipe_permohonan,
                            pendok.status_entry_bkp,
                            pendok.status_verifikasi,
                            pendok.kategori_pendaftaran,
                            pendok.propinsi_pendaftaran,
                            pendok.kabkota_pendaftaran
                    FROM    db_pendaftaran_kapal.mst_kapal as kapal
                            JOIN        db_pendaftaran_kapal.trs_bkp as buku
                            ON          kapal.id_bkp_terakhir = buku.id_bkp
                            JOIN        db_pendaftaran_kapal.trs_pendok as pendok
                            ON          pendok.id_pendok = kapal.id_pendok_terakhir
                    WHERE pendok.status_pendok = 'FINAL' 
                            and buku.aktif = 'YA' ";
                    switch ($kategori_pengguna) {
                        case '1':
                            $query .= "";
                            break;
                        
                        case '2':
                            $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                            break;

                        case '3':
                            $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                            break;

                        default:
                            $query .= "";
                        break;
                    }

        $query .=  "ORDER BY pendok.no_rekam_pendok
                    DESC
                   ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_buku_kapal_cetak($kategori_pengguna='', $id_propinsi_pengguna='', $id_kabkota_pengguna='')
    {
    
        $query =   "SELECT  pendok.id_pendok,
                            pendok.no_rekam_pendok,
                            pendok.nama_penanggung_jawab,
                            pendok.tipe_permohonan,
                            pendok.status_cetak_bkp,
                            pendok.status_terima_bkp,
                            pendok.kategori_pendaftaran,
                            kapal.no_register,
                            buku.nama_kapal,
                            buku.id_pengguna_buat,
                            buku.id_bkp
                    FROM    db_pendaftaran_kapal.mst_kapal as kapal
                            JOIN        db_pendaftaran_kapal.trs_bkp as buku
                            ON          kapal.id_bkp_terakhir = buku.id_bkp
                            JOIN        db_pendaftaran_kapal.trs_pendok as pendok
                            ON          pendok.id_pendok = kapal.id_pendok_terakhir
                    WHERE   pendok.status_verifikasi = 'YA'
                            and pendok.status_entry_bkp = '2' ";
                    switch ($kategori_pengguna) {
                        case '1':
                            $query .= "";
                            break;
                        
                        case '2':
                            $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                            break;

                        case '3':
                            $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                            break;

                        default:
                            $query .= "";
                        break;
                    }
         $query .= "ORDER BY pendok.no_rekam_pendok
                    DESC
                   ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_buku_kapal($id_pendok)
    {
        
        $query =   "SELECT  kapal.no_register,
                            buku.id_pendok,
                            buku.no_seri_bkp,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.id_pengguna_buat,
                            buku.kasie_id,
                            buku.nama_kasie,
                            buku.kasubdit_id,
                            buku.nama_kasubdit,
                            buku.direktur_id,
                            buku.nama_direktur,
                            buku.nip_direktur,
                            buku.lokasi_pengesahan_id,
                            buku.tgl_ttd_direktur,
                            buku.id_kapal,
                            buku.nama_kapal,
                            buku.lokasi_pengesahan
                    FROM    db_pendaftaran_kapal.mst_kapal as kapal
                            JOIN        db_pendaftaran_kapal.trs_bkp as buku
                            ON          kapal.id_bkp_terakhir = buku.id_bkp
                            JOIN        db_pendaftaran_kapal.trs_pendok as pendok
                            ON          pendok.id_kapal = kapal.id_kapal
                    WHERE   pendok.id_pendok = '$id_pendok'
                    ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_cetak_buku($id_pendok)
    {
        $query =   "SELECT  kapal.no_register,
                            jenis_kapal.nama_jenis_kapal,
                            bahan_kapal.nama_bahan_kapal,
                            pendok.tipe_permohonan,
                            pendok.nama_perusahaan,
                            pendok.alamat_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.kategori_pendaftaran,
                            pendok.propinsi_pendaftaran,
                            pendok.kabkota_pendaftaran,
                            buku.no_identitas_penanggung_jawab,
                            buku.ttl_penanggung_jawab,
                            buku.id_bkp,
                            buku.id_kapal,
                            buku.id_pendok,
                            buku.id_perusahaan,
                            buku.id_jenis_kapal,
                            buku.id_alat_tangkap,
                            buku.alat_tangkap,
                            buku.id_bahan_kapal,
                            buku.no_seri_bkp,
                            buku.nama_kapal,
                            buku.nama_kapal_sblm,
                            buku.no_mesin,
                            buku.merek_mesin,
                            buku.tipe_mesin,
                            buku.daya_kapal,
                            buku.satuan_mesin_utama_kapal,
                            buku.jumlah_palka,
                            buku.kapasitas_palka,
                            buku.tempat_pendaftaran_id,
                            buku.tempat_grosse_akte,
                            buku.no_grosse_akte,
                            buku.tanggal_grosse_akte,
                            buku.panjang_kapal,
                            buku.lebar_kapal,
                            buku.dalam_kapal,
                            buku.panjang_loa_kapal,
                            buku.gt_kapal,
                            buku.nt_kapal,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.nama_kasie,
                            buku.nama_kasubdit,
                            buku.nama_direktur,
                            buku.nip_direktur,
                            buku.tgl_ttd_direktur,
                            buku.tahun_pembangunan,
                            buku.tempat_pembangunan,
                            buku.no_telp_perusahaan,
                            buku.perusahaan_sblm,
                            buku.kabkota_pendaftaran

                            -- alat_tangkap.kode_penandaan,
                            -- kabkota.nama_kabupaten_kota as tempat_pembangunan,
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        -- db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                       )
                            ON          (
                                        buku.id_bkp = kapal.id_bkp_terakhir
                                        and buku.id_pendok = pendok.id_pendok
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        -- and buku.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        )
                    WHERE   buku.id_pendok = '$id_pendok'
                    ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    
    public function update($id_pendok, $data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok',$id_pendok);
        $this->db->update('trs_bkp',$data);

        $this->db->last_query();

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;
        }
        else
        {
            $affected = false;
        }

        return $affected;
    }

    public function set_status_bkp($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $data = array('status_entry_bkp' => '2');
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function set_cetak_bkp($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $id_pengguna = $this->mksess->id_pengguna();
        $data = array('status_cetak_bkp' => 'YA'
                        ,'tanggal_ubah' => $date_terima
                        ,'id_pengguna_ubah' => $id_pengguna
                    );
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function set_terima_bkp($id_pendok)
    {
        $date_terima = date('Y-m-d');
        $id_pengguna = $this->mksess->id_pengguna();
        $data = array('status_terima_bkp' => 'YA'
                        ,'tanggal_ubah' => $date_terima
                        ,'id_pengguna_ubah' => $id_pengguna
                    );
        $this->db->where('id_pendok', $id_pendok);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function get_tipe_perubahan_per_pendok($id_pendok, $id_kategori_perubahan)
    {
        $query =   "SELECT tp.id_tipe_perubahan,mtp.kolom_db from trs_perubahan tp, mst_tipe_perubahan mtp, mst_kategori_perubahan mkp
                        where mtp.id_kategori_perubahan = mkp.id_kategori_perubahan 
                        and tp.id_tipe_perubahan = mtp.id_tipe_perubahan
                        and tp.id_pendok = '$id_pendok'
                        and mtp.id_kategori_perubahan = '$id_kategori_perubahan'
                        and tp.aktif = 'ya'
                    ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_tipe_perubahan_jenis_kapal($id_pendok)
    {
        $query =   "SELECT tp.id_perubahan 
                        from trs_perubahan tp
                        where id_pendok = '$id_pendok'
                        and id_tipe_perubahan = 10
                    ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_tipe_perubahan_alat_tangkap($id_pendok)
    {
        $query =   "SELECT tp.id_perubahan 
                        from trs_perubahan tp
                        where id_pendok = '$id_pendok'
                        and id_tipe_perubahan = 11
                    ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_data_perubahan($id_pendok)
    {
        /*di pake pada saat print PERUBAHAN*/
        $query =   "SELECT  kapal.no_register,
                            jenis_kapal.nama_jenis_kapal,
                            -- alat_tangkap.nama_alat_tangkap,
                            bahan_kapal.nama_bahan_kapal,
                            pendok.tipe_permohonan,
                            pendok.nama_perusahaan,
                            pendok.alamat_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.no_identitas_penanggung_jawab,
                            buku.id_bkp,
                            buku.id_kapal,
                            buku.id_pendok,
                            buku.id_perusahaan,
                            buku.id_jenis_kapal,
                            buku.id_alat_tangkap,
                            buku.alat_tangkap,
                            buku.id_bahan_kapal,
                            buku.no_seri_bkp,
                            buku.nama_kapal,
                            buku.nama_kapal_sblm,
                            buku.no_mesin,
                            buku.merek_mesin,
                            buku.tipe_mesin,
                            buku.daya_kapal,
                            buku.satuan_mesin_utama_kapal,
                            buku.jumlah_palka,
                            buku.kapasitas_palka,
                            buku.tempat_pendaftaran_id,
                            buku.tempat_grosse_akte,
                            buku.no_grosse_akte,
                            buku.tanggal_grosse_akte,
                            buku.panjang_kapal,
                            buku.lebar_kapal,
                            buku.dalam_kapal,
                            buku.panjang_loa_kapal,
                            buku.gt_kapal,
                            buku.nt_kapal,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.nama_kasie,
                            buku.nama_kasubdit,
                            buku.nama_direktur,
                            buku.tgl_ttd_direktur,
                            buku.tempat_pembangunan,
                            buku.tahun_pembangunan
                            -- buku.*
                            -- kabkota.nama_kabupaten_kota as tempat_pembangunan,
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        -- db_master.mst_perusahaan as perusahaan,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        -- db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                        -- db_master.mst_kabupaten_kota as kabkota
                                       )
                            ON          (
                                        buku.id_bkp = kapal.id_bkp_terakhir
                                        and buku.id_pendok = pendok.id_pendok
                                        -- and buku.id_perusahaan = perusahaan.id_perusahaan
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        -- and buku.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        -- and buku.tempat_pembangunan_id = kabkota.id_kabupaten_kota
                                        )
                    WHERE   buku.id_pendok = '$id_pendok'
                            and
                            buku.aktif = 'ya'
                    ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_data_sebelum($id_pendok)
    {
        
        $query =   "SELECT  pendok.tipe_permohonan,
                            jenis_kapal.nama_jenis_kapal,
                            -- alat_tangkap.nama_alat_tangkap,
                            bahan_kapal.nama_bahan_kapal,
                            pendok.nama_perusahaan,
                            pendok.alamat_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.no_identitas_penanggung_jawab,
                            buku.id_bkp,
                            buku.id_kapal,
                            buku.id_pendok,
                            buku.id_perusahaan,
                            buku.id_jenis_kapal,
                            buku.id_alat_tangkap,
                            buku.alat_tangkap,
                            buku.id_bahan_kapal,
                            buku.no_seri_bkp,
                            buku.nama_kapal,
                            buku.nama_kapal_sblm,
                            buku.no_mesin,
                            buku.merek_mesin,
                            buku.tipe_mesin,
                            buku.daya_kapal,
                            buku.satuan_mesin_utama_kapal,
                            buku.jumlah_palka,
                            buku.kapasitas_palka,
                            buku.tempat_pendaftaran_id,
                            buku.tempat_grosse_akte,
                            buku.no_grosse_akte,
                            buku.tanggal_grosse_akte,
                            buku.panjang_kapal,
                            buku.lebar_kapal,
                            buku.dalam_kapal,
                            buku.panjang_loa_kapal,
                            buku.gt_kapal,
                            buku.nt_kapal,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.nama_kasie,
                            buku.nama_kasubdit,
                            buku.nama_direktur,
                            buku.tgl_ttd_direktur,
                            buku.tempat_pembangunan,
                            buku.tahun_pembangunan
                            -- buku.*
                            -- kabkota.nama_kabupaten_kota as tempat_pembangunan,
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        -- db_master.mst_perusahaan as perusahaan,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        -- db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                        -- db_master.mst_kabupaten_kota as kabkota
                                       )
                            ON          (
                                        buku.id_pendok = pendok.id_pendok
                                        -- and buku.id_perusahaan = perusahaan.id_perusahaan
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        -- and buku.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        -- and buku.tempat_pembangunan_id = kabkota.id_kabupaten_kota
                                        )
                    WHERE   buku.id_pendok = '$id_pendok'
                            and
                            buku.aktif = 'ya'
                    ";
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}

?>