<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_buku_kapal');
		$this->load->model('pendok/mdl_pendok');
		$this->load->model('kapal/mdl_kapal');
		$this->load->config('globals');
		$this->load->config('labels');
	}

	public function index($id_pendok)
	{

		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];
		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_hal_1', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', 'A5-L');
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output('BKP_Halaman1.pdf','I');

	}

	public function hal1($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		// vdump($data['detail_buku'], true);

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetakhal1.css');
		$dokumen = $this->load->view('print_hal_1', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		// $this->mpdf=new mPDF('!DOCTYPE', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output($data['detail_buku']['no_register'].'_hal1.pdf','I');

	}

	public function hal2($id_pendok)
	{
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$detail_buku = (array) $get_detail_buku[0];

	    switch ($detail_buku['kategori_pendaftaran']) {
	        case 'PUSAT':
	            $tempat_pendaftaran = "JAKARTA";
	            break;

	        case 'PROPINSI':
	            $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
	            break;
	        
	        case 'KAB/KOTA':
	            $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
	            break;

	        default:
	            $tempat_pendaftaran = "JAKARTA";
	            break;
	    }

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD), 11); 

		//set rotation
		// $page->rotate(276.5, 205.5, M_PI/1); 

		// Draw something on a page 
		$page->drawText('KKP', 230, 307);
		$page->drawText(kos($detail_buku['no_tanda_pengenal'], '-'), 230, 287);
		$page->drawText(kos($tempat_pendaftaran, '-').", ".TanggalIndo(kos($detail_buku['tgl_ttd_direktur'], '-')), 274, 227);
		$page->drawText(kos($detail_buku['nama_direktur'], '-'), 268, 167);
		$page->drawText('NIP. '.kos($detail_buku['nip_direktur'], '-'), 290, 150);

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal2_test($id_pendok)
	{
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$detail_buku = (array) $get_detail_buku[0];

	    switch ($detail_buku['kategori_pendaftaran']) {
	        case 'PUSAT':
	            $tempat_pendaftaran = "JAKARTA";
	            break;

	        case 'PROPINSI':
	            $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
	            break;
	        
	        case 'KAB/KOTA':
	            $tempat_pendaftaran = $detail_buku['kabkota_pendaftaran'];
	            break;

	        default:
	            $tempat_pendaftaran = "JAKARTA";
	            break;
	    }

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 11); 

		//set rotation
		$page->rotate(276.5, 205.5, M_PI/1); 

		// Draw something on a page 
		$page->drawText('KKP', 230, 307);
		$page->drawText(kos($detail_buku['no_tanda_pengenal'], '-'), 230, 287);
		$page->drawText(kos($tempat_pendaftaran, '-').", ".TanggalIndo(kos($detail_buku['tgl_ttd_direktur'], '-')), 274, 227);
		$page->drawText(kos($detail_buku['nama_direktur'], '-'), 268, 167);
		$page->drawText('NIP. '.kos($detail_buku['nip_direktur'], '-'), 268, 150);

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal4_potong($id_pendok)
	{
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$detail_buku = (array) $get_detail_buku[0];
		// vdump($detail_buku,true);

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		// Add new page to the document 
		// Set font 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 11); 
		
		//set rotation
		$page->rotate(276.5, 205.5, M_PI/1); 

		// Draw something on a page 
		$page->drawText(kos($detail_buku['nama_perusahaan'], '-'), 230, 255);

		//potong alamat perusahaan jika lebih panjang nya lebih dari 49 huruf
		$panjang = strlen( kos($detail_buku['alamat_perusahaan'], '-') );
		if($panjang > 40){
			$alamat = $detail_buku['alamat_perusahaan'];
			$baris_1 = substr($alamat, 0, 40);
			$baris_2 = substr($alamat, 40, 55);
			if($panjang > 98){
				$baris_3 = "";
				$baris_3 = substr($alamat, 104);
				$page->drawText($baris_3, 230, 200);

			}
			$page->drawText($baris_1, 230, 225);
			$page->drawText($baris_2, 230, 213);
		}else{
			$page->drawText(kos($detail_buku['alamat_perusahaan'], '-'), 230, 225);
		}

		$page->drawText(kos($detail_buku['no_telp_perusahaan'], '-'), 295, 179);
		$page->drawText(kos($detail_buku['nama_penanggung_jawab'], '-'), 230, 133);
		$page->drawText(kos($detail_buku['ttl_penanggung_jawab'], '-'), 230, 110);
		$page->drawText(kos($detail_buku['no_identitas_penanggung_jawab'], '-'), 230, 87);
		$page->drawText(kos($detail_buku['perusahaan_sblm'], '-'), 230, 60);

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal4_test($id_pendok)
	{
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$detail_buku = (array) $get_detail_buku[0];
		// vdump($detail_buku,true);

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		// Add new page to the document 
		// $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4); 
		// $page = $pdf->newPage('195:145:'); 
		// Set font 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD), 11); 
		
		//set rotation
		// $page->rotate(276.5, 205.5, M_PI/1); 

		// Draw something on a page 
		$page->drawText(kos($detail_buku['nama_perusahaan'], '-'), 230, 255);
		//potong alamat perusahaan jika lebih panjang nya lebih dari 49 huruf
		$panjang = strlen( kos($detail_buku['alamat_perusahaan'], '-') );
		if($panjang > 49){
			$alamat = $detail_buku['alamat_perusahaan'];
			$baris_1 = substr($alamat, 0, 49);
			$baris_2 = substr($alamat, 49, 55);
			if($panjang > 98){
				$baris_3 = "";
				$baris_3 = substr($alamat, 104);
				$page->drawText($baris_3, 230, 200);

			}
			$page->drawText($baris_1, 230, 225);
			$page->drawText($baris_2, 230, 213);
		}else{
			$page->drawText(kos($detail_buku['alamat_perusahaan'], '-'), 230, 225);
		}
		$page->drawText(kos($detail_buku['no_telp_perusahaan'], '-'), 295, 179);
		$page->drawText(kos($detail_buku['nama_penanggung_jawab'], '-'), 230, 133);
		$page->drawText(kos($detail_buku['ttl_penanggung_jawab'], '-'), 230, 110);
		$page->drawText(kos($detail_buku['no_identitas_penanggung_jawab'], '-'), 230, 87);
		$page->drawText(kos($detail_buku['perusahaan_sblm'], '-'), 230, 60);

		// $page->drawText('KKP', 200, 700);
		// $page->drawText($detail_buku['no_tanda_pengenal'], 200, 680);
		// $page->drawText('JAKARTA', 200, 600);
		// $page->drawText(kos($detail_buku['nama_direktur'], '-'), 200, 530);
		// $page->drawText('19621228 199002 1001', 200, 510);
		// $page->drawText(kos($detail_buku['nip_direktur'], '-'), 200, 510);

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal2_mpdf($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		// vdump($data['detail_buku'], true);

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_hal_2', $data, TRUE);
		
		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		
		$pdf_out = $paths['dokumen_rotate']."/".$data['detail_buku']['no_register']."000014_hal2_r.pdf";
		$pdf_out_r = $paths['dokumen_rotate']."/".$data['detail_buku']['no_register']."_hal2r.pdf";
		$pdf_in = $paths['dokumen_rotate']."/000014_hal2.pdf";

		$this->mpdf->Output($pdf_out,'I');
		
		// unlink($pdf_in);
		// echo $pdf_out;
		// echo $command = "pdftk $pdf_in cat 1-endright output /opt/lampp/htdocs/dev_kapi_v2/uploads/document/tes.pdf";
		// echo $command = "pdftk $pdf_in cat 1-endright output $pdf_out";
		// echo $command = "pdftk http://localhost/dev_kapi_v2/uploads/document/000014_hal2.pdf cat 1-endright output /opt/lampp/htdocs/dev_kapi_v2/uploads/document/tes.pdf";
      	// system("pdftk http://localhost/dev_kapi_v2/uploads/document/000014_hal2.pdf cat 1-endright output /opt/lampp/htdocs/dev_kapi_v2/uploads/document/tes.pdf");

	}

	public function hal3($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		// vdump($data['detail_buku'], true);

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_hal_3', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);

		$this->mpdf->Output($data['detail_buku']['no_register'].'_hal3.pdf','I');

	}

	public function hal4($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		// vdump($data['detail_buku'], true);

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_hal_4', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output($data['detail_buku']['no_register'].'_hal4.pdf','I');

	}

	public function hal_pemilik_($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');

		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 1);

		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);


			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA <br> ";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$semula .= $nama_field.": ".$data_selected[$value];
					$semula .= "<br> ";
					
				}
			}
			$semula .= "alamat perusahaan: ".$data_selected['alamat_perusahaan'];
			$semula .= "<br> ";
			$semula .= "penanggung jawab: ".$data_selected['nama_penanggung_jawab'];
			$semula .= "<br> ";


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI <br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$menjadi .= $nama_field.": ".$data_selected[$value];
					$menjadi .= "<br> ";
				}
			}
			$menjadi .= "alamat perusahaan: ".$data_selected['alamat_perusahaan'];
			$menjadi .= "<br> ";
			$menjadi .= "penanggung jawab: ".$data_selected['nama_penanggung_jawab'];
			$menjadi .= "<br> ";

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan pemilik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetakhal1.css');
		$dokumen = $this->load->view('print_ubah_pemilik', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output($data_selected['no_register'].'_pemilik.pdf','I');

	}

	

	public function hal_fisik_($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');


		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 2);
		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);

			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA <br> ";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$semula .= $nama_field.": ".$data_selected[$value]." ".$satuan_mesin;
						$semula .= "<br> ";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";

					}
				}
			}


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br> MENJADI <br> ";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value]." ".$satuan_mesin;
						$menjadi .= "<br> ";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";

					}
				}
			}

		// echo $semula;
		// echo $menjadi;

		// die;

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan fisik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetakhal1.css');
		$dokumen = $this->load->view('print_ubah_fisik', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output($data_selected['no_register'].'_fisik.pdf','I');

	}

	public function hal_tanda_pengenal_($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');


		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan_at = $this->mdl_buku_kapal->get_tipe_perubahan_alat_tangkap($id_pendok);
		$get_tipe_perubahan_jk = $this->mdl_buku_kapal->get_tipe_perubahan_jenis_kapal($id_pendok);
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 3);

		if(($get_tipe_perubahan_at) || ($get_tipe_perubahan_jk) || ($get_tipe_perubahan) ){
			// vdump($get_tipe_perubahan_at, true);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

			$semula = "";
			$semula .= "SEMULA <br> ";
			//NO TANDA PENGENAL SEBELUM
			$semula .= $data_selected['no_tanda_pengenal'];
			$semula .= "<br> ";

			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br> MENJADI <br> ";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

			//NO TANDA PENGENAL BARU
			$menjadi .= $data_selected['no_tanda_pengenal'];

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan tanda pengenal";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetakhal1.css');
		$dokumen = $this->load->view('print_ubah_fisik', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);

		$this->mpdf->Output($data_selected['no_register'].'_tanda.pdf','I');

	}

	public function hal_pemilik($id_pendok) //pemilik hal 6 dan 8
	{
		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 1);

		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);


			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

			$semula = "";
			$semula .= "SEMULA<br>";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {
				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$semula .= "- ".ucwords($nama_field).":<br>".$data_selected[$value];
					$semula .= "<br>";
				}
			}
			$semula .= "- Alamat Perusahaan:<br>".ucwords(strtolower($data_selected['alamat_perusahaan']));
			$semula .= "<br>";
			$semula .= "- Penanggung Jawab:<br>".$data_selected['nama_penanggung_jawab'];
			$semula .= "<br>";


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {
				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$menjadi .= "- ".ucwords($nama_field).":<br>".$data_selected[$value];
					$menjadi .= "<br>";
				}
			}
			$menjadi .= "- Alamat Perusahaan:<br>".ucwords(strtolower($data_selected['alamat_perusahaan']));
			$menjadi .= "<br>";
			$menjadi .= "- Penanggung Jawab:<br>".$data_selected['nama_penanggung_jawab'];
			$menjadi .= "<br>";

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan pemilik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		// $page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		$page->drawText('1.', 131, 327);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 312, 327);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 154, 327 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal_pemilik_genap($id_pendok) //pemilik hal 6 dan 8
	{
		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 1);

		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);


			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA<br>";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$semula .= "- ".ucwords($nama_field).":<br>".$data_selected[$value];
					$semula .= "<br>";
					
				}
			}
			$semula .= "- Alamat Perusahaan:<br>".ucwords(strtolower($data_selected['alamat_perusahaan']));
			$semula .= "<br>";
			$semula .= "- Penanggung Jawab:<br>".$data_selected['nama_penanggung_jawab'];
			$semula .= "<br>";


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$menjadi .= "- ".ucwords($nama_field).":<br>".$data_selected[$value];
					$menjadi .= "<br>";
				}
			}
			$menjadi .= "- Alamat Perusahaan:<br>".ucwords(strtolower($data_selected['alamat_perusahaan']));
			$menjadi .= "<br>";
			$menjadi .= "- Penanggung Jawab:<br>".$data_selected['nama_penanggung_jawab'];
			$menjadi .= "<br>";

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan pemilik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		$page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		// $page->drawText($semula, 10, 150);
		$page->drawText('1.', 98, 222);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 272, 222);
		// $page->drawText($menjadi, 157, 255);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		// $semula_ucwords = ucwords(strtolower($semula));
		// $menjadi_ucwords = ucwords(strtolower($menjadi));
		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 120, 222 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function hal_fisik($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');


		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 2);
		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);

			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA<br>";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value]." ".$satuan_mesin;
						$semula .= "<br>";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br>";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$semula .= "<br>";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$semula .= "<br>";

					}
				}
			}


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value]." ".$satuan_mesin;
						$menjadi .= "<br>";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";

					}
				}
			}

		// echo $semula;
		// echo $menjadi;

		// die;

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan fisik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		// $page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		// $page->drawText($semula, 10, 150);
		$page->drawText('1.', 131, 327);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 312, 327);
		// $page->drawText($menjadi, 157, 255);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		// $semula_ucwords = ucwords(strtolower($semula));
		// $menjadi_ucwords = ucwords(strtolower($menjadi));
		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 154, 327 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 

	}

	public function hal_fisik_genap($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');


		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 2);
		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);

			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA<br>";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value]." ".$satuan_mesin;
						$semula .= "<br>";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br>";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$semula .= "<br>";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$semula .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$semula .= "<br>";

					}
				}
			}


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value]." ".$satuan_mesin;
						$menjadi .= "<br>";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= "- ".ucwords($nama_field).": ".$data_selected[$value];
						$menjadi .= "<br>";

					}
				}
			}

		// echo $semula;
		// echo $menjadi;

		// die;

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan fisik";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		$page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		// $page->drawText($semula, 10, 150);
		$page->drawText('1.', 98, 222);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 272, 222);
		// $page->drawText($menjadi, 157, 255);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		// $semula_ucwords = ucwords(strtolower($semula));
		// $menjadi_ucwords = ucwords(strtolower($menjadi));
		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 120, 222 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 

	}

	public function hal_tanda_pengenal($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');


		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan_at = $this->mdl_buku_kapal->get_tipe_perubahan_alat_tangkap($id_pendok);
		$get_tipe_perubahan_jk = $this->mdl_buku_kapal->get_tipe_perubahan_jenis_kapal($id_pendok);
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 3);

		if(($get_tipe_perubahan_at) || ($get_tipe_perubahan_jk) || ($get_tipe_perubahan) ){
			// vdump($get_tipe_perubahan_at, true);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

			$semula = "";
			$semula .= "SEMULA<br>";
			//NO TANDA PENGENAL SEBELUM
			$semula .= $data_selected['no_tanda_pengenal'];
			$semula .= "<br>";

			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

			//NO TANDA PENGENAL BARU
			$menjadi .= $data_selected['no_tanda_pengenal'];

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan tanda pengenal";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		// $page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		// $page->drawText($semula, 10, 150);
		$page->drawText('1.', 131, 327);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 312, 327);
		// $page->drawText($menjadi, 157, 255);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 154, 327 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();

		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 

	}

	public function hal_tanda_pengenal_genap($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');

		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan_at = $this->mdl_buku_kapal->get_tipe_perubahan_alat_tangkap($id_pendok);
		$get_tipe_perubahan_jk = $this->mdl_buku_kapal->get_tipe_perubahan_jenis_kapal($id_pendok);
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 3);

		if(($get_tipe_perubahan_at) || ($get_tipe_perubahan_jk) || ($get_tipe_perubahan) ){

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

			$semula = "";
			$semula .= "SEMULA<br>";
			//NO TANDA PENGENAL SEBELUM
			$semula .= $data_selected['no_tanda_pengenal'];
			$semula .= "<br>";

			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br>MENJADI<br>";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

			//NO TANDA PENGENAL BARU
			$menjadi .= $data_selected['no_tanda_pengenal'];

			$data['semula'] = $semula;
			$data['perubahan'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula'] = "tidak ada perubahan tanda pengenal";
			$data['perubahan'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$this->load->add_package_path(APPPATH.'third_party/zf');
		$this->load->library('zf');

		// Create new PDF 
		$pdf = new Zend_Pdf(); 

		$page = $pdf->newPage('553:411:'); 
		$pdf->pages[] = $page; 
		$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 8); 

		//set rotation
		$page->rotate(276.5, 205.5, M_PI/1);

		// Draw something on a page 
		// $page->drawText($semula, 10, 150);
		$page->drawText('1.', 98, 222);
		$page->drawText(fmt_tgl($data['tgl_pengesahan']), 272, 222);
		// $page->drawText($menjadi, 157, 255);

		// Draw text    
		$charsPerLine = 37;
		$heightPerLine = 10;

		$text = $semula.$menjadi;
		$lines = array();

		foreach (explode('<br>', $text) as $line) {
		    $lines = array_merge(
		        	    	$lines, 
		        	    	explode(
		        	    	    	"\n",
		        	    	    	wordwrap($line, $charsPerLine, "\n")
		        	    	)
		    );
		}

		foreach ( $lines as $i=>$line ) {
		    $page->drawText($line, 120, 222 - $i * $heightPerLine, 'UTF-8');
		}

		// Get PDF document as a string 
		$pdfData = $pdf->render();
		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/pdf"); 
		echo $pdfData; 
	}

	public function preview($id_pendok)
	{	
		$data['id_pendok'] = $id_pendok;

		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);
		$tipe_permohonan = $get_permohonan['tipe_permohonan'];
			// vdump($tipe_permohonan['tipe_permohonan'], true);

		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];
			// vdump($data['detail_buku'], true);

		if($tipe_permohonan == 'BARU'){
			$view_file = 'preview_print_baru';
		}else{
			$view_file = 'preview_print_perubahan';
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							$view_file, //nama file view
							'label_print_bkp', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function preview_draft($id_pendok)
	{	
		$data['id_pendok'] = $id_pendok;

		$get_permohonan = (array)$this->mdl_pendok->get_permohonan($id_pendok);
		$tipe_permohonan = $get_permohonan['tipe_permohonan'];
			// vdump($tipe_permohonan['tipe_permohonan'], true);
		if($tipe_permohonan == 'BARU'){
			$data['tipe_draft'] = 'baru';
		}else{
			$data['tipe_draft'] = 'perubahan';
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							'preview_print_draft', //nama file view
							'label_print_draft', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}


	public function draft_kapal($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');
		$get_detail_buku = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok);
		$data['detail_buku'] = (array) $get_detail_buku[0];

		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;

		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_draft', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		// $this->mpdf=new mPDF('utf-8', array(195,145));
		$this->mpdf=new mPDF('utf-8', 'A4','','',4,4,5,5,0,0);
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);
		$this->mpdf->Output($data['detail_buku']['no_register'].'_draft.pdf','I');

	}

	public function draft_kapal_perubahan($id_pendok)
	{
		$timestamp =  date('d/m/Y');
		$paths = $this->config->item('assets_paths');
		$forms = $this->config->item('label_form_buku_kapal');
		$nama_hari = $this->config->item('Hari_f');
		$nama_bulan = $this->config->item('Bulan_f');

		//==========================================
		//PERUBAHAN PEMILIK

		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 1);

		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);


			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $data) {
				foreach ($data as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA <br> ";

			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$semula .= $nama_field.": ".$data_selected[$value];
					$semula .= "<br> ";
					
				}
			}
			$semula .= "alamat perusahaan: ".$data_selected['alamat_perusahaan'];
			$semula .= "<br> ";
			$semula .= "penanggung jawab: ".$data_selected['nama_penanggung_jawab'];
			$semula .= "<br> ";


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br> MENJADI <br> ";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $data) {
				foreach ($data as $key => $value) {

				//jika kolom mengandung id_ diganti nama_
					$value = str_replace('id_', 'nama_', $value);
					$nama_field = str_replace('_', ' ', $value);
					$menjadi .= $nama_field.": ".$data_selected[$value];
					$menjadi .= "<br> ";
				}
			}
			$menjadi .= "alamat perusahaan: ".$data_selected['alamat_perusahaan'];
			$menjadi .= "<br> ";
			$menjadi .= "penanggung jawab: ".$data_selected['nama_penanggung_jawab'];
			$menjadi .= "<br> ";

			$data['semula_pemilik'] = $semula;
			$data['perubahan_pemilik'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula_pemilik'] = "tidak ada perubahan pemilik";
			$data['perubahan_pemilik'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		//==========================================

		//PERUBAHAN FISIK
		
		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 2);
		if($get_tipe_perubahan){
			// vdump($get_tipe_perubahan, true);

			$arr_kategori_perubahan = array();

			$i = 0;
			$last = count($get_tipe_perubahan);
			foreach ($get_tipe_perubahan as $item) {
				foreach ($item as $key => $value) {
					$temp_value = str_replace('trs_bkp.', '', $value);
					$arr_kategori_perubahan[$i] = explode(",", $temp_value);
				}
				$i++;
			}

		// vdump($arr_kategori_perubahan, false);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

		// vdump($data_selected, false);

			$semula = "";
			$semula .= "SEMULA <br> ";

			foreach ($arr_kategori_perubahan as $item) {
				foreach ($item as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$semula .= $nama_field.": ".$data_selected[$value]." ".$satuan_mesin;
						$semula .= "<br> ";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";
					}else if ($value === "kapasitas_palka"){
						$nama_field = "kapasitas palka";
						$semula .= $nama_field.": ".$data_selected[$value]." m<sup>3</sup>";
						$semula .= "<br> ";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						$value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$semula .= $nama_field.": ".$data_selected[$value];
						$semula .= "<br> ";

					}
				}
			}


			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br> MENJADI <br> ";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

		// vdump($data_selected, false);
			foreach ($arr_kategori_perubahan as $item) {
				foreach ($item as $key => $value) {

					if($value === "satuan_mesin_utama_kapal"){

					}else if($value === "daya_kapal"){
						switch ($data_selected['satuan_mesin_utama_kapal']) {
							case 1:
								# code...
								$satuan_mesin = "PK";
								break;

							case 2:
								# code...
								$satuan_mesin = "DK";
								break;

							case 3:
								# code...
								$satuan_mesin = "HP";
								break;
							
							default:
								$satuan_mesin = "PK";
								break;
						}
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value]." ".$satuan_mesin;
						$menjadi .= "<br> ";
					}else if ($value === "id_alat_tangkap"){
						$nama_field = "alat tangkap";
						/* --------- perubahan QUERY mdl_buku_kapal->get_data_perubahan() dan get_data_sebelum()
						id_alat_tangkap di anggap mempengaruhi kelengkapan data, karen id_alat_tangkap ada yg NULL 
						maka revisi di ubah 
						mempengaruhi kodingan baris dibawah ini untuk print BKP [FATAL di tanggal 10April2015 ]*/
						// $value = str_replace('id_', 'nama_', $value);
						$value = str_replace('id_', '', $value);
						//  ----------------------------------------------------------------------------------------------
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";
					}else if ($value === "id_jenis_kapal"){
						$nama_field = "jenis kapal";
						$value = str_replace('id_', 'nama_', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";
					}else if ($value === "kapasitas_palka"){
						$nama_field = "kapasitas palka";
						$menjadi .= $nama_field.": ".$data_selected[$value]." m<sup>3</sup>";
						$menjadi .= "<br> ";
					}
					else{
					//jika kolom mengandung id_ diganti nama_
						// $value = str_replace('id_', 'nama_', $value);
						$nama_field = str_replace('_', ' ', $value);
						$menjadi .= $nama_field.": ".$data_selected[$value];
						$menjadi .= "<br> ";

					}
				}
			}

		// echo $semula;
		// echo $menjadi;

		// die;

			$data['semula_fisik'] = $semula;
			$data['perubahan_fisik'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula_fisik'] = "tidak ada perubahan fisik";
			$data['perubahan_fisik'] = " ";
			$data['tgl_pengesahan'] = " ";
		}

		//==========================================

		//PERUBAHAN TANDA PENGENAL

		/*mengambil kolom2 apa saja yang berubah*/
		$get_tipe_perubahan_at = $this->mdl_buku_kapal->get_tipe_perubahan_alat_tangkap($id_pendok);
		$get_tipe_perubahan_jk = $this->mdl_buku_kapal->get_tipe_perubahan_jenis_kapal($id_pendok);
		$get_tipe_perubahan = $this->mdl_buku_kapal->get_tipe_perubahan_per_pendok($id_pendok, 3);

		if(($get_tipe_perubahan_at) || ($get_tipe_perubahan_jk) || ($get_tipe_perubahan)){
			// vdump($get_tipe_perubahan_at, true);

			/*mengambil data2 sebelum perubahan*/
			$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
			$id_kapal = $get_id_kapal['id_kapal'];

			$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
			$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];

			$data_sebelum = $this->mdl_buku_kapal->get_data_sebelum($id_pendok_sebelum);
			$data_selected = (array) $data_sebelum[0];

			$semula = "";
			$semula .= "SEMULA <br> ";
			//NO TANDA PENGENAL SEBELUM
			$semula .= $data_selected['no_tanda_pengenal'];
			$semula .= "<br> ";

			/*mengambil data2 perubahan*/
			$menjadi = "";
			$menjadi .= "<br> MENJADI <br> ";

			$get_perubahan_fisik = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
			$data_selected = (array) $get_perubahan_fisik[0];

			//NO TANDA PENGENAL BARU
			$menjadi .= $data_selected['no_tanda_pengenal'];

			$data['semula_tanda'] = $semula;
			$data['perubahan_tanda'] = $menjadi;
			$data['tgl_pengesahan'] = $data_selected['tgl_ttd_direktur'];

		}else{
			$data['semula_tanda'] = "tidak ada perubahan tanda pengenal";
			$data['perubahan_tanda'] = " ";
			$data['tgl_pengesahan'] = " ";
		}
		//==========================================

		/* ===================================================================
		mengambil data2 sebelum perubahan untuk di tampilkan di buku awal*/
		$get_id_kapal = (array) $this->mdl_kapal->get_id_kapal($id_pendok);
		$id_kapal = $get_id_kapal['id_kapal'];

		$get_id_pendok_sebelum = (array) $this->mdl_kapal->get_id_pendok_sebelum($id_kapal);
		$id_pendok_sebelum = $get_id_pendok_sebelum['id_pendok'];
		
		$get_detail_buku_awal = $this->mdl_buku_kapal->detail_cetak_buku($id_pendok_sebelum);
		$data['detail_buku'] = (array) $get_detail_buku_awal[0];
		/* ===================================================================
		-- end-- mengambil data2 sebelum perubahan untuk di tampilkan di buku awal*/

		$get_perubahan = $this->mdl_buku_kapal->get_data_perubahan($id_pendok);
		$data_verifikator = (array) $get_perubahan[0];

		$data['verifikator'] = $data_verifikator;
		$data['is_preview'] = FALSE;
		$data['css'] = NULL;
		$data['forms'] = $forms['form'];
		$data['nama_hari'] = $nama_hari;
		$data['nama_bulan'] = $nama_bulan;
		$data['id_pendok'] = $id_pendok;


		$css = file_get_contents($paths['main_css'].'/cetak_pendok.css');
		$dokumen = $this->load->view('print_draft_perubahan', $data, TRUE);

		ob_end_clean();
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('c','A4','','',32,25,1,1,0,0);
		$this->mpdf->WriteHTML($css,1);
		$this->mpdf->WriteHTML($dokumen,2);

		$this->mpdf->Output($data_selected['no_register'].'_draft.pdf','I');
	}

}


?>
