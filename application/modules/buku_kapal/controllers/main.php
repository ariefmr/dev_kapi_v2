<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_buku_kapal');
	}

	public function entry($id_pendok = '')
	{	
		if($id_pendok !== '')
		{
			$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
			$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];

		}

		$data['submit_form'] = 'buku_kapal/functions/update';

		$data['aksi'] = 'entry';
		$file_view = 'form_buku_kapal';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							$file_view, //nama file view
							'label_form_buku_kapal', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function edit($id_pendok = '')
	{	
		if($id_pendok !== '')
		{
			$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
			$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];
		}

		// $data['submit_form'] = 'pendok/edit/update';
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

	public function views()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'buku_kapal';
		$views = 'tabel_buku_kapal';
		$labels = 'label_view_buku_kapal';

		$data['list_bkp_pendok'] = $this->mdl_buku_kapal->list_buku_kapal($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function views_cetak()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'buku_kapal';
		$views = 'tabel_buku_kapal_cetak';
		$labels = 'label_view_buku_kapal_cetak';

		$data['list_bkp_pendok'] = $this->mdl_buku_kapal->list_buku_kapal_cetak($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}



	public function view($id_pendok)
	{	
		$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
		$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];

		$data['aksi'] = 'view';
		if($data['detail_buku_kapal']['no_register']==NULL){
			$data['button'] = 'entry';
		}else{
			$data['button'] = 'edit';
			$data['cetak'] = TRUE;
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							'form_buku_kapal', //nama file view
							'label_form_buku_kapal', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

}