<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MX_Controller{

	function __construct()
	{
		parent::__construct();
	}


	public function input()
	{
		$this->load->model('mdl_buku_kapal');
		$array_input = $this->input->post(NULL,TRUE);

		//=============BEGIN- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$id_lokasi = $this->mksess->id_lokasi();
		
		$info_lokasi_pengguna = (array)$this->mdl_pendok->get_info_lokasi($id_propinsi_pengguna, $id_kabkota_pengguna);

		$array_input['id_propinsi_pendaftaran'] = $id_propinsi_pengguna;
		$array_input['id_kabkota_pendaftaran'] = $id_kabkota_pengguna;
		$array_input['propinsi_pendaftaran'] = $info_lokasi_pengguna['nama_propinsi'];
		$array_input['kabkota_pendaftaran'] = $info_lokasi_pengguna['nama_kabupaten_kota'];

		//=============END- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		switch ($id_lokasi) {
			case '2':
				$array_input['kategori_pendaftaran'] = 'PROPINSI';
				break;

			case '3':
				$array_input['kategori_pendaftaran'] = 'KAB/KOTA';
				break;

			default:
				$array_input['kategori_pendaftaran'] = 'PUSAT';
				break;
		}

		$array_input['id_pengguna_buat'] = $this->mksess->id_pengguna();
		$array_input['tanggal_buat'] = date('Y-m-d H:i:s');
		$id_pendok 	 = $array_input['id_pendok'];
		/*echo "<pre>";
		var_dump($array_input);
		echo "</pre>";*/
		/* Start Logic Array */
		/* == Kasie == */
		$array_input['kasie_id'] = $array_input['id_pejabat_Kasie'];

		$array_input['nama_kasie'] = $array_input['nama_pejabat_Kasie'];

		unset($array_input['id_pejabat_Kasie']);
		unset($array_input['nama_pejabat_Kasie']);

		/* == Kasubdit == */
		$array_input['kasubdit_id'] = $array_input['id_pejabat_Kasubdit'];

		$array_input['nama_kasubdit'] = $array_input['nama_pejabat_Kasubdit'];

		unset($array_input['id_pejabat_Kasubdit']);
		unset($array_input['nama_pejabat_Kasubdit']);

		/* == Direktur == */
		$array_input['direktur_id'] = $array_input['id_pejabat_Direktur'];

		$array_input['nama_direktur'] = $array_input['nama_pejabat_Direktur'];

		unset($array_input['id_pejabat_Direktur']);
		unset($array_input['nama_pejabat_Direktur']);

		/* == nip direktur == */
		$array_input['nip_direktur'] = $array_input['nip_direktur_real'];
		unset($array_input['nip_direktur_real']);

		/* End Logic Array */
		$url = base_url('buku_kapal/main/views');
		/*echo "<pre>";
		var_dump($array_input);
		echo "</pre>";*/

		if( $this->mdl_buku_kapal->input($array_input) )
		{
			$this->mdl_buku_kapal->set_status_bkp($id_pendok);
			redirect($url);
		}
		else
		{
			redirect($url);
		}

	}

	public function update()
	{
		
		$this->load->model('mdl_buku_kapal');
		$array_input = $this->input->post(NULL, TRUE);
		$id_pendok 	 = $array_input['id_pendok'];

		/* Start Logic Array */

		//explode untuk array input lokasi_pengesahan
		$arr_lok_peng = explode("|", $array_input['lok_peng']);
		$array_input['lokasi_pengesahan_id'] = $arr_lok_peng[0];
		$array_input['lokasi_pengesahan'] = $arr_lok_peng[1];
		unset($array_input['lok_peng']);		

		/*No register*/
		unset($array_input['no_register']);

		/* == Kasie == */
		$array_input['kasie_id'] = $array_input['id_pejabat_Kasie'];

		$array_input['nama_kasie'] = $array_input['nama_pejabat_Kasie'];

		unset($array_input['id_pejabat_Kasie']);
		unset($array_input['nama_pejabat_Kasie']);

		/* == Kasubdit == */
		$array_input['kasubdit_id'] = $array_input['id_pejabat_Kasubdit'];

		$array_input['nama_kasubdit'] = $array_input['nama_pejabat_Kasubdit'];

		unset($array_input['id_pejabat_Kasubdit']);
		unset($array_input['nama_pejabat_Kasubdit']);

		/* == Direktur == */
		$array_input['direktur_id'] = $array_input['id_pejabat_Direktur'];

		$array_input['nama_direktur'] = $array_input['nama_pejabat_Direktur'];

		unset($array_input['id_pejabat_Direktur']);
		unset($array_input['nama_pejabat_Direktur']);

		/* == nip direktur == */
		$array_input['nip_direktur'] = $array_input['nip_direktur_real'];
		unset($array_input['nip_direktur_real']);

		// unset($array_input['id_lokasi']);

		unset($array_input['nama_lokasi']);

		/* End Logic Array */

		// vdump($array_input, true);
		
		if( $this->mdl_buku_kapal->update($id_pendok, $array_input) ){
			$this->mdl_buku_kapal->set_status_bkp($id_pendok);
			$url = base_url('buku_kapal/main/view/'.$id_pendok);
			redirect($url);
		}else{
			$url = base_url('buku_kapal/main/view/'.$id_pendok);
			redirect($url);
		}

	}

	public function status_cetak_bkp($id_pendok)
	{
		$this->load->model('mdl_buku_kapal');

		if( $this->mdl_buku_kapal->set_cetak_bkp($id_pendok) ){
			$url = base_url('buku_kapal/main/views_cetak');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

	public function status_terima_bkp($id_pendok)
	{
		$this->load->model('mdl_buku_kapal');

		if( $this->mdl_buku_kapal->set_terima_bkp($id_pendok) ){
			$url = base_url('buku_kapal/main/views_cetak');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

}

?>