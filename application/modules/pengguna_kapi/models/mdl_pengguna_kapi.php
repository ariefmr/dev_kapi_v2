<?php 

class Mdl_pengguna_kapi extends CI_Model
{

    private $db_dss;
    private $db_kapi;

	function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);
    }

    function list_pengguna_kapi()
    {
    	$query 		= "
    				   SELECT *
    				   FROM   db_pendaftaran_kapal.mst_pengguna_kapi
    				  ";
    	$run_query 	= $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    function input($data)
    {
        $this->db->insert('mst_pengguna_kapi', $data);
    }

    function detil_pengguna_kapi($id_pengguna_kapi)
    {
    	$sql = "SELECT *
                    FROM db_pendaftaran_kapal.mst_pengguna_kapi
                    WHERE mst_pengguna_kapi.id_pengguna_kapi = $id_pengguna_kapi ";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    function info_pengguna_kapi($id_pengguna)
    {
        $sql = "SELECT  mpk.id_propinsi,
                        mpk.id_kabupaten_kota,
                        mpk.kategori_pengguna,
                        mpk.kode_propinsi,
                        mpk.kode_kab_kota,
                        mpk.nama_propinsi,
                        mpk.nama_kabupaten_kota
                    FROM db_pendaftaran_kapal.mst_pengguna_kapi mpk
                    WHERE mpk.id_pengguna = $id_pengguna ";

        $run_query = $this->db->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function update($data)
    {
        $this->db->where('id_pengguna_kapi', $data['id_pengguna_kapi']);
        $query = $this->db->update('mst_pengguna_kapi',$data);

        if($this->db->affected_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;
    }

    public function delete($id)
    {
        $this->db->where('id_pengguna_kapi',$id);
        $query = $this->db->delete('mst_pengguna_kapi');

        if($query)
        {
            $result = true;
        }
        else{
            $result = false;
        }
        return $result;
    }

    function search_by_q($search_like)
    {
        $sql = "SELECT 
                        db_master.mst_pengguna.id_pengguna,
                        db_master.mst_pengguna.id_lokasi,
                        db_master.mst_pengguna.nama_pengguna
                FROM 
                        db_master.mst_pengguna
                WHERE   db_master.mst_pengguna.nama_pengguna LIKE '%".$search_like."%' ";

    $run_query = $this->db_dss->query($sql);                            

        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}

?>