	<?php
	/*OLAH DATA TAMPIL*/
	$template = array( "table_open" => "<table id='table_pengguna_kapi' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	$link_entry = '<a class="btn btn-primary" href="'.base_url('pengguna_kapi/main/entry/input').'">Entry Pengguna KAPI</a>';

	if($list_pengguna_kapi){
		foreach ($list_pengguna_kapi as $item) {
			$link_edit = '<a class="btn btn-warning" href="'.base_url('pengguna_kapi/main/edit/'.$item->id_pengguna_kapi).'">Edit</a>';
			$link_delete = '<a class="btn btn-danger" href="'.base_url('pengguna_kapi/main/delete/'.$item->id_pengguna_kapi).'">Hapus</a>';

			$this->table->add_row(
								$counter.'.',
								$item->nama_pengguna,
								$item->kategori_pengguna,
								$item->nama_propinsi,
								$item->nama_kabupaten_kota,
								$item->kode_propinsi,
								$item->kode_kab_kota,
								$link_edit." ".$link_delete
								);
			$counter++;
		}
	}

	$table_list_dokumen = $this->table->generate();
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
<!-- TAMPIL DATA -->
		<?php
			echo $table_list_dokumen;
		?>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		$('#table_pengguna_kapi').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>