<?php
// vdump($detail_pendok_online, true);
    $hidden_input = array(
                          'id_pendok_online'    => $detail_pendok_online['id_pendok_online']);
    if($aksi === 'view'){
?>
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group col-lg-10">
          </div>
          <div class="form-group col-lg-2">
              <div class="col-sm-offset-2 col-sm-4">
<?php 
             $link_aksi = '<a class="btn btn-primary" href="'.base_url('pendok_online/main/entry/'.$detail_pendok_online['id_pendok_online']).'">'.$button.'</a>';
             $link_set_status = '<a class="btn btn-primary" href="'.base_url('pendok_online/functions/set_status_diterima/'.$detail_pendok_online['id_pendok_online']).'">Diterima</a>';
              echo $link_aksi.$link_set_status; 
?>
            </div>
          </div>
  </div>
</div>
<div class="row">
    <div class="col-lg-12">

<?php
        echo form_open('id="form_entry" class="form-horizontal" role="form"', $hidden_input);
    }else{
?>

<div class="row">
    <div class="col-lg-12">

<?php
        echo form_open($submit_form, 'id="form_entry" class="form-horizontal" role="form"', $hidden_input);
    }
?>
<h3>Data Pendok</h3>
<hr>
<?php
    /* no_surat_permohonan */
    $no_surat_permohonan = array(   'name' => $form['no_surat_permohonan']['name'],
                                'label' => $form['no_surat_permohonan']['label'],
                                'value' => kos($detail_pendok_online['no_surat_permohonan'])
                    );
    echo $this->mkform->input_text($no_surat_permohonan);

    /* tipe_permohonan */
    $tipe_permohonan = array(   'name' => $form['tipe_permohonan']['name'],
                                'label' => $form['tipe_permohonan']['label'],
                                'value' => kos($detail_pendok_online['tipe_permohonan']),
                                'disabled' => true
                    );
    echo $this->mkform->input_text($tipe_permohonan);

    /* nama_kapal */
    $attr_nama_kapal = array(   'name' => $form['nama_kapal']['name'],
                                'label' => $form['nama_kapal']['label'],
                                'value' => kos($detail_pendok_online['nama_kapal']),
                                'disabled' => true
                    );
    echo $this->mkform->input_text($attr_nama_kapal);

    /* nama_kapal_sblm */
    $attr_nama_kapal_sblm = array(   'name' => $form['nama_kapal_sblm']['name'],
                                    'label' => $form['nama_kapal_sblm']['label'],
                                    'value' => kos($detail_pendok_online['nama_kapal_sblm']),
                                    'disabled' => true
                    );
    echo $this->mkform->input_text($attr_nama_kapal_sblm);

    /* nama_perusahaan */
    $nama_perusahaan = array(   'name' => $form['nama_perusahaan']['name'],
                                'label' => $form['nama_perusahaan']['label'],
                                'value' => kos($detail_pendok_online['nama_perusahaan']),
                                'disabled' => false
                    );
    echo $this->mkform->input_text($nama_perusahaan);

    /* nama_penanggung_jawab */
    $nama_penanggung_jawab = array(   'name' => $form['nama_penanggung_jawab']['name'],
                                'label' => $form['nama_penanggung_jawab']['label'],
                                'value' => kos($detail_pendok_online['nama_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($nama_penanggung_jawab);

    /* no_identitas_penanggung_jawab */
    $no_identitas_penanggung_jawab = array(   'name' => $form['no_identitas_penanggung_jawab']['name'],
                                'label' => $form['no_identitas_penanggung_jawab']['label'],
                                'value' => kos($detail_pendok_online['no_identitas_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($no_identitas_penanggung_jawab);

    /* ttl_penanggung_jawab */
    $ttl_penanggung_jawab = array(   'name' => $form['ttl_penanggung_jawab']['name'],
                                'label' => $form['ttl_penanggung_jawab']['label'],
                                'value' => kos($detail_pendok_online['ttl_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($ttl_penanggung_jawab);

    /* alamat_perusahaan */
    $alamat_perusahaan = array(   'name' => $form['alamat_perusahaan']['name'],
                                'label' => $form['alamat_perusahaan']['label'],
                                'value' => kos($detail_pendok_online['alamat_perusahaan'])
                    );
    echo $this->mkform->input_text($alamat_perusahaan);

    /* no_telp_perusahaan */
    $no_telp_perusahaan = array(   'name' => $form['no_telp_perusahaan']['name'],
                                'label' => $form['no_telp_perusahaan']['label'],
                                'value' => kos($detail_pendok_online['no_telp_perusahaan'])
                    );
    echo $this->mkform->input_text($no_telp_perusahaan);

    /* keterangan_pendok */
    $keterangan_pendok = array(   'name' => $form['keterangan_pendok']['name'],
                                'label' => $form['keterangan_pendok']['label'],
                                'value' => kos($detail_pendok_online['keterangan_pendok'])
                    );
    echo $this->mkform->input_text($keterangan_pendok);
// ===============================================================================================================================
?>

<hr>
<hr>
<h3>Data Kapal</h3>
<hr>

<?php
  $attr_tahun_pembangunan = array(   'name' => $form['tahun_pembangunan']['name'],
                                        'label' => $form['tahun_pembangunan']['label'],
                                        'value' => kos($detail_pendok_online['tahun_pembangunan'])
                                );
    echo $this->mkform->input_text($attr_tahun_pembangunan);

    $attr_bahan_kapal = array(
                                'input_id' => $form['bahan_kapal']['name'],
                                'input_name' => $form['bahan_kapal']['name'],
                                'label_text' => $form['bahan_kapal']['label'],
                                'array_opsi' => '', 
                                'opsi_selected' => kos($detail_pendok_online["id_bahan_kapal"]),  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label',
                                'from_table' => 'mst_bahan_kapal', 
                                'field_value' => 'id_bahan_kapal',
                                'field_text' => 'nama_bahan_kapal'
                            );
    $attr_bahan_kapal['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->dropdown_dss($attr_bahan_kapal);

    $attr_jenis_kapal = array(
                                'input_id' => 'id_jenis_kapal', 
                                'input_name' => 'id_jenis_kapal',
                                'label_text' => 'Jenis Kapal <em>*</em>',
                                'array_opsi' => '', 
                                'opsi_selected' => $detail_pendok_online["id_jenis_kapal"],  
                                'input_width' => 'col-lg-6 manual_input', 
                                'input_class' => 'form-control test', 
                                'label_class' => 'col-lg-3 manual_input control-label',
                                'from_table' => 'mst_jenis_kapal', 
                                'field_value' => 'id_jenis_kapal',
                                'field_text' => 'nama_jenis_kapal'
                            );
    $attr_jenis_kapal['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->dropdown_dss($attr_jenis_kapal);

    $attr_jenis_alat_tangkap = array( 'name' => 'id_alat_tangkap',
                                      'label' => $form['alat_tangkap']['label'],
                                      'opsi' => Modules::run('refdss/mst_alat_tangkap/list_alat_tangkap_array'),
                                      'value' => $detail_pendok_online['id_alat_tangkap'].'|'.$detail_pendok_online['id_alat_tangkap']
                    );

    $attr_jenis_alat_tangkap['enable'] = ($aksi === "view")? "false" : "true";
    echo $this->mkform->input_select2($attr_jenis_alat_tangkap);

    $attr_merk_mesin_kapal = array( 'name' => $form['merek_mesin']['name'],
                                    'label' => $form['merek_mesin']['label'],
                                    'value' => kos($detail_pendok_online['merek_mesin'])
                    );
    echo $this->mkform->input_text($attr_merk_mesin_kapal);

    $attr_tipe_mesin_kapal = array( 'name' => $form['tipe_mesin']['name'],
                                    'label' => $form['tipe_mesin']['label'],
                                    'value' => kos($detail_pendok_online['tipe_mesin'])
                    );
    echo $this->mkform->input_text($attr_tipe_mesin_kapal);

    $attr_daya_kapal = array(   'name' => $form['daya_kapal']['name'],
                                'label' => $form['daya_kapal']['label'],
                                'value' => kos_value($detail_pendok_online['daya_kapal']),
                                'tipe' => 'text'
                    );
    // echo $this->mkform->input_text($attr_daya_kapal);

    $attr_satuan_mesin_utama_kapal = array( 'name' => $form['satuan_mesin_utama_kapal']['name'],
                                            'label' => $form['satuan_mesin_utama_kapal']['label'],
                                            'opsi' => array('1' => 'PK', 
                                                            '2' => 'DK',
                                                            '3' => 'HP'),
                                            'value' => kos($detail_pendok_online['satuan_mesin_utama_kapal']),
                                            'tipe' => 'select'
                    );
    $attr_satuan_mesin_utama_kapal['enable'] = ($aksi === "view")? "false" : "true";
    // echo $this->mkform->input_select($attr_satuan_mesin_utama_kapal);

    $attr_multi_input_daya_mesin = array (
                            'name'  => "multi_input",
                            'label'  => "Daya Mesin Kapal",
                            'arr'   => array(
                                        $attr_daya_kapal,
                                        $attr_satuan_mesin_utama_kapal
                                        ),
                            'tipe' => 'true'
                        );
    echo $this->mkform->one_row($attr_multi_input_daya_mesin);

    $attr_no_mesin = array( 'name' => $form['no_mesin']['name'],
                            'label' => $form['no_mesin']['label'],
                            'value' => kos($detail_pendok_online['no_mesin'])
                    );
    echo $this->mkform->input_text($attr_no_mesin);

    $attr_jumlah_palka = array( 'name' => $form['jumlah_palka']['name'],
                                'label' => $form['jumlah_palka']['label'],
                                'value' => kos_value($detail_pendok_online['jumlah_palka'])
                    );
    echo $this->mkform->input_text($attr_jumlah_palka);

    $attr_kapasitas_palka = array(  'name' => $form['kapasitas_palka']['name'],
                                    'label' => $form['kapasitas_palka']['label'],
                                    'value' => kos_value($detail_pendok_online['kapasitas_palka'])
                    );
    echo $this->mkform->input_text($attr_kapasitas_palka);

    $attr_tempat_grosse_akte = array(   'name' => $form['tempat_grosse_akte']['name'],
                                        'label' => $form['tempat_grosse_akte']['label'],
                                        'value' => kos($detail_pendok_online['tempat_grosse_akte'])
                    );
    echo $this->mkform->input_text($attr_tempat_grosse_akte);

    $attr_no_grosse_akte = array(   'name' => $form['no_grosse_akte']['name'],
                                    'label' => $form['no_grosse_akte']['label'],
                                    'value' => kos($detail_pendok_online['no_grosse_akte'])
                    );
    echo $this->mkform->input_text($attr_no_grosse_akte);

    $attr_path_file_gross = array(
                                    'name'  =>  $form['path_file_gross']['name'],
                                    'label' =>  $form['path_file_gross']['label'],
                                    'view_file' => false,
                                    'value' =>  kos($detail_pendok_online['path_file_gross'])
                                );
    echo $this->mkform->input_image($attr_path_file_gross);

    $attr_imo_number = array(   'name' => $form['imo_number']['name'],
                                    'label' => $form['imo_number']['label'],
                                    'value' => kos($detail_pendok_online['imo_number'])
                    );
    echo $this->mkform->input_text($attr_imo_number);

    $attr_tanggal_grosse_akte = array(  
                                    // 'mindate' => array('time' => '1 year'), // opsi: '', array('time' => '1 year'), '2012-10-11', tidak wajib ada
                                    // 'maxdate' => 'various', // opsi: '', array('time' => '1 year'), '2013-10-11', tidak wajib ada
                                    'default_date' => kos($detail_pendok_online['tanggal_grosse_akte']),
                                    'placeholder' => '', // wajib ada atau '' (kosong)
                                    'name' => $form['tanggal_grosse_akte']['name'], // wajib ada
                                    'label' => $form['tanggal_grosse_akte']['label'], // wajib ada
                                    'curdate' => false
                    );
    echo $this->mkform->input_date($attr_tanggal_grosse_akte);

/*  
//-------------------------------------------------------------------------------
FORM UNTUK PANJANG, LEBAR, DALAM KAPAL SEBELUM MENGGUNAKAN MKFORM ONE_ROW()
//-------------------------------------------------------------------------------

    echo $this->mkform->input_text($attr_panjang_kapal);

    echo $this->mkform->input_text($attr_lebar_kapal);

    echo $this->mkform->input_text($attr_dalam_kapal);

//-------------------------------------------------------------------------------
END FORM UNTUK PANJANG, LEBAR, DALAM KAPAL SEBELUM MENGGUNAKAN MKFORM ONE_ROW()
//-------------------------------------------------------------------------------
*/

// FORM INPUT UNTUK PANJANG, LEBAR, DAN DALAM KAPAL MENGGUNAKAN FUNGSI MKFORM ONE_ROW()
    $attr_panjang_kapal = array(    'name' => $form['panjang_kapal']['name'],
                                    'label' => $form['panjang_kapal']['label'],
                                    'value' => kos_value($detail_pendok_online['panjang_kapal'])
                    );
    $attr_lebar_kapal = array( 'name' => $form['lebar_kapal']['name'],
                                'label' => $form['lebar_kapal']['label'],
                                'value' => kos_value($detail_pendok_online['lebar_kapal'])
                    );
    $attr_dalam_kapal = array( 'name' => $form['dalam_kapal']['name'],
                                'label' => $form['dalam_kapal']['label'],
                                'value' => kos_value($detail_pendok_online['dalam_kapal'])
                    );

    $attr_multi_input = array (
                            'name'  => "multi_input",
                            'label'  => "Panjang x Lebar x Dalam",
                            'arr'   => array(
                                        $attr_panjang_kapal,
                                        $attr_lebar_kapal,
                                        $attr_dalam_kapal
                                        )
                        );
    echo $this->mkform->one_row($attr_multi_input);
// END FORM INPUT UNTUK PANJANG, LEBAR, DAN DALAM KAPAL MENGGUNAKAN FUNGSI MKFORM ONE_ROW()

    $attr_panjang_loa_kapal = array(    'name' => $form['panjang_loa_kapal']['name'],
                                        'label' => $form['panjang_loa_kapal']['label'],
                                        'value' => kos_value($detail_pendok_online['panjang_loa_kapal'])
                    );
    echo $this->mkform->input_text($attr_panjang_loa_kapal);

    $attr_gt_kapal = array( 'name' => $form['gt_kapal']['name'],
                            'label' => $form['gt_kapal']['label'],
                            'value' => kos($detail_pendok_online['gt_kapal'])
                    );
    echo $this->mkform->input_text($attr_gt_kapal);

    $attr_nt_kapal = array( 'name' => $form['nt_kapal']['name'],
                            'label' => $form['nt_kapal']['label'],
                            'value' => kos($detail_pendok_online['nt_kapal'])
                    );
    echo $this->mkform->input_text($attr_nt_kapal);

// ---------------------------------------------------------------------------------------------------------------------
//SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO TELP PERUSAHAAN. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN
// ---------------------------------------------------------------------------------------------------------------------
    $attr_telp_perusahaan = array( 'name' => 'no_telp_perusahaan',
                            'label' => 'No Telp Perusahaan',
                            'value' => kos($detail_pendok_online['no_telp_perusahaan'])
                    );
    echo $this->mkform->input_text($attr_telp_perusahaan);    

    $attr_nt_kapal = array( 'name' => 'perusahaan_sblm',
                            'label' => 'Perusahaan Sebelum',
                            'value' => kos($detail_pendok_online['perusahaan_sblm'])
                    );
    echo $this->mkform->input_text($attr_nt_kapal);
// --------------------------------------------------------------------------------------------------------------------------
// END SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO TELP PERUSAHAAN. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN
// --------------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA TEMPAT TANGGAL LAHIR. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 8 april 2015
// ---------------------------------------------------------------------------------------------------------------------
    $attr_ttl = array( 'name' => 'ttl_penanggung_jawab',
                            'label' => 'Tempat Tanggal Lahir',
                            'value' => kos($detail_pendok_online['ttl_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($attr_ttl);
// --------------------------------------------------------------------------------------------------------------------------
// END SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA TEMPAT TANGGAL LAHIR. JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 8 april 2015
// --------------------------------------------------------------------------------------------------------------------------

    //SEMENTARA, KARENA DARI DSS BELUM MENYEDIAKAN DATA NO. KTP JADI NYA INPUT MANUAL. INI PROPOS DARI PAK HASAN, 19 Juni 2015
// ---------------------------------------------------------------------------------------------------------------------
    $attr_ktp = array( 'name' => 'no_identitas_penanggung_jawab',
                            'label' => 'No. KTP Penanggung Jawab',
                            'value' => kos($detail_pendok_online['no_identitas_penanggung_jawab'])
                    );
    echo $this->mkform->input_text($attr_ktp);

	?>
  </div>
</div>




<?php

if($aksi === 'view'){
        $link_aksi = '<a class="btn btn-primary" href="'.base_url('pendok_online/main/entry/'.$detail_pendok_online['id_pendok_online']).'">'.$button.'</a>';
    }else{
        $link_aksi = '<button type="submit" class="btn btn-primary btn-submit" >SIMPAN</button>';
    }
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group col-lg-6">
          </div>
          <div class="form-group col-lg-4">
              <div class="col-sm-offset-2 col-sm-4">
<?php
              if(isset($cetak)){
                $link_cetak = '<a class="btn btn-warning" href="'.base_url('kapal/cetak/preview/'.$detail_pendok_online['id_pendok_online']).'">CETAK DRAFT DATA KAPAL</a>';
                // echo $link_cetak; 
              }
?>
              </div>
          </div>
          <div class="form-group col-lg-2">
              <div class="col-sm-offset-2 col-sm-4">

<?php 
              echo $link_aksi; 
?>

            </div>
        </div>

    </div>
</div>

</form>
<script>
  var is_aksi = "<?php echo $aksi ?>";

  var cek_aksi = function(){
    if(is_aksi==='view'){
      $("input").prop("disabled", true).removeAttr('class').css({"background-color":"#f7f8f2","border":"0px","margin-top":"8px"});
      $(".col-sm-8").prepend(": ");
      $("select").prop("disabled", true);
      $("textarea").prop("disabled", true);
    }
  }
  $("input[name=nama_kapal]").prop("disabled", true);
  $("input[name=nama_kapal_sebelumnya]").prop("disabled", true);

  var submit_listener = function(){
    $(".btn-submit").click(function(event){
      event.preventDefault();
      var submit_to = $(this).data('submitTo');
      $("input[name=submit_to]").val(submit_to);
      $("#form_entry").submit();
    });
  };
  s_func.push(submit_listener);
  s_func.push(cek_aksi);
</script>