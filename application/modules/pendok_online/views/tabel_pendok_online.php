<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading($constants['th_table']);
	$counter = 1;
	if($list_data){
		foreach ($list_data as $item) {
			$link_kapal = '<a href="'.base_url('pendok_online/main/view/'.$item->id_pendok_online).'">'.$item->nama_kapal.'</a>';
			$this->table->add_row(
								$counter.'.',
								$item->status_pendok_online,
								$link_kapal,
								$item->nama_perusahaan,
								$item->tipe_permohonan,
								tgl($item->tanggal_buat)." | -".kos($item->nama_pemohon, '-')
								);
			$counter++;
		}
	}

	$table_list_data = $this->table->generate();
	// vdump($table_list_data);
?>

<p></p>
<!-- TAMPIL DATA -->
	<div id="loading_info" class="panel panel-default">
		<div class="panel-body">
		   <p class="text-center">
		   		Memuat Data Pendok. Harap Tunggu...
		   </p>
		</div>
	</div>

	<div id="table_container" class="panel-body overflowed hidden">
		<?php
			echo $table_list_data;
		?>
	</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		// $("#table_container").hide();

		$('#table_daftar_pendok').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
        	"iDisplayLength": 30,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
		    }
		} );
	} );
</script>