<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MX_Controller{

	function __construct()
	{
		parent::__construct();
	}


	public function input()
	{
		$this->load->model('mdl_pendok_online');
		$array_input = $this->input->post(NULL,TRUE);

		//=============BEGIN- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$id_lokasi = $this->mksess->id_lokasi();
		
		$info_lokasi_pengguna = (array)$this->mdl_pendok->get_info_lokasi($id_propinsi_pengguna, $id_kabkota_pengguna);

		$array_input['id_propinsi_pendaftaran'] = $id_propinsi_pengguna;
		$array_input['id_kabkota_pendaftaran'] = $id_kabkota_pengguna;
		$array_input['propinsi_pendaftaran'] = $info_lokasi_pengguna['nama_propinsi'];
		$array_input['kabkota_pendaftaran'] = $info_lokasi_pengguna['nama_kabupaten_kota'];

		//=============END- SET UNTUK KEBUTUHAN INFO LOKASI PENGGUNA APLIKASI didapat dari session===============

		/* End Logic Array */
		$url = base_url('pendok_online/main/views');
		/*echo "<pre>";
		var_dump($array_input);
		echo "</pre>";*/

		if( $this->mdl_pendok_online->input($array_input) )
		{
			$this->mdl_pendok_online->set_status_bkp($id_pendok_online);
			redirect($url);
		}
		else
		{
			redirect($url);
		}

	}

	public function update()
	{
		
		$this->load->model('mdl_pendok_online');
		$array_input = $this->input->post(NULL, TRUE);
		$id_pendok_online 	 = $array_input['id_pendok_online'];

		// vdump($array_input, true);
		
		if( $this->mdl_pendok_online->update($id_pendok_online, $array_input) ){
			$url = base_url('pendok_online/main/view/'.$id_pendok_online);
			redirect($url);
		}else{
			$url = base_url('pendok_online/main/view/'.$id_pendok_online);
			redirect($url);
		}

	}

	public function set_status_diterima($id_pendok_online)
	{
		$this->load->model('mdl_pendok_online');

		$get_detail_pendok_online = (array)$this->mdl_pendok_online->detail_pendok_online($id_pendok_online);
		$array_input = (array) $get_detail_pendok_online[0];

		$i = 1;
		foreach ($array_input as $key => $value) {
			if($i <= 19){
				if( ($key == 'id_pendok_online') || ($key == 'id_pemohon_online') ){
					$array_input_pendok[$key] = $value;
					$array_input_kapal[$key] = $value;
				}else{
					$array_input_pendok[$key] = $value;
					
				}
			}else{
				$array_input_kapal[$key] = $value;
			}
			$i++;
			// echo $key." = ".$value;
			// echo "<br>";
		}
		// vdump($array_input_pendok);
		// vdump($array_input_kapal, true);

		if( $this->mdl_pendok_online->update_status_pendok_online($id_pendok_online)){

			$this->mdl_pendok_online->input_trs_pendok_testing($array_input_pendok);
			$this->mdl_pendok_online->input_trs_bkp_testing($array_input_kapal);
			
			$url = base_url('pendok_online/main/views');
			redirect($url);
		}else{
			$url = base_url('pendok_online/main/views');
			redirect($url);
		}
	}

	public function status_cetak_bkp($id_pendok_online)
	{
		$this->load->model('mdl_pendok_online');

		if( $this->mdl_pendok_online->set_cetak_bkp($id_pendok_online) ){
			$url = base_url('pendok_online/main/views_cetak');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

	public function status_terima_bkp($id_pendok_online)
	{
		$this->load->model('mdl_pendok_online');

		if( $this->mdl_pendok_online->set_terima_bkp($id_pendok_online) ){
			$url = base_url('pendok_online/main/views_cetak');
			redirect($url);
		}else{
			$url = base_url('kapal/main/views');
			redirect($url);
		}
	}

}

?>