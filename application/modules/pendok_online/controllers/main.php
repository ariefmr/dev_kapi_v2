<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_pendok_online');
	}

	public function entry($id_pendok_online = '')
	{	
		if($id_pendok_online !== '')
		{
			$get_detail_pendok_online = $this->mdl_pendok_online->detail_pendok_online($id_pendok_online);
			$data['detail_pendok_online'] = (array) $get_detail_pendok_online[0];

		}

		$data['submit_form'] = 'pendok_online/functions/update';

		$data['aksi'] = 'entry';
		$file_view = 'form_pendok_online';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok_online', //nama module
							$file_view, //nama file view
							'label_form_pendok_online', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function views()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'pendok_online';
		$views = 'tabel_pendok_online';
		$labels = 'label_view_pendok_online';

		$data['list_data'] = $this->mdl_pendok_online->list_pendok_online();

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function view($id_pendok_online)
	{	
		$get_detail_pendok_online = $this->mdl_pendok_online->detail_pendok_online($id_pendok_online);
		$data['detail_pendok_online'] = (array) $get_detail_pendok_online[0];

		$data['aksi'] = 'view';
		if($data['detail_pendok_online']['nama_pemohon']==NULL){
			$data['button'] = 'entry';
		}else{
			$data['button'] = 'edit';
			$data['cetak'] = TRUE;
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok_online', //nama module
							'form_pendok_online', //nama file view
							'label_form_pendok_online', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function views_cetak()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->id_lokasi();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'pendok_online';
		$views = 'tabel_pendok_online_cetak';
		$labels = 'label_view_pendok_online_cetak';

		$data['list_bkp_pendok'] = $this->mdl_pendok_online->list_pendok_online_cetak($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}

	public function edit($id_pendok_online = '')
	{	
		if($id_pendok_online !== '')
		{
			$get_detail_pendok_online = $this->mdl_pendok_online->detail_pendok_online($id_pendok_online);
			$data['detail_pendok_online'] = (array) $get_detail_pendok_online[0];
		}

		// $data['submit_form'] = 'pendok/edit/update';
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok_online', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

}