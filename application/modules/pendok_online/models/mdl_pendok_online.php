<?php

class Mdl_pendok_online extends CI_Model
{
	function __construct()
    {
        $this->load->database();
    }

    public function list_pendok_online()
    {
    
        $query =   "SELECT  *
                    FROM    db_pendaftaran_kapal.trs_pendok_online as tpo
                    WHERE tpo.aktif = 'YA' ";

        $query .=  "ORDER BY tpo.id_pendok_online DESC
                   ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function detail_pendok_online($id_pendok_online)
    {
        
        $query =   "SELECT  *
                    FROM    db_pendaftaran_kapal.trs_pendok_online as tpo
                    WHERE tpo.aktif = 'YA' 
                        and tpo.id_pendok_online = $id_pendok_online
                    ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    
    public function update($id_pendok_online, $data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_ubah'] = $this->mksess->id_pengguna();
        $data['tanggal_ubah'] = $tanggal;

        $this->db->where('id_pendok_online',$id_pendok_online);
        $this->db->update('trs_pendok_online',$data);

        $this->db->last_query();

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;
        }
        else
        {
            $affected = false;
        }

        return $affected;
    }

    public function input_trs_pendok_testing($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;
        unset($data['nama_kapal_sblm']);
        unset($data['perusahaan_sblm']);
        unset($data['no_telp_perusahaan']);
        unset($data['no_register']);
        unset($data['nama_pemohon']);


        $this->db->insert('trs_pendok_testing', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function input_trs_bkp_testing($data)
    {

        $tanggal = date('Y-m-d');
        $data['id_pengguna_buat'] = $this->mksess->id_pengguna();
        $data['tanggal_buat'] = $tanggal;
        
        $data['id_alat_tangkap'] = $data['id_alat_tangkap'];
        $data['id_bahan_kapal'] = 999;
        $data['id_jenis_kapal'] = 999;

        unset($data['status_pendok_online']);
        unset($data['id_alat_tangkap']);

        $this->db->insert('trs_bkp_testing', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $id_pendok = $this->db->insert_id();    
        }else{
            $id_pendok = false;
        }

        return $id_pendok;
    }

    public function update_status_pendok_online($id_pendok_online)
    {
        $date_terima = date('Y-m-d');
        $data = array('status_pendok_online' => 'TERIMA');
        $this->db->where('id_pendok_online', $id_pendok_online);
        $this->db->where('status_pendok_online', 'TUNDA');
        $this->db->update('trs_pendok_online', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

    public function update_status_pendok($id_pendok_online)
    {
        $date_terima = date('Y-m-d');
        $data = array('status_entry_bkp' => '2');
        $this->db->where('id_pendok_online', $id_pendok_online);
        $this->db->update('trs_pendok', $data);

        $aff_rows = $this->db->affected_rows();
        if($aff_rows > 0)
        {
            $affected = true;   
        }else{
            $affected = false;
        }

        return $affected;

    }

}

?>