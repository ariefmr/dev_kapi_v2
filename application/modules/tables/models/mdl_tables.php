<?php
/*
 * class Mdl_konfigurasi
 */

class Mdl_tables extends CI_Model
{
    
    /*
     * __construct()
     * @param $arg
     */
    
    private $db_dss;
    private $db_kapi;

    function __construct()
    {
        $this->db_dss = $this->load->database('db_dss', TRUE);
        $this->db_kapi = $this->load->database('default', TRUE);        
    }

    public function next_no_rekam_pendok($tipe_lokasi, $id_lokasi)
    {
        $this->db_kapi->select('counter');
        $query = $this->db_kapi->get_where('counter_pendok',
                                   array('tipe_lokasi'=> $tipe_lokasi, 
                                         'id_lokasi' => $id_lokasi)
                                );

        if($query->num_rows() > 0){
            $result = $query->row()->counter;
            $result++;
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_counter_pendok($tipe_lokasi, $id_lokasi)
    {
        $sql_update = "UPDATE counter_pendok SET counter = counter + 1 WHERE tipe_lokasi = '$tipe_lokasi' AND id_lokasi = '$id_lokasi' ";
        $run_query = $this->db_kapi->query($sql_update);
    }

    public function next_no_register($tipe_lokasi, $id_lokasi)
    {
        $this->db_kapi->select('counter');
        $query = $this->db_kapi->get_where('counter_register',
                                   array('tipe_lokasi'=> $tipe_lokasi, 
                                         'id_lokasi' => $id_lokasi)
                                );

        if($query->num_rows() > 0){
            $result = $query->row()->counter;            
            $result++;
        }else{
            $result = false;
        }
        return $result;
    }

    public function update_counter_register($tipe_lokasi, $id_lokasi)
    {
        $sql_update = "UPDATE counter_register SET counter = counter + 1 WHERE tipe_lokasi = '$tipe_lokasi' AND id_lokasi = '$id_lokasi' ";
        $run_query = $this->db_kapi->query($sql_update);
    }

    public function list_all_tables($from_db)
    {
        $list_tables = $from_db === 'db_kapi' ? $this->db_kapi->list_tables() : $this->db_dss->list_tables();
        return $list_tables;
    }

    public function is_field_exist($from_db, $nama_tabel, $field)
    {
        $is_field_exist  = $from_db === 'db_kapi' ? $this->db_kapi->field_exists($field, $nama_tabel) : $this->db_dss->field_exists($field, $nama_tabel);
        return $is_field_exist;
    }
    
    public function kapi_tbl_opsi($nama_tabel, $field_id, $field_text, $is_json = FALSE)
    {
        if($is_json){
            $this->db_kapi->select($field_id.' AS id , '.$field_text.' AS text');        
        }else{
            $this->db_kapi->select($field_id.' , '.$field_text);        
        }
        $query = $this->db_kapi->get($nama_tabel);
        $this->db_kapi->where('aktif','Ya');
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function dss_tbl_opsi($nama_tabel, $field_id, $field_text, $is_json = FALSE)
    {
        if($is_json){
            $this->db_dss->select($field_id.' AS id , '.$field_text.' AS text', FALSE);        
        }else{
            $this->db_dss->select($field_id.' , '.$field_text, FALSE);        
        }
        $this->db_dss->where('aktif','Ya');
        $query = $this->db_dss->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;

    }

    public function kapi_get_all($nama_tabel, $order_field = '')
    {
        if($order_field !== '')
        {
            $this->db_kapi->order_by($order_field , 'asc');
        }
        $query = $this->db_kapi->get($nama_tabel);
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function dss_get_all($nama_tabel, $order_field = '')
    {
        if($order_field !== '')
        {
            $this->db_dss->order_by($order_field , 'asc');
        }
        $query = $this->db_dss->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function dss_get_all_by_id($nama_tabel, $id = '')
    {
        $this->db_dss->where('id_propinsi',$id);
        $query = $this->db_dss->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function kapi_get_all_by_id($nama_tabel, $id = '')
    {
        $this->db_kapi->where('id_propinsi',$id);
        $query = $this->db_kapi->get($nama_tabel);
        
        if($query->num_rows() > 0){
            $result = $query->result();
        }else{
            $result = false;
        }
        return $result;
    }
    
    public function delete_from($nama_tabel, $primary_key, $id)
    {
        $sql = " UPDATE db_kapi.".$nama_tabel." SET aktif='Tidak' WHERE $primary_key = $id ";
        $run_query = $this->db_kapi->query($sql);
        // //var_dump($sql);
    } 

}
?>