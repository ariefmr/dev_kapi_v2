<?php

	//Olah data tampil untuk buku kapal
	$template = array( "table_open" 	=> 	"<table id='table_progres' class='table table-hover table-bordered'>"
					);
	$this->table->set_template($template);
	//$this->table->set_heading($constants['th_table']);
	$counter = 1;
	/*if($list_bkp_pendok){
		// var_dump($list_bkp_pendok);
		foreach ($list_bkp_pendok as $item) {
			$link_entry = '<a href="'.base_url('buku_kapal/main/view/'.$item->id_pendok).'">'.$item->nama_kapal.'</a>';
			
			$btn_blm_entry = '<a class="btn btn-warning" href="'.base_url('kapal/main/entry/'.$item->id_pendok).'">PROSES ENTRY</a>';
			$btn_blm_bkp = '<a class="btn btn-warning" href="'.base_url('buku_kapal/main/entry/'.$item->id_pendok).'">PROSES BKP</a>';
			$btn_blm_ver = '<button OnClick="warning_validasi('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">PROSES VERIFIKASI</button>';
			$btn_sdh_ver = '<a class="btn btn-primary" href="'.base_url('kapal/cetak/preview/'.$item->id_pendok).'">VERIFIKASI</a>';
			$link_cetak = '<a class="btn btn-success" href="'.base_url('buku_kapal/cetak/preview_draft/'.$item->id_pendok).'">DRAFT BUKU</a>';
			// $btn_blm_terima = '<button OnClick="warning_validasi('.$item->id_pendok.')" type="button" class="btn btn-danger" data-pendok="'.$item->id_pendok.'">BELUM</button>';
			// $btn_sdh_terima = '<a class="btn btn-primary" href="'.base_url('kapal/cetak/preview/'.$item->id_pendok).'">SUDAH</a>';

			switch ($item->status_entry_bkp) {
				case '0':
					$btn_verifikasi = $btn_blm_entry;
					break;

				case '1':
					$btn_verifikasi = $btn_blm_bkp;
					break;
				
				case '2':
					if($item->status_verifikasi === "YA"){
						$btn_verifikasi = $btn_sdh_ver;
					}else{
						$btn_verifikasi = $btn_blm_ver;
					}
					break;

				default:
					$btn_verifikasi = $btn_blm_entry;
					break;
			}

			$this->table->add_row(
								$counter.'.',
								$item->no_rekam_pendok,
								$item->no_register,
								$item->tipe_permohonan,
								$link_entry,
								$item->nama_penanggung_jawab,
								$item->gt,
								$item->kategori_pendaftaran,
								$link_cetak,
								$btn_verifikasi
								/*$link_cetak,
								$btn_verifikasi*/
								/*,
								'<a href="" >Check</a>',
								'<a href="" >Check</a>'
								);
			$counter++;
		}
	}
	*/

	//$table_buku_kapal = $this->table->generate();

?>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="#compare" data-toggle="tab">Compare</a></li>
  <li><a href="#perusahaan" data-toggle="tab">Tanpa Perusahaan</a></li>
  <li><a href="#alat_tangkap" data-toggle="tab">Tanpa ALat Tangkap</a></li>
  <li><a href="#gt" data-toggle="tab">Tanpa GT</a></li>
  <li><a href="#grosse_akte" data-toggle="tab">Tanpa Grosse AKte</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="compare">

  	<table id="table_mapping" class="table">
	<thead>
		<tr>
			<td colspan="11" align="left" >Belum mapping : <?=$belum_mapping?> | Sudah mapping : <?=$sudah_mapping?></td>
		</tr>
		<tr>
			<th colspan="6" class="text-center" >DSS</th>
			<th colspan="6" class="text-center" >KAPI</th>
		</tr>
		<tr>
			<th>ID Kapal</th>
			<th>ID Izin</th>
			<th>Nama Kapal</th>
			<th>Grosse Akte</th>
			<th>Tanda Selar</th>
			<th>GT Kapal</th>
			<th>No Rekam Pendok</th>
			<th>ID Kapal</th>
			<th>Nama Kapal</th>
			<th>ID Kapal</th>
			<th>No Grosse Akte</th>
			<th>GT Kapal</th>
			<th>View</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($list_compare as $key => $value) {
				?>
				<tr>
					<td><?=$value->id_kapal_dss?></td>
					<td><?=$value->id_izin_dss?></td>
					<td><?=$value->nama_kapal_dss?></td>
					<td><?=$value->gross_akte_dss?></td>
					<td><?=$value->tanda_selar?></td>
					<td><?=$value->gt_kapal_dss?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->id_kapal_kapi?></td>
					<td><?=$value->nama_kapal_kapi?></td>
					<td><?=$value->id_bkp_kapi?></td>
					<td><?=$value->gross_akte_kapi?></td>
					<td><?=$value->gt_kapal_kapi?></td>
					<td><a href="<?=base_url('kapal/main/view/'.$value->no_rekam_pendok)?>" target="_blank" >KAPI</a>&nbsp;|&nbsp;<a href="http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=<?=$value->id_kapal_dss?>&id_izin=<?=$value->id_izin_dss?>" target="_blank" >DSS</a></td>
					<td>
						<a target="_blank" href="http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=<?=$value->id_kapal_dss?>&id_izin=<?=$value->id_izin_dss?>#ui-tabs-1" > Mapping</a>
					</td>
				</tr>
				<?php
			}
		?>
		<tr>
			<td>ID Kapal</td>
			<td>ID Izin</td>
			<td>Nama Kapal</td>
			<td>Grosse Akte</td>
			<td>Tanda Gelar</td>
			<td>GT Kapal</td>
			<td>No Rekam Pendok</td>
			<td>ID Kapal</td>
			<td>Nama Kapal</td>
			<td>ID Kapal</td>
			<td>No Grosse Akte</td>
			<td>GT Kapal</td>
		</tr>
	</tbody>
</table>
  </div>
  <div class="tab-pane" id="perusahaan">
  	<table class="table" id="table_tperusahaan" >
	<thead>
		<tr>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($list_tperusahaan as $key => $value) {
				
				?>
				<tr>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->gt?></td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>
  </div>
  <div class="tab-pane" id="alat_tangkap">
  	<table class="table" id="table_talattangkap" >
	<thead>
		<tr>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($list_talattangkap as $key => $value) {
				
				?>
				<tr>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->gt?></td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>
  </div>
  <div class="tab-pane" id="gt">
  	<table class="table" id="table_tgt" >
	<thead>
		<tr>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($list_tgt as $key => $value) {
				
				?>
				<tr>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->gt?></td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>
  </div>
  <div class="tab-pane" id="grosse_akte">
  	<table class="table" id="table_tgrosseakte" >
	<thead>
		<tr>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>No. Grosse Akte</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($list_tgrosseakte as $key => $value) {
				
				?>
				<tr>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->no_grosse_akte?></td>
				</tr>
				<?php
			}
		?>
	</tbody>
</table>
  </div>
</div>

<!-- ADTTIONAL JAVASCRIPT -->
<script>

	  $(function () {
	    $('#myTab a:last').tab('show')
	  })

	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {



		$('#table_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tperusahaan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_talattangkap').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tgt').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tgrosseakte').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>