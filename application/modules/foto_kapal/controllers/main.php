<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_foto_kapal');
	}

	public function get()
	{	
		//ambil dari url
		$id_kapal = $this->input->get('id', NULL);

		$data = array(
					'id_kapal'	=> $id_kapal
				);

		$search_result	= $this->mdl_foto_kapal->get_image($data);

		$jumlah_result	= count($search_result);
		
		if(!$search_result){
			$jumlah_result = 0;
		}else{
			foreach ($search_result as $key => $item) {
				$search_result[$key]['image_url'] = site_url("uploads/foto_kapal")."/".$item['nama_foto'];
				$tmp_nama_foto = $item['nama_foto'];
				$tmp = explode(".", $tmp_nama_foto);
				$thumbnail = $tmp[0]."_thumb.jpg";
				$search_result[$key]['thumbnail_url'] = site_url("uploads/foto_kapal")."/".$thumbnail;
			}
		}

		$json			= array(

							'jumlah_result'	=>	$jumlah_result,

							'path_foto'		=>	site_url("uploads/foto_kapal"),

							'result'		=>	$search_result

							);
		echo json_encode($json);
	}

}