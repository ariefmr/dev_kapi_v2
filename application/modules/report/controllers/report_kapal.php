<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_kapal extends MX_Controller {

  function __construct()
    {
      parent::__construct();
      $this->load->model('mdl_report_kapal');
    }

  public function views()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $template = 'templates/page/v_form';
    $modules = 'report';
    $views = 'view_report_kapal';
    $labels = 'label_view_report_kapal';

    $data['list_cmf'] = $this->mdl_report_kapal->list_kapal_kapi($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
    // $data['list_cmf'] = $this->mdl_report_kapal->list_kapal_kapi_perbulan();
    // vdump($data, true);

    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function views_perbulan($tahun = '', $bulan = '')
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $template = 'templates/page/v_form';
    $modules = 'report';
    $views = 'view_report_kapal_perbulan';
    $labels = 'label_view_report_kapal_perbulan';

    $tgl_catat = array( 'bulan' => $bulan ,
                              'tahun' => $tahun
                          );
    if(empty($bulan) && empty($tahun)){
        $tgl_catat = array( 'bulan' => date("m", time() ), 
                              'tahun' => date("Y", time() )
                          );
        $bulan_ini = date('m');
        $tahun_ini = date('Y');
    }else{
      $bulan_ini = $bulan;
      $tahun_ini = $tahun;
      // vdump($tahun_ini, true);
    }

    $data['tmp_tgl_catat'] = $tgl_catat['tahun'].'-'.$tgl_catat['bulan'];

    // $data['list_cmf'] = $this->mdl_report_kapal->list_kapal_kapi();
    $data['list_cmf'] = $this->mdl_report_kapal->list_kapal_kapi_perbulan($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna, $tahun, $bulan);
    // vdump($data, true);

    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function xls()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();


    $list_cmf = $this->mdl_report_kapal->list_kapal_kapi($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
    // $list_cmf = $this->mdl_report_kapal->list_kapal_kapi_perbulan();

    $this->load->library('excel');

    $filename = "Daftar Kapal Terdaftar.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Daftar Kapal Terdaftar');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Daftar Kapal Terdaftar');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $array_thead = array( 'No. ',
                          'No Register',
                          'Tanda Pengenal',
                          'Nama Kapal',
                          'Nama Kapal Sebelumnya',
                          'Tempat Pembangunan',
                          'Bahan Utama Kapal',
                          'Fungsi Kapal',
                          'Jenis Alat Tangkap',
                          'Merek Mesin Utama',
                          'Daya Mesin Utama',
                          'No Mesin Utama',
                          'Jumlah Palka',
                          'Kapasitas Palka',
                          'Tempat Pendaftaran',
                          'Tempat Gross Akte',
                          'No Gross Akte',
                          'Tanggal Gross Akte',
                          'Panjang',
                          'Lebar',
                          'Dalam',
                          'LOA',
                          'GT',
                          'NT',
                          'Nama Pemilik',
                          'Alamat Pemilik',
                          'Keterangan',
                          'Tanggal Terima Dokumen',
                          'Tanggal Tanda Tangan Direktur'
                        );

    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'3', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 4;

    $number = 1;
    foreach ($list_cmf as $item) {
      /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
        $tempat_pembangunan = $item->tempat_pembangunan;
        switch ($item->kategori_pendaftaran) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $item->propinsi_pendaftaran;
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $item->kabkota_pendaftaran;
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }

      $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));


      $sheet->setCellValue('A'.$cell_number,  $number);
      $sheet->getStyle('B')->getNumberFormat()->setFormatCode('000000'); //format untuk noregister supaya formated
      $sheet->setCellValue('B'.$cell_number,  $item->no_register);
      $sheet->setCellValue('C'.$cell_number,  $item->no_tanda_pengenal);
      $sheet->setCellValue('D'.$cell_number,  $item->nama_kapal);
      $sheet->setCellValue('E'.$cell_number,  $item->nama_kapal_sblm );
      $sheet->setCellValue('F'.$cell_number,  $tempat_pembangunan);
      $sheet->setCellValue('G'.$cell_number,  $item->nama_bahan_kapal);
      $sheet->setCellValue('H'.$cell_number,  $item->nama_jenis_kapal );
      $sheet->setCellValue('I'.$cell_number,  $item->alat_tangkap);
      $sheet->setCellValue('J'.$cell_number,  $item->merek_mesin);
      $sheet->setCellValue('K'.$cell_number,  $item->daya_kapal);
      $sheet->setCellValue('L'.$cell_number,  $item->no_mesin);
      $sheet->setCellValue('M'.$cell_number,  $item->jumlah_palka);
      $sheet->setCellValue('N'.$cell_number,  $item->kapasitas_palka);
      $sheet->setCellValue('O'.$cell_number,  $tempat_pendaftaran);
      $sheet->setCellValue('P'.$cell_number,  $item->tempat_grosse_akte);
      $sheet->setCellValue('Q'.$cell_number,  $item->no_grosse_akte);
      $sheet->setCellValue('R'.$cell_number,  fmt_tgl($item->tanggal_grosse_akte));
      $sheet->setCellValue('S'.$cell_number,  $item->panjang_kapal);
      $sheet->setCellValue('T'.$cell_number,  $item->lebar_kapal);
      $sheet->setCellValue('U'.$cell_number,  $item->dalam_kapal);
      $sheet->setCellValue('V'.$cell_number,  $item->panjang_loa_kapal);
      $sheet->setCellValue('W'.$cell_number,  $item->gt_kapal);
      $sheet->setCellValue('X'.$cell_number,  $item->nt_kapal);
      $sheet->setCellValue('Y'.$cell_number,  $item->nama_perusahaan);
      $sheet->setCellValue('Z'.$cell_number,  $item->alamat_perusahaan);
      $sheet->setCellValue('AA'.$cell_number,  $item->keterangan_pendok);
      $sheet->setCellValue('AB'.$cell_number, fmt_tgl($item->tanggal_surat_permohonan));
      $sheet->setCellValue('AC'.$cell_number, fmt_tgl($item->tgl_ttd_direktur));
      $cell_number++;
      $number++;           
    }



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 5,
                            'B' => 20,
                            'C' => 40,
                            'D' => 40,
                            'E' => 40,
                            'F' => 40,
                            'G' => 25,
                            'H' => 25,
                            'I' => 50,
                            'J' => 40,
                            'K' => 20,
                            'L' => 20,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 30,
                            'Q' => 15,
                            'R' => 20,
                            'S' => 20,
                            'T' => 20,
                            'U' => 20,
                            'V' => 20,
                            'W' => 20,
                            'X' => 20,
                            'Y' => 50,
                            'Z' => 100,
                            'AA' => 40,
                            'AB' => 40,
                            'AC' => 40

                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Daftar Kapal Terdaftar.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }

  public function xls_perbulan($tahun = '', $bulan = '')
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    // $list_cmf = $this->mdl_report_kapal->list_kapal_kapi();
    $list_cmf = $this->mdl_report_kapal->list_kapal_kapi_perbulan($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna, $tahun, $bulan);

    $this->load->library('excel');

    $filename = "Daftar Kapal Terdaftar Perbulan.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Daftar Kapal Terdaftar Perbulan');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Daftar Kapal Terdaftar Perbulan');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $array_thead = array( 'No. ',
                          'No Register',
                          'Tipe Permohonan',
                          'Tanggal Pendaftaran',
                          'Tanda Pengenal',
                          'Nama Kapal',
                          'Nama Kapal Sebelumnya',
                          'Tempat Pembangunan',
                          'Bahan Utama Kapal',
                          'Fungsi Kapal',
                          'Jenis Alat Tangkap',
                          'Merek Mesin Utama',
                          'Daya Mesin Utama',
                          'No Mesin Utama',
                          'Jumlah Palka',
                          'Kapasitas Palka',
                          'Tempat Pendaftaran',
                          'Tempat Gross Akte',
                          'No Gross Akte',
                          'Tanggal Gross Akte',
                          'Panjang',
                          'Lebar',
                          'Dalam',
                          'LOA',
                          'GT',
                          'NT',
                          'Nama Pemilik',
                          'Alamat Pemilik',
                          'Keterangan',
                          'Tanggal Terima Dokumen',
                          'Tanggal Tanda Tangan Direktur'
                        );

    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'3', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 4;

    $number = 1;
    foreach ($list_cmf as $item) {
      /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
        $tempat_pembangunan = $item->tempat_pembangunan;
        switch ($item->kategori_pendaftaran) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $item->propinsi_pendaftaran;
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $item->kabkota_pendaftaran;
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }

      $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));


      $sheet->setCellValue('A'.$cell_number,  $number);
      $sheet->getStyle('B')->getNumberFormat()->setFormatCode('000000'); //format untuk noregister supaya formated
      $sheet->setCellValue('B'.$cell_number,  $item->no_register);
      $sheet->setCellValue('C'.$cell_number,  $item->tipe_permohonan);
      $sheet->setCellValue('D'.$cell_number,  fmt_tgl($item->tanggal_buat));
      $sheet->setCellValue('E'.$cell_number,  $item->no_tanda_pengenal);
      $sheet->setCellValue('F'.$cell_number,  $item->nama_kapal);
      $sheet->setCellValue('G'.$cell_number,  $item->nama_kapal_sblm );
      $sheet->setCellValue('H'.$cell_number,  $tempat_pembangunan);
      $sheet->setCellValue('I'.$cell_number,  $item->nama_bahan_kapal);
      $sheet->setCellValue('J'.$cell_number,  $item->nama_jenis_kapal );
      $sheet->setCellValue('K'.$cell_number,  $item->alat_tangkap);
      $sheet->setCellValue('L'.$cell_number,  $item->merek_mesin);
      $sheet->setCellValue('M'.$cell_number,  $item->daya_kapal);
      $sheet->setCellValue('N'.$cell_number,  $item->no_mesin);
      $sheet->setCellValue('O'.$cell_number,  $item->jumlah_palka);
      $sheet->setCellValue('P'.$cell_number,  $item->kapasitas_palka);
      $sheet->setCellValue('Q'.$cell_number,  $tempat_pendaftaran);
      $sheet->setCellValue('R'.$cell_number,  $item->tempat_grosse_akte);
      $sheet->setCellValue('S'.$cell_number,  $item->no_grosse_akte);
      $sheet->setCellValue('T'.$cell_number,  fmt_tgl($item->tanggal_grosse_akte));
      $sheet->setCellValue('U'.$cell_number,  $item->panjang_kapal);
      $sheet->setCellValue('V'.$cell_number,  $item->lebar_kapal);
      $sheet->setCellValue('W'.$cell_number,  $item->dalam_kapal);
      $sheet->setCellValue('X'.$cell_number,  $item->panjang_loa_kapal);
      $sheet->setCellValue('Y'.$cell_number,  $item->gt_kapal);
      $sheet->setCellValue('Z'.$cell_number,  $item->nt_kapal);
      $sheet->setCellValue('AA'.$cell_number,  $item->nama_perusahaan);
      $sheet->setCellValue('AB'.$cell_number,  $item->alamat_perusahaan);
      $sheet->setCellValue('AC'.$cell_number,  $item->keterangan_pendok);
      $sheet->setCellValue('AD'.$cell_number, fmt_tgl($item->tanggal_surat_permohonan));
      $sheet->setCellValue('AE'.$cell_number, fmt_tgl($item->tgl_ttd_direktur));
      $cell_number++;
      $number++;           
    }



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 5,
                            'B' => 20,
                            'C' => 20,
                            'D' => 20,
                            'E' => 40,
                            'F' => 40,
                            'G' => 40,
                            'H' => 40,
                            'I' => 25,
                            'J' => 25,
                            'K' => 50,
                            'L' => 40,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 20,
                            'Q' => 20,
                            'R' => 30,
                            'S' => 15,
                            'T' => 20,
                            'U' => 20,
                            'V' => 20,
                            'W' => 20,
                            'X' => 20,
                            'Y' => 20,
                            'Z' => 20,
                            'AA' => 50,
                            'AB' => 100,
                            'AC' => 40,
                            'AD' => 40,
                            'AE' => 40

                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Daftar Kapal Terdaftar.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );
    

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */