<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_pemilik_pemohon_kapal extends MX_Controller {

  function __construct()
    {
      parent::__construct();
      $this->load->model('mdl_report_pemilik_pemohon_kapal');
    }

  public function views()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $template = 'templates/page/v_form';
    $modules = 'report';
    $views = 'view_report_pemilik_pemohon_kapal';
    $labels = 'label_view_report_pemilik_pemohon_kapal';

    $data['list_cmf'] = $this->mdl_report_pemilik_pemohon_kapal->list_pemilik_pemohon_kapal($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
    // vdump($data, true);

    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function xls()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();


    $list_cmf = $this->mdl_report_pemilik_pemohon_kapal->list_pemilik_pemohon_kapal($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
    // $list_cmf = $this->mdl_report_kapal->list_kapal_kapi_perbulan();

    $this->load->library('excel');

    // $filename = "Daftar Pemilik GT 300.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Daftar Pemilik dan Pemohon');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Daftar Pemilik dan Pemohon Kapal');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $array_thead = array( 'No. ',
                          'Nama Pemilik',
                          'Nama Penanggung Jawab',
                          'Alamat Pemilik',
                          'No. Telp Pemilik',
                          'Nama Pemohon',
                          'No. Telp Pemohon'
                        );
  
    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'3', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 4;

    $number = 1;
    foreach ($list_cmf as $item) {

      $sheet->setCellValue('A'.$cell_number,  $number);
      $sheet->setCellValue('B'.$cell_number,  $item->nama_perusahaan);
      $sheet->setCellValue('C'.$cell_number,  $item->nama_penanggung_jawab);
      $sheet->setCellValue('D'.$cell_number,  $item->alamat_perusahaan);
      $sheet->setCellValue('E'.$cell_number,  $item->no_telp_perusahaan);
      $sheet->setCellValue('F'.$cell_number,  $item->nama_pemohon);
      $sheet->setCellValue('G'.$cell_number,  $item->no_telp_pemohon);
      $cell_number++;
      $number++;           

    }                      

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 5,
                            'B' => 45,
                            'C' => 45,
                            'D' => 70,
                            'E' => 15,
                            'F' => 45,
                            'G' => 18
                          );
    foreach ($array_thwidth as $column => $width) {
        // $sheet->getColumnDimension($column)->setWidth($width);
        $sheet->getColumnDimension($column)->setAutoSize(true);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
    $sheet->getStyle('G4:H4'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Daftar Pemilik dan Pemohon Kapal.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */