<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_kapal_tercetak extends MX_Controller {

  function __construct()
    {
      parent::__construct();
      $this->load->model('mdl_report_kapal_tercetak');
    }

  public function views()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $add_js = array('select2.min.js', 'jquery.dataTables.min.js');
    $add_css = array('select2.css', 'jquery.dataTables.css');

    $template = 'templates/page/v_form';
    $modules = 'report';
    $views = 'view_report_kapal_tercetak';
    $labels = 'label_view_report_kapal_tercetak';

    $data['list_cmf'] = $this->mdl_report_kapal_tercetak->list_kapal_kapi($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);
    // vdump($data, true);

    echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
  }

  public function xls()
  {
    //info session userdata
    $kategori_pengguna = $this->mksess->id_lokasi();
    $id_propinsi_pengguna = $this->mksess->id_propinsi();
    $id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
    
    $list_cmf = $this->mdl_report_kapal_tercetak->list_kapal_kapi($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

    $this->load->library('excel');

    $filename = "Daftar Kapal Tercetak.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Daftar Kapal Tercetak');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Daftar Kapal Tercetak');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');

    $array_thead = array( 'No. ',
                          'No Register',
                          'Tanda Pengenal',
                          'Nama Kapal',
                          'Nama Kapal Sebelumnya',
                          'Tempat Pembangunan',
                          'Bahan Utama Kapal',
                          'Fungsi Kapal',
                          'Jenis Alat Tangkap',
                          'Merek Mesin Utama',
                          'Daya Mesin Utama',
                          'No Mesin Utama',
                          'Jumlah Palka',
                          'Kapasitas Palka',
                          'Tempat Pendaftaran',
                          'Tempat No & Tanggal Gross Akte',
                          'Panjang',
                          'Lebar',
                          'Dalam',
                          'GT',
                          'NT',
                          'Nama Pemilik',
                          'Alamat Pemilik',
                          'Keterangan',
                          'Tanggal Terima Dokumen',
                          'Tanggal Tanda Tangan Direktur',
                          'Halaman 1 Dicetak oleh',
                          'Halaman 2 Dicetak oleh',
                          'Halaman 3 Dicetak oleh',
                          'Halaman 4 Dicetak oleh'
                        );

    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'3', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 4;

    $number = 1;
    foreach ($list_cmf as $item) {
      /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
        $tempat_pembangunan = $item->tempat_pembangunan;
        switch ($item->kategori_pendaftaran) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $item->propinsi_pendaftaran;
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $item->kabkota_pendaftaran;
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }

      $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));


      $sheet->setCellValue('A'.$cell_number,  $number);
      $sheet->getStyle('B')->getNumberFormat()->setFormatCode('000000'); //format untuk noregister supaya formated
      $sheet->setCellValue('B'.$cell_number,  $item->no_register);
      $sheet->setCellValue('C'.$cell_number,  $item->no_tanda_pengenal);
      $sheet->setCellValue('D'.$cell_number,  $item->nama_kapal);
      $sheet->setCellValue('E'.$cell_number,  $item->nama_kapal_sblm );
      $sheet->setCellValue('F'.$cell_number,  $tempat_pembangunan);
      $sheet->setCellValue('G'.$cell_number,  $item->nama_bahan_kapal);
      $sheet->setCellValue('H'.$cell_number,  $item->nama_jenis_kapal );
      $sheet->setCellValue('I'.$cell_number,  $item->alat_tangkap);
      $sheet->setCellValue('J'.$cell_number,  $item->merek_mesin);
      $sheet->setCellValue('K'.$cell_number,  $item->daya_kapal);
      $sheet->setCellValue('L'.$cell_number,  $item->no_mesin);
      $sheet->setCellValue('M'.$cell_number,  $item->jumlah_palka);
      $sheet->setCellValue('N'.$cell_number,  $item->kapasitas_palka);
      $sheet->setCellValue('O'.$cell_number,  $tempat_pendaftaran);
      $sheet->setCellValue('P'.$cell_number,  $gross_akte);
      $sheet->setCellValue('Q'.$cell_number,  $item->panjang_kapal);
      $sheet->setCellValue('R'.$cell_number,  $item->lebar_kapal);
      $sheet->setCellValue('S'.$cell_number,  $item->dalam_kapal);
      $sheet->setCellValue('T'.$cell_number,  $item->gt_kapal);
      $sheet->setCellValue('U'.$cell_number,  $item->nt_kapal);
      $sheet->setCellValue('V'.$cell_number,  $item->nama_perusahaan);
      $sheet->setCellValue('W'.$cell_number,  $item->alamat_perusahaan);
      $sheet->setCellValue('X'.$cell_number,  $item->keterangan_pendok);
      $sheet->setCellValue('Y'.$cell_number, $item->tanggal_surat_permohonan);
      $sheet->setCellValue('Z'.$cell_number, $item->tgl_ttd_direktur);
      $sheet->setCellValue('AA'.$cell_number, $item->nama_pengguna);
      $sheet->setCellValue('AB'.$cell_number, $item->nama_pengguna);
      $sheet->setCellValue('AC'.$cell_number, $item->nama_pengguna);
      $sheet->setCellValue('AD'.$cell_number, $item->nama_pengguna);
      $cell_number++;
      $number++;           
    }          

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 5,
                            'B' => 20,
                            'C' => 40,
                            'D' => 40,
                            'E' => 40,
                            'F' => 40,
                            'G' => 25,
                            'H' => 25,
                            'I' => 50,
                            'J' => 40,
                            'K' => 20,
                            'L' => 20,
                            'M' => 20,
                            'N' => 20,
                            'O' => 20,
                            'P' => 50,
                            'Q' => 20,
                            'R' => 20,
                            'S' => 20,
                            'T' => 20,
                            'U' => 20,
                            'V' => 50,
                            'W' => 100,
                            'X' => 40,
                            'Y' => 40,
                            'Z' =>  50,
                            'AA' => 50,
                            'AB' => 50,
                            'AC' => 50,
                            'AD' => 50
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Daftar Kapal Tercetak.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */