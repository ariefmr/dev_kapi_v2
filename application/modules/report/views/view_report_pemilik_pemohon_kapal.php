<?php
  //OLAH DATA TAMPIL
  $template = array( "table_open" => "<table id='table_daftar_pemilik_pemohon_kapal' class='table table-hover table-bordered'>");
  $this->table->set_template($template);
  $this->table->set_heading('No. ',
                            'Nama Pemilik',
                            'Nama Penanggung Jawab',
                            'Alamat Pemilik',
                            'No. Telp Pemilik',
                            'Nama Pemohon',
                            'No. Telp Pemohon'
                          );

  if($list_cmf)
    {
      $number = 1;
      foreach ($list_cmf as $key => $item) {
       
        $this->table->add_row( 
                              $number,
                              $item->nama_perusahaan,
                              $item->nama_penanggung_jawab,
                              $item->alamat_perusahaan,
                              $item->no_telp_perusahaan,
                              $item->nama_pemohon,
                              $item->no_telp_pemohon
                              );
        $number++;
      }
    }

  $table_report_kapal = $this->table->generate();

    $this->table->clear();
  $link_entry = '<a href="xls" class="btn btn-primary">Export to XLS</a>';
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<p></p>

<!-- TAMPIL DATA -->
	<div id="loading_info" class="panel panel-default">
		<div class="panel-body">
		   <p class="text-center">
		   		Memuat Data Kapal. Harap Tunggu...
		   </p>
		</div>
	</div>

	<div class="row">
	  <div class="col-lg-12" style="overflow-x: auto;">
	    <?php echo $table_report_kapal; ?>    
	  </div>
	</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>
  
<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		// $("#table_container").hide();

		$('#table_daftar_pemilik_pemohon_kapal').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
		    }
		} );
	} );
</script>