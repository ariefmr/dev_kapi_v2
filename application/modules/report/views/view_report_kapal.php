<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading('No. ',
                              'No Register',
                              'Tipe Permohonan',
                              'Tanggal Pendaftaran',
                              'Tanda Pengenal',
                              'Nama Kapal',
                              'Nama Kapal Sebelumnya',
                              'Tempat Pembangunan',
                              'Bahan Utama Kapal',
                              'Fungsi Kapal',
                              'Jenis Alat Tangkap',
                              'Merek Mesin Utama',
                              'Daya Mesin Utama',
                              'No Mesin Utama',
                              'Jumlah Palka',
                              'Kapasitas Palka',
                              'Tempat Pendaftaran',
                              'Tempat Gross Akte',
                              'No Gross Akte',
                              'Tanggal Gross Akte',
                              'Panjang',
                              'Lebar',
                              'Dalam',
                              'LOA',
                              'GT',
                              'NT',
                              'Nama Pemilik',
                              'Alamat Pemilik',
                              'Keterangan',
                              'Tanggal Terima Dokumen',
                              'Tanggal Tanda Tangan Direktur'
                               );

	if($list_cmf)
    {
      $number = 1;
      foreach ($list_cmf as $key => $item) {
        /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
                                           
        $tempat_pembangunan = $item->tempat_pembangunan;
        switch ($item->kategori_pendaftaran) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $item->propinsi_pendaftaran;
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $item->kabkota_pendaftaran;
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }
        $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));
        $this->table->add_row( 
                              $number ,
                              noreg($item->no_register) ,
                              $item->tipe_permohonan ,
                              fmt_tgl($item->tanggal_buat) ,
                              $item->no_tanda_pengenal ,
                              $item->nama_kapal,
                              $item->nama_kapal_sblm ,
                              $tempat_pembangunan ,
                              $item->nama_bahan_kapal,
                              $item->nama_jenis_kapal ,
                              $item->alat_tangkap ,
                              $item->merek_mesin ,
                              $item->daya_kapal ,
                              $item->no_mesin,
                              $item->jumlah_palka,
                              $item->kapasitas_palka,
                              $tempat_pendaftaran,
                              $item->tempat_grosse_akte ,
                              $item->no_grosse_akte ,
                              fmt_tgl($item->tanggal_grosse_akte) ,
                              $item->panjang_kapal ,
                              $item->lebar_kapal ,
                              $item->dalam_kapal ,
                              $item->panjang_loa_kapal ,
                              $item->gt_kapal ,
                              $item->nt_kapal ,
                              kos($item->nama_perusahaan,'-' ),
                              kos($item->alamat_perusahaan,'-' ),
                              kos($item->keterangan_pendok,'-' ),
                              fmt_tgl($item->tanggal_surat_permohonan),
                              fmt_tgl($item->tgl_ttd_direktur)
                              );
        $number++;
      }
    }

	$table_report_kapal = $this->table->generate();

    $this->table->clear();
	$link_entry = '<a href="xls" class="btn btn-primary">Export to XLS</a>';
?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<p></p>

<!-- TAMPIL DATA -->
	<div id="loading_info" class="panel panel-default">
		<div class="panel-body">
		   <p class="text-center">
		   		Memuat Data Kapal. Harap Tunggu...
		   </p>
		</div>
	</div>

	<div class="row">
	  <div class="col-lg-12" style="overflow-x: auto;">
	    <?php echo $table_report_kapal; ?>    
	  </div>
	</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
	$(document).ready( function () {
		// $("#table_container").hide();

		$('#table_daftar_pendok').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
		    }
		} );
	} );
</script>