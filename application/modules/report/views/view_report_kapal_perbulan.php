<?php
	//OLAH DATA TAMPIL
	$template = array( "table_open" => "<table id='table_daftar_pendok' class='table table-hover table-bordered'>");
	$this->table->set_template($template);
	$this->table->set_heading('No. ',
                              'No Register',
                              'Tipe Permohonan',
                              'Tanggal Pendaftaran',
                              'Tanda Pengenal',
                              'Nama Kapal',
                              'Nama Kapal Sebelumnya',
                              'Tempat Pembangunan',
                              'Bahan Utama Kapal',
                              'Fungsi Kapal',
                              'Jenis Alat Tangkap',
                              'Merek Mesin Utama',
                              'Daya Mesin Utama',
                              'No Mesin Utama',
                              'Jumlah Palka',
                              'Kapasitas Palka',
                              'Tempat Pendaftaran',
                              'Tempat Gross Akte',
                              'No Gross Akte',
                              'Tanggal Gross Akte',
                              'Panjang',
                              'Lebar',
                              'Dalam',
                              'LOA',
                              'GT',
                              'NT',
                              'Nama Pemilik',
                              'Alamat Pemilik',
                              'Keterangan',
                              'Tanggal Terima Dokumen',
                              'Tanggal Tanda Tangan Direktur'
                               );

	if($list_cmf)
    {
      $number = 1;
      foreach ($list_cmf as $key => $item) {
        /*if($item->tempat_pembangunan === 'Indonesia' ){
                                                $tempat_pembangunan = $item->tempat_pembangunan;
                                           }else{
                                           }*/
                                           
        $tempat_pembangunan = $item->tempat_pembangunan;
        switch ($item->kategori_pendaftaran) {
                                            case 'PUSAT':
                                                $tempat_pendaftaran = "KKP";
                                                break;

                                            case 'PROPINSI':
                                                $tempat_pendaftaran = $item->propinsi_pendaftaran;
                                                break;
                                            
                                            case 'KAB/KOTA':
                                                $tempat_pendaftaran = $item->kabkota_pendaftaran;
                                                break;

                                            default:
                                                $tempat_pendaftaran = "KKP";
                                                break;
                                        }
        $gross_akte = $item->tempat_grosse_akte.", ".$item->no_grosse_akte.", ".fmt_tgl(kos($item->tanggal_grosse_akte));
        $this->table->add_row( 
                              $number ,
                              noreg($item->no_register) ,
                              $item->tipe_permohonan ,
                              fmt_tgl($item->tanggal_buat) ,
                              $item->no_tanda_pengenal ,
                              $item->nama_kapal,
                              $item->nama_kapal_sblm ,
                              $tempat_pembangunan ,
                              $item->nama_bahan_kapal,
                              $item->nama_jenis_kapal ,
                              $item->alat_tangkap ,
                              $item->merek_mesin ,
                              $item->daya_kapal ,
                              $item->no_mesin,
                              $item->jumlah_palka,
                              $item->kapasitas_palka,
                              $tempat_pendaftaran,
                              $item->tempat_grosse_akte ,
                              $item->no_grosse_akte ,
                              fmt_tgl($item->tanggal_grosse_akte) ,
                              $item->panjang_kapal ,
                              $item->lebar_kapal ,
                              $item->dalam_kapal ,
                              $item->panjang_loa_kapal ,
                              $item->gt_kapal ,
                              $item->nt_kapal ,
                              kos($item->nama_perusahaan,'-' ),
                              kos($item->alamat_perusahaan,'-' ),
                              kos($item->keterangan_pendok,'-' ),
                              fmt_tgl($item->tanggal_surat_permohonan),
                              fmt_tgl($item->tgl_ttd_direktur)
                              );
        $number++;
      }
    }

	$table_report_kapal = $this->table->generate();

    $this->table->clear();
	$link_entry = '<a href="" class="btn btn-primary" id="link_export">Export to XLS</a>';

  $mons = array("01"=> "Januari","02"=> "Februari","03"=> "Maret","04"=> "April","05"=> "Mei", 
        "06" => "Juni","07"=> "Juli","08"=> "Agustus","09"=> "September", 
        "10" => "Oktober", "11" => "November", "12" => "Desember");

    $date = getdate();
    $bulan_x = explode('-',$tmp_tgl_catat);
    //$month = $date['mon'];
    $month = $bulan_x[1];
    $month_name = $mons[$month];
    // echo $month;
    // echo $month_name;

?>

<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-10">
                <h3 class="panel-title">Laporan Bulan : <?php $attr_datepick_bulan_tahun = array ( 'button_id' => 'ganti_tanggal',
                                                 'datepick_id' => 'datepicker_jurnal',
                                                 'default_text' => fmt_bulan_tahun($tmp_tgl_catat),
                                                 'input_name' => 'tgl_catat',
                                                 'input_value' => $tmp_tgl_catat.'-01'
                                                );
                echo $this->mkform->datepick_bulan_tahun($attr_datepick_bulan_tahun);
                ?>
                <button type="button" id="start_filter" class="btn btn-success">Filter</button> </h3>
            </div>
            <div class="col-sm-2">
            	<?php echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<p></p>

<!-- TAMPIL DATA -->
	<div id="loading_info" class="panel panel-default">
		<div class="panel-body">
		   <p class="text-center">
		   		Memuat Data Kapal. Harap Tunggu...
		   </p>
		</div>
	</div>

	<div class="row">
	  <div class="col-lg-12" style="overflow-x: auto;">
	    <?php echo $table_report_kapal; ?>    
	  </div>
	</div>
		
<div class="row">
  <div class="col-lg-12"> 
          <div class="form-group">
            <div class="col-sm-3">
            	<?php //echo $link_entry; ?>
            </div>
          </div>
  </div>
</div>

<!-- ADDITIONAL JAVASCRIPT -->
<script>
  var array_uri = ['report','report_kapal','xls_perbulan'];
  var array_uri_xls = ['report','report_kapal','xls_perbulan'];

  function start_filter()
  {
    var new_segments = array_uri.join("/"),
        new_url = site_url+new_segments+"/",
        get_tanggal = $('#datepicker_jurnal').val(),
        arr_tanggal = get_tanggal.split('-'),
        new_tanggal = arr_tanggal[0]+"/"+arr_tanggal[1],
        url_redirect = new_url+new_tanggal; 
  
    var new_segments_xls = array_uri_xls.join("/"),
        new_url_xls = site_url+new_segments_xls+"/",
        get_tanggal_xls = $('#datepicker_jurnal').val(),
        arr_tanggal_xls = get_tanggal_xls.split('-'),
        new_tanggal_xls = arr_tanggal_xls[0]+"/"+arr_tanggal_xls[1],
        url_redirect_xls = new_url_xls+new_tanggal_xls; 

    var a = document.getElementById('link_export');
      a.setAttribute("href", "url_redirect_xls");

    // console.log(url_redirect);
    window.open(url_redirect,'_self');
  }

  function get_link_export()
  {
    var new_segments_xls = array_uri_xls.join("/"),
        new_url_xls = site_url+new_segments_xls+"/",
        get_tanggal_xls = $('#datepicker_jurnal').val(),
        arr_tanggal_xls = get_tanggal_xls.split('-'),
        new_tanggal_xls = arr_tanggal_xls[0]+"/"+arr_tanggal_xls[1],
        url_redirect_xls = new_url_xls+new_tanggal_xls; 

    var a = document.getElementById('link_export');
      a.setAttribute("href", "url_redirect_xls");
  
    // console.log(url_redirect);
    window.open(url_redirect,'_self');
  }

	$(document).ready( function () {
		// $("#table_container").hide();

    $("#start_filter").click(function(){
                    start_filter();
                });

		$('#table_daftar_pendok').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": false,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true,
	        "fnInitComplete": function(oSettings, json) {
		      	$("#loading_info").hide();
				$("#table_container").removeClass('hidden');
		    }
		} );
	} );
</script>