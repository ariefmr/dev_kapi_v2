<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_report_pemilik_kapal extends CI_Model
{
  
    function __construct()
    {

    }

    public function list_pemilik_kapal($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $query =   "SELECT 
                        mp.id_perusahaan,
                        mp.no_siup,
                        mp.nama_perusahaan as nama_pemilik,
                        mjp.nama_jenis_perusahaan,
                        count(mk.id_kapal) as jumlah_kapal,
                        sum(tb.gt_kapal) as jumlah_gt
                    FROM
                        db_pendaftaran_kapal.mst_kapal mk
                            left join
                        db_pendaftaran_kapal.trs_bkp tb ON tb.id_kapal = mk.id_kapal
                            left join
                        db_master.mst_perusahaan mp ON mp.id_perusahaan = tb.id_perusahaan
                            left join
                        db_master.mst_jenis_perusahaan mjp ON mjp.id_jenis_perusahaan = mp.id_jenis_perusahaan
                    where
                        mp.id_jenis_perusahaan = 2
                            and mk.aktif = 'ya'
                    group by id_perusahaan
                    having sum(tb.gt_kapal) >= 300
                    UNION SELECT 
                        mp.id_perusahaan,
                        mp.no_siup,
                        mp.nama_perusahaan as nama_pemilik,
                        mjp.nama_jenis_perusahaan,
                        count(mk.id_kapal) as jumlah_kapal,
                        sum(tb.gt_kapal) as jumlah_gt
                    FROM
                        db_pendaftaran_kapal.mst_kapal mk
                            left join
                        db_pendaftaran_kapal.trs_bkp tb ON tb.id_kapal = mk.id_kapal
                            left join
                        db_master.mst_perusahaan mp ON mp.id_perusahaan = tb.id_perusahaan
                            left join
                        db_master.mst_jenis_perusahaan mjp ON mjp.id_jenis_perusahaan = mp.id_jenis_perusahaan
                    where
                        mp.id_jenis_perusahaan is null
                            and mp.nama_perusahaan like CONCAT('%', mp.nama_penanggung_jawab, '%')
                            and mk.aktif = 'ya'
                    
                     ";

                    switch ($kategori_pengguna) {
                            case '1':
                                $query .= "";
                                break;
                            
                            case '2':
                                $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                                break;

                            case '3':
                                $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                                break;

                            default:
                                $query .= "";
                                break;
                        } 
                    $query .=  "GROUP BY tb.id_perusahaan
                                HAVING SUM(tb.gt_kapal) >= 300
                                ORDER BY id_perusahaan ASC
                                LIMIT 0, 1000
                                ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}