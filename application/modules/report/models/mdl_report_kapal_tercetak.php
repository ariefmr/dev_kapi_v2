<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_report_kapal_tercetak extends CI_Model
{
  
    function __construct()
    {

    }

  public function list_kapal_kapi($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $query =   "SELECT  kapal.no_register,
                            jenis_kapal.nama_jenis_kapal,
                            bahan_kapal.nama_bahan_kapal,
                            pendok.tipe_permohonan,
                            pendok.nama_perusahaan,
                            pendok.alamat_perusahaan,
                            pendok.nama_penanggung_jawab,
                            pendok.no_identitas_penanggung_jawab,
                            pendok.ttl_penanggung_jawab,
                            pendok.tanggal_surat_permohonan,
                            pendok.kategori_pendaftaran,
                            pendok.keterangan_pendok,
                            buku.id_bkp,
                            buku.id_kapal,
                            buku.id_pendok,
                            buku.id_perusahaan,
                            buku.id_jenis_kapal,
                            buku.id_alat_tangkap,
                            buku.alat_tangkap,
                            buku.id_bahan_kapal,
                            buku.no_seri_bkp,
                            buku.nama_kapal,
                            buku.nama_kapal_sblm,
                            buku.no_mesin,
                            buku.merek_mesin,
                            buku.tipe_mesin,
                            buku.daya_kapal,
                            buku.satuan_mesin_utama_kapal,
                            buku.jumlah_palka,
                            buku.kapasitas_palka,
                            buku.tempat_pendaftaran_id,
                            buku.tempat_grosse_akte,
                            buku.no_grosse_akte,
                            buku.tanggal_grosse_akte,
                            buku.panjang_kapal,
                            buku.lebar_kapal,
                            buku.dalam_kapal,
                            buku.panjang_loa_kapal,
                            buku.gt_kapal,
                            buku.nt_kapal,
                            buku.no_tanda_pengenal,
                            buku.keterangan_bkp,
                            buku.nama_kasie,
                            buku.nama_kasubdit,
                            buku.nama_direktur,
                            buku.nip_direktur,
                            buku.tgl_ttd_direktur,
                            buku.tahun_pembangunan,
                            buku.tempat_pembangunan,
                            buku.no_telp_perusahaan,
                            buku.perusahaan_sblm,
                            buku.propinsi_pendaftaran,
                            buku.kabkota_pendaftaran,
                            pengguna.nama_pengguna

                            -- alat_tangkap.kode_penandaan,
                            -- kabkota.nama_kabupaten_kota as tempat_pembangunan,
                    FROM    db_pendaftaran_kapal.trs_bkp as buku
                            LEFT JOIN (
                                        db_pendaftaran_kapal.mst_kapal as kapal,
                                        db_pendaftaran_kapal.trs_pendok as pendok,
                                        db_master.mst_jenis_kapal as jenis_kapal,
                                        -- db_master.mst_alat_tangkap as alat_tangkap,
                                        db_master.mst_bahan_kapal as bahan_kapal
                                       )
                            ON          (
                                        buku.id_bkp = kapal.id_bkp_terakhir
                                        and buku.id_pendok = pendok.id_pendok
                                        and buku.id_jenis_kapal = jenis_kapal.id_jenis_kapal
                                        -- and buku.id_alat_tangkap = alat_tangkap.id_alat_tangkap
                                        and buku.id_bahan_kapal = bahan_kapal.id_bahan_kapal
                                        )
                            LEFT JOIN db_master.mst_pengguna pengguna ON pengguna.id_pengguna = buku.id_pengguna_buat
                    WHERE   pendok.status_cetak_bkp = 'YA' ";

                    switch ($kategori_pengguna) {
                            case '1':
                                $query .= "";
                                break;
                            
                            case '2':
                                $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                                break;

                            case '3':
                                $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                                break;

                            default:
                                $query .= "";
                                break;
                        }
                    $query .= " LIMIT 0, 9999 ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
      $query = "";

      $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}