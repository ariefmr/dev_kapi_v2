<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_report_pemilik_pemohon_kapal extends CI_Model
{
  
    function __construct()
    {

    }

    public function list_pemilik_pemohon_kapal($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna)
    {
        $query =   "SELECT 
                        tp.nama_perusahaan,
                        tp.nama_penanggung_jawab,
                        tp.alamat_perusahaan,
                        tb.no_telp_perusahaan,
                        mp.nama_pemohon,
                        mp.no_telp_pemohon
                    FROM trs_pendok tp
                        left join mst_pemohon mp on mp.id_pemohon = tp.id_pemohon
                        left join trs_bkp tb on tb.id_pendok = tp.id_pendok
                    WHERE
                        tp.id_pemohon is not null 
                        and tp.id_pemohon not in (0)
                        and tp.kategori_pendaftaran = 'PUSAT'
                        and tp.tanggal_buat is not null
                        and YEAR(tp.tanggal_buat) in (2014)
                        and tp.nama_perusahaan is not null
                        and tp.nama_perusahaan <> ''
                        
                    ";

                    switch ($kategori_pengguna) {
                            case '1':
                                $query .= "";
                                break;
                            
                            case '2':
                                $query .= "and pendok.id_propinsi_pendaftaran = $id_propinsi_pengguna ";
                                break;

                            case '3':
                                $query .= "and pendok.id_kabkota_pendaftaran = $id_kabkota_pengguna ";
                                break;

                            default:
                                $query .= "";
                                break;
                        } 
                    $query .=  "GROUP BY tp.nama_perusahaan, mp.nama_pemohon
                                LIMIT 0, 1000
                                ";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

}