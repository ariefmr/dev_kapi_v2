<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {

	private $range_baris = 100;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_mapping');
	}

	public function entry($id_pendok = '')
	{	
		if($id_pendok !== '')
		{
			$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
			$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];

		}

		$data['submit_form'] = 'buku_kapal/functions/update';

		$data['aksi'] = 'entry';
		$file_view = 'form_buku_kapal';

		$add_js = array('select2.min.js', 'jquery.validationEngine.js','jquery.validationEngine-en.js');
		$add_css = array('select2.css', 'validationEngine.jquery.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							$file_view, //nama file view
							'label_form_buku_kapal', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function edit($id_pendok = '')
	{	
		if($id_pendok !== '')
		{
			$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
			$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];
		}

		// $data['submit_form'] = 'pendok/edit/update';
		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		
		echo Modules::run('templates/page/v_form', //tipe template
							'pendok', //nama module
							$file_view, //nama file view
							'form_pendok', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view

	}

	public function views($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_progress_mapping';
		
		// $data['list_compare'] 		= $this->mdl_mapping->list_compare();
		$data['list_compare_done'] 	= $this->mdl_mapping->list_compare_done($up_to, $down_to);
		$data['list_belum_mapping'] = $this->mdl_mapping->list_belum_mapping();
		
		$data['list_tperusahaan'] 	= $this->mdl_mapping->list_tperusahaan();
		$data['list_talattangkap'] 	= $this->mdl_mapping->list_talattangkap();
		$data['list_tgt'] 			= $this->mdl_mapping->list_tgt();
		$data['list_tgrosseakte'] 	= $this->mdl_mapping->list_tgrosseakte();

		//vdump($data['list_compare'],false);
		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_compare($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->list_compare($up_to, $down_to);
		$data['page'] = $page;
		//vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_compare_noreg($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->list_compare_noreg($up_to, $down_to);
		$data['page'] = $page;
		//vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_test($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_test();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_inka_mina($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_inka_mina();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_tidak_aktif($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_tidak_aktif();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_aktif($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_aktif();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_aktif_noreg($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_aktif_noreg();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function views_tidak_aktif_noreg($page = 1)
	{
		if($page > 0)
		{
			$up_to = $page * $this->range_baris;
			if($page === 1)
			{
				$down_to = 0;
			}else{
				$down_to = ($page - 1)*$this->range_baris;				
			}
		}

		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_mapping';

		$data['list_compare'] = $this->mdl_mapping->get_kapal_tidak_aktif_noreg();
		$data['page'] = $page;
		// vdump($data['list_compare'],false);

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function compare_done()
	{
		$data['url_kapi'] = $this->input->get('k');		//mengambil variable url_kapi
		$data['url_dss'] = $this->input->get('d');		//mengambil variable url_dss
		$data['url_mapping'] = $this->input->get('m');	//mengambil variable url-mapping
		$data['id_kapal'] = $this->input->get('id');

		$data['is_skipped'] = $this->mdl_mapping->is_skipped($data['id_kapal']);

		$data['url_kapi'] = base64_decode($data['url_kapi'])."#div_path_foto";
		$url_edit = explode('view', $data['url_kapi']);
		$data['url_edit'] = $url_edit[0].'entry'.$url_edit[1];
		$data['url_dss'] = base64_decode($data['url_dss'])."#ui-tabs-1";
		$data['url_mapping'] = base64_decode($data['url_mapping']);

		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'tabel_compare';

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);
	}

	public function report_mapping()
	{
		
		$data = null;
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');
		$template = 'templates/page/v_form';
		$modules = 'mapping';
		$views = 'report_mapping';
		$data['report'] = $this->mdl_mapping->report_mapping();

		echo Modules::run($template, $modules, $views, '', $add_js, $add_css, $data);

	}

	public function xls()
  {
    $list_cmf = $this->mdl_mapping->report_mapping();

    $this->load->library('excel');

    $filename = "Report Mapping.xlsx";


    // JUDUL
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);
    $this->excel->getActiveSheet()->setTitle('Report Mapping');

    $sheet = $this->excel->getActiveSheet();
    $sheet      -> setCellValue('A1', 'Report Mapping');
                      $styleArray = array('font' => array('bold' => true,'name' => 'Trebuchet MS','size' => 16), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
                      $sheet->getStyle('A1')->applyFromArray($styleArray);
                      $sheet->mergeCells('A1:G1');
    /*$array_thead = array('Document Numbers',
                          'Catch Tagging Form Document Numbers',
                          'Wild or Farmed (W or F)',
                          'Name of Catching Vessel',
                          'Registration Number',
                          'Flag State/ Fishing Entity',
                          'CCSBT Farm Serial Number',
                          'Name of Farm',
                          'Document Number(s) of associated Farm Stocking(FS) Format(s)',
                          'F (Fresh) / FR (Frozen)',
                          'Type (RD/GGO/GGT/DRO/DRT/FL/OT*)',
                          'Month of Catch/ Harvest',
                          'Gear Code',
                          'CCSBT Statistical Area',
                          'Net Weight (Kg)',
                          'Total Number of Whole Fish (Incld RD/GGO/GGT/DRO/DRT)',
                          'Type of Product for Other (OT)',
                          'Conversion Factor for  Other (OT)',
                          'Name of Processing Establishment',
                          'Address of Processing Establishment',
                          'Name of Validator',
                          'Title of Validator',
                          'Signature (Y/N)',
                          'Date of validation',
                          'Official Stamp Seal',
                          'Name of Fishing Master',
                          );*/

    $array_thead = array( 'No. ',
                          'Nama Kapal',
                          'Sudah Mapping',
                          'Belum Mapping',
                          'Keterangan'
                        );

    $column_letter = 'A';

    foreach ($array_thead as $key => $value) {
       $sheet      -> setCellValue($column_letter.'3', $value);
       $column_letter++;
    }                  
    $styleArray = array('font' => array('bold' => true), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A3:BG3')->applyFromArray($styleArray);                          
    // MASUKKAN DATA
    $cell_number = 4;

    $number = 1;
    $sudah = 0;
    $belum = 0;
    foreach ($list_cmf as $item) {

      $sheet->setCellValue('A'.$cell_number,  $number);
      $sheet->setCellValue('B'.$cell_number,  $item->nama_kapal_terbaru);
      $sheet->setCellValue('C'.$cell_number,  ($item->id_kapal_dss != null)? "@" : " " );
      $sheet->setCellValue('D'.$cell_number,  ($item->id_kapal_dss == null)? "@" : " " );
      $sheet->setCellValue('E'.$cell_number,  "-");
      $cell_number++;           
      $number++;           
      if($item->id_kapal_dss != null){
			$sudah++;
		}else{
			$belum++;
		}
    }

    $sheet->setCellValue('C'.$cell_number,  $sudah);
    $sheet->setCellValue('D'.$cell_number,  $belum);



                            

    //MENGATUR UKURAN KOLOM
    $array_thwidth = array( 'A' => 5,
                            'B' => 40,
                            'C' => 20,
                            'D' => 20,
                            'E' => 20
                          );
    foreach ($array_thwidth as $column => $width) {
        $sheet->getColumnDimension($column)->setWidth($width);
    }

    $styleArray = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $sheet->getStyle('A4:BG'.$cell_number)->applyFromArray($styleArray);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Daftar Kapal Terdaftar.xlsx"');
    header('Cache-Control: max-age=0');



    //MENGATUR BORDER Table 1

    // $styleArray = array(
    //   'borders' => array(
    //     'allborders' => array(
    //       'style' => PHPExcel_Style_Border::BORDER_THIN
    //     )
    //   )
    // );

    // $this->excel->getActiveSheet('A5:C'.$cell_number)->getStyle('A5:C'.$cell_number)->applyFromArray($styleArray);
    // unset($styleArray);



    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $objWriter->save('php://output');
  }

	public function stat()
	{
		$data['belum_mapping'] = $this->mdl_mapping->belum_mapping();
		$data['sudah_mapping'] = $this->mdl_mapping->sudah_mapping()->banyak;

		echo json_encode($data);
	}

	public function stat_noreg()
	{
		$data['belum_mapping'] = $this->mdl_mapping->belum_mapping_noreg();
		$data['sudah_mapping'] = $this->mdl_mapping->sudah_mapping()->banyak;

		echo json_encode($data);
	}

	public function views_cetak()
	{
		//info session userdata
		$kategori_pengguna = $this->mksess->kategori_pengguna();
		$id_propinsi_pengguna = $this->mksess->id_propinsi();
		$id_kabkota_pengguna = $this->mksess->id_kabupaten_kota();
		
		$add_js = array('select2.min.js', 'jquery.dataTables.min.js');
		$add_css = array('select2.css', 'jquery.dataTables.css');

		$template = 'templates/page/v_form';
		$modules = 'buku_kapal';
		$views = 'tabel_buku_kapal_cetak';
		$labels = 'label_view_buku_kapal_cetak';

		$data['list_bkp_pendok'] = $this->mdl_buku_kapal->list_buku_kapal_cetak($kategori_pengguna, $id_propinsi_pengguna, $id_kabkota_pengguna);

		echo Modules::run($template, $modules, $views, $labels, $add_js, $add_css, $data);
	}



	public function view($id_pendok)
	{	
		$get_detail_buku_kapal = $this->mdl_buku_kapal->detail_buku_kapal($id_pendok);
		$data['detail_buku_kapal'] = (array) $get_detail_buku_kapal[0];

		$data['aksi'] = 'view';
		if($data['detail_buku_kapal']['no_register']==NULL){
			$data['button'] = 'entry';
		}else{
			$data['button'] = 'edit';
			$data['cetak'] = TRUE;
		}

		$add_js = array('select2.min.js');
		$add_css = array('select2.css');
		echo Modules::run('templates/page/v_form', //tipe template
							'buku_kapal', //nama module
							'form_buku_kapal', //nama file view
							'label_form_buku_kapal', //dari labels.php
							$add_js, //plugin javascript khusus untuk page yang akan di load
							$add_css, //file css khusus untuk page yang akan di load
							$data); //array data yang akan digunakan di file view
	}

	public function create_table()
	{	
		$get_detail_buku_kapal = $this->mdl_mapping->create_table_skip_mapping();

		echo "done";
	}

	public function skip($id_kapal)
	{	
		$get_detail_buku_kapal = $this->mdl_mapping->insert_id_kapal_skip($id_kapal);
		// echo "id kapal skipped";
	}

}
