
<table id="report_mapping" >
	<thead>
		<tr>
			<th rowspan="2" width="10px" >No.</th>
			<th rowspan="2" >Nama Kapal</th>
			<th colspan="2" >Mapping</th>
			<tr rowspan="2" >
				<th>Sudah</th>
				<th>Belum</th>
				<th>Keterangan</th>
			</tr>
		</tr>
	</thead>
	<tbody>
	<?php
		$i = 1;
		$sudah = 0;
		$belum = 0;
		foreach ($report as $item) {
			# code...
			$tombol = "<span class='glyphicon glyphicon-ok'></span>";
			?>
			<tr>
				<td><?=$i?></td>
				<td><?=$item->nama_kapal_terbaru?></td>
				<td><?=($item->id_kapal_dss != null)?$tombol:" "?></td>
				<td><?=($item->id_kapal_dss == null)?$tombol:" "?></td>
				<td> - </td>
			</tr>
			<?php
			$i++;
			if($item->id_kapal_dss != null){
				$sudah++;
			}else{
				$belum++;
			}
		}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" class="text-center" >Sudah dimapping : <?=$sudah?></td>
			<td colspan="3" class="text-center" >Belum dimapping : <?=$belum?></td>
		</tr>
	</tfoot>
</table>

<script>
		
	var stat_url = "<?php echo site_url('mapping/main/stat'); ?>";

	  $(function () {
	    $('#myTab a:last').tab('show')
	  })

	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {

		$("#updateStat").click(function(){
			$("#updateStat").text("Harap tunggu..");
			var jqxhr = $.ajax({
		          async: false,
		          global: false,
		          dataType: "json",
		          url: stat_url,
		          success: function(data) {
		            $("#updateStat").text( "Belum mapping: "+data.belum_mapping+". Sudah mapping: "+data.sudah_mapping );
		          }
		        });

		    jqxhr.always(function(data) {
		          console.log(data);
		        });   
		});
		

		$('#report_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": false,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
		
	} );
</script>