<?php 
	$is_prev_disabled = $page === 1 ? "disabled" : ""; 
	$prev = $page > 0 ? $page - 1 : 0;  
	$next = $page + 1; 

	$prev_url = site_url("mapping/main/views_compare/".$prev);
	$next_url = site_url("mapping/main/views_compare/".$next); 

 ?>
 <div class="row">
	<div class="col-lg-3 col-lg-offset-8">
		<button id="updateStat" class="btn btn-large btn-info">Klik untuk melihat progress mapping.</button>
	</div>
</div>
<p></p>
<div class="row">
	<div class="col-lg-6">
		<a href="<?php echo $prev_url ?>" class="btn btn-large btn-block btn-primary" <?php echo $is_prev_disabled; ?>>Previous</a>
	</div>
	<div class="col-lg-6">
		<a href="<?php echo $next_url ?>" class="btn btn-large btn-block btn-primary">Next</a>
		
	</div>
</div>
<p></p>
<div class="tab-pane active" id="compare">
  	<table id="table_mapping" class="table table-hover">
	<thead>
		<tr>
			<th>Nama Kapal DSS</th>
			<th>Nama Kapal Terbaru</th>
			<th>Tahun Bangun DSS</th>
			<th>Tahun Bangun KAPI</th>
			<th>No Register KAPI</th>
			<th>Gross Akte DSS</th>
			<th>Gross Akte KAPI</th>
			<th>Skip</th>
			<th>compare</th>
			<th>GT Kapal DSS</th>
			<th>GT Kapal KAPI</th>
			<th>Panjang Kapal DSS</th>
			<th>Panjang Kapal KAPI</th>
			<th>Lebar Kapal DSS</th>
			<th>Lebar Kapal KAPI</th>
			<th>Dalam Kapal DSS</th>
			<th>Dalam Kapal KAPI</th>
			<th>View</th>
			<th colspan="1">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($list_compare){

				foreach ($list_compare as $key => $value) {

					$skipurl = 'mapping/main/skip/'.$value->id_kapal;

					$link_compare = base_url('mapping/main/compare_done?');
					$k = base_url('kapal/main/view_for_compare/'.$value->id_pendok);
					$d = "http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=".$value->id_kapal_dss;
					$m = "http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=".$value->id_kapal_dss."#ui-tabs-1";
					$k = base64_encode($k);
					$d = base64_encode($d);
					$m = base64_encode($m);
					$color = '';
					$bgcolor = '';
					$color_register = '';
					$bgcolor_reg = '';
					if(($value->no_grosse_akte !== '')&&($value->gross_akte_dss !== '')){
						if($value->no_grosse_akte === $value->gross_akte_dss){
							$color = 'red';
							$bgcolor = '#FFFF99';
						}
					}
					if(($value->no_register !== '')&&($value->gross_akte_dss !== '')){
						if(noreg($value->no_register) === $value->gross_akte_dss){
							$color_register = 'blue';
							$bgcolor_reg = '#FFFF99';
							$bgcolor = '#FFFF99';
						}
					}
					?>
					<tr>
						<td><?=$value->nama_kapal_dss?></td>
						<td><?=$value->nama_kapal_terbaru?></td>
						<td><?=$value->tahun_pembangunan_dss?></td>
						<td><?=$value->tahun_pembangunan?></td>
						<td bgcolor="<?=$bgcolor_reg?>"><font color="<?=$color_register?>"><?=noreg($value->no_register)?></font></td>
						<td bgcolor="<?=$bgcolor?>"><font color="<?=$color?>"><?=$value->gross_akte_dss?></font></td>
						<td bgcolor="<?=$bgcolor?>"><font color="<?=$color?>"><?=$value->no_grosse_akte?></font></td>
						<td>
							<a target="_blank" href="<?=base_url($skipurl)?>">Skip</a>
						</td>
						<td>
							<a target="_blank" href="<?=$link_compare."&k=".$k."&d=".$d."&m=".$m."&id=".$value->id_kapal?>">Compare</a>
						</td>
						<td><?=$value->gt_kapal_dss?></td>
						<td><?=$value->gt_kapal?></td>
						<td><?=$value->panjang_kapal_dss?></td>
						<td><?=$value->panjang_kapal?></td>
						<td><?=$value->lebar_kapal_dss?></td>
						<td><?=$value->lebar_kapal?></td>
						<td><?=$value->dalam_kapal_dss?></td>
						<td><?=$value->dalam_kapal?></td>
						<td><a href="<?=base_url('kapal/main/view/'.$value->id_pendok)?>" target="_blank" >KAPI</a>&nbsp;|&nbsp;<a href="http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=<?=$value->id_kapal_dss?>" target="_blank" >DSS</a></td>
						<td>
							<a target="_blank" href="http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=<?=$value->id_kapal_dss?>#ui-tabs-1" > Mapping</a>
						</td>
						
					</tr>
					<?php
				}
			}
		?>
	</tbody>
</table>
</div>
<p></p>
<div class="row">
	<div class="col-lg-6">
		<a href="<?php echo $prev_url ?>" class="btn btn-large btn-block btn-primary" <?php echo $is_prev_disabled; ?>>Previous</a>
	</div>
	<div class="col-lg-6">
		<a href="<?php echo $next_url ?>" class="btn btn-large btn-block btn-primary">Next</a>
		
	</div>
</div>
<script>
		
	var stat_url = "<?php echo site_url('mapping/main/stat'); ?>";

	  $(function () {
	    $('#myTab a:last').tab('show')
	  })

	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {

		$("#updateStat").click(function(){
			$("#updateStat").text("Harap tunggu..");
			var jqxhr = $.ajax({
		          async: false,
		          global: false,
		          dataType: "json",
		          url: stat_url,
		          success: function(data) {
		            $("#updateStat").text( "Belum mapping: "+data.belum_mapping+". Sudah mapping: "+data.sudah_mapping );
		          }
		        });

		    jqxhr.always(function(data) {
		          console.log(data);
		        });   
		});
		

		$('#table_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
	        "bFilter": false,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
		
	} );
</script>