<p></p>
<div class="row" >
	<p class="text-center" ><strong>ID Kapal KAPI untuk dimapping : <p><h3 class="text-center" ><?=$id_kapal?></h3></strong></p>
</div>
<div class="row" >
	<div class="col-lg-6">
		<p class="text-center" ><strong>KAPI</strong></p>
		<iframe src="<?=$url_kapi?>" width="100%" height="600px" ></iframe>
	</div>
	<div class="col-lg-6">
		<p class="text-center" ><strong>DSS</strong></p>
		<iframe src="<?=$url_dss?>" width="100%" height="600px" ></iframe>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<a href="<?=$url_edit?>" class="btn btn-large btn-block btn-primary" >Edit KAPI</a>
	</div>
	<div class="col-lg-6">
		<a href="<?=$url_mapping?>" class="btn btn-large btn-block btn-primary">Mapping DSS</a>
		
	</div>
</div>
<?php $stringurl = 'mapping/main/skip/'.$id_kapal ?>
<?php 
		if($is_skipped){
			?>
			<p class="text-center" ><strong><p><h5 class="text-center" >sudah di skip</h5></strong></p>
			<?php
		}else{
			?>
			<p class="text-center" ><strong><p><h5 class="text-center" ><a href="<?php echo base_url($stringurl) ?>">SKIP</a></h5></strong></p>
			<?php
		}
	?>
<script>
		
	var stat_url = "<?php echo site_url('mapping/main/stat'); ?>";

	  $(function () {
	    $('#myTab a:last').tab('show')
	  })

	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {

		$("#updateStat").click(function(){
			$("#updateStat").text("Harap tunggu..");
			var jqxhr = $.ajax({
		          async: false,
		          global: false,
		          dataType: "json",
		          url: stat_url,
		          success: function(data) {
		            $("#updateStat").text( "Belum mapping: "+data.belum_mapping+". Sudah mapping: "+data.sudah_mapping );
		          }
		        });

		    jqxhr.always(function(data) {
		          console.log(data);
		        });   
		});
		

		$('#table_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": false,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": false,
	        "bSort": true
		} );
		
	} );
</script>