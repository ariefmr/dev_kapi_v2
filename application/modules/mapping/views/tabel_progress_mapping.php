<div class="row">
	<div class="col-lg-3 col-lg-offset-8">
		<button id="updateStat" class="btn btn-large btn-info">Klik untuk melihat progress mapping.</button>
	</div>
</div>

<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active" ><a href="#compare_done" data-toggle="tab">Sudah dimapping</a></li>
  <li><a href="#list_belum_mapping" data-toggle="tab">Belum dimapping</a></li>
  <li><a href="#perusahaan" data-toggle="tab">Tanpa Perusahaan</a></li>
  <li><a href="#alat_tangkap" data-toggle="tab">Tanpa ALat Tangkap</a></li>
  <li><a href="#gt" data-toggle="tab">Tanpa GT</a></li>
  <li><a href="#grosse_akte" data-toggle="tab">Tanpa Grosse AKte</a></li>
</ul>

<div class="tab-content">

  <div class="tab-pane active" id="compare_done">

  	<table id="table_mapping" class="table">
	<thead>
		<tr>
			<th>No.</th>
			<th>ID Kapi DSS</th>
			<th>No Rekam Pendok</th>
			<th>ID Kapal DSS</th>
			<th>ID Kapal Kapi</th>
			<th>ID BKP DSS</th>
			<th>ID BKP Kapi</th>
			<th>Nama Kapal DSS</th>
			<th>Nama Kapal kapi</th>
			<th>GT Kapal DSS</th>
			<th>GT Kapal kapi</th>
			<th>Gross Akte DSS</th>
			<th>Gross Akte kapi</th>
			<th>View</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			foreach ($list_compare_done as $key => $value) {
				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->id_kapi_dss?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->id_kapal_dss?></td>
					<td><?=$value->id_kapal_kapi?></td>
					<td><?=$value->id_bkp_dss?></td>
					<td><?=$value->id_bkp_kapi?></td>
					<td><?=$value->nama_kapal_dss?></td>
					<td><?=$value->nama_kapal_kapi?></td>
					<td><?=$value->gt_kapal_pendaftaran?></td>
					<td><?=$value->gt_kapal_kapi?></td>
					<td><?=$value->gross_akte_dss?></td>
					<td><?=$value->gross_akte_kapi?></td>
					<td><a href="<?=base_url('kapal/main/view/'.$value->id_pendok)?>" target="_blank" >KAPI</a>&nbsp;|&nbsp;<a href="http://integrasi.djpt.kkp.go.id/search/kapal/detail/?id=<?=$value->id_kapal_dss?>&id_izin=<?=$value->id_izin_dss?>" target="_blank" >DSS</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

  <div class="tab-pane" id="list_belum_mapping">
  	<table class="table" id="table_tlist_belum_mapping" >
	<thead>
		<tr>
			<th>No.</th>
			<th>Id Kapal KAPI</th>
			<th>No Register</th>
			<th>Nama Kapal</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$i = 1;
			foreach ($list_belum_mapping as $key => $value) {
				
				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->id_kapal?></td>
					<td><?=$value->no_register?></td>
					<td><?=$value->nama_kapal_terbaru?></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

  <div class="tab-pane" id="perusahaan">
  	<table class="table" id="table_tperusahaan" >
	<thead>
		<tr>
			<th>No.</th>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
			<th>Edit Perusahaan</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$i = 1;
			foreach ($list_tperusahaan as $key => $value) {
				
				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><a href="<?=base_url('kapal/main/edit_perusahaan/'.$value->id_pendok)?>" target="_blank" ><?=$value->nama_kapal_bkp?></a></td>
					<td><?=$value->gt?></td>
					<td align="center"><a href="<?=base_url('kapal/main/edit_perusahaan/'.$value->id_pendok)?>" target="_blank" >Edit</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

  <div class="tab-pane" id="alat_tangkap">
  	<table class="table" id="table_talattangkap" >
	<thead>
		<tr>
			<th>No.</th>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
			<th>Edit Alat Tangkap</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$i = 1;
			foreach ($list_talattangkap as $key => $value) {
				
				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->gt?></td>
					<td align="center"><a href="<?=base_url('kapal/main/view/'.$value->id_pendok)?>" target="_blank" >Edit</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

  <div class="tab-pane" id="gt">
  	<table class="table" id="table_tgt" >
	<thead>
		<tr>
			<th>No.</th>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>GT</th>
			<th>Edit GT</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$i = 1;
			foreach ($list_tgt as $key => $value) {

				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->gt?></td>
					<td align="center"><a href="<?=base_url('kapal/main/view/'.$value->id_pendok)?>" target="_blank" >Edit</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

  <div class="tab-pane" id="grosse_akte">
  	<table class="table" id="table_tgrosseakte" >
	<thead>
		<tr>
			<th>No.</th>
			<th>No Pendok</th>
			<th>Nama Kapal</th>
			<th>No. Grosse Akte</th>
			<th>Edit Gross Akte</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$i=1;
			foreach ($list_tgrosseakte as $key => $value) {
				
				?>
				<tr>
					<td><?=$i?></td>
					<td><?=$value->no_rekam_pendok?></td>
					<td><?=$value->nama_kapal_bkp?></td>
					<td><?=$value->no_grosse_akte?></td>
					<td align="center"><a href="<?=base_url('kapal/main/view/'.$value->id_pendok)?>" target="_blank" >Edit</a></td>
				</tr>
				<?php
				$i++;
			}
		?>
		</tbody>
	</table>
  </div>

</div>

<!-- ADTTIONAL JAVASCRIPT -->


<script>

	var stat_url = "<?php echo site_url('mapping/main/stat'); ?>";

	  $(function () {
	    $('#myTab a:last').tab('show')
	  })

	function warning_validasi(id_pendok)
	{
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?";
		  	// id_pendok = $(this).data('pendok');
		if(confirm(pesan))
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');

		}
	}

	// kenapa kalau pake ini ga bisa di pakai untuk banyak row..? nanti tanyakan pada yang master
	/*$("#btn-set-verifikasi").click(function(){
		var url_set_verifikasi = "<?php echo base_url('kapal/functions/verifikasi/') ?>",
		  	pesan = "Yakin sudah di Verifikasi?",
		  	id_pendok = $(this).data('pendok');
		if( confirm(pesan) )
		{
		  window.open("<?php echo base_url('kapal/functions/verifikasi/"+id_pendok+"') ?>", '_self');
		}
	});*/

	$(document).ready( function () {

		$("#updateStat").click(function(){
			$("#updateStat").text("Harap tunggu..");
			var jqxhr = $.ajax({
		          async: false,
		          global: false,
		          dataType: "json",
		          url: stat_url,
		          success: function(data) {
		            $("#updateStat").text( "Belum mapping: "+data.belum_mapping+". Sudah mapping: "+data.sudah_mapping );
		          }
		        });

		    jqxhr.always(function(data) {
		          console.log(data);
		        });   
		});

		$('#table_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tlist_belum_mapping').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tperusahaan').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_talattangkap').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tgt').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );

		$('#table_tgrosseakte').dataTable( {
			"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"aoColumns":  [
                        {"sClass": "text-center"},
                        {"sClass": "text-center"},
                        {"sClass": "text-center"}
                      ],
	        "bFilter": true,
	        "bAutoWidth": true,
	        "bInfo": false,
	        "bPaginate": true,
	        "bSort": true
		} );
	} );
</script>