<?php

class Mdl_mapping extends CI_Model
{
	function __construct()
    {
        $this->load->database();
    }

    public function list_compare($up_to = 100, $down_to = 0)
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.no_register,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                GROUP BY id_kapal_dss
                LIMIT $down_to , $up_to";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_compare_noreg($up_to = 100, $down_to = 0)
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.no_register,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.no_register = dss.gross_akte_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                GROUP BY id_kapal_dss
                LIMIT $down_to , $up_to";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_test()
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    kapi.no_register,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss,
                    dss.aktif
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                            mkp.aktif
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal NOT IN (SELECT
                                                    kapi.id_kapal
                                                FROM
                                                    (SELECT 
                                                            mkp.nama_kapal AS nama_kapal_dss,
                                                            mkp.id_kapal AS id_kapal_dss,
                                                            mkp.panjang_kapal AS panjang_kapal_dss,
                                                            mkp.dalam_kapal AS dalam_kapal_dss,
                                                            mkp.lebar_kapal AS lebar_kapal_dss,
                                                            mkp.gt_kapal AS gt_kapal_dss,
                                                            mkp.nomor_gross_akte AS gross_akte_dss,
                                                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                                                            mkp.aktif
                                                    FROM
                                                        db_master.mst_kapal mkp) dss
                                                        INNER JOIN
                                                    (SELECT 
                                                        kpl_kapi.id_kapal,
                                                        kpl_kapi.no_register,
                                                        trp.id_pendok,
                                                        kpl_kapi.nama_kapal_terbaru,
                                                        kbkp.gt_kapal,
                                                        kbkp.panjang_kapal,
                                                        kbkp.lebar_kapal,
                                                        kbkp.dalam_kapal,
                                                        kbkp.no_grosse_akte,
                                                        kpl_kapi.id_pendok_terakhir,
                                                        trp.no_rekam_pendok,
                                                        kbkp.tahun_pembangunan
                                                        FROM
                                                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                                                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                                                            AND kbkp.no_grosse_akte IS NOT NULL
                                                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                                                        WHERE
                                                            kpl_kapi.aktif = 'YA'
                                                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                                                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                                                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                                                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                                                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                                                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                                                    AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                                                    AND dss.aktif = 'TIDAK'
                                                GROUP BY id_kapal_dss)
                GROUP BY id_kapal_dss";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_tidak_aktif()
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    kapi.no_register,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss,
                    dss.aktif
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                            mkp.aktif
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                    AND dss.aktif = 'TIDAK'
                GROUP BY id_kapal_dss";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_aktif()
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    kapi.no_register,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss,
                    dss.aktif
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                            mkp.aktif
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                    AND dss.aktif = 'YA'
                GROUP BY id_kapal_dss";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_aktif_noreg()
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    kapi.no_register,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss,
                    dss.aktif
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                            mkp.aktif
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.no_register = dss.gross_akte_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                    AND dss.aktif = 'YA'
                GROUP BY id_kapal_dss";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_tidak_aktif_noreg()
    {
       
       $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    kapi.no_register,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss,
                    dss.aktif
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                            mkp.aktif
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                        kpl_kapi.nama_kapal_terbaru,
                        kbkp.gt_kapal,
                        kbkp.panjang_kapal,
                        kbkp.lebar_kapal,
                        kbkp.dalam_kapal,
                        kbkp.no_grosse_akte,
                        kpl_kapi.id_pendok_terakhir,
                        trp.no_rekam_pendok,
                        kbkp.tahun_pembangunan
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.no_register = dss.gross_akte_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                    AND dss.aktif = 'TIDAK'
                GROUP BY id_kapal_dss";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_inka_mina()
    {
       
       $query = "SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                            kpl_kapi.nama_kapal_terbaru,
                            kbkp.gt_kapal,
                            kbkp.panjang_kapal,
                            kbkp.lebar_kapal,
                            kbkp.dalam_kapal,
                            kbkp.no_grosse_akte,
                            kpl_kapi.id_pendok_terakhir,
                            trp.no_rekam_pendok,
                            kbkp.tahun_pembangunan
                    FROM
                        db_pendaftaran_kapal.mst_kapal kpl_kapi
                    INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                        AND kbkp.no_grosse_akte IS NOT NULL
                    INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                    WHERE
                        kpl_kapi.aktif = 'YA'
                        and kbkp.panjang_kapal = 0
                        and kbkp.lebar_kapal = 0
                        and kbkp.dalam_kapal = 0
                    GROUP BY kpl_kapi.id_kapal";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_kapal_double_id()
    {
       
       $query = "SELECT * FROM skip_mapping sm where sm.id_kapal NOT IN (SELECT 
                           kapi.id_kapal
                       FROM
                           (SELECT 
                                   mkp.nama_kapal AS nama_kapal_dss,
                                   mkp.id_kapal AS id_kapal_dss,
                                   mkp.panjang_kapal AS panjang_kapal_dss,
                                   mkp.dalam_kapal AS dalam_kapal_dss,
                                   mkp.lebar_kapal AS lebar_kapal_dss,
                                   mkp.gt_kapal AS gt_kapal_dss,
                                   mkp.nomor_gross_akte AS gross_akte_dss,
                                   mkp.tahun_pembangunan AS tahun_pembangunan_dss,
                                   mkp.aktif
                           FROM
                               db_master.mst_kapal mkp) dss
                               INNER JOIN
                           (SELECT 
                               kpl_kapi.id_kapal,
                               kpl_kapi.no_register,
                               trp.id_pendok,
                                   kpl_kapi.nama_kapal_terbaru,
                                   kbkp.gt_kapal,
                                   kbkp.panjang_kapal,
                                   kbkp.lebar_kapal,
                                   kbkp.dalam_kapal,
                                   kbkp.no_grosse_akte,
                                   kpl_kapi.id_pendok_terakhir,
                                   trp.no_rekam_pendok,
                                   kbkp.tahun_pembangunan
                           FROM
                               db_pendaftaran_kapal.mst_kapal kpl_kapi
                           INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                               AND kbkp.no_grosse_akte IS NOT NULL
                           INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                           WHERE
                               kpl_kapi.aktif = 'YA'
                           GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                           AND kapi.panjang_kapal = dss.panjang_kapal_dss
                           AND kapi.lebar_kapal = dss.lebar_kapal_dss
                           AND kapi.dalam_kapal = dss.dalam_kapal_dss
                           AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                           AND kapi.id_kapal IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                           AND dss.aktif = 'TIDAK'
                        GROUP BY id_kapal_dss)";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function report_mapping()
    {
        $query =    "SELECT
                        all_rows.id_kapal as id_kapal_kapi,
                        bkp.id_kapal as id_kapal_dss,
                        all_rows.nama_kapal_terbaru,
                        all_rows.gt_kapal,
                        all_rows.no_grosse_akte,
                        all_rows.id_pendok_terakhir,
                        all_rows.no_rekam_pendok
                    from
                        (SELECT 
                            kpl_kapi.id_kapal,
                                kpl_kapi.nama_kapal_terbaru,
                                kbkp.gt_kapal,
                                kbkp.no_grosse_akte,
                                kpl_kapi.id_pendok_terakhir,
                                trp.no_rekam_pendok
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) as all_rows
                            LEFT JOIN
                        db_master.mst_buku_kapal bkp ON all_rows.id_kapal = bkp.id_kapal_pendaftaran
                    group by all_rows.id_kapal
                    order by all_rows.id_kapal asc
                    LIMIT 0 , 9000";
        $run_query = $this->db->query($query);

        if($run_query->num_rows() > 0)
        {
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;

    }
    public function list_compare_done($up_to, $down_to)
    {
        $query =   "SELECT 
                       mbkp.id_kapal_pendaftaran AS id_kapi_dss,
                       mk.nama_kapal AS nama_kapal_dss,
                       bkp.nama_kapal AS nama_kapal_kapi,
                       mbkp.nama_kapal_pendaftaran,
                       trp.id_pendok,
                       trp.no_rekam_pendok,
                       mbkp.id_kapal AS id_kapal_dss,
                       mbkp.id_buku_kapal AS id_bkp_dss,
                       mbkp.gt_kapal_pendaftaran,
                       bkp.gt_kapal AS gt_kapal_kapi,
                       trp.id_kapal AS id_kapal_kapi,
                       bkp.id_bkp AS id_bkp_kapi,
                       bkp.no_grosse_akte AS gross_akte_kapi,
                       mk.nomor_gross_akte AS gross_akte_dss,
                       mz.id_izin AS id_izin_dss
                   FROM
                       db_pendaftaran_kapal.trs_pendok trp
                           INNER JOIN
                       db_pendaftaran_kapal.trs_bkp bkp ON bkp.id_pendok = trp.id_pendok
                           INNER JOIN
                       db_master.mst_buku_kapal mbkp ON mbkp.id_kapal_pendaftaran = trp.no_rekam_pendok
                           LEFT JOIN
                       db_master.mst_kapal mk ON mk.id_kapal = mbkp.id_kapal
                    INNER JOIN
                    db_master.mst_izin mz ON mz.id_kapal = mk.id_kapal
                    WHERE trp.aktif = 'YA'
                    GROUP BY mk.id_kapal
                    LIMIT ".$down_to.", ".$up_to."
                    ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }



    public function list_belum_mapping()
    {

        $query =   "SELECT * 
                        from (SELECT 
                            kpl_kapi.id_kapal,
                            kpl_kapi.no_register,
                                kpl_kapi.nama_kapal_terbaru,
                                kbkp.gt_kapal,
                                kbkp.no_grosse_akte,
                                kpl_kapi.id_pendok_terakhir,
                                trp.no_rekam_pendok
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) subq
                        where subq.id_kapal not in 
                        (select mbkp.id_kapal_pendaftaran from db_master.mst_buku_kapal mbkp) 
                        ORDER BY subq.no_register DESC";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function belum_mapping()
    {
        /*$query =   "SELECT count(*) as banyak
                        from (SELECT 
                            kpl_kapi.id_kapal,
                            kpl_kapi.no_register,
                                kpl_kapi.nama_kapal_terbaru,
                                kbkp.gt_kapal,
                                kbkp.no_grosse_akte,
                                kpl_kapi.id_pendok_terakhir,
                                trp.no_rekam_pendok
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) subq
                        where subq.id_kapal not in 
                        (select mbkp.id_kapal_pendaftaran from db_master.mst_buku_kapal mbkp) 
                        and subq.id_kapal not in 
                        (select id_kapal_skip from db_pendaftaran_kapal.skip_mapping) 
                        ORDER BY subq.no_register DESC";*/
        $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.no_register,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                            kpl_kapi.nama_kapal_terbaru,
                            kbkp.gt_kapal,
                            kbkp.panjang_kapal,
                            kbkp.lebar_kapal,
                            kbkp.dalam_kapal,
                            kbkp.no_grosse_akte,
                            kpl_kapi.id_pendok_terakhir,
                            trp.no_rekam_pendok,
                            kbkp.tahun_pembangunan
                    FROM
                        db_pendaftaran_kapal.mst_kapal kpl_kapi
                    INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                        AND kbkp.no_grosse_akte IS NOT NULL
                    INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                    WHERE
                        kpl_kapi.aktif = 'YA'
                    GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.no_grosse_akte = dss.gross_akte_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                GROUP BY id_kapal_dss
                ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->num_rows();
        }else{
            $result = false;
        }
        return $result;
    }

    public function belum_mapping_noreg()
    {

        $query = "SELECT 
                    dss.id_kapal_dss,
                    kapi.id_kapal,
                    kapi.no_register,
                    kapi.id_pendok,
                    dss.nama_kapal_dss,
                    kapi.nama_kapal_terbaru,
                    dss.tahun_pembangunan_dss,
                    kapi.tahun_pembangunan,
                    dss.gross_akte_dss,
                    kapi.no_grosse_akte,
                    dss.gt_kapal_dss, 
                    kapi.gt_kapal,
                    dss.panjang_kapal_dss,
                    kapi.panjang_kapal,
                    dss.lebar_kapal_dss,
                    kapi.lebar_kapal,
                    kapi.dalam_kapal,
                    dss.dalam_kapal_dss
                FROM
                    (SELECT 
                            mkp.nama_kapal AS nama_kapal_dss,
                            mkp.id_kapal AS id_kapal_dss,
                            mkp.panjang_kapal AS panjang_kapal_dss,
                            mkp.dalam_kapal AS dalam_kapal_dss,
                            mkp.lebar_kapal AS lebar_kapal_dss,
                            mkp.gt_kapal AS gt_kapal_dss,
                            mkp.nomor_gross_akte AS gross_akte_dss,
                            mkp.tahun_pembangunan AS tahun_pembangunan_dss
                    FROM
                        db_master.mst_kapal mkp) dss
                        INNER JOIN
                    (SELECT 
                        kpl_kapi.id_kapal,
                        kpl_kapi.no_register,
                        trp.id_pendok,
                            kpl_kapi.nama_kapal_terbaru,
                            kbkp.gt_kapal,
                            kbkp.panjang_kapal,
                            kbkp.lebar_kapal,
                            kbkp.dalam_kapal,
                            kbkp.no_grosse_akte,
                            kpl_kapi.id_pendok_terakhir,
                            trp.no_rekam_pendok,
                            kbkp.tahun_pembangunan
                    FROM
                        db_pendaftaran_kapal.mst_kapal kpl_kapi
                    INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                        AND kbkp.no_grosse_akte IS NOT NULL
                    INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                    WHERE
                        kpl_kapi.aktif = 'YA'
                    GROUP BY kpl_kapi.id_kapal) kapi ON kapi.gt_kapal = dss.gt_kapal_dss
                    AND kapi.no_register = dss.gross_akte_dss
                    AND kapi.panjang_kapal = dss.panjang_kapal_dss
                    AND kapi.lebar_kapal = dss.lebar_kapal_dss
                    AND kapi.dalam_kapal = dss.dalam_kapal_dss
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_pendaftaran FROM db_master.mst_buku_kapal)
                    AND kapi.id_kapal NOT IN (SELECT id_kapal_skip FROM db_pendaftaran_kapal.skip_mapping)
                GROUP BY id_kapal_dss
                ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->num_rows();
        }else{
            $result = false;
        }
        return $result;
    }

    public function sudah_mapping()
    {
        /*$query =   "SELECT count(*) as banyak
                        from (SELECT 
                            kpl_kapi.id_kapal,
                            kpl_kapi.no_register,
                                kpl_kapi.nama_kapal_terbaru,
                                kbkp.gt_kapal,
                                kbkp.no_grosse_akte,
                                kpl_kapi.id_pendok_terakhir,
                                trp.no_rekam_pendok
                        FROM
                            db_pendaftaran_kapal.mst_kapal kpl_kapi
                        INNER JOIN db_pendaftaran_kapal.trs_bkp kbkp ON kbkp.id_bkp = kpl_kapi.id_bkp_terakhir
                            AND kbkp.no_grosse_akte IS NOT NULL
                        INNER JOIN db_pendaftaran_kapal.trs_pendok trp ON trp.id_pendok = kpl_kapi.id_pendok_terakhir
                        WHERE
                            kpl_kapi.aktif = 'YA'
                        GROUP BY kpl_kapi.id_kapal) subq
                        where subq.id_kapal IN 
                        (select mbkp.id_kapal_pendaftaran from db_master.mst_buku_kapal mbkp) 
                        and subq.id_kapal not in 
                        (select id_kapal_skip from db_pendaftaran_kapal.skip_mapping) 
                        ORDER BY subq.no_register DESC";*/

        $query =   "SELECT count(*) as banyak
                        from db_master.mst_buku_kapal";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->row();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_tperusahaan()
    {    
        $query =   "SELECT 
                        pendok.id_pendok,
                        pendok.no_rekam_pendok,
                        bkp.nama_kapal as nama_kapal_bkp,
                        bkp.gt_kapal as gt,
                        bkp.id_perusahaan
                    FROM
                        db_pendaftaran_kapal.trs_pendok pendok
                            LEFT JOIN
                        db_pendaftaran_kapal.trs_bkp bkp ON bkp.id_pendok = pendok.id_pendok
                    WHERE
                        pendok.aktif = 'YA'
                            and bkp.id_perusahaan = 0 
                    order by pendok.no_rekam_pendok DESC
                    ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_talattangkap()
    {
        $query =   "SELECT 
                        bkp.id_alat_tangkap,
                        bkp.alat_tangkap,
                        pendok.id_pendok,
                        pendok.no_rekam_pendok,
                        bkp.nama_kapal as nama_kapal_bkp,
                        bkp.gt_kapal as gt
                    FROM
                        db_pendaftaran_kapal.trs_pendok pendok
                            LEFT JOIN
                        db_pendaftaran_kapal.trs_bkp bkp ON bkp.id_pendok = pendok.id_pendok
                    WHERE
                        pendok.aktif = 'YA'
                            and bkp.id_alat_tangkap is null
                    order by pendok.no_rekam_pendok DESC";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_tgt()
    {
        $query =   "SELECT 
                    pendok.id_pendok, 
                    pendok.no_rekam_pendok, 
                    bkp.nama_kapal as nama_kapal_bkp,
                    bkp.gt_kapal as gt
                    FROM db_pendaftaran_kapal.trs_pendok pendok
                    LEFT JOIN db_pendaftaran_kapal.trs_bkp bkp
                    ON bkp.id_pendok = pendok.id_pendok
                    WHERE bkp.gt_kapal = 0 OR bkp.gt_kapal IS NULL
                        and bkp.aktif = 'YA'
                            order by pendok.no_rekam_pendok
                            DESC";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function list_tgrosseakte()
    {
        $query =   "SELECT 
                    pendok.id_pendok, 
                    pendok.no_rekam_pendok, 
                    bkp.nama_kapal as nama_kapal_bkp,
                    bkp.no_grosse_akte
                    FROM db_pendaftaran_kapal.trs_pendok pendok
                    LEFT JOIN db_pendaftaran_kapal.trs_bkp bkp
                    ON bkp.id_pendok = pendok.id_pendok 
                    WHERE bkp.no_grosse_akte IS NULL
                        and bkp.aktif = 'YA'
                   ";
        
        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function create_table_skip_mapping()
    {
        $query =   "CREATE TABLE IF NOT EXISTS skip_mapping (
                      id_kapal_skip INT NOT NULL,
                      UNIQUE INDEX id_kapal_skip_UNIQUE (id_kapal_skip ASC),
                      PRIMARY KEY (id_kapal_skip));
                   ";
        
        $run_query = $this->db->query($query);                            

    }

    public function insert_id_kapal_skip($id_kapal)
    {
        $arr_data = array(
                'id_kapal_skip' => $id_kapal
            );
        
        if($this->db->insert('skip_mapping', $arr_data)){
            echo "skipped";
        }else{
            echo "udah ada di table skip";
        }

    }

    public function is_skipped($id_kapal)
    {
        $arr_data = array(
                'id_kapal_skip' => $id_kapal
            );
        
        $run_query = $this->db->get_where('skip_mapping', $arr_data);                            
        
        if($run_query->num_rows() > 0){
            $result = true;
        }else{
            $result = false;
        }
        return $result;

    }

}

?>