<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**
 * Library untuk bikin form input 
 */
 
class Mksess
{
  private $CI;
  private $id_aplikasi_kapi = "4";


  function __construct()
  {
      $this->CI =& get_instance();
  }

  function nama_pengguna()
  {
    return $this->CI->session->userdata('nama_pengguna');
  }

  function fullname()
  {
    return $this->CI->session->userdata('fullname');
  }

  function id_pengguna()
  {
    return $this->CI->session->userdata('id_pengguna');
  }

  function id_lokasi()
  {
    return $this->CI->session->userdata('id_lokasi');
  }

  function id_propinsi()
  {
    return $this->CI->session->userdata('id_propinsi');
  }

  function id_kabupaten_kota()
  {
    return $this->CI->session->userdata('id_kabupaten_kota');
  }

  function nama_propinsi()
  {
    return $this->CI->session->userdata('nama_propinsi');
  }

  function nama_kabupaten_kota()
  {
    return $this->CI->session->userdata('nama_kabupaten_kota');
  }

  function kategori_pengguna()
  {
    return $this->CI->session->userdata('kategori_pengguna');
  }

  function kode_propinsi()
  {
    return $this->CI->session->userdata('kode_propinsi');
  }

  function kode_kab_kota()
  {
    return $this->CI->session->userdata('kode_kab_kota');
  }

  function id_grup_pengguna()
  {
    $id_grup_pengguna = 0;
    $arr_data_otoritas = $this->CI->session->userdata('data_otoritas_array');
    foreach ($arr_data_otoritas as $key) {
      if($key['id_aplikasi_otoritas'] === $this->id_aplikasi_kapi)
      {
        $id_grup_pengguna = $key['id_grup_otoritas'];
      }
    }
    return $id_grup_pengguna;
  }

  function info_is_admin($index='')
  {
    $info_is_admin = array();
    $is_admin_session = $this->CI->session->userdata('is_admin') > 0 ? TRUE : FALSE;
    $is_super_admin = $this->CI->session->userdata('is_super_admin') > 0 ? TRUE : FALSE;
    $is_admin_otoritas = false;

    // vdump($is_admin_session);

    $arr_data_otoritas = $this->CI->session->userdata('data_otoritas_array');
    if($arr_data_otoritas){
      // vdump($arr_data_otoritas);
      foreach ($arr_data_otoritas as $key) {
        if($key['id_aplikasi_otoritas'] === $this->id_aplikasi_kapi)
        {
          if($key['id_grup_otoritas'] === "8")
          {
            $is_admin_otoritas = true;                    
          }else{
            $is_admin_otoritas = false;
          }
        }
      }
    }

    $info_is_admin = array('is_admin_otoritas' => $is_admin_otoritas,
                'is_super_admin' => $is_super_admin,
                'is_admin_session' => $is_admin_session
                 );


    // return $info_is_admin[$index];
    return $is_admin_session;
  }
}
?>