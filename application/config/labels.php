<?php
	$item = '$item->';
	$config = Array(
					'label_view_table_index' => Array(
													'title' => Array(
															'page' => 'Tabel Proses Pendaftaran',
															'panel'=> 'Tabel Proses Pendaftaran'

														),
													'form'		=> '',
													'th_table'	=> Array(

																	'0' => Array('data' => 'No Pendoks', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'1' => Array('data' => 'Nama Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'2' => Array('data' => 'Pemilik Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'3' => Array('data' => 'Pendok', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'4' => Array('data' => 'Entri Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'5' => Array('data' => 'Verifikasi Kapal', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'6' => Array('data' => 'Cetak BKP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;'),
																	'7' => Array('data' => 'Terima BKP', 'width' => '50', 'style' => 'padding: 10px 10px;font-style:bold;')

																	)
												),
					'form_pendok_label' => Array(
									'title' => Array( 
													'page' => 'Entri Pendok',
													'panel' => 'Entri Pendok',
													),
									'form' => Array(
													'no_surat_permohonan' 		=> Array( 	'name' 	=> 	'no_surat_permohonan',
																							'label'	=> 	'No. Surat Permohonan'
																				  		),
													'tanggal_surat_permohonan' 	=> Array( 	'name' 	=> 	'tanggal_surat_permohonan',
																					 		'label' =>	'Tgl. Surat Permohonan'
																				   		),
													'nama_kapal' 				=> Array( 	'name'	=> 	'nama_kapal',
																							'label' => 	'Nama Kapal'
																						),
													'nama_perusahaan' 			=> Array( 	'name' 	=> 	'nama_perusahaan',
																							'label' => 	'Nama (Perorangan/Perusahaan)'
																						),
													'tipe_permohonan' 			=> Array( 	'name' 	=> 	'tipe_permohonan',
																							'label' => 	'Tipe Permohonan'
																						),
													'nama_pemohon' 				=> Array( 	'name' 	=>	'nama_pemohon',
																			  				'label' => 	'Nama Pemohon'
																			 			),
													'tempat_lahir_pemohon'		=> Array( 	'name' 	=> 	'tempat_lahir_pemohon',
																							'label' =>	'Tempat Lahir Pemohon'
																						),
													'tanggal_lahir_pemohon'		=> Array( 	'name' 	=> 	'tanggal_lahir_pemohon',
																							'label' =>	'Tgl. Lahir Pemohon'
																						),
													'no_ktp' 					=> Array( 	'name' 	=> 	'no_ktp',
																	   						'label' =>	'No. KTP'
																	   					),
													'email_pemohon' 			=> Array( 	'name' 	=> 	'email_pemohon',
																			  				'label' => 	'Email Pemohon'
																			  			),
													'alamat_pemohon' 			=> Array( 	'name' 	=> 	'alamat_pemohon',
																			   				'label' =>	'Alamat Pemohon'
																			  			),
													'jabatan_pemohon' 			=> Array( 	'name' 	=> 	'jabatan_pemohon',
																			  				'label' => 	'Jabatan Pemohon'
																			  			),
													'no_telp_pemohon' 			=> Array( 	'name' 	=> 	'no_telp_pemohon',
																			  				'label' =>	'No. Telp'
																			  			),
													'keterangan_pendok' 		=> Array( 	'name' 	=> 	'keterangan_pendok',
																			   				'label' =>	'Keterangan Permohonan'
																			 			),
													'formulir_permohonan' 		=> Array( 	'name' 	=> 	'formulir_permohonan',
																			  				'label' =>	'Surat Permohonan Pendaftaran dan Penandaan Kapal Perikanan (Formulir Permohonan)'
																			  			),
													'ftcp_siup' 				=> Array( 	'name' 	=> 	'ftcp_siup',
																		  					'label' =>	'Fotocopy SIUP'
																		 				),
													'ftcp_sipi' 				=> Array( 	'name' 	=> 	'ftcp_sipi',
																		  					'label' =>	'Fotocopy SIPI/SIKPI'
																						),
													'ftcp_gross' 				=> Array( 	'name' 	=> 	'ftcp_gross',
																		   					'label' =>	'Fotocopy Grosse Akte',
																		   				),
													'ftcp_ktp' 					=> Array( 	'name' 	=> 	'ftcp_ktp',
																	     					'label' =>	'Fotocopy KTP'
																	     				),
													'formulir_periksa_fisik' 	=> Array( 	'name' 	=> 	'formulir_periksa_fisik',
																			  				'label' => 	'Surat Permohonan Pemeriksaan Fisik Kapal Perikanan'
																			  			),
													'ftcp_hasil_periksa_fisik' 	=> Array( 	'name' 	=> 	'ftcp_hasil_periksa_fisik',
																			  			 	'label' =>	'Fotocopy Hasil Pemeriksaan Fisik Kapal Perikanan'
																			  			),
													'ftcp_ukur_kapal' 			=> Array( 	'name' 	=> 	'ftcp_ukur_kapal',
																			    			'label' =>	'Fotocopy Surat Ukur Kapal'
																			    		),
													'ftcp_laut_pas' 			=> Array( 	'name' 	=> 	'ftcp_laut_pas',
																			  				'label' => 	'Fotocopy Surat Laut Pas Tahunan'
																						),
													'ftcp_kelaikan_sertifikat_keselamatan' => Array( 	'name' 	=> 	'ftcp_kelaikan_sertifikat_keselamatan',
																			  							'label' =>	'Fotocopy Surat Kelaikan dan Pengawakan Bagi Kapal Penangkap Ikan atau Sertifikat Keselamatan Bagi Kapal Pengangkut Ikan'
																			  						),
													'penghapusan_kapal' 		=> Array( 	'name' 	=> 	'penghapusan_kapal',
																			  				'label' => 	'Surat keterangan Penghapusan Kapal (Deletion Certificate)'
																			  			),
													'foto_kapal' 				=> Array( 	'name' 	=> 	'foto_kapal',
																		   					'label' =>	'Foto Kapal Ukuran 10 cm x 5 cm 2 Lembar (Tampak Samping)'
																						),
													'pernyataan'				=> Array( 	'name' 	=> 	'pernyataan',
																		   					'label' =>	'Surat Pernyataan Dari Pemohon yang Menyatakan Bertanggung Jawab Atas Kebenaran Data dan Informasi yang Disampaikan'
																		   				),
													'kepemilikan'				=> Array( 	'name' => 'kepemilikan',
																							'label' => 'Perubahan Kepemilikan Kapal'
																						),
													'alat_tangkap'				=> Array( 	'name' => 'alat_tangkap',
																							'label' => 'Perubahan Alat Tangkap Kapal'
																						),
													'fungsi_kapal'				=> Array( 	'name' => 'fungsi_kapal',
																							'label' => 'Perubahan Fungsi Kapal'
																						),
													'mesin'						=> Array( 	'name' => 'mesin',
																							'label' => 'Perubahan Mesin Kapal'
																						),
													'no_register'				=> Array( 	'name' => 'no_register',
																							'label' => 'No. Register'
																						)
													)
										  ),
					'form_kapal_label' => Array(
									'title' => Array( 
													'page' => 'Entri Kapal',
													'panel' => 'Entri Kapal'
													),
									'form' => Array(
													'path_foto' 				=> Array( 	'name' 	=> 'path_foto',
																					 		'label'	=> 'Foto Kapal'
																						),
													'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																					 		'label'	=> 'Nama Kapal'
																						),
													'nama_kapal_sblm' 	=> Array( 	'name' 	=> 'nama_kapal_sblm',
																 					  		'label' => 'Nama Kapal Sebelum'
 																						),
													'tempat_pembangunan' 		=> Array( 	'name' 	=> 'tempat_pembangunan_id',
																					 		'label'	=> 'Tempat Pembangunan'
																						),
													'tahun_pembangunan' 		=> Array( 	'name' 	=> 'tahun_pembangunan',
																					 		'label'	=> 'Tahun Pembangunan'
																						),
													'bahan_kapal' 		=> Array( 	'name' 	=> 'id_bahan_kapal',
																					 		'label'	=> 'Bahan Utama Kapal'
																						),
													'type_kapal' 				=> Array( 	'name' 	=> 'type_kapal',
																					 		'label'	=> 'Type/Jenis Kapal'
																						),
													'alat_tangkap' 	=> Array( 	'name' 	=> 'id_alat_tangkap',
																			 			'label'	=> 'Jenis Alat Penangkap Ikan'
																							),
													'merek_mesin' 	=> Array( 	'name' 	=> 'merek_mesin',
																					 			'label'	=> 'Merek Mesin Utama Kapal'
																							),
													'tipe_mesin' 	=> Array( 	'name' 	=> 'tipe_mesin',
																					 			'label'	=> 'Type Mesin Utama Kapal'
																							),
													'daya_kapal' 	=> Array( 	'name' 	=> 'daya_kapal',
																					 		'label'	=> 'Daya Mesin Utama Kapal'
																						),
													'satuan_mesin_utama_kapal' 	=> Array( 	'name' 	=> 'satuan_mesin_utama_kapal',
																					 		'label'	=> ''
																						),
													'no_mesin' 		=> Array( 	'name' 	=> 'no_mesin',
																					 		'label'	=> 'Nomor Mesin Utama Kapal'
																						),
													'jumlah_palka' 				=> Array( 	'name' 	=> 'jumlah_palka',
																					 		'label'	=> 'Jumlah Palka (unit)'
																						),
													'kapasitas_palka' 			=> Array( 	'name' 	=> 'kapasitas_palka',
																					 		'label'	=> 'Kapasitas Palka (m<sup>3</sup>)'
																						),
													'tempat_pendaftaran' 		=> Array( 	'name' 	=> 'tempat_pendaftaran_id',
																					 		'label'	=> 'Tempat Pendaftaran BKP'
																						),
													'tempat_grosse_akte' 		=> Array( 	'name' 	=> 'tempat_grosse_akte',
																					 		'label'	=> 'Tempat Grosse Akte'
																						),
													'no_grosse_akte' 			=> Array( 	'name' 	=> 'no_grosse_akte',
																					 		'label'	=> 'No. Grosse Akte'
																						),
													'path_file_gross' 			=> Array( 	'name' 	=> 'path_file_gross',
																					 		'label'	=> 'File Grosse Akte'
																						),
													'imo_number' 				=> Array( 	'name' 	=> 'imo_number',
																					 		'label'	=> 'IMO'
																						),
													'tanggal_grosse_akte' 			=> Array( 	'name' 	=> 'tanggal_grosse_akte',
																					 		'label'	=> 'Tgl. Grosse Akte'
																						),
													'panjang_kapal' 			=> Array( 	'name' 	=> 'panjang_kapal',
																					 		'label'	=> 'Panjang Kapal (m)'
																						),
													'lebar_kapal' 			=> Array( 	'name' 	=> 'lebar_kapal',
																					 		'label'	=> 'Lebar Kapal (m)'
																						),
													'dalam_kapal' 			=> Array( 	'name' 	=> 'dalam_kapal',
																					 		'label'	=> 'Dalam Kapal (m)'
																						),
													'panjang_loa_kapal' 			=> Array( 	'name' 	=> 'panjang_loa_kapal',
																					 		'label'	=> 'Panjang Keseluruhan (LoA) (m)'
																						),
													'gt_kapal' 					=> Array( 	'name' 	=> 'gt_kapal',
																					 		'label'	=> 'GT'
																						),
													'nt_kapal' 					=> Array( 	'name' 	=> 'nt_kapal',
																					 		'label'	=> 'NT'
																						),
													'no_siup' 					=> Array( 	'name' 	=> 'no_siup',
																					 		'label'	=> 'No. SIUP'
																						),
													'jenis_perusahaan' 			=> Array( 	'name' 	=> 'jenis_perusahaan',
																					 		'label'	=> 'Jenis Perusahaan'
																						),
													'email_perusahaaan' 		=> Array( 	'name' 	=> 'email_perusahaaan',
																					 		'label'	=> 'Email Perusahaan'
																						),
													'nama_pemilik' 				=> Array( 	'name' 	=> 'nama_pemilik',
																					 		'label'	=> 'Nama Pemilik'
																						),
													'alamat_perusahaan' 		=> Array( 	'name' 	=> 'alamat_perusahaan',
																					 		'label'	=> 'Alamat Perusahaan'
																						),
													'nama_penanggung_jawab' 	=> Array( 	'name' 	=> 'nama_penanggung_jawab',
																					 		'label'	=> 'Nama Penanggung Jawab'
																						),
													'email_penanggung_jawab' 	=> Array( 	'name' 	=> 'email_penanggung_jawab',
																					 		'label'	=> 'Email Penanggung Jawab'
																						),
													)
									),
					'form_pemohon' => Array(
										'title' => Array( 
														'page' 	=> 'Entry Pemohon',
														'panel' => 'Entry Pemohon'
														),
										'form' => Array(
														'nama_pemohon' 				=> Array( 	'name' 	=>	'nama_pemohon',
																			  					'label' => 	'Nama'
																				 			),
														'tempat_lahir_pemohon'		=> Array( 	'name' 	=> 	'tempat_lahir_pemohon',
																								'label' =>	'Tempat Lahir'
																							),
														'tanggal_lahir_pemohon'		=> Array( 	'name' 	=> 	'tanggal_lahir_pemohon',
																								'label' =>	'Tgl. Lahir'
																							),
														'no_ktp' 					=> Array( 	'name' 	=> 	'no_ktp',
																		   						'label' =>	'No. KTP'
																		   					),
														'email_pemohon' 			=> Array( 	'name' 	=> 	'email_pemohon',
																				  				'label' => 	'Email'
																				  			),
														'alamat_pemohon' 			=> Array( 	'name' 	=> 	'alamat_pemohon',
																				   				'label' =>	'Alamat'
																				  			),
														'kode_pos' 			=> Array( 	'name' 	=> 	'kode_pos',
																				   				'label' =>	'Kode Pos'
																				  			),
														'jabatan_pemohon' 			=> Array( 	'name' 	=> 	'jabatan_pemohon',
																				  				'label' => 	'Jabatan'
																				  			),
														'no_telp_pemohon' 			=> Array( 	'name' 	=> 	'no_telp_pemohon',
																				  				'label' =>	'No. Telp'
																				  			)
													)
									),
					'form_dokumen' => Array(
										'title' => Array( 
														'page' 	=> 'Konfigurasi Dokumen',
														'panel' => 'Konfigurasi Dokumen'
														),
										'form' => Array(
														'nama_dokumen' 				=> Array( 	'name' 	=>	'nama_dokumen',
																			  					'label' => 	'Nama Dokumen'
																				 			),
														'peraturan'		=> Array( 	'name' 	=> 	'peraturan',
																								'label' =>	'Peraturan'
																							),
														'status_tampil'		=> Array( 	'name' 	=> 	'status_tampil',
																								'label' =>	'Status Tampil'
																							),
														'wajib' 					=> Array( 	'name' 	=> 	'wajib',
																		   						'label' =>	'Wajib'
																		   					)
													)
									),
					'form_pengaturan_pejabat' => Array(
												'title'	=> Array(
															'page'	=> 'Pengaturan Pejabat',
															'panel'	=> 'Pengaturan Pejabat'
															),
												'form'	=> Array(
															'nama_pejabat'	=> Array(
																				'name'	=> 'nama_pejabat',
																				'label'	=> 'Nama Pejabat'
																				),
															'nip_fake'			=> Array(
																				'name'	=> 'nip_fake',
																				'label'	=> 'NIP'
																				),
															'nip_real'			=> Array(
																				'name'	=> 'nip_real',
																				'label'	=> 'NIP'
																				),
															'jabatan'			=> Array(
																				'name'	=> 'jabatan',
																				'label'	=> 'Jabatan'
																				)
															)
												),
					'label_view_pengaturan_pejabat' => Array(
														'title' 	=> Array(
																		'page' 	=> 'Pengaturan Pejabat',
																		'panel'	=> 'Pengaturan Pejabat'
																		),
														'form'		=> '',
														'th_table' 	=> Array(
																		'0'	=> Array('data' => 'No','width' => '20'),
																		'1'	=> Array('data' => 'NIP'),
																		'2'	=> Array('data' => 'Nama Pemohon'),
																		'3'	=> Array('data' => 'Jabatan'),
																		'4'	=> Array('data' => 'Aksi')
																		)
														),
					'label_view_kapal' => Array(
											'title' => Array(
															'page' => 'Daftar Kapal di DSS',
															'panel' => 'Daftar Kapal di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => 'SIPI',
															'5' => Array('data' => 'Tanggal Sipi', 'width' => '150'),
															'6' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'7' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'8' => 'Jenis Kapal',
															'9' => 'Bahan Kapal',
															'10' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'11' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)
											),
					'label_view_kapal_daerah' => Array(
											'title' => Array(
															'page' => 'Tabel Kapal Daerah di DSS',
															'panel' => 'Tabel Kapal Daerah di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal Daerah',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => 'SIPI',
															'5' => Array('data' => 'Tanggal Sipi', 'width' => '150'),
															'6' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'7' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'8' => 'Jenis Kapal',
															'9' => 'Bahan Kapal',
															'10' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'11' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)

											),
					'label_view_kapal_kapi' => Array(
											'title' => Array(
															'page' => 'Tabel Kapal di KAPI',
															'panel' => 'Tabel Kapal di KAPI'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'5' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'6' => 'Jenis Kapal',
															'7' => 'Bahan Kapal',
															'8' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'9' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)
											),
					'label_view_kapal_daerah_kapi' => Array(
											'title' => Array(
															'page' => 'Tabel Kapal Daerah di KAPI',
															'panel' => 'Tabel Kapal Daerah di KAPI'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Nama Kapal Daerah',
															'2' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'3' => 'Nama Kapal Sebelumnya',
															'4' => 'SIPI',
															'5' => Array('data' => 'Tanggal Sipi', 'width' => '150'),
															'6' => Array('data' => 'Tonase <br> (GT & NT)', 'width' => '150'),
															'7' => Array('data' => 'Ukuran Kapal', 'width' => '100'),
															'8' => 'Jenis Kapal',
															'9' => 'Bahan Kapal',
															'10' => Array('data' => 'No. Mesin<br>Merek Mesin', 'width' => '150'),
															'11' => Array('data' => 'Alat Tangkap', 'width' => '300')
														)

											),
					'label_view_perusahaan' => Array(
											'title' => Array(
															'page' => 'Tabel Perusahaan di DSS',
															'panel' => 'Tabel Perusahaan di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Perusahaan', 'width' => '100'),
															'2' => Array('data' => 'Nomor SIUP', 'width' => '100'),
															'3' => Array('data' => 'Tanggal SIUP', 'width' => '100'),
															'4' => Array('data' => 'Nomor NPWP', 'width' => '100'),
															'5' => Array('data' => 'Nama Penanggung Jawab', 'width' => '200'),
															'6' => Array('data' => 'No Identitas<br>Penanggung Jawab', 'width' => '200'),
															'7' => Array('data' => 'Alamat Perusahaan', 'width' => '200')
														)
											),
					'label_view_perusahaan_daerah' => Array(
											'title' => Array(
															'page' => 'Tabel Perusahaan Daerah di DSS',
															'panel' => 'Tabel Perusahaan Daerah di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Perusahaan', 'width' => '100'),
															'2' => Array('data' => 'Nomor SIUP', 'width' => '100'),
															'3' => Array('data' => 'Tanggal SIUP', 'width' => '100'),
															'4' => Array('data' => 'Nomor NPWP', 'width' => '100'),
															'5' => Array('data' => 'Nama Penanggung Jawab', 'width' => '200'),
															'6' => Array('data' => 'No Identitas<br>Penanggung Jawab', 'width' => '200'),
															'7' => Array('data' => 'Alamat Perusahaan', 'width' => '200')
														)
											),
					'label_view_perusahaan_kapi' => Array(
											'title' => Array(
															'page' => 'Tabel Perusahaan di DSS',
															'panel' => 'Tabel Perusahaan di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Perusahaan', 'width' => '100'),
															'2' => Array('data' => 'Nomor SIUP', 'width' => '100'),
															'3' => Array('data' => 'Tanggal SIUP', 'width' => '100'),
															'4' => Array('data' => 'Nomor NPWP', 'width' => '100'),
															'5' => Array('data' => 'Nama Penanggung Jawab', 'width' => '200'),
															'6' => Array('data' => 'No Identitas<br>Penanggung Jawab', 'width' => '200'),
															'7' => Array('data' => 'Alamat Perusahaan', 'width' => '200')
														)
											),
					'label_view_perusahaan_daerah_kapi' => Array(
											'title' => Array(
															'page' => 'Tabel Perusahaan Daerah di DSS',
															'panel' => 'Tabel Perusahaan Daerah di DSS'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Perusahaan', 'width' => '100'),
															'2' => Array('data' => 'Nomor SIUP', 'width' => '100'),
															'3' => Array('data' => 'Tanggal SIUP', 'width' => '100'),
															'4' => Array('data' => 'Nomor NPWP', 'width' => '100'),
															'5' => Array('data' => 'Nama Penanggung Jawab', 'width' => '200'),
															'6' => Array('data' => 'No Identitas<br>Penanggung Jawab', 'width' => '200'),
															'7' => Array('data' => 'Alamat Perusahaan', 'width' => '200')
														)
											),
					'label_view_pemohon' => Array(
											'title' => Array(
															'page' => 'Tabel Pemohon',
															'panel' => 'Tabel Pemohon'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Pemohon', 'width' => '150'),
															'2' => Array('data' => 'Nomor KTP', 'width' => '100'),
															'3' => Array('data' => 'Tempat, Tanggal Lahir', 'width' => '200'),
															'4' => Array('data' => 'Alamat', 'width' => '150'),
															'5' => Array('data' => 'Kode Pos', 'width' => '150'),
															'6' => Array('data' => 'Jabatan', 'width' => '100'),
															'7' => Array('data' => 'No. Telp', 'width' => '100'),
															'8' => Array('data' => 'Email', 'width' => '100'),
															'9' => Array('data' => 'Aksi', 'width' => '150')
														)
											// 'td_table' => Array(
											// 				'1' => Array('data' => $item.$form_pendok['form']['nama_pemohon'], 'width' => '100'),
											// 				'2' => Array('data' => $item.$form_pendok['form']['no_ktp'], 'width' => '100'),
											// 				'3' => Array('data' => $item.$form_pendok['form']['tempat_lahir_pemohon'].', '
											// 									   .$item.$form_pendok['form']['tanggal_lahir_pemohon'], 'width' => '100'),
											// 				'4' => Array('data' => $item.$form_pendok['form']['alamat_pemohon'], 'width' => '100'),
											// 				'5' => Array('data' => $item.$form_pendok['form']['jabatan_pemohon'], 'width' => '200'),
											// 				'6' => Array('data' => $item.$form_pendok['form']['no_telp_pemohon'], 'width' => '200'),
											// 				'7' => Array('data' => $item.$form_pendok['form']['email_pemohon'], 'width' => '200')
											// 			)
											),
					'label_view_pendok' => Array(
											'title' => Array(
															'page' => 'Daftar Penerimaan Dokumen',
															'panel' => 'Daftar Penerimaan Dokumen'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'No Pendok', 'width' => '20'),
															'2' => Array('data' => 'Nama Kapal', 'width' => '150'),
															'3' => Array('data' => 'Nama Pemilik', 'width' => '100'),
															'4' => Array('data' => 'Jenis Permohonan', 'width' => '50'),
															'5' => Array('data' => 'Kategori', 'width' => '100'),
															'6' => Array('data' => 'Tanggal | Petugas', 'width' => '150'),
															'7' => Array('data' => 'Status', 'width' => '150')
														)
											// 'td_table' => Array(
											// 				'1' => Array('data' => $item.$form_pendok['form']['nama_pemohon'], 'width' => '100'),
											// 				'2' => Array('data' => $item.$form_pendok['form']['no_ktp'], 'width' => '100'),
											// 				'3' => Array('data' => $item.$form_pendok['form']['tempat_lahir_pemohon'].', '
											// 									   .$item.$form_pendok['form']['tanggal_lahir_pemohon'], 'width' => '100'),
											// 				'4' => Array('data' => $item.$form_pendok['form']['alamat_pemohon'], 'width' => '100'),
											// 				'5' => Array('data' => $item.$form_pendok['form']['jabatan_pemohon'], 'width' => '200'),
											// 				'6' => Array('data' => $item.$form_pendok['form']['no_telp_pemohon'], 'width' => '200'),
											// 				'7' => Array('data' => $item.$form_pendok['form']['email_pemohon'], 'width' => '200')
											// 			)
											),
		  			'label_view_kapal_pendok' => Array(
					  									'title' => Array(
																		'page' => 'Daftar Entri Kapal Pendok',
																		'panel' => 'Daftar Entri Kapal Pendok'
					  												),
					  									'form' => '',
					  									'th_table' => Array(
					  													'0' => Array('data' => 'No', 'width' => '20'),
																		'1' => Array('data' => 'No Pendok', 'width' => '50'),
																		'2' => Array('data' => 'No Registrasi', 'width' => '50'),
																		'3' => Array('data' => 'Nama Kapal', 'width' => '150'),
																		'4' => Array('data' => 'Nama Pemilik', 'width' => '100'),
																		'5' => Array('data' => 'GT ', 'width' => '50'),
																		'6' => Array('data' => 'Jenis Permohonan', 'width' => '100'),
																		'7' => Array('data' => 'Kategori', 'width' => '50'),
																		'8' => Array('data' => 'Status', 'width' => '50')
					  												)
														),
					'label_view_buku_kapal' => Array(
											'title' => Array(
															'page' => 'Daftar Buku Kapal',
															'panel' => 'Daftar Buku Kapal'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'No. Pendok', 'width' => '20'),
															'2' => Array('data' => 'No. Registrasi', 'width' => '20'),
															'3' => Array('data' => 'Nama Kapal', 'width' => '200'),
															'4' => Array('data' => 'Nama Pemilik', 'width' => '200'),
															'5' => Array('data' => 'GT', 'width' => '50'),
															'6' => Array('data' => 'Jenis Permohonan', 'width' => '100'),
															'7' => Array('data' => 'Kategori', 'width' => '50'),
															'8' => Array('data' => 'Cetak', 'width' => '150'),
															'9' => Array('data' => 'Status', 'width' => '50')
															// '9' => Array('data' => 'Serah Terima', 'width' => '50')
														)
											// 'td_table' => Array(
											// 				'1' => Array('data' => $item.$form_pendok['form']['nama_pemohon'], 'width' => '100'),
											// 				'2' => Array('data' => $item.$form_pendok['form']['no_ktp'], 'width' => '100'),
											// 				'3' => Array('data' => $item.$form_pendok['form']['tempat_lahir_pemohon'].', '
											// 									   .$item.$form_pendok['form']['tanggal_lahir_pemohon'], 'width' => '100'),
											// 				'4' => Array('data' => $item.$form_pendok['form']['alamat_pemohon'], 'width' => '100'),
											// 				'5' => Array('data' => $item.$form_pendok['form']['jabatan_pemohon'], 'width' => '200'),
											// 				'6' => Array('data' => $item.$form_pendok['form']['no_telp_pemohon'], 'width' => '200'),
											// 				'7' => Array('data' => $item.$form_pendok['form']['email_pemohon'], 'width' => '200')
											// 			)
											),
					'label_view_buku_kapal_cetak' => Array(
											'title' => Array(
															'page' => 'Daftar Cetak Buku Kapal',
															'panel' => 'Daftar Cetak Buku Kapal'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No.', 'width' => '20'),
															'1' => Array('data' => 'No. Pendok', 'width' => '20'),
															'2' => Array('data' => 'No. Registrasi', 'width' => '20'),
															'3' => Array('data' => 'Jenis Permohonan', 'width' => '150'),
															'4' => Array('data' => 'Nama Kapal', 'width' => '200'),
															'5' => Array('data' => 'Nama Pemilik', 'width' => '100'),
															'6' => Array('data' => 'Kategori', 'width' => '50'),
															'7' => Array('data' => 'Cetak', 'width' => '50'),
															'8' => Array('data' => 'Status', 'width' => '50'),
															'9' => Array('data' => 'Serah Terima', 'width' => '50')

														)
											// 'td_table' => Array(
											// 				'1' => Array('data' => $item.$form_pendok['form']['nama_pemohon'], 'width' => '100'),
											// 				'2' => Array('data' => $item.$form_pendok['form']['no_ktp'], 'width' => '100'),
											// 				'3' => Array('data' => $item.$form_pendok['form']['tempat_lahir_pemohon'].', '
											// 									   .$item.$form_pendok['form']['tanggal_lahir_pemohon'], 'width' => '100'),
											// 				'4' => Array('data' => $item.$form_pendok['form']['alamat_pemohon'], 'width' => '100'),
											// 				'5' => Array('data' => $item.$form_pendok['form']['jabatan_pemohon'], 'width' => '200'),
											// 				'6' => Array('data' => $item.$form_pendok['form']['no_telp_pemohon'], 'width' => '200'),
											// 				'7' => Array('data' => $item.$form_pendok['form']['email_pemohon'], 'width' => '200')
											// 			)
											),
					'label_view_dokumen' => Array(
											'title' => Array(
															'page' => 'Pengaturan Dokumen',
															'panel' => 'Pengaturan Dokumen'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Dokumen', 'width' => '150'),
															'2' => Array('data' => 'Peraturan', 'width' => '100'),
															'3' => Array('data' => 'Status Tampil', 'width' => '200'),
															'4' => Array('data' => 'Wajib', 'width' => '150'),
															'5' => Array('data' => 'Aksi', 'width' => '150')
														)
											// 'td_table' => Array(
											// 				'1' => Array('data' => $item.$form_pendok['form']['nama_pemohon'], 'width' => '100'),
											// 				'2' => Array('data' => $item.$form_pendok['form']['no_ktp'], 'width' => '100'),
											// 				'3' => Array('data' => $item.$form_pendok['form']['tempat_lahir_pemohon'].', '
											// 									   .$item.$form_pendok['form']['tanggal_lahir_pemohon'], 'width' => '100'),
											// 				'4' => Array('data' => $item.$form_pendok['form']['alamat_pemohon'], 'width' => '100'),
											// 				'5' => Array('data' => $item.$form_pendok['form']['jabatan_pemohon'], 'width' => '200'),
											// 				'6' => Array('data' => $item.$form_pendok['form']['no_telp_pemohon'], 'width' => '200'),
											// 				'7' => Array('data' => $item.$form_pendok['form']['email_pemohon'], 'width' => '200')
											// 			)
											),		
					'label_form_buku_kapal' => Array(
										'title' => Array( 
														'page' 	=> 'Detail Buku Kapal',
														'panel' => 'Detail Buku Kapal'
														),
										'form' => Array(
														'nama_kapal' 	=> Array( 	'name' 	=> 	'nama_kapal',
																					'label'	=> 	'Nama Kapal'
																				  	),
														'no_register' 	=> Array( 	'name' 	=> 	'no_register',
																					'label'	=> 	'No. Register'
																				  	),
														'no_seri_bkp' 		=> Array( 	'name' 	=> 	'no_seri_bkp',
																						'label'	=> 	'No. Seri Buku Kapal'
																					  	),
														'no_tanda_pengenal' 	=> Array( 	'name' 	=> 	'no_tanda_pengenal',
																							'label'	=> 	'No. Tanda Pengenal'
																					  		),
														'keterangan_bkp' => Array( 	'name' 	=> 	'keterangan_bkp',
																					'label'	=> 	'Keterangan Buku Kapal'
																					),
														'id_pengguna_buat' 		=> Array( 	'name' 	=> 	'id_pengguna_buat',
																							'label'	=> 	'Nama pegawai Input'
																					  		),

														'nama_kasie' 			=> Array( 	'name' 	=> 	'nama_kasie',
																							'label'	=> 	'Kasie'
																					  		),							  			
														'nama_kasubdit' 		=> Array( 	'name' 	=> 	'nama_kasubdit',
																							'label'	=> 	'Kasubdit'
																					  		),
														'nama_direktur' 		=> Array( 	'name' 	=> 	'nama_direktur',
																							'label'	=> 	'Direktur'
																					  		),
														'nip_direktur_fake' 			=> Array( 	'name' 	=> 	'nip_direktur_fake',
																							'label'	=> 	'NIP. Direktur'
																					  		),
														'nip_direktur_real' 			=> Array( 	'name' 	=> 	'nip_direktur_real',
																							'label'	=> 	'NIP. Direktur'
																					  		),
														'lokasi_pengesahan_id' 	=> Array( 	'name' 	=> 	'lokasi_pengesahan_id',
																							'label'	=> 	'Lokasi Pengesahan'
																					  		),
														'tgl_ttd_direktur' 	=> Array( 	'name' 	=> 	'tgl_ttd_direktur',
																							'label'	=> 	'Tanggal ttd Direktur'
																					  		)
													)
									),		
					'label_form_pendok_online' => Array(
										'title' => Array( 
														'page' 	=> 'Detail Buku Kapal',
														'panel' => 'Detail Buku Kapal'
														),
										'form' => Array(
														'no_surat_permohonan' => 	Array(	'name' => 'no_surat_permohonan',
																							'label' => 'No Surat Permohonan'
														),
														'tipe_permohonan' => 	Array(	'name' => 'tipe_permohonan',
																						'label' => 'Tipe Permohonan'
														),
														'nama_kapal' => 	Array(	'name' => 'nama_kapal',
																					'label' => 'Nama Kapal'
														),
														'nama_kapal_sblm' => 	Array(	'name' => 'nama_kapal_sblm',
																						'label' => 'Nama Kapal Sebelum'
														),
														'nama_perusahaan' => 	Array(	'name' => 'nama_perusahaan',
																						'label' => 'Nama Perusahaan'
														),
														'nama_penanggung_jawab' => 	Array(	'name' => 'nama_penanggung_jawab',
																							'label' => 'Nama Penanggung Jawab'
														),
														'no_identitas_penanggung_jawab' => 	Array(	'name' => 'no_identitas_penanggung_jawab',
																									'label' => 'No. Identitas Penanggung Jawab'
														),
														'ttl_penanggung_jawab' => 	Array(	'name' => 'ttl_penanggung_jawab',
																							'label' => 'Ttl Penanggung Jawab'
														),
														'alamat_perusahaan' => 	Array(	'name' => 'alamat_perusahaan',
																						'label' => 'Alamat Perusahaan'
														),
														'no_telp_perusahaan' => 	Array(	'name' => 'no_telp_perusahaan',
																							'label' => 'No Telp Perusahaan'
														),
														'keterangan_pendok' => 	Array(	'name' => 'keterangan_pendok',
																						'label' => 'Keterangan Pendok'
														),
														'path_foto' 				=> Array( 	'name' 	=> 'path_foto',
																					 		'label'	=> 'Foto Kapal'
																						),
														'nama_kapal' 				=> Array( 	'name' 	=> 'nama_kapal',
																						 		'label'	=> 'Nama Kapal'
																							),
														'nama_kapal_sblm' 	=> Array( 	'name' 	=> 'nama_kapal_sblm',
																	 					  		'label' => 'Nama Kapal Sebelum'
	 																						),
														'tempat_pembangunan' 		=> Array( 	'name' 	=> 'tempat_pembangunan_id',
																						 		'label'	=> 'Tempat Pembangunan'
																							),
														'tahun_pembangunan' 		=> Array( 	'name' 	=> 'tahun_pembangunan',
																						 		'label'	=> 'Tahun Pembangunan'
																							),
														'bahan_kapal' 		=> Array( 	'name' 	=> 'id_bahan_kapal',
																						 		'label'	=> 'Bahan Utama Kapal'
																							),
														'type_kapal' 				=> Array( 	'name' 	=> 'type_kapal',
																						 		'label'	=> 'Type/Jenis Kapal'
																							),
														'alat_tangkap' 	=> Array( 	'name' 	=> 'id_alat_tangkap',
																				 			'label'	=> 'Jenis Alat Penangkap Ikan'
																								),
														'merek_mesin' 	=> Array( 	'name' 	=> 'merek_mesin',
																						 			'label'	=> 'Merek Mesin Utama Kapal'
																								),
														'tipe_mesin' 	=> Array( 	'name' 	=> 'tipe_mesin',
																						 			'label'	=> 'Type Mesin Utama Kapal'
																								),
														'daya_kapal' 	=> Array( 	'name' 	=> 'daya_kapal',
																						 		'label'	=> 'Daya Mesin Utama Kapal'
																							),
														'satuan_mesin_utama_kapal' 	=> Array( 	'name' 	=> 'satuan_mesin_utama_kapal',
																						 		'label'	=> ''
																							),
														'no_mesin' 		=> Array( 	'name' 	=> 'no_mesin',
																						 		'label'	=> 'Nomor Mesin Utama Kapal'
																							),
														'jumlah_palka' 				=> Array( 	'name' 	=> 'jumlah_palka',
																						 		'label'	=> 'Jumlah Palka (unit)'
																							),
														'kapasitas_palka' 			=> Array( 	'name' 	=> 'kapasitas_palka',
																						 		'label'	=> 'Kapasitas Palka (m<sup>3</sup>)'
																							),
														'tempat_pendaftaran' 		=> Array( 	'name' 	=> 'tempat_pendaftaran_id',
																						 		'label'	=> 'Tempat Pendaftaran BKP'
																							),
														'tempat_grosse_akte' 		=> Array( 	'name' 	=> 'tempat_grosse_akte',
																						 		'label'	=> 'Tempat Grosse Akte'
																							),
														'no_grosse_akte' 			=> Array( 	'name' 	=> 'no_grosse_akte',
																						 		'label'	=> 'No. Grosse Akte'
																							),
														'path_file_gross' 			=> Array( 	'name' 	=> 'path_file_gross',
																						 		'label'	=> 'File Grosse Akte'
																							),
														'imo_number' 				=> Array( 	'name' 	=> 'imo_number',
																						 		'label'	=> 'IMO'
																							),
														'tanggal_grosse_akte' 			=> Array( 	'name' 	=> 'tanggal_grosse_akte',
																						 		'label'	=> 'Tgl. Grosse Akte'
																							),
														'panjang_kapal' 			=> Array( 	'name' 	=> 'panjang_kapal',
																						 		'label'	=> 'Panjang Kapal (m)'
																							),
														'lebar_kapal' 			=> Array( 	'name' 	=> 'lebar_kapal',
																						 		'label'	=> 'Lebar Kapal (m)'
																							),
														'dalam_kapal' 			=> Array( 	'name' 	=> 'dalam_kapal',
																						 		'label'	=> 'Dalam Kapal (m)'
																							),
														'panjang_loa_kapal' 			=> Array( 	'name' 	=> 'panjang_loa_kapal',
																						 		'label'	=> 'Panjang Keseluruhan (LoA) (m)'
																							),
														'gt_kapal' 					=> Array( 	'name' 	=> 'gt_kapal',
																						 		'label'	=> 'GT'
																							),
														'nt_kapal' 					=> Array( 	'name' 	=> 'nt_kapal',
																						 		'label'	=> 'NT'
																							),
														'no_siup' 					=> Array( 	'name' 	=> 'no_siup',
																						 		'label'	=> 'No. SIUP'
																							),
														'jenis_perusahaan' 			=> Array( 	'name' 	=> 'jenis_perusahaan',
																						 		'label'	=> 'Jenis Perusahaan'
																							),
														'email_perusahaaan' 		=> Array( 	'name' 	=> 'email_perusahaaan',
																						 		'label'	=> 'Email Perusahaan'
																							),
														'nama_pemilik' 				=> Array( 	'name' 	=> 'nama_pemilik',
																						 		'label'	=> 'Nama Pemilik'
																							),
														'alamat_perusahaan' 		=> Array( 	'name' 	=> 'alamat_perusahaan',
																						 		'label'	=> 'Alamat Perusahaan'
																							),
														'nama_penanggung_jawab' 	=> Array( 	'name' 	=> 'nama_penanggung_jawab',
																						 		'label'	=> 'Nama Penanggung Jawab'
																							),
														'email_penanggung_jawab' 	=> Array( 	'name' 	=> 'email_penanggung_jawab',
																						 		'label'	=> 'Email Penanggung Jawab'
																							),
														)
									),
					'label_check_perubahan' => Array(
										'form' => Array(
														'check_nama_kapal'	=> Array(
																					'name'	=>	'check_nama_kapal',
																					'label'	=>	'Nama Kapal'
																				)
													)
									),
					'label_print_pendok' => Array(
										'title' => Array( 
														'page' 	=> 'Cetak Tanda Terima',
														'panel' => 'Cetak Tanda Terima'
														),
										'form' => Array(
														'check_nama_kapal'	=> Array(
																					'name'	=>	'check_nama_kapal',
																					'label'	=>	'Nama Kapal'
																				)
													)
											),
					'label_print_draft' => Array(
										'title' => Array( 
														'page' 	=> 'Cetak Draft Buku Kapal',
														'panel' => 'Cetak Draft Buku Kapal'
														),
										'form' => Array(
														'check_nama_kapal'	=> Array(
																					'name'	=>	'check_nama_kapal',
																					'label'	=>	'Nama Kapal'
																				)
													)
											),
					'form_edit_perusahaan_label' => Array(
										'title' => Array( 
														'page' 	=> 'Mapping Edit Perusahaan',
														'panel' => 'Mapping Edit Perusahaan'
														),
										'form' => Array(
														'nama_kapal' => Array( 	'name'	=> 	'nama_kapal',
																				'label' => 	'Nama Kapal'
																			)
													)
											),
					'label_print_bkp' => Array(
										'title' => Array( 
														'page' 	=> 'Cetak Buku Kapal',
														'panel' => 'Cetak Buku Kapal'
														),
										'form' => Array(
														'check_nama_kapal'	=> Array(
																					'name'	=>	'check_nama_kapal',
																					'label'	=>	'Nama Kapal'
																				)
													)
											),
					'label_view_pengguna_kapi' => Array(
											'title' => Array(
															'page' => 'Pengaturan User KAPI',
															'panel' => 'Pengaturan User KAPI'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => Array('data' => 'Nama Pengguna', 'width' => '150'),
															'2' => Array('data' => 'Kategori Pengguna', 'width' => '100'),
															'3' => Array('data' => 'Provinsi', 'width' => '200'),
															'4' => Array('data' => 'Kabupaten', 'width' => '150'),
															'5' => Array('data' => 'Kode Provinsi', 'width' => '150'),
															'6' => Array('data' => 'Kode Kab/Kota', 'width' => '150'),
															'7' => Array('data' => 'Aksi', 'width' => '150')
														)
											),
					'label_view_report_kapal' => Array(
											'title' => Array(
															'page' => 'Daftar Kapal Terdaftar',
															'panel' => 'Daftar Kapal Terdaftar'
														),
											'form' => ''
											),
					'label_view_report_kapal_perbulan' => Array(
											'title' => Array(
															'page' => 'Daftar Kapal Terdaftar Bulanan',
															'panel' => 'Daftar Kapal Terdaftar Bulanan'
														),
											'form' => ''
											),
					'label_view_pencarian_kapal' => Array(
											'title' => Array(
															'page' => 'Pencarian Kapal KAPI',
															'panel' => 'Pencarian Kapal KAPI'
														),
											'form' => ''
											),
					'label_view_hasil_pencarian' => Array(
											'title' => Array(
															'page' => 'Hasil Pencarian Kapal',
															'panel' => 'Hasil Pencarian Kapal'
														),
											'form' => ''
											),
					'label_view_report_kapal_tercetak' => Array(
											'title' => Array(
															'page' => 'Daftar Kapal Tercetak',
															'panel' => 'Daftar Kapal Tercetak'
														),
											'form' => ''
											),
					'label_view_pendok_online' => Array(
											'title' => Array(
															'page' => 'Daftar Permohonan Dokumen Online',
															'panel' => 'Daftar Permohonan Dokumen Online'
														),
											'form' => '',
											'th_table' => Array(
															'0' => Array('data' => 'No', 'width' => '20'),
															'1' => 'Status',
															'2' => 'Nama Kapal',
															'3' => Array('data' => 'Perusahaan <br>/<br> Pemilik', 'width' => '200'),
															'4' => 'Tipe Permohonan',
															'5' => 'Pengguna'
														)
											),
					'label_view_report_pemilik_pemohon_kapal' => Array(
											'title' => Array(
															'page' => 'Daftar Pemilik dan Pemohon Kapal',
															'panel' => 'Daftar Pemilik dan Pemohon Kapal'
														),
											'form' => ''
											),
					'label_view_history' => Array(
											'title' => Array(
															'page' => 'Data Histori Kapal',
															'panel' => 'Data Histori Kapal'
														),
											'form' => ''
											),
					'form_pengguna_kapi' => Array(
												'title'	=> Array(
															'page'	=> 'Pengaturan Pejabat',
															'panel'	=> 'Pengaturan Pejabat'
															),
												'form'	=> Array(
															'nama_pengguna'	=> Array(
																				'name'	=> 'nama_pengguna',
																				'label'	=> 'Nama Pengguna'
																				),
															'kategori_pengguna'	=> Array(
																				'name'	=> 'kategori_pengguna',
																				'label'	=> 'Kategori Pengguna'
																				),
															'nama_propinsi' => Array(
																				'name'	=> 'nama_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'nama_kabupaten_kota'=> Array(
																				'name'	=> 'nama_kabupaten_kota',
																				'label'	=> 'Kode Kab/Kota'
																				),
															'kode_propinsi' => Array(
																				'name'	=> 'kode_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'kode_kab_kota'	=> Array(
																				'name'	=> 'kode_kab_kota',
																				'label'	=> 'Kode Kab/Kota'
																				)
															)
												),
					'form_login' => Array(
												'title'	=> Array(
															'page'	=> 'Login',
															'panel'	=> 'Login'
															),
												'form'	=> Array(
															'nama_pengguna'	=> Array(
																				'name'	=> 'nama_pengguna',
																				'label'	=> 'Nama Pengguna'
																				),
															'kategori_pengguna'	=> Array(
																				'name'	=> 'kategori_pengguna',
																				'label'	=> 'Kategori Pengguna'
																				),
															'nama_propinsi' => Array(
																				'name'	=> 'nama_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'nama_kabupaten_kota'=> Array(
																				'name'	=> 'nama_kabupaten_kota',
																				'label'	=> 'Kode Kab/Kota'
																				),
															'kode_propinsi' => Array(
																				'name'	=> 'kode_propinsi',
																				'label'	=> 'Kode Propinsi'
																				),
															'kode_kab_kota'	=> Array(
																				'name'	=> 'kode_kab_kota',
																				'label'	=> 'Kode Kab/Kota'
																				)
															)
												)
					
			);

/* PETUNJUK PEMAKAIAN :

---Masih di controller---
Load dulu file config labels.php:
$this->load->config('labels');

Simpan ke data constant:
$data['constant'] = $this->config->item('form_pendok');
---Selesai di controller---

---Penggunaan Di file views---
// Menampilkan text untuk page title (biasanya di gunakan di templates header untuk judul page)
echo $constant['title']['page']; 

// Menampilkan text untuk judul panel
echo $constant['title']['panel'];

// Saat set attribut untuk form input, misal field Nomor Surat Permohonan
<label for="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"> 
<?php echo $constant['form']['no_surat_permohonan']['label'];?>
</label>
<input 
type="text" 
id="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
name="<?php echo $constant['form']['no_surat_permohonan']['name'];?>"
value=""/>

---Selesai di file views---
*/



/* TEMPLATE:
	// PERHATIKAN TANDA KOMA SEBELUM DAN SESUDAH ARRAY
					'form_kapal' => Array(
									'title' => Array( 
													'page' => 'Halaman Entri Kapal',
													'panel' => 'Entri Kapal'
													),
									'form' => Array(
													'nama_kapal' => Array( 'name' => 'nama_kapal',
																					'label'	=> 'Nama Kapal'
																				),
													'' => Array( 'name' => '',
																 'label' => ''
 																)
													)
									)
*/
?>