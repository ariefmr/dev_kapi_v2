<?php
$config = Array( 'database_timeline' => 
Array(

	0 => Array(	'tanggal_update' => '9 Des 2013',
			   	'link_dropbox' => 'https://www.dropbox.com/s/ji2r8jblyy0tl0m/trs_pendok_091213.sql',
				'nama_file_dropbox' => 'trs_pendok_091213.sql',
				'notes' => 'Rubah tabel trs_pendok, menambahkan field - field berbau dokumen fisik (yang Ceklis pake set("Tidak","Ada")) ',
				'pelaku' => 'ricki'
				),
	1 => Array(	'tanggal_update' => '9 Des 2013',
			   	'link_dropbox' => 'https://www.dropbox.com/s/ji2r8jblyy0tl0m/trs_pendok_091213.sql',
				'nama_file_dropbox' => 'trs_pendok_091213.sql',
				'notes' => 'Rubah tabel trs_pendok, menambahkan field - field berbau dokumen fisik (yang Ceklis pake set("Tidak","Ada")) ',
				'pelaku' => 'ricki'
				),
	2 => Array(	'tanggal_update' => '11 Des 2013',
			   	'link_dropbox' => 'https://www.dropbox.com/s/lxf8gn7vyvu30zs/db_pendaftaran_kapal_111213.sql',
				'nama_file_dropbox' => 'db_pendaftaran_kapal_111213.sql',
				'notes' => 'Menambahkan tabel mst_kapal, mst_buku_kapal, dan mst_pemohon sesuai labels yg dari winda ',
				'pelaku' => 'ricki'
				),
	3 => Array(	'tanggal_update' => '11 Des 2013',
			   	'link_dropbox' => 'https://www.dropbox.com/s/u8er97qrbwjtew7/trs_pendok_111213_fren.sql.zip',
				'nama_file_dropbox' => 'trs_pendok_111213_fren.sql.zip',
				'notes' => 'Tambah field status_dokumen, tambah field traceabilty [PENTING WAJIB ADA DI SEMUA TABLE]: id_pengguna_buat, tanggal_buat, id_pengguna_ubah, tanggal_ubah
				, aktif.',
				'pelaku' => 'fren'
				),
	4 => Array(	'tanggal_update' => '15 Des 2013',
			   	'link_dropbox' => 'https://www.dropbox.com/s/pvrznn3a3ihxjc1/trs_pendok_151213_fren.sql.zip',
					'nama_file_dropbox' => 'trs_pendok_151213_fren.sql.zip',
					'notes' => 'Tambah status dokumen, flag status dokumen final apa masih di proses',
					'pelaku' => 'fren'
				),		
	)
);?>