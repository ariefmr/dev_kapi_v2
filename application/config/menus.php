<?php
	$config = Array(
					'lists' => Array(
		        					'Pendok' => Array( 'Daftar Penerimaan Dokumen' => 'pendok/main/views',
		        										'Pendaftaran Penerimaan Dokumen' => 'pendok/main/entry'/*,
		        										'Search Kapal' => '#'*/
		        									 ),
		        					'Kapal' => Array(	'Entri Kapal Pendok' => 'kapal/main/views',
		        										'Daftar Buku Kapal' => 'buku_kapal/main/views'/*'kapal/main/views'*/),
		        					'Cetak Buku' => Array(	'Daftar Cetak Buku' => 'buku_kapal/main/views_cetak'),
		        					//'Buku Kapal' => Array( 'Tabel Buku Kapal' => 'buku_kapal/main/views'),
		        					// 'Pengurus' => Array( 'Tabel Pengurus' => 'refkapi/mst_pemohon'),
		        					'Laporan' => Array( 'Daftar Kapal Terdaftar' => 'report/report_kapal/views',
		        										'Daftar Kapal Perbulan' => 'report/report_kapal/views_perbulan',
		        										'Daftar Pemilik dan Pemohon Kapal' => 'report/report_pemilik_pemohon_kapal/views',
		        										'Jumlah Kapal Terbit atau Tercetak' => 'report/report_kapal_tercetak/views',
		        										'Histori Kapal Berdasarkan No Register' => 'refkapi/mst_history/views',
		        										'Pencarian' => 'pencarian/pencarian_kapal/views',
		        										'Laporan Pemilik GT>300' => 'report/report_pemilik_kapal/views'
		        										// 'Jumlah Kapal Penangkap' => '#',
		        										// 'Jumlah Kapal Pengangkut' => '#',
		        										// 'Jumlah Kapal Operasional' => '#',
		        										// 'Jumlah Kapal Budidaya' => '#',
		        										// 'Jumlah Kapal Perikanan Berdasarkan Ukuran dan WPP' => '#',
		        										// 'Jumlah Kapal Perikanan Berdasarkan Jenis, Alat dan WPP' => '#'
		        										),
		        					'Master DSS' => Array( 'Daftar Kapal >= 30 GT' => 'refdss/mst_kapal',
		        										'Daftar Kapal < 30 GT' => 'refdss/mst_kapal/kapal/daerah',
		        										'Daftar Perusahaan di Pusat' => 'refdss/mst_perusahaan',
		        										'Daftar Perusahaan di Daerah' => 'refdss/mst_perusahaan/perusahaan/daerah',
		        										'Mapping' => 'mapping/main/views_compare',
		        										'Progress Mapping' => 'mapping/main/views'
		        										),
		        					'Pengaturan' => Array( 	
		        											'Pejabat' => 'pengaturan/main',
		        											'Pengguna KAPI' => 'pengguna_kapi/main',
		        											'Pengurus' => 'refkapi/mst_pemohon',
		        											'Kelengkapan Dokumen' => 'refkapi/mst_dokumen/pengaturan'
		        											/*'Search Kapal' => '#'*/
		        									 ),
		        					'Pendok Online' => Array( 	
		        											'Daftar Pendok Online' => 'pendok_online/main/views'
		        									 )
		        					)
					);
?>