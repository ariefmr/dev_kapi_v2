<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('kos'))
{
    function kos(&$var, $default = '')
    {
        if(!isset($var) || !empty($var))
        {
          $return_var = $var;
        }else
        {
          $return_var = $default;
        }
        return $return_var;
    }   
}

if ( ! function_exists('kos_value'))
{
    function kos_value(&$var, $default = '-')
    {
        if(!isset($var) || !empty($var))
        {
          $return_var = $var;
        }else
        {
          $return_var = $default;
        }

        //untuk value bilangan
        if ($var == 0) {
          $return_var = $default;
        }

        return $return_var;
    }   
}

if ( ! function_exists('noreg'))
{
    function noreg(&$var, $default = '')
    {
        if(!isset($var) || !empty($var))
        {
          $pjg = strlen($var);
          while(($pjg>0)&&($pjg<6))
          {
            $var = "0".$var;
            $pjg = strlen($var);

          }
          $return_var = $var;
        }else
        {
          $return_var = $default;
        }
        return $return_var;
    }   
}

if ( ! function_exists('tgl'))
{
    function tgl(&$tanggal, $default_format = 'd/m/Y')
    {
        if(empty($tanggal)){
          return "-";
        }
        $totime = strtotime($tanggal);
        $tanggal_formatted = date($default_format, $totime); 
        return $tanggal_formatted;
    }   
}

if (!function_exists('vdump'))
{
  function vdump($variable, $isdie = FALSE)
  {
    echo '<pre>';
    var_dump($variable);
    echo '</pre>'; 
    if($isdie){
      die;
    }
  }
}

if (!function_exists('fmt_tgl'))
{
  function fmt_tgl($str_tanggal)
  {
    if($str_tanggal === "0000-00-00"|| empty($str_tanggal))
    {
      $return_var = "-";
    }else{
      $return_var = date("d/m/Y", strtotime($str_tanggal));
    }

    return $return_var;
  }
}

if (!function_exists('fmt_bulan_tahun'))
{
  function fmt_bulan_tahun($str_tanggal)
  {
    // var_dump('1-'.$str_tanggal);
    return date("M, Y", strtotime($str_tanggal.'-1'));
  }
}

/*
kode di ambil dari http://tutorialweb.net/merubah-format-tanggal-date-ke-format-tanggal-indonesia/
*/
if (!function_exists('TanggalIndo'))
{
  function TanggalIndo($date){
    $BulanIndo = array("JANUARI", "FEBRUARI", "MARET", "APRIL", "MEI", "JUNI", "JULI", "AGUSTUS", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER");

    $tahun = substr($date, 0, 4);
    $bulan = substr($date, 5, 2);
    $tgl   = substr($date, 8, 2);

    $result = "        " . $BulanIndo[(int)$bulan-1] . " ". $tahun;   
    return($result);
  }
}


?>