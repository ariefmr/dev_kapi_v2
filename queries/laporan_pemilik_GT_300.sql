-- query untuk ambil daftar pemilik yang memiliki jumlah GT >= 300
-- permintaan Bu Direkktur
-- 22 Mei 2015

SELECT 
    mp.id_perusahaan,
    mp.no_siup,
    mp.nama_perusahaan as nama_pemilik,
    mjp.nama_jenis_perusahaan,
    count(mk.id_kapal) as jumlah_kapal,
    sum(tb.gt_kapal) as jumlah_gt
FROM
    db_pendaftaran_kapal.mst_kapal mk
        left join
    db_pendaftaran_kapal.trs_bkp tb ON tb.id_kapal = mk.id_kapal
        left join
    db_master.mst_perusahaan mp ON mp.id_perusahaan = tb.id_perusahaan
        left join
    db_master.mst_jenis_perusahaan mjp ON mjp.id_jenis_perusahaan = mp.id_jenis_perusahaan
where
    mp.id_jenis_perusahaan = 2
        and mk.aktif = 'ya'
group by id_perusahaan
having sum(tb.gt_kapal) >= 300
UNION SELECT 
    mp.id_perusahaan,
    mp.no_siup,
    mp.nama_perusahaan as nama_pemilik,
    mjp.nama_jenis_perusahaan,
    count(mk.id_kapal) as jumlah_kapal,
    sum(tb.gt_kapal) as jumlah_gt
FROM
    db_pendaftaran_kapal.mst_kapal mk
        left join
    db_pendaftaran_kapal.trs_bkp tb ON tb.id_kapal = mk.id_kapal
        left join
    db_master.mst_perusahaan mp ON mp.id_perusahaan = tb.id_perusahaan
        left join
    db_master.mst_jenis_perusahaan mjp ON mjp.id_jenis_perusahaan = mp.id_jenis_perusahaan
where
    mp.id_jenis_perusahaan is null
        and mp.nama_perusahaan like CONCAT('%', mp.nama_penanggung_jawab, '%')
        and mk.aktif = 'ya'
group by tb.id_perusahaan
having sum(tb.gt_kapal) >= 300
order by id_perusahaan
;