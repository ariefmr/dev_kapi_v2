SELECT 
	tp.no_rekam_pendok,
	DATE_FORMAT(tp.tanggal_buat, '%d-%m-%Y') tanggal_masuk,
	mp.nama_pemohon,
	mp.no_telp_pemohon,
	tp.nama_perusahaan,
	tp.nama_penanggung_jawab,
	tp.alamat_perusahaan,
	tp.nama_kapal,
	tp.status_pendok
FROM trs_pendok tp
	left join mst_pemohon mp on mp.id_pemohon = tp.id_pemohon
WHERE
	tp.id_pemohon is not null 
	and tp.id_pemohon not in (0)
	and tp.kategori_pendaftaran = 'PUSAT'
        and tp.tanggal_buat is not null
        and YEAR(tp.tanggal_buat) in (2014, 2015)
union

SELECT 
	tp.no_rekam_pendok,
	"null" as tanggal_masuk,
	mp.nama_pemohon,
	mp.no_telp_pemohon,
	tp.nama_perusahaan,
	tp.nama_penanggung_jawab,
	tp.alamat_perusahaan,
	tp.nama_kapal,
	tp.status_pendok
FROM trs_pendok tp
	left join mst_pemohon mp on mp.id_pemohon = tp.id_pemohon
WHERE
	tp.id_pemohon is not null 
	and tp.id_pemohon not in (0)
	and tp.kategori_pendaftaran = 'PUSAT'
        and tp.tanggal_buat is null
ORDER BY `tp`.`no_rekam_pendok` ASC


======================================================================================


SELECT 
	tp.nama_perusahaan as "NAMA PEMILIK",
	tp.nama_penanggung_jawab as "NAMA PENANGGUNG JAWAB",
	tp.alamat_perusahaan AS "ALAMAT PEMILIK",
	tb.no_telp_perusahaan AS "NO. TELP PEMILIK",
	mp.nama_pemohon AS "NAMA PEMOHON",
	mp.no_telp_pemohon AS "NO. TELP PEMOHON"
FROM trs_pendok tp
	left join mst_pemohon mp on mp.id_pemohon = tp.id_pemohon
	left join trs_bkp tb on tb.id_pendok = tp.id_pendok
WHERE
	tp.id_pemohon is not null 
	and tp.id_pemohon not in (0)
	and tp.kategori_pendaftaran = 'PUSAT'
    and tp.tanggal_buat is not null
    and YEAR(tp.tanggal_buat) in (2014, 2015)
    and tp.nama_perusahaan is not null
    and tp.nama_perusahaan <> ''
GROUP BY tp.nama_perusahaan, mp.nama_pemohon